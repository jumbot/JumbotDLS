﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.IO;
using System.Web;
namespace JumbotDLS.Common.Handler
{
    public class FileHandler : IHttpHandler
    {
        public FileHandler()
        {
        }

        public void ProcessRequest(HttpContext context)
        {
            string fileName = context.Request.QueryString["file"];
            string[] filetype = { ".rar", ".zip", ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".pdf" };    //文件允许格式
            string fileext = Path.GetExtension(fileName);
            if (File.Exists(fileName))
            {
                context.Response.AppendHeader("Content-Type", "application/octet-stream");
                context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + context.Server.UrlEncode(Path.GetFileName(fileName)));
                context.Response.WriteFile(fileName, false);
            }
            else
            {
                context.Response.Status = "404 File Not Found";
                context.Response.StatusCode = 404;
                context.Response.StatusDescription = "File Not Found";
                context.Response.Write("File Not Found");
            }
        }

        public bool IsReusable
        {
            get { return true; }
        }
    }
}
