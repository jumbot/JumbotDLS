﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JumbotDLS.Utils;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Net.Http;
namespace JumbotDLS.Common.SMS
{
    public class SendCloudSMS
    {
        public static String Smsapi_Url = "http://www.sendcloud.net/smsapi/send";
        public static String Smsapi_User = "kisds_www";
        public static String Smsapi_Key = "S8Y7JrLc28Cco0RH3f7TlSVSiI0NMwPZ";


        public static String generate_md5(String str)
        {
            System.Security.Cryptography.MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text
            md5.ComputeHash(Encoding.GetEncoding("utf-8").GetBytes(str));

            //get hash result after compute it
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        /// <summary>
        /// 新增图书
        /// </summary>
        /// <param name="name">通知人</param>
        /// <param name="name">操作人</param>
        /// <param name="title">书名</param>
        /// <param name="tel">手机号</param>
        /// <returns></returns>
        public static bool AddBook(string name, string name2, string title, string tel)
        {
            List<KeyValuePair<String, String>> paramList = new List<KeyValuePair<String, String>>();
            paramList.Add(new KeyValuePair<String, String>("smsUser", Smsapi_User));
            paramList.Add(new KeyValuePair<String, String>("templateId", "27527"));
            paramList.Add(new KeyValuePair<String, String>("phone", tel));
            paramList.Add(new KeyValuePair<String, String>("msgType", "0"));
            paramList.Add(new KeyValuePair<String, String>("vars", "{\"%name%\":\"" + name + "\",\"%name2%\":\"" + name2 + "\",\"%title%\":\"" + title + "\"}"));

            paramList.Sort(
                delegate(KeyValuePair<String, String> p1, KeyValuePair<String, String> p2)
                {
                    return p1.Key.CompareTo(p2.Key);
                }
            );

            var param_str = "";
            foreach (var param in paramList)
            {
                param_str += param.Key.ToString() + "=" + param.Value.ToString() + "&";
            }

            String sign_str = Smsapi_Key + "&" + param_str + Smsapi_Key;
            String sign = generate_md5(sign_str);

            paramList.Add(new KeyValuePair<String, String>("signature", sign));

            HttpClient client = null;
            HttpResponseMessage response = null;

            try
            {
                client = new HttpClient();
                response = client.PostAsync(Smsapi_Url, new FormUrlEncodedContent(paramList)).Result;
                String result = response.Content.ReadAsStringAsync().Result;
                JumbotDLS.Utils.Logs.Info("sendsms", result);
                return true;

            }
            catch (Exception e)
            {
                JumbotDLS.Utils.Logs.Error("sendsms", e + "");
                return false;
            }
            finally
            {
                if (null != response)
                {
                    response.Dispose();
                }
                if (null != client)
                {
                    client.Dispose();
                }
            }
        }

        /// <summary>
        /// 修改图书
        /// </summary>
        /// <param name="name">通知人</param>
        /// <param name="name">操作人</param>
        /// <param name="title">书名</param>
        /// <param name="tel">手机号</param>
        /// <returns></returns>
        public static bool ModifyBook(string name, string title, string tel)
        {
            List<KeyValuePair<String, String>> paramList = new List<KeyValuePair<String, String>>();
            paramList.Add(new KeyValuePair<String, String>("smsUser", Smsapi_User));
            paramList.Add(new KeyValuePair<String, String>("templateId", "28185"));
            paramList.Add(new KeyValuePair<String, String>("phone", tel));
            paramList.Add(new KeyValuePair<String, String>("msgType", "0"));
            paramList.Add(new KeyValuePair<String, String>("vars", "{\"%name%\":\"" + name + "\",\"%title%\":\"" + title + "\"}"));

            paramList.Sort(
                delegate(KeyValuePair<String, String> p1, KeyValuePair<String, String> p2)
                {
                    return p1.Key.CompareTo(p2.Key);
                }
            );

            var param_str = "";
            foreach (var param in paramList)
            {
                param_str += param.Key.ToString() + "=" + param.Value.ToString() + "&";
            }

            String sign_str = Smsapi_Key + "&" + param_str + Smsapi_Key;
            String sign = generate_md5(sign_str);

            paramList.Add(new KeyValuePair<String, String>("signature", sign));

            HttpClient client = null;
            HttpResponseMessage response = null;

            try
            {
                client = new HttpClient();
                response = client.PostAsync(Smsapi_Url, new FormUrlEncodedContent(paramList)).Result;
                String result = response.Content.ReadAsStringAsync().Result;
                JumbotDLS.Utils.Logs.Info("SendCloudSMS.sendsms", result);
                return true;

            }
            catch (Exception e)
            {
                JumbotDLS.Utils.Logs.Error("SendCloudSMS.sendsms", e + "");
                return false;
            }
            finally
            {
                if (null != response)
                {
                    response.Dispose();
                }
                if (null != client)
                {
                    client.Dispose();
                }
            }
        }

        /// <summary>
        /// 新的办公任务
        /// </summary>
        /// <param name="name"></param>
        /// <param name="tel"></param>
        /// <returns></returns>
        public static bool NewTask(string name, string tel)
        {
            List<KeyValuePair<String, String>> paramList = new List<KeyValuePair<String, String>>();
            paramList.Add(new KeyValuePair<String, String>("smsUser", Smsapi_User));
            paramList.Add(new KeyValuePair<String, String>("templateId", "28320"));
            paramList.Add(new KeyValuePair<String, String>("phone", tel));
            paramList.Add(new KeyValuePair<String, String>("msgType", "0"));
            paramList.Add(new KeyValuePair<String, String>("vars", "{\"%name%\":\"" + name + "\"}"));

            paramList.Sort(
                delegate(KeyValuePair<String, String> p1, KeyValuePair<String, String> p2)
                {
                    return p1.Key.CompareTo(p2.Key);
                }
            );

            var param_str = "";
            foreach (var param in paramList)
            {
                param_str += param.Key.ToString() + "=" + param.Value.ToString() + "&";
            }

            String sign_str = Smsapi_Key + "&" + param_str + Smsapi_Key;
            String sign = generate_md5(sign_str);

            paramList.Add(new KeyValuePair<String, String>("signature", sign));

            HttpClient client = null;
            HttpResponseMessage response = null;

            try
            {
                client = new HttpClient();
                response = client.PostAsync(Smsapi_Url, new FormUrlEncodedContent(paramList)).Result;
                String result = response.Content.ReadAsStringAsync().Result;
                JumbotDLS.Utils.Logs.Info("SendCloudSMS.sendsms", result);
                return true;

            }
            catch (Exception e)
            {
                JumbotDLS.Utils.Logs.Error("SendCloudSMS.sendsms", e + "");
                return false;
            }
            finally
            {
                if (null != response)
                {
                    response.Dispose();
                }
                if (null != client)
                {
                    client.Dispose();
                }
            }
        }

        /// <summary>
        /// 新的员工请假
        /// </summary>
        /// <param name="name"></param>
        /// <param name="phoneNumbers"></param>
        /// <returns></returns>
        public static bool NewLeaveAsk(string name, string tel)
        {
            List<KeyValuePair<String, String>> paramList = new List<KeyValuePair<String, String>>();
            paramList.Add(new KeyValuePair<String, String>("smsUser", Smsapi_User));
            paramList.Add(new KeyValuePair<String, String>("templateId", "28323"));
            paramList.Add(new KeyValuePair<String, String>("phone", tel));
            paramList.Add(new KeyValuePair<String, String>("msgType", "0"));
            paramList.Add(new KeyValuePair<String, String>("vars", "{\"%name%\":\"" + name + "\"}"));

            paramList.Sort(
                delegate(KeyValuePair<String, String> p1, KeyValuePair<String, String> p2)
                {
                    return p1.Key.CompareTo(p2.Key);
                }
            );

            var param_str = "";
            foreach (var param in paramList)
            {
                param_str += param.Key.ToString() + "=" + param.Value.ToString() + "&";
            }

            String sign_str = Smsapi_Key + "&" + param_str + Smsapi_Key;
            String sign = generate_md5(sign_str);

            paramList.Add(new KeyValuePair<String, String>("signature", sign));

            HttpClient client = null;
            HttpResponseMessage response = null;

            try
            {
                client = new HttpClient();
                response = client.PostAsync(Smsapi_Url, new FormUrlEncodedContent(paramList)).Result;
                String result = response.Content.ReadAsStringAsync().Result;
                JumbotDLS.Utils.Logs.Info("SendCloudSMS.sendsms", result);
                return true;

            }
            catch (Exception e)
            {
                JumbotDLS.Utils.Logs.Error("SendCloudSMS.sendsms", e + "");
                return false;
            }
            finally
            {
                if (null != response)
                {
                    response.Dispose();
                }
                if (null != client)
                {
                    client.Dispose();
                }
            }
        }

        /// <summary>
        /// 审批员工请假
        /// </summary>
        /// <param name="name"></param>
        /// <param name="phoneNumbers"></param>
        /// <returns></returns>
        public static bool AuditLeaveAsk(string name, string auditresult,string tel)
        {
            List<KeyValuePair<String, String>> paramList = new List<KeyValuePair<String, String>>();
            paramList.Add(new KeyValuePair<String, String>("smsUser", Smsapi_User));
            paramList.Add(new KeyValuePair<String, String>("templateId", "28326"));
            paramList.Add(new KeyValuePair<String, String>("phone", tel));
            paramList.Add(new KeyValuePair<String, String>("msgType", "0"));
            paramList.Add(new KeyValuePair<String, String>("vars", "{\"%name%\":\"" + name + "\",\"%auditresult%\":\"" + auditresult + "\"}"));

            paramList.Sort(
                delegate(KeyValuePair<String, String> p1, KeyValuePair<String, String> p2)
                {
                    return p1.Key.CompareTo(p2.Key);
                }
            );

            var param_str = "";
            foreach (var param in paramList)
            {
                param_str += param.Key.ToString() + "=" + param.Value.ToString() + "&";
            }

            String sign_str = Smsapi_Key + "&" + param_str + Smsapi_Key;
            String sign = generate_md5(sign_str);

            paramList.Add(new KeyValuePair<String, String>("signature", sign));

            HttpClient client = null;
            HttpResponseMessage response = null;

            try
            {
                client = new HttpClient();
                response = client.PostAsync(Smsapi_Url, new FormUrlEncodedContent(paramList)).Result;
                String result = response.Content.ReadAsStringAsync().Result;
                JumbotDLS.Utils.Logs.Info("SendCloudSMS.sendsms", result);
                return true;

            }
            catch (Exception e)
            {
                JumbotDLS.Utils.Logs.Error("SendCloudSMS.sendsms", e + "");
                return false;
            }
            finally
            {
                if (null != response)
                {
                    response.Dispose();
                }
                if (null != client)
                {
                    client.Dispose();
                }
            }
        }
    }
}
