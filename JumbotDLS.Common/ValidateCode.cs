﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Web;
namespace JumbotDLS.Common
{
    /// <summary>
    /// 验证码操作
    /// </summary>
    public static class ValidateCode
    {
        /// <summary>
        /// 判断验证码,如果判断正确则生成新的验证码
        /// </summary>
        /// <param name="_code">不能是空值，否则为false</param>
        /// <returns></returns>
        public static bool CheckValidateCode(string _code, ref string _realcode)
        {
            _realcode = GetValidateCode(4, false);//获取当前的验证码
            if (_code == null || _code.Length == 0)
                return false;
            if (_realcode.ToLower() == _code.ToLower())
            {
                //CreateValidateCode(4, true);
                return true;
            }
            return false;
        }
        /// <summary>
        ///  获得验证码
        /// </summary>
        /// <param name="_code">需要判断的值</param>
        /// <param name="_init">是否初始化新的值</param>
        /// <returns></returns>
        public static string GetValidateCode(int _length, bool _init)
        {
            if (_init)
                CreateValidateCode(_length, true);
            return JumbotDLS.Utils.Session.Get("ValidateCode");
        }
        /// <summary>
        /// 创建验证码
        /// </summary>
        /// <param name="_length"></param>
        /// <param name="_cover">是否覆盖老的值</param>
        public static void CreateValidateCode(int _length, bool _cover)
        {
            if (_cover)
                SaveCookie(_length);
            else
            {
                if (JumbotDLS.Utils.Session.Get("ValidateCode") == null)
                    SaveCookie(_length);
            }
        }
        public static void SaveCookie(int _length)
        {
            char[] chars = "3459ABCDEFGHJKLMNPRSTUVWXYZ".ToCharArray();
            Random random = new Random();
            string validateCode = string.Empty;
            for (int i = 0; i < _length; i++)
                validateCode += chars[random.Next(0, chars.Length)].ToString();
            JumbotDLS.Utils.Session.Add("ValidateCode", validateCode);
        }
    }
}
