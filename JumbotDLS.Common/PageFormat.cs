﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Web;
namespace JumbotDLS.Common
{
    /// <summary>
    /// 页面地址格式
    /// </summary>
    public static class PageFormat
    {
        /// <summary>
        /// 站点首页
        /// </summary>
        public static string Site(string _siteDir, bool _isHtml)
        {
            string strXmlFile = HttpContext.Current.Server.MapPath(_siteDir + "_data/config/pageformat.config");
            JumbotDLS.DBUtility.XmlControl XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
            string TempUrl = "";
            if (_isHtml)
                TempUrl = XmlTool.GetText("Pages/Site/P_2");
            else
                TempUrl = XmlTool.GetText("Pages/Site/P_1");
            XmlTool.Dispose();
            return TempUrl;
        }
        /// <summary>
        /// 频道首页
        /// </summary>
        public static string Channel(bool _isHtml, string _siteDir, bool urlRewrite, int page)
        {
            string strXmlFile = HttpContext.Current.Server.MapPath(_siteDir + "_data/config/pageformat.config");
            JumbotDLS.DBUtility.XmlControl XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
            string TempUrl = "";
            if (_isHtml)
            {
                if (page == 1)
                    TempUrl = XmlTool.GetText("Pages/Channel/P_2_1");
                else
                    TempUrl = XmlTool.GetText("Pages/Channel/P_2_N");
            }
            else
            {
                if (urlRewrite)
                    if (page == 1)
                        TempUrl = XmlTool.GetText("Pages/Channel/P_1_1");
                    else
                        TempUrl = XmlTool.GetText("Pages/Channel/P_1_N");
                else
                    if (page == 1)
                        TempUrl = XmlTool.GetText("Pages/Channel/P_0_1");
                    else
                        TempUrl = XmlTool.GetText("Pages/Channel/P_0_N");
            }
            XmlTool.Dispose();
            return TempUrl;
        }
        /// <summary>
        /// 栏目页
        /// </summary>
        public static string Class(bool _isHtml, string _siteDir, bool urlRewrite, int page)
        {
            string strXmlFile = HttpContext.Current.Server.MapPath(_siteDir + "_data/config/pageformat.config");
            JumbotDLS.DBUtility.XmlControl XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
            string TempUrl = "";
            if (_isHtml)
            {
                if (page == 1)
                    TempUrl = XmlTool.GetText("Pages/Class/P_2_1");
                else
                    TempUrl = XmlTool.GetText("Pages/Class/P_2_N");
            }
            else
            {
                if (urlRewrite)
                    if (page == 1)
                        TempUrl = XmlTool.GetText("Pages/Class/P_1_1");
                    else
                        TempUrl = XmlTool.GetText("Pages/Class/P_1_N");
                else
                    if (page == 1)
                        TempUrl = XmlTool.GetText("Pages/Class/P_0_1");
                    else
                        TempUrl = XmlTool.GetText("Pages/Class/P_0_N");
            }
            XmlTool.Dispose();
            return TempUrl;
        }
        /// <summary>
        /// RSS页
        /// </summary>
        public static string Rss(bool _isHtml, string _siteDir, bool urlRewrite, int page)
        {
            string strXmlFile = HttpContext.Current.Server.MapPath(_siteDir + "_data/config/pageformat.config");
            JumbotDLS.DBUtility.XmlControl XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
            string TempUrl = "";
            if (urlRewrite)
                if (page == 1)
                    TempUrl = XmlTool.GetText("Pages/Rss/P_1_1");
                else
                    TempUrl = XmlTool.GetText("Pages/Rss/P_1_N");
            else
                if (page == 1)
                    TempUrl = XmlTool.GetText("Pages/Rss/P_0_1");
                else
                    TempUrl = XmlTool.GetText("Pages/Rss/P_0_N");
            XmlTool.Dispose();
            return TempUrl;
        }
        /// <summary>
        /// 内容页
        /// </summary>
        public static string View(bool _isHtml, string _siteDir, bool urlRewrite, int page)
        {
            string strXmlFile = HttpContext.Current.Server.MapPath(_siteDir + "_data/config/pageformat.config");
            JumbotDLS.DBUtility.XmlControl XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
            string TempUrl = "";
            if (_isHtml)
            {
                if (page == 1)
                    TempUrl = XmlTool.GetText("Pages/View/P_2_1");
                else
                    TempUrl = XmlTool.GetText("Pages/View/P_2_N");
            }
            else
            {
                if (urlRewrite)
                    if (page == 1)
                        TempUrl = XmlTool.GetText("Pages/View/P_1_1");
                    else
                        TempUrl = XmlTool.GetText("Pages/View/P_1_N");
                else
                    if (page == 1)
                        TempUrl = XmlTool.GetText("Pages/View/P_0_1");
                    else
                        TempUrl = XmlTool.GetText("Pages/View/P_0_N");
            }
            XmlTool.Dispose();
            return TempUrl;
        }
    }
}
