﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Web;
using System.Data;
using System.Text;
namespace JumbotDLS.UI
{
    public class FrontPassport : BasicPage
    {
        override protected void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (JumbotDLS.Utils.Cookie.GetValue("passport_theme") == null)
                PassportTheme = site.PassportTheme;
            else
                PassportTheme = JumbotDLS.Utils.Cookie.GetValue("passport_theme");
        }
        public string PassportTheme = "";
        /// <summary>
        /// 判断接口是否已经启用
        /// </summary>
        /// <param name="_oauthcode"></param>
        public void CheckOAuthState(string _oauthcode)
        {
            if (new JumbotDLS.DAL.Normal_UserOAuthDAL().Running(_oauthcode))
                return;
            Response.Write("接口未启动");
            Response.End();
        }

    }
}