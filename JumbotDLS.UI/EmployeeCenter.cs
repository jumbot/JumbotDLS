﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using System.IO;
using System.Text;
using JumbotDLS.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace JumbotDLS.UI
{
    /// <summary>
    /// html页使用
    /// </summary>
    public class EmployeeCenter : BasicPage
    {
        /// <summary>
        /// 所长id
        /// </summary>
        protected string DirectorID = "24";
        /// <summary>
        /// 所长姓名
        /// </summary>
        protected string DirectorName = "李健";
        /// <summary>
        /// 所长openid
        /// </summary>
        protected string DirectorOpenID = "1425C9E7-E6A2-4EB4-AECE-2098057ED93E";
        /// <summary>
        /// 所长weixinid
        /// </summary>
        protected string DirectorWenxinID = "owW5uw8D1v3j-HG9AowRdNRs1rw0";
        protected string EmployeeId = "0";
        protected string EmployeeName = string.Empty;
        protected string EmployeeAdminOpenID = string.Empty;
        protected string EmployeeAppList = string.Empty;
        protected bool EmployeeIsLogin = false;
        /// <summary>
        /// 当前员工
        /// </summary>
        protected JumbotDLS.Entity.Employee CurrentEmployee = null;
        /// <summary>
        /// 列表内容通用方法,必须重写
        /// </summary>
        protected virtual void getListBox() { }
        /// <summary>
        /// 编辑内容通用方法,必须重写
        /// </summary>
        protected virtual void editBox() { }
        /// <summary>
        /// 验证登陆
        /// </summary>
        private void chkLogin()
        {
            if (Cookie.GetValue(site.CookiePrev + "employee") != null)
            {
                EmployeeId = Str2Str(Cookie.GetValue(site.CookiePrev + "employee", "id"));
                EmployeeName = Cookie.GetValue(site.CookiePrev + "employee", "name");
                if (EmployeeId.Length != 0 && EmployeeName.Length != 0)
                {
                    CurrentEmployee = new JumbotDLS.DAL.EmployeeDAL().GetEntity(EmployeeId);
                    if (CurrentEmployee != null)
                    {
                        this.EmployeeIsLogin = true;
                        EmployeeAppList = CurrentEmployee.EmployeeAppList;
                        EmployeeAdminOpenID = CurrentEmployee.AdminOpenID;
                    }
                }
            }
        }
        /// <summary>
        /// 验证权限
        /// 系统管理员永远有效
        /// </summary>
        /// <param name="s">空时只要求登录</param>
        protected bool IsPower(string s)
        {
            if (s == "ok") return true;
            if (!this.EmployeeIsLogin)//验证一次本地信息
                chkLogin();
            if (s == "") return (this.EmployeeIsLogin);
            return (this.EmployeeAppList.Contains("," + s + ","));
        }
        /// <summary>
        /// 验证权限
        /// 系统管理员永远有效
        /// </summary>
        /// <param name="s"></param>
        /// <param name="pageType">页面分为html和json</param>
        protected void chkPower(string s, string pageType)
        {
            if (pageType == "json" && !CheckFormUrl())//不可直接在url下访问
            {
                Response.End();
            }
            if (!IsPower(s))
            {
                showErrMsg("未登录或当前用户的权限不足", pageType);
            }
        }
        /// <summary>
        /// 输出错误信息
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="pageType">页面分为html和json</param>
        protected void showErrMsg(string msg, string pageType)
        {

            if (pageType == "html")
            {
                HttpContext.Current.Response.Redirect("login.aspx");
                return;
            }
            else if (pageType == "stop")
            {
                HttpContext.Current.Response.Redirect("stop.htm");
                HttpContext.Current.Response.End();
                return;
            }

            HttpContext.Current.Response.Clear();
            if (!this.EmployeeIsLogin)
            {
                HttpContext.Current.Response.Write(base.JsonResult(-1, msg));
            }
            else
            {
                HttpContext.Current.Response.Write(base.JsonResult(0, msg));
            }
            HttpContext.Current.Response.End();

        }
        /// <summary>
        /// 管理中心初始
        /// </summary>
        /// <param name="powerNum">权限,为空表示验证是否登录</param>
        /// <param name="pageType">页面分为html和json</param>
        protected void Employee_Load(string powerNum, string pageType)
        {
            chkPower(powerNum, pageType);
        }
    }
}