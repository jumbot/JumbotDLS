﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using System.IO;
using System.Text;
using JumbotDLS.Utils;
namespace JumbotDLS.UI
{
    /// <summary>
    /// html页使用
    /// </summary>
    public class AdminCenter : BasicPage
    {
        public int publicMenu = 7;
        public JumbotDLS.Entity.Normal_Channel MainChannel = null;
        public string ChannelId = "0";
        public string ChannelName = string.Empty;
        public string ChannelType = "system";
        public string ChannelDir = string.Empty;
        public string ChannelItemName = string.Empty;
        public string ChannelItemUnit = string.Empty;
        public int ChannelClassDepth = 0;
        public bool ChannelIsHtml = false;
        public string ChannelUploadPath = string.Empty;
        public string ChannelUploadType = string.Empty;
        public int ChannelUploadSize = 0;
        public string ModuleUploadPath = string.Empty;
        public string ModuleUploadType = string.Empty;
        public int ModuleUploadSize = 0;
        public string id = "0";
        protected string AdminId = "0";
        protected string AdminName = string.Empty;
        protected string AdminPass = string.Empty;
        protected string AdminSign = string.Empty;
        protected string AdminSetting = string.Empty;
        protected string AdminCookiess = string.Empty;
        protected bool AdminIsLogin = false;
        protected bool AdminIsFounder = false;
        /// <summary>
        /// 列表内容通用方法,必须重写
        /// </summary>
        protected virtual void getListBox() { }
        /// <summary>
        /// 编辑内容通用方法,必须重写
        /// </summary>
        protected virtual void editBox() { }
        /// <summary>
        /// 验证登陆
        /// </summary>
        private void chkLogin()
        {
            if (Cookie.GetValue(site.CookiePrev + "admin") != null)
            {
                AdminId = Str2Str(Cookie.GetValue(site.CookiePrev + "admin", "id"));
                AdminName = Cookie.GetValue(site.CookiePrev + "admin", "name");
                AdminCookiess = Cookie.GetValue(site.CookiePrev + "admin", "cookiess");
                if (AdminId.Length != 0 && AdminName.Length != 0)
                {
                    doh.Reset();
                    doh.ConditionExpress = "adminid=@adminid and cookiess=@cookiess";//禁止一个用户同时登录
                    doh.AddConditionParameter("@adminid", AdminId);
                    doh.AddConditionParameter("@cookiess", AdminCookiess);
                    object[] _value = doh.GetFields("View_Admin", "AdminPass,AdminSign,AdminSetting");
                    if (_value != null)
                    {
                        this.AdminIsLogin = true;
                        AdminPass = _value[0].ToString();
                        AdminSign = _value[1].ToString();
                        AdminSetting = _value[2].ToString();
                        string Founders = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "Founders");
                        this.AdminIsFounder = (Founders.ToLower().Contains("." + AdminName.ToLower() + "."));
                    }
                }
            }
        }
        /// <summary>
        /// 验证权限
        /// 系统管理员永远有效
        /// </summary>
        /// <param name="s">空时只要求登录</param>
        protected bool IsPower(string s)
        {
            if (s == "ok") return true;
            if (!this.AdminIsLogin)//验证一次本地信息
                chkLogin();
            if (s == "") return (this.AdminIsLogin);
            if (this.AdminIsFounder) return (this.AdminIsLogin);
            return (this.AdminSetting.Contains("," + s + ","));
        }
        /// <summary>
        /// 验证权限
        /// 系统管理员永远有效
        /// </summary>
        /// <param name="s"></param>
        /// <param name="pageType">页面分为html和json</param>
        protected void chkPower(string s, string pageType)
        {
            if (pageType == "json" && !CheckFormUrl())//不可直接在url下访问
            {
                Response.End();
            }
            if (!IsPower(s))
            {
                showErrMsg("未登录或当前用户的权限不足", pageType);
            }
        }
        /// <summary>
        /// 输出错误信息
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="pageType">页面分为html和json</param>
        protected void showErrMsg(string msg, string pageType)
        {

            if (pageType == "html")
            {
                base.FinalMessage(msg, "login.aspx", 0);
                return;
            }
            else if (pageType == "stop")
            {
                HttpContext.Current.Response.Redirect("stop.htm");
                HttpContext.Current.Response.End();
                return;
            }

            HttpContext.Current.Response.Clear();
            if (!this.AdminIsLogin)
            {
                HttpContext.Current.Response.Write(base.JsonResult(-1, msg));
            }
            else
            {
                HttpContext.Current.Response.Write(base.JsonResult(0, msg));
            }
            HttpContext.Current.Response.End();

        }
        /// <summary>
        /// 管理中心初始
        /// </summary>
        /// <param name="powerNum">权限,为空表示验证是否登录</param>
        /// <param name="pageType">页面分为html和json</param>
        protected void Admin_Load(string powerNum, string pageType)
        {
            chkPower(powerNum, pageType);
        }

        /// <summary>
        /// 管理中心初始,并获得频道的各项参数值
        /// </summary>
        /// <param name="powerNum">权限</param>
        /// <param name="isChannel">如果为false就表示ChannelId可以为0</param>
        protected void Admin_Load(string powerNum, string pageType, bool isChannel)
        {
            chkPower(powerNum, pageType);
            if (isChannel && ChannelId == "0")
            {
                showErrMsg("参数错误,请不要在外部提交数据", pageType);
                return;
            }
            if (ChannelId != "0")
            {
                JumbotDLS.Entity.Normal_Channel _Channel = new JumbotDLS.DAL.Normal_ChannelDAL().GetEntity(ChannelId);
                ChannelName = _Channel.Title;
                ChannelDir = _Channel.Dir;
                ChannelType = _Channel.Type;
                ChannelItemName = _Channel.ItemName;
                ChannelItemUnit = _Channel.ItemUnit;
                ChannelClassDepth = _Channel.ClassDepth;
                ChannelIsHtml = _Channel.IsHtml;
                //去掉标签后的实际路径
                ChannelUploadPath = _Channel.UploadPath;
                ChannelUploadType = _Channel.UploadType;
                ChannelUploadSize = _Channel.UploadSize;
                MainChannel = _Channel;
            }
        }
        /// <summary>
        /// 管理菜单
        /// </summary>
        /// <returns></returns>
        protected string[,] leftMenu()
        {
            string[,] menu = new string[publicMenu + 2, 40];
            menu[0, 0] = "系统管理$0$global";
            menu[0, 1] = "configset_default.aspx|站点信息";
            menu[0, 2] = "adminrole_list.aspx|管理员角色";
            menu[0, 3] = "admin_list.aspx|后台管理员";
            menu[0, 4] = "";// "modules_list.aspx|内容模型";
            menu[0, 5] = "channel_list.aspx|子库管理";
            menu[0, 6] = "";// "extends_list.aspx|站点插件";
            menu[0, 7] = "organization_list.aspx|机构管理";
            menu[0, 8] = "forbidip_list.aspx|IP黑名单";
            menu[0, 9] = "";// "mail_setting.aspx|邮件设置";
            menu[0, 10] = "";// "useroauth_list.aspx|OAuth整合";
            menu[0, 11] = "";//"discuz_api.aspx|Discuz!NT整合";
            menu[0, 12] = "";//"database" + base.DBType + ".aspx|数据库维护";
            menu[0, 13] = "";//"executesql_default.aspx|在线执行SQL";
            menu[0, 14] = "adminlogs_list.aspx|管理员操作日志";


            menu[1, 0] = "用户管理$0$member";
            menu[1, 1] = "user_list.aspx?gid=1|普通会员";
            menu[1, 2] = "user_list.aspx?gid=2|专家会员";

            menu[2, 0] = "模板管理$0$template";
            menu[2, 1] = "template_list.aspx?pid=1|HTML模板";
            menu[2, 2] = "templateinclude_list.aspx?pid=1|include模块";
            menu[2, 3] = "dict_list.aspx|字典管理";//"page_list.aspx|单页内容管理";
            menu[2, 4] = "javascript_list.aspx|自定义外站调用";

            menu[3, 0] = "后台首页$0$config";
            menu[3, 1] = "home.aspx|前台更新";
            //menu[3, 2] = "announcement_list.aspx|内部公告";
            menu[3, 2] = "myinfo_password.aspx|修改密码";

            menu[4, 0] = "邮件群发$0$email";

            menu[5, 0] = "插件管理$0$extends";
            int j = 0;
            doh.Reset();
            doh.SqlCmd = "SELECT Id,[Title],[Name] FROM [jdls_normal_extends] WHERE [Enabled]=1 ORDER BY pId";
            DataTable dtExtend = doh.GetDataTable();
            for (int i = 0; i < dtExtend.Rows.Count; i++)
            {
                j += 1;
                string mExtendTitle = dtExtend.Rows[i]["Title"].ToString();
                string mExtendName = dtExtend.Rows[i]["Name"].ToString().ToLower();
                menu[5, j] = "extend_" + mExtendName + "_list.aspx|" + mExtendTitle;
            }
            dtExtend.Clear();
            dtExtend.Dispose();

            menu[6, 0] = "信息采集$0$collect";
            doh.Reset();
            doh.SqlCmd = "SELECT Id,Title FROM [jdls_normal_channel] WHERE [Type]='article' AND [Enabled]=1 AND [CanCollect]=1 ORDER BY pId";
            DataTable dtChannel1 = doh.GetDataTable();
            for (int m = 0; m < dtChannel1.Rows.Count; m++)
            {
                string mChannelId1 = dtChannel1.Rows[m]["Id"].ToString();
                string mChannelName1 = dtChannel1.Rows[m]["Title"].ToString();
                menu[6, m + 1] = "module_article_collitem_list1.aspx?ccid=" + mChannelId1 + "|" + mChannelName1 + "|" + mChannelId1;

            }
            dtChannel1.Clear();
            dtChannel1.Dispose();

            menu[7, 0] = "内容管理$0$content";
            doh.Reset();
            doh.SqlCmd = "SELECT Id,Title,Type FROM [jdls_normal_channel] WHERE [Enabled]=1 ORDER BY pId";
            DataTable dtChannel2 = doh.GetDataTable();
            for (int i = 0; i < dtChannel2.Rows.Count; i++)
            {
                string mChannelId2 = dtChannel2.Rows[i]["Id"].ToString();
                string mChannelName2 = dtChannel2.Rows[i]["Title"].ToString();
                string mChannelType2 = dtChannel2.Rows[i]["Type"].ToString().ToLower();
                menu[7, i + 1] = IsPower(mChannelId2 + "-00") ? "module_" + mChannelType2 + "_list.aspx?ccid=" + mChannelId2 + "&ctype=" + mChannelType2 + "|" + mChannelName2 + "|" + mChannelId2 : "";
            }
            dtChannel2.Clear();
            dtChannel2.Dispose();

            menu[8, 0] = "综合管理$0$total";
            menu[8, 1] = "";// IsPower("link-mng") ? "link_list.aspx|友情链接" : "";
            menu[8, 2] = "";// IsPower("adv-mng") ? "adv_list.aspx|广告管理" : "";
            menu[8, 3] = IsPower("question-mng") ? "question_list.aspx|在线答疑" : "";
            menu[8, 4] = "";// IsPower("review-mng") ? "review_list.aspx|评论管理" : "";
            return menu;
        }

        /// <summary>
        /// 编辑内容时,向栏目专题标题颜色等DropDownList中添加内容
        /// </summary>
        /// <param name="ddlClassId">栏目ID</param>
        /// <param name="ClassDepth">栏目深度</param>
        /// <param name="ddlReadGroup">阅读权限</param>
        protected void getEditDropDownList(int ModuleId, ref DropDownList ddlClassId, int ClassDepth, ref DropDownList ddlReadGroup)
        {
            if (!Page.IsPostBack)
            {
                if (ChannelClassDepth > 0)
                {
                    doh.Reset();
                    doh.SqlCmd = "SELECT [ID],[Title],[code] FROM [jdls_normal_class] WHERE [IsOut]=0 AND [ChannelId]=" + ChannelId + " AND ([DefaultModuleId]=-1 OR [DefaultModuleId]=" + ModuleId+")";
                    if (ClassDepth > 0)
                        doh.SqlCmd += " AND left(code)<=" + (4 * ClassDepth).ToString();
                    doh.SqlCmd += " ORDER BY code";
                    DataTable dtClass = doh.GetDataTable();
                    if (dtClass.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtClass.Rows.Count; i++)
                        {
                            ddlClassId.Items.Add(new ListItem(getListName(dtClass.Rows[i]["Title"].ToString(), dtClass.Rows[i]["code"].ToString()), dtClass.Rows[i]["Id"].ToString()));
                        }
                    }
                    dtClass.Clear();
                    dtClass.Dispose();
                }
                else
                {
                    ddlClassId.Items.Add(new ListItem("根栏目", "0"));

                }
                doh.Reset();
                doh.SqlCmd = "SELECT [ID],[GroupName] FROM [jdls_normal_usergroup] ORDER BY id ASC";
                DataTable dtGroup = doh.GetDataTable();
                ddlReadGroup.Items.Clear();
                if (ChannelClassDepth > 0)
                    ddlReadGroup.Items.Add(new ListItem("继承栏目权限", "-1"));
                ddlReadGroup.Items.Add(new ListItem("匿名游客", "0"));
                for (int i = 0; i < dtGroup.Rows.Count; i++)
                {
                    ddlReadGroup.Items.Add(new ListItem(dtGroup.Rows[i]["GroupName"].ToString(), dtGroup.Rows[i]["Id"].ToString()));
                }
                dtGroup.Clear();
                dtGroup.Dispose();
            }
        }
        /// <summary>
        /// 统计用户组用户总数
        /// <param name="_gId">用户组Id，为0表示更新所有的</param>
        /// </summary>
        protected void UserGroupCount(string _gId)
        {
            doh.Reset();
            doh.SqlCmd = "SELECT ID FROM [jdls_normal_usergroup]";
            if (_gId != "0") doh.SqlCmd += " WHERE [Id]=" + _gId;
            DataTable dtUserGroup = doh.GetDataTable();
            int total = dtUserGroup.Rows.Count;
            int tmp;
            for (int i = 0; i < total; i++)
            {
                doh.Reset();
                doh.SqlCmd = "SELECT ID FROM [jdls_normal_user] WHERE [Group]=" + dtUserGroup.Rows[i]["Id"].ToString();
                tmp = doh.GetDataTable().Rows.Count;
                doh.Reset();
                doh.ConditionExpress = "id=" + dtUserGroup.Rows[i]["Id"].ToString();
                doh.AddFieldItem("UserTotal", tmp);
                doh.Update("jdls_normal_usergroup");
            }
            dtUserGroup.Clear();
            dtUserGroup.Dispose();
        }
        /// <summary>
        /// 统计邮箱组邮箱总数
        /// <param name="_gId">邮箱组Id，为0表示更新所有的</param>
        /// </summary>
        protected void EmailGroupCount(string _gId)
        {
            doh.Reset();
            doh.SqlCmd = "SELECT ID FROM [jdls_email_usergroup]";
            if (_gId != "0") doh.SqlCmd += " WHERE [Id]=" + _gId;
            DataTable dtEmailGroup = doh.GetDataTable();
            int total = dtEmailGroup.Rows.Count;
            int tmp;
            for (int i = 0; i < total; i++)
            {
                doh.Reset();
                doh.SqlCmd = "SELECT ID FROM [jdls_email_user] WHERE [GroupId]=" + dtEmailGroup.Rows[i]["Id"].ToString();
                tmp = doh.GetDataTable().Rows.Count;
                doh.Reset();
                doh.ConditionExpress = "id=" + dtEmailGroup.Rows[i]["Id"].ToString();
                doh.AddFieldItem("EmailTotal", tmp);
                doh.Update("jdls_email_usergroup");
            }
            dtEmailGroup.Clear();
            dtEmailGroup.Dispose();
        }

        /// <summary>
        /// 取得内容列表
        /// </summary>
        /// <param name="_classId">栏目Id</param>
        /// <param name="keyType">搜索关键字类型{Author,title,summary}</param>
        /// <param name="keyWord">搜索关键字</param>
        /// <param name="sDate">日期{1d=今天,1w=本周,1m=本月}</param>
        /// <param name="isPass">状态{0=待审,1=已审,-1=已删,否则=全部}</param>
        /// <param name="isImg"></param>
        /// <param name="isTop"></param>
        /// <param name="isFocus"></param>
        /// <param name="isHead"></param>
        /// <param name="PSize">每页记录数</param>
        /// <param name="page">页码</param>
        /// <returns></returns>
        protected string GetContentList(string _classId, string keyType, string keyWord, string sDate, string isPass, string isImg, string isTop, string isFocus, string isHead, int PSize, int page)
        {
            string _returnStr = string.Empty;
            _classId = Str2Str(_classId);
            keyType = keyType.ToLower();
            keyWord = JumbotDLS.Utils.Strings.SafetyLikeValue(keyWord);
            int totalCount = 0;
            string sqlStr = "";
            string whereStr = " [ParentID]=0 AND [ChannelId]=" + ChannelId;//分页条件(不带A.)
            if (_classId != "0")
            {
                whereStr += " And [ClassId] in (SELECT ID FROM [jdls_normal_class] WHERE [Code] Like (SELECT Code FROM [jdls_normal_class] WHERE [Id]=" + _classId + ")+'%')";
            }
            if (("title,title2,tags,summary,author".Contains(keyType)) && (keyWord.Length > 0))
            {
                whereStr += " AND " + keyType + " LIKE '%" + keyWord + "%'";
            }
            switch (isPass)
            {
                case "0":
                    whereStr += " AND [IsPass]=0";
                    break;
                case "1":
                    whereStr += " AND [IsPass]=1";
                    break;
                case "-1":
                    whereStr += " AND [IsPass]=-1";
                    break;
                default:
                    break;
            }
            switch (isImg)
            {
                case "2":
                    whereStr += " AND ([isImg]=1 AND left(Img,7)='http://')";
                    break;
                case "1":
                    whereStr += " AND [isImg]=1";
                    break;
                case "-1":
                    whereStr += " AND [isImg]=0";
                    break;
                default:
                    break;
            }
            switch (isTop)
            {
                case "1":
                    whereStr += " AND [isTop]=1";
                    break;
                case "-1":
                    whereStr += " AND [isTop]=0";
                    break;
                default:
                    break;
            }
            switch (isFocus)
            {
                case "1":
                    whereStr += " AND [isFocus]=1";
                    break;
                case "-1":
                    whereStr += " AND [isFocus]=0";
                    break;
                default:
                    break;
            }
            switch (isHead)
            {
                case "1":
                    whereStr += " AND [isHead]=1";
                    break;
                case "-1":
                    whereStr += " AND [isHead]=0";
                    break;
                default:
                    break;
            }
            if (sDate != "")
            {
                if (DBType == "0")
                {
                    switch (sDate)
                    {
                        case "1d":
                            whereStr += " AND datediff('d',AddDate,'" + DateTime.Now.ToShortDateString() + "')=0";
                            break;
                        case "1w":
                            whereStr += " AND datediff('ww',AddDate,'" + DateTime.Now.ToShortDateString() + "')=0";
                            break;
                        case "1m":
                            whereStr += " AND datediff('m',AddDate,'" + DateTime.Now.ToShortDateString() + "')=0";
                            break;
                    }
                }
                else
                {
                    switch (sDate)
                    {
                        case "1d":
                            whereStr += " AND datediff(d,AddDate,'" + DateTime.Now.ToShortDateString() + "')=0";
                            break;
                        case "1w":
                            whereStr += " AND datediff(ww,AddDate,'" + DateTime.Now.ToShortDateString() + "')=0";
                            break;
                        case "1m":
                            whereStr += " AND datediff(m,AddDate,'" + DateTime.Now.ToShortDateString() + "')=0";
                            break;
                    }
                }
            }
            doh.Reset();
            doh.ConditionExpress = whereStr;
            totalCount = doh.Count("jdls_module_" + ChannelType);
            NameValueCollection orders = new NameValueCollection();
            orders.Add("AddDate", "desc");
            orders.Add("Id", "desc");
            string FieldList = "Id,ChannelId,ContentCode,Title,title2,TColor,IsPass,Locked,IsImg,IsTop,IsFocus,IsHead,FirstPage,AddDate,adminid,(select top 1 adminname from [jdls_normal_user] where adminid=[jdls_module_" + ChannelType + "].adminid) as AdminName,(select title from [jdls_normal_class] where channelid=" + ChannelId + " and id=[jdls_module_" + ChannelType + "].classid) as ClassName,subclasslist";
            if (keyType.ToLower() != "title" && keyType.Length > 0)
                FieldList += "," + keyType;
            string _modulelist2 = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "ModuleList2");
            if (_modulelist2.Contains("." + ChannelType + "."))
                FieldList += ",points";
            if (ChannelId=="9")
                FieldList += ",CustomField01,CustomField02";
            sqlStr = JumbotDLS.Utils.SqlHelp.GetSql1(FieldList, "jdls_module_" + ChannelType, totalCount, PSize, page, orders, whereStr);
            if (site.SiteDataSize > 300000)
                sqlStr = JumbotDLS.Utils.SqlHelp.GetSql0(FieldList, "jdls_module_" + ChannelType, "id", PSize, page, "desc", whereStr);

            doh.Reset();
            doh.SqlCmd = sqlStr;
            DataTable dt = doh.GetDataTable();
            doh.Reset();
            doh.ConditionExpress = "ispass=0 and [ChannelId]=" + ChannelId;
            int NewNum = doh.Count("jdls_module_" + ChannelType);
            _returnStr = "{\"result\" :\"1\"," +
                "\"returnval\" :\"操作成功\"," +
                "\"newnum\" :" + NewNum + "," +
                "\"returmodule\" :\"" + ChannelType + "\"," +
                "\"pagebar\" :\"" + JumbotDLS.Utils.PageBar.GetPageBar(3, "js", 2, totalCount, PSize, page, "javascript:ajaxList($#page#$);") + "\"," +
                JumbotDLS.Utils.dtHelper.DT2JSON(dt, (PSize * (page - 1))) +
                "}";
            dt.Clear();
            dt.Dispose();
            return _returnStr;
        }
        protected string GetContentSubList(string _parentid, string _moduleid, int PSize, int page)
        {
            string _returnStr = string.Empty;
            int totalCount = 0;
            string sqlStr = "";
            string whereStr = "parentid=" + _parentid;//分页条件(不带A.)
            whereStr += " And [module]=(SELECT [type] FROM [jdls_module_article_module] WHERE [ID]=" + _moduleid + ")";

            doh.Reset();
            doh.ConditionExpress = whereStr;
            totalCount = doh.Count("jdls_module_article");
            NameValueCollection orders = new NameValueCollection();
            orders.Add("title", "asc");
            orders.Add("Id", "desc");
            string FieldList = "*";
            sqlStr = JumbotDLS.Utils.SqlHelp.GetSql1(FieldList, "jdls_module_article", totalCount, PSize, page, orders, whereStr);

            doh.Reset();
            doh.SqlCmd = sqlStr;
            DataTable dt = doh.GetDataTable();
            _returnStr = "{\"result\" :\"1\"," +
                "\"returnval\" :\"操作成功\"," +
                "\"pagebar\" :\"" + JumbotDLS.Utils.PageBar.GetPageBar(3, "js", 2, totalCount, PSize, page, "javascript:ajaxList($#page#$);") + "\"," +
                JumbotDLS.Utils.dtHelper.DT2JSON(dt, (PSize * (page - 1))) +
                "}";
            dt.Clear();
            dt.Dispose();
            return _returnStr;
        }

        /// <summary>
        /// 统计数据
        /// </summary>
        /// <param name="_channelID"></param>
        protected void CreateCount(string _channelID)
        {
            int _classCount = 0;
            int _contentCount = 0;
            if (_channelID == "0")
            {
                string TempCountStr = "var ___JSON_SystemCount = /*请勿手动修改*/\r\n{";
                doh.Reset();
                doh.SqlCmd = "SELECT [ID],[Title],[Type] FROM [jdls_normal_channel] WHERE [Enabled]=1 ORDER BY pId";
                DataTable dtChannel = doh.GetDataTable();
                TempCountStr += "table: [";
                if (dtChannel.Rows.Count > 0)
                {
                    for (int c = 0; c < dtChannel.Rows.Count; c++)
                    {
                        if (c > 0) TempCountStr += ",";
                        string _ccId = dtChannel.Rows[c]["Id"].ToString();
                        string _ccTitle = dtChannel.Rows[c]["Title"].ToString();
                        string _ccType = dtChannel.Rows[c]["Type"].ToString();
                        doh.Reset();
                        doh.ConditionExpress = "ChannelId=" + _ccId + " and ParentId=0";
                        _classCount = doh.Count("jdls_normal_class");
                        doh.Reset();
                        doh.ConditionExpress = "ChannelId=" + _ccId + " and IsPass=1";
                        _contentCount = doh.Count("jdls_module_" + _ccType);
                        TempCountStr += "{" +
                            "\"channelid\": " + _ccId + "," +
                            "\"classid\": 0," +
                            "\"title\": '" + _ccTitle + "'," +
                            "\"classcount\": " + _classCount + "," +
                            "\"contentcount\": " + _contentCount + "}";

                        doh.Reset();
                        doh.SqlCmd = "SELECT Id,Title,[Code] FROM [jdls_normal_class] WHERE [ParentId]=0 AND [ChannelId]=" + _ccId + " ORDER BY code";
                        DataTable dtClass = doh.GetDataTable();
                        if (dtClass.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtClass.Rows.Count; i++)
                            {
                                string _cId = dtClass.Rows[i]["Id"].ToString();
                                string _cTitle = dtClass.Rows[i]["Title"].ToString();
                                string _cCode = dtClass.Rows[i]["Code"].ToString();
                                doh.Reset();
                                doh.ConditionExpress = "ChannelId=" + _ccId + " and ParentId=" + _cId;
                                _classCount = doh.Count("jdls_normal_class");
                                doh.Reset();
                                doh.ConditionExpress = "ChannelId=" + _ccId + " and IsPass=1 And [ClassID] in (SELECT ID FROM [jdls_normal_class] WHERE [IsOut]=0 AND [Code] Like '" + _cCode + "%')";
                                _contentCount = doh.Count("jdls_module_" + _ccType);
                                TempCountStr += "," + "{" +
                                    "\"channelid\": " + _ccId + "," +
                                    "\"classid\": " + _cId + "," +
                                    "\"title\": '" + _cTitle + "'," +
                                    "\"classcount\": " + _classCount + "," +
                                    "\"contentcount\": " + _contentCount + "}";
                            }
                        }
                    }
                }
                TempCountStr += "]";
                TempCountStr += "}";
                JumbotDLS.Utils.DirFile.SaveFile(TempCountStr, "~/_data/json/_systemcount.js");
            }
        }
        protected void CreateNavigate(string _ccID)
        {
            doh.Reset();
            doh.SqlCmd = "SELECT ID FROM [jdls_normal_class] WHERE [ChannelId]=" + _ccID + " ORDER BY code";
            DataTable dtClass = doh.GetDataTable();
            if (dtClass.Rows.Count > 0)
            {
                for (int i = 0; i < dtClass.Rows.Count; i++)
                {
                    string _classId = dtClass.Rows[i]["Id"].ToString();
                    CreateClassNavigate(_ccID, _classId);
                }
            }
            dtClass.Clear();
            dtClass.Dispose();
        }
        protected void CreateClassNavigate(string _ccID, string _classId)
        {
            JumbotDLS.Entity.Normal_Channel _Channel = new JumbotDLS.DAL.Normal_ChannelDAL().GetEntity(_ccID);
            string jsfile = Server.MapPath(site.Dir + _Channel.Dir + "/js/classnav_" + _classId + ".js");
            string jsstr = "document.write('" + ClassFullNavigateHtml(_ccID, _classId) + "');";
            SaveJsFile(jsstr, jsfile);
        }

        /// <summary>
        /// 执行内容的移动,审核,删除等操作
        /// </summary>
        /// <param name="_act">操作类型{pass=审核,nopass=未审,remove=移入回收站,del=彻底删除}</param>
        /// <param name="_ids">id字符串,以","串联起来</param>
        /// <param name="pageType">页面分为html和json</param>
        public void BatchContent(string _act, string _tocid, string _ids, string _ccId, string _cType, string pageType)
        {
            string[] idValue;
            idValue = _ids.Split(',');
            if (_act == "createhtml")
            {
                Admin_Load(_ccId + "-04", "json");
                for (int i = 0; i < idValue.Length; i++)
                {
                    if (ChannelIsHtml)
                        CreateContentFile(MainChannel, idValue[i], -1);//生成内容页
                }
                JumbotDLS.DAL.TemplateEngineDAL teDAL = new JumbotDLS.DAL.TemplateEngineDAL(_ccId);
                CreateChannelFile(MainChannel);
                return;
            }
            if (_act == "pass")
            {
                Admin_Load(_ccId + "-04", "json");
                for (int i = 0; i < idValue.Length; i++)
                {
                    doh.Reset();
                    doh.ConditionExpress = "id=" + idValue[i] + " and [IsPass]<1";
                    doh.AddFieldItem("IsPass", 1);
                    if (doh.Update("jdls_module_" + _cType) == 1)
                    {
                        if (ChannelIsHtml)
                            CreateContentFile(MainChannel, idValue[i], -1);//生成内容页
                    }
                }
                return;
            }
            if (_act == "nopass")
            {
                Admin_Load(_ccId + "-04", "json");
                for (int i = 0; i < idValue.Length; i++)
                {
                    doh.Reset();
                    doh.ConditionExpress = "id=" + idValue[i] + " and [IsPass]=1";
                    doh.AddFieldItem("IsPass", 0);
                    if (doh.Update("jdls_module_" + _cType) == 1)
                    {
                        if (ChannelIsHtml)
                            DeleteContentFile(idValue[i]);//删内容页
                    }
                }
                return;
            }
            #region 关联会议数据
            if (_act == "bindmeetingfiles")
            {
                Admin_Load(_ccId + "-01", "json");
                string _UploadPath1 = System.Configuration.ConfigurationManager.AppSettings["JumbotDLS:UploadPath"];
                if (!_UploadPath1.Contains(":"))
                    _UploadPath1 = Server.MapPath(_UploadPath1);
                for (int i = 0; i < idValue.Length; i++)
                {
                    doh.Reset();
                    doh.ConditionExpress = "id=" + idValue[i] + " and [Module]='meeting'";
                    string _meetingname = doh.GetField("jdls_module_" + _cType, "Title").ToString();
                    string _forderpath = string.Empty;
                    DirectoryInfo TheFolder = null;
                    string _filename = string.Empty;
                    string _moduletype = string.Empty;
                    #region 处理发言稿
                    _forderpath = _UploadPath1 + "\\meeting\\" + _meetingname + "\\发言稿";
                    TheFolder = new DirectoryInfo(_forderpath);
                    
                    foreach (FileInfo NextFile in TheFolder.GetFiles())
                    {
                        _filename = NextFile.Name;
                        _moduletype = "meeting_speech";
                        doh.Reset();
                        doh.ConditionExpress = "parentid=@parentid and title=@title and module=@module";
                        doh.AddConditionParameter("@parentid", idValue[i]);
                        doh.AddConditionParameter("@title", _meetingname);
                        doh.AddConditionParameter("@module", _moduletype);
                        if (!doh.Exist("jdls_module_article"))
                        {
                            doh.Reset();
                            doh.AddFieldItem("Title", _filename);
                            doh.AddFieldItem("CustomField01", "/uploads/meeting/" + _meetingname + "/发言稿/" + _filename);
                            doh.AddFieldItem("ChannelId", ChannelId);
                            doh.AddFieldItem("ParentId", idValue[i]);
                            doh.AddFieldItem("Module", _moduletype);
                            doh.AddFieldItem("IsImg", 0);
                            doh.AddFieldItem("IsPass", 1);
                            doh.AddFieldItem("AddDate", DateTime.Now.ToString());
                            doh.AddFieldItem("UserID", AdminId);
                            doh.AddFieldItem("AdminID", AdminId);
                            int _insertID = doh.Insert("jdls_module_article");
                        }
                    }
                    #endregion
                    #region 处理图片音频
                    _forderpath = _UploadPath1 + "\\meeting\\" + _meetingname + "\\图片音频";
                    TheFolder = new DirectoryInfo(_forderpath);

                    foreach (FileInfo NextFile in TheFolder.GetFiles())
                    {
                        _filename = NextFile.Name;
                        _moduletype = "meeting_media";
                        doh.Reset();
                        doh.ConditionExpress = "parentid=@parentid and title=@title and module=@module";
                        doh.AddConditionParameter("@parentid", idValue[i]);
                        doh.AddConditionParameter("@title", _meetingname);
                        doh.AddConditionParameter("@module", _moduletype);
                        if (!doh.Exist("jdls_module_article"))
                        {
                            doh.Reset();
                            doh.AddFieldItem("Title", _filename);
                            doh.AddFieldItem("CustomField01", "/uploads/meeting/" + _meetingname + "/图片音频/" + _filename);
                            doh.AddFieldItem("ChannelId", ChannelId);
                            doh.AddFieldItem("ParentId", idValue[i]);
                            doh.AddFieldItem("Module", _moduletype);
                            doh.AddFieldItem("IsImg", 0);
                            doh.AddFieldItem("IsPass", 1);
                            doh.AddFieldItem("AddDate", DateTime.Now.ToString());
                            doh.AddFieldItem("UserID", AdminId);
                            doh.AddFieldItem("AdminID", AdminId);
                            int _insertID = doh.Insert("jdls_module_article");
                        }
                    }
                    #endregion
                }
                return;
            }
            #endregion
            #region 关联人物档案
            if (_act == "bindpersonprofile")
            {
                return;
            }
            #endregion
            if (_act == "top")
            {
                Admin_Load(_ccId + "-05", "json");
                for (int i = 0; i < idValue.Length; i++)
                {
                    doh.Reset();
                    doh.ConditionExpress = "id=" + idValue[i];
                    doh.AddFieldItem("IsTop", 1);
                    doh.Update("jdls_module_" + _cType);
                }
                return;
            }
            if (_act == "notop")
            {
                Admin_Load(_ccId + "-05", "json");
                for (int i = 0; i < idValue.Length; i++)
                {
                    doh.Reset();
                    doh.ConditionExpress = "id=" + idValue[i];
                    doh.AddFieldItem("IsTop", 0);
                    doh.Update("jdls_module_" + _cType);
                }
                return;
            }
            if (_act == "focus")
            {
                Admin_Load(_ccId + "-05", "json");
                for (int i = 0; i < idValue.Length; i++)
                {
                    doh.Reset();
                    doh.ConditionExpress = "id=" + idValue[i];
                    doh.AddFieldItem("IsFocus", 1);
                    doh.Update("jdls_module_" + _cType);
                }
                return;
            }
            if (_act == "nofocus")
            {
                Admin_Load(_ccId + "-05", "json");
                for (int i = 0; i < idValue.Length; i++)
                {
                    doh.Reset();
                    doh.ConditionExpress = "id=" + idValue[i];
                    doh.AddFieldItem("IsFocus", 0);
                    doh.Update("jdls_module_" + _cType);
                }
                return;
            }
            if (_act == "head")
            {
                Admin_Load(_ccId + "-05", "json");
                for (int i = 0; i < idValue.Length; i++)
                {
                    doh.Reset();
                    doh.ConditionExpress = "id=" + idValue[i];
                    doh.AddFieldItem("IsHead", 1);
                    doh.Update("jdls_module_" + _cType);
                }
                return;
            }
            if (_act == "nohead")
            {
                Admin_Load(_ccId + "-05", "json");
                for (int i = 0; i < idValue.Length; i++)
                {
                    doh.Reset();
                    doh.ConditionExpress = "id=" + idValue[i];
                    doh.AddFieldItem("IsHead", 0);
                    doh.Update("jdls_module_" + _cType);
                }
                return;
            }
            if (_act == "move2class")
            {
                Admin_Load(_ccId + "-06", "json");
                for (int i = 0; i < idValue.Length; i++)
                {
                    doh.Reset();
                    doh.ConditionExpress = "id=" + idValue[i];
                    doh.AddFieldItem("ClassId", _tocid);
                    doh.Update("jdls_module_" + _cType);
                    if (ChannelIsHtml)
                        CreateContentFile(MainChannel, idValue[i], -1);//生成内容页
                }
                return;
            }
            if (_act == "sdel")
            {
                Admin_Load(_ccId + "-03", "json");
                for (int i = 0; i < idValue.Length; i++)
                {
                    if (ChannelIsHtml)
                        DeleteContentFile(idValue[i]);//删内容页
                    doh.Reset();
                    doh.ConditionExpress = "id=" + idValue[i];
                    doh.AddFieldItem("IsPass", -1);
                    doh.Update("jdls_module_" + _cType);//放入回收站
                }
                return;
            }
            if (_act == "del")
            {
                Admin_Load("master", "json");
                for (int i = 0; i < idValue.Length; i++)
                {
                    if (ChannelIsHtml)
                        DeleteContentFile(idValue[i]);//先删内容页
                    doh.Reset();
                    doh.ConditionExpress = "locked=0 and (id=" + idValue[i] + " OR parentid=" + idValue[i] + ")";
                    doh.Delete("jdls_module_" + _cType);
                }
                return;
            }
        }
        protected void ajaxCreateJavascript()
        {
            string strXmlFile = HttpContext.Current.Server.MapPath("~/_data/config/javascript.config");
            JumbotDLS.DBUtility.XmlControl XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
            try
            {
                XmlTool.RemoveAll("Lis");
                XmlTool.Save();
                doh.Reset();
                doh.SqlCmd = "Select [Id],[Title],[Code],[TemplateContent] FROM [jdls_normal_javascript] ORDER BY id asc";
                DataTable dt = doh.GetDataTable();
                string _id = string.Empty;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _id = dt.Rows[i]["Id"].ToString();
                    XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
                    XmlTool.InsertNode("Lis", "Li", "ID", _id);
                    XmlTool.InsertElement("Lis/Li[ID=\"" + _id + "\"]", "Title", dt.Rows[i]["Title"].ToString(), false);
                    XmlTool.InsertElement("Lis/Li[ID=\"" + _id + "\"]", "Code", dt.Rows[i]["Code"].ToString(), true);
                    XmlTool.InsertElement("Lis/Li[ID=\"" + _id + "\"]", "TemplateContent", dt.Rows[i]["TemplateContent"].ToString(), true);
                    XmlTool.Save();
                }
                XmlTool.Dispose();
            }
            catch
            {
            }
        }
        /// <summary>
        /// 删除网站地图文件
        /// </summary>

        protected void DeleteSiteMapFile()
        {
            JumbotDLS.Utils.DirFile.DeleteFile("~/sitemap" + site.StaticExt);
        }
        #region 生成静态页面
        /// <summary>
        /// 删除栏目文件
        /// </summary>
        /// <param name="_classId"></param>
        /// <param name="DeleteParent"></param>
        protected void DeleteClassFile(string _classId, bool DeleteParent)
        {
            if (System.IO.Directory.Exists(Server.MapPath("~/" + ChannelDir)))
            {
                string htmFile = Server.MapPath(Go2Class(1, true, ChannelId, _classId, true));
                if (System.IO.File.Exists(htmFile))
                    System.IO.File.Delete(htmFile);
                string[] htmFiles = System.IO.Directory.GetFiles(Server.MapPath("~/" + ChannelDir), "class_" + _classId + "_*" + site.StaticExt);
                foreach (string fileName in htmFiles)
                {
                    if (System.IO.File.Exists(fileName))
                        System.IO.File.Delete(fileName);
                }
                doh.Reset();
                doh.SqlCmd = "SELECT Id, ParentId FROM [jdls_normal_class] WHERE [IsOut]=0 AND [ChannelId]=" + ChannelId + " AND [Id]=" + _classId;
                DataTable dtClass = doh.GetDataTable();
                if (dtClass.Rows.Count > 0 && dtClass.Rows[0]["ParentId"].ToString() != "0" && DeleteParent == true)
                {
                    DeleteClassFile(dtClass.Rows[0]["ParentId"].ToString(), true);
                }
                dtClass.Clear();
                dtClass.Dispose();
            }

        }

        /// <summary>
        /// 删除内容文件
        /// </summary>

        protected void DeleteContentFile(string _contentID)
        {
            JumbotDLS.DAL.ModuleCommand.DeleteContent(ChannelType, ChannelId, _contentID);
        }
        #endregion
        /// <summary>
        /// 批量更新广告
        /// </summary>
        /// <returns></returns>
        public bool BatchUpdateAdv()
        {
            doh.Reset();
            doh.SqlCmd = "SELECT id,State FROM [jdls_normal_adv]";
            DataTable dt = doh.GetDataTable();
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string aId = dt.Rows[i][0].ToString();
                    string aState = dt.Rows[i][1].ToString();
                    new JumbotDLS.DAL.AdvDAL().CreateAdv(aId, aState);
                }
            }
            dt.Clear();
            dt.Dispose();
            return true;
        }
    }
}