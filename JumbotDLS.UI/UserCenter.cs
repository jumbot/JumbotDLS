﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using System.Text;
using JumbotDLS.Utils;
namespace JumbotDLS.UI
{
    /// <summary>
    /// </summary>
    public class UserCenter : FrontHtml
    {
        public int publicMenu = 2;
        public string ChannelId = "0";
        public string ChannelName = string.Empty;
        public string ChannelType = "system";
        public string ChannelDir = string.Empty;
        public string ChannelItemName = string.Empty;
        public string ChannelItemUnit = string.Empty;
        public int ChannelClassDepth = 0;
        public bool ChannelIsHtml = false;
        public string id = "0";
        protected string UserId = "0";
        protected string UserName = string.Empty;
        protected int UserPoints = 0;
        protected string UserSign = string.Empty;
        protected string UserKey = string.Empty;
        protected string UserEmail = string.Empty;
        protected string UserGroupId = "0";
        protected string UserPower = string.Empty;
        protected string UserSetting = string.Empty;
        protected string UserCookies = string.Empty;
        protected bool UserIsLogin = false;
        public string ModuleUploadPath = string.Empty;
        public string ModuleUploadType = string.Empty;
        public int ModuleUploadSize = 0;
        /// <summary>
        /// 当前用户对象
        /// </summary>
        protected JumbotDLS.Entity.Normal_User CurrentUser = null;
        /// <summary>
        /// 验证登陆
        /// </summary>
        private void chkLogin()
        {
            if (Cookie.GetValue(site.CookiePrev + "user") != null)
            {
                UserId = Str2Str(Cookie.GetValue(site.CookiePrev + "user", "id"));
                UserGroupId = Str2Str(""+Cookie.GetValue(site.CookiePrev + "user", "groupid"));
                UserName = Cookie.GetValue(site.CookiePrev + "user", "name");
                UserKey = Cookie.GetValue(site.CookiePrev + "user", "userkey");
                UserCookies = Cookie.GetValue(site.CookiePrev + "user", "cookies");
                if (UserId.Length != 0 && UserName.Length != 0)
                {
                    CurrentUser = new JumbotDLS.DAL.Normal_UserDAL().GetEntity(UserId, true, IPHelp.ClientIP);
                    if (CurrentUser.UserName.Length > 0)
                    {
                        this.UserIsLogin = true;
                        UserEmail = CurrentUser.Email;
                        UserSetting = CurrentUser.UserSetting;
                        UserPoints = CurrentUser.Points;//需要实时获取
                        UserSign = CurrentUser.UserSign;//需要实时获取
                    }
                }
            }
        }
        /// <summary>
        /// 验证权限
        /// </summary>
        /// <param name="s">空时只要求登录</param>
        private bool IsPower(string s)
        {
            if (s == "ok") return true;
            if (!this.UserIsLogin)//验证一次本地信息
                chkLogin();
            if (s == "") return (this.UserIsLogin);
            return (this.UserPower.Contains("," + s + ","));
        }

        /// <summary>
        /// 验证权限
        /// </summary>
        /// <param name="s"></param>
        /// <param name="pageType">页面分为html和json</param>
        private void chkPower(string s, string pageType)
        {
            if (!IsPower(s))
            {
                showErrMsg("未登录或当前用户的权限不足", pageType);
            }
        }
        /// <summary>
        /// 输出错误信息
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="pageType">页面分为html和json</param>
        private void showErrMsg(string msg, string pageType)
        {
            if (pageType != "json")
                FinalMessage(msg, site.Dir + "passport/login.aspx", 0);
            else
            {
                HttpContext.Current.Response.Clear();
                if (!this.UserIsLogin)
                    HttpContext.Current.Response.Write(JsonResult(-1, msg));
                else
                    HttpContext.Current.Response.Write(JsonResult(0, msg));
                HttpContext.Current.Response.End();
            }
        }
        /// <summary>
        /// 管理中心初始
        /// </summary>
        /// <param name="powerNum">权限,为空表示验证是否登录</param>
        /// <param name="pageType">页面分为html和json</param>
        protected void User_Load(string powerNum, string pageType)
        {
            chkPower(powerNum, pageType);
        }

        /// <summary>
        /// 管理中心初始,并获得频道的各项参数值
        /// </summary>
        /// <param name="powerNum">权限</param>
        /// <param name="isChannel">如果为false就表示ChannelId可以为0</param>
        protected void User_Load(string powerNum, string pageType, bool isChannel)
        {
            chkPower(powerNum, pageType);
            if (isChannel && ChannelId == "0")
            {
                showErrMsg("参数错误,请不要在外部提交数据", pageType);
                return;
            }
            if (ChannelId != "0")
            {
                JumbotDLS.Entity.Normal_Channel _Channel = new JumbotDLS.DAL.Normal_ChannelDAL().GetEntity(ChannelId);
                ChannelName = _Channel.Title;
                ChannelDir = _Channel.Dir;
                ChannelType = _Channel.Type;
                ChannelItemName = _Channel.ItemName;
                ChannelItemUnit = _Channel.ItemUnit;
                ChannelClassDepth = _Channel.ClassDepth;
                ChannelIsHtml = _Channel.IsHtml;
            }
        }
        /// <summary>
        /// 页面初始化，获取当前访问者是否登录的信息
        /// </summary>
        protected void GetLoginedInfo()
        {
            chkLogin();
        }
        /// <summary>
        /// 必须在机构IP范围内或已登录
        /// </summary>
        protected void CheckClientPower()
        {
            if ((!this.UserIsLogin) && ClientOrganizationId == "0")
            {
                Response.Redirect("/passport/login.aspx");
                Response.End();
            }
        }
    }
}