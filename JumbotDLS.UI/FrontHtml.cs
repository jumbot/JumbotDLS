﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Web;
using System.Data;
using System.Text;
namespace JumbotDLS.UI
{
    public class FrontHtml : BasicPage
    {
        override protected void OnInit(EventArgs e)
        {
            base.OnInit(e);
            CheckRegisterInfo();
            CheckClientIP();
            ClientUserIP = JumbotDLS.Common.Const.GetUserIp;
            ClientOrganizationId = new JumbotDLS.DAL.Normal_WhiteIPDAL().GetOrganizationIdByIP(ClientUserIP);
            ClientOrganizationInfo = new JumbotDLS.DAL.Normal_OrganizationDAL().GetEntity(ClientOrganizationId);
        }
        /// <summary>
        /// 验证注册信息
        /// </summary>
        protected void CheckRegisterInfo()
        {
            if (!site.SiteIsGenuine)
                Response.Redirect(site.Dir + "aspx/register.aspx");
        }
        /// <summary>
        /// 机构ID
        /// </summary>
        protected string ClientOrganizationId = "0";
        /// <summary>
        /// 机构名称
        /// </summary>
        protected string ClientOrganizationName = string.Empty;
        protected string ClientUserIP = string.Empty;
        protected JumbotDLS.Entity.Normal_Organization ClientOrganizationInfo = null;
        public bool CheckCookiesCode()
        {
            string _code = q("code");
            string _realcode = "";
            return JumbotDLS.Common.ValidateCode.CheckValidateCode(_code, ref _realcode);
        }
        /// <summary>
        /// 解析主站的基本信息(只适合aspx，所以要处理<!--#include virtual=的标签）
        /// </summary>
        /// <param name="PageStr"></param>
        protected void ReplaceSiteTags(ref string PageStr)
        {
            JumbotDLS.DAL.TemplateEngineDAL teDAL = new JumbotDLS.DAL.TemplateEngineDAL("0");
            teDAL.IsHtml = site.IsHtml;
            teDAL.ReplaceSiteTags(ref PageStr);
        }

        protected string GetContentFile(string _channelID, string _channelType, string _contentID, int _currentPage)
        {
            return JumbotDLS.DAL.ModuleCommand.GetContent(_channelType, _channelID, _contentID, _currentPage);
        }
        /// <summary>
        /// 判断插件是否已经启用
        /// </summary>
        /// <param name="ExtendName"></param>
        public void CheckExtendState(string _extendname, string _pagetype)
        {
            if (new JumbotDLS.DAL.Normal_ExtendsDAL().Running(_extendname))
                return;
            if (_pagetype != "js")
                Response.Write("插件未启动");
            else
                Response.Write("document.write('插件未启动');");
            Response.End();
        }

    }
}