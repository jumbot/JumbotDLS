﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Text;
namespace JumbotDLS.UI
{
    public class FrontAjax : BasicPage
    {
        override protected void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!CheckFormUrl())//不可直接在url下访问
            {
                Response.End();
            }
            CheckClientIP();
        }
    }
}
