﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
namespace JumbotDLS.DAL
{
    public class ModuleCommand
    {
        public static IModule IMD;
        static ModuleCommand()
        {

        }
        /// <summary>
        /// 得到内容页地址
        /// </summary>
        /// <param name="_page"></param>
        /// <param name="_ishtml"></param>
        /// <param name="_channelid"></param>
        /// <param name="_contentid"></param>
        /// <returns></returns>
        public static string GetContentLink(string _module, int _page, bool _ishtml, string _channelid, string _contentid, bool _truefile)
        {
            IMD = (IModule)Activator.CreateInstance(Type.GetType(String.Format("JumbotDLS.DAL.Module_{0}DAL", _module), true, true));
            return IMD.GetContentLink(_page, _ishtml, _channelid, _contentid, _truefile);
        }
        /// <summary>
        /// 生成内容页
        /// </summary>
        /// <param name="_ChannelId"></param>
        /// <param name="_ContentId"></param>
        /// <param name="_CurrentPage"></param>
        public static void CreateContent(string _module, string _ChannelId, string _ContentId, int _CurrentPage)
        {
            IMD = (IModule)Activator.CreateInstance(Type.GetType(String.Format("JumbotDLS.DAL.Module_{0}DAL", _module), true, true));
            IMD.CreateContent(_ChannelId, _ContentId, _CurrentPage);
        }
        /// <summary>
        /// 得到内容页
        /// </summary>
        /// <param name="_ChannelId"></param>
        /// <param name="_ContentId"></param>
        /// <param name="_CurrentPage"></param>
        public static string GetContent(string _module, string _ChannelId, string _ContentId, int _CurrentPage)
        {
            IMD = (IModule)Activator.CreateInstance(Type.GetType(String.Format("JumbotDLS.DAL.Module_{0}DAL", _module), true, true));
            return IMD.GetContent(_ChannelId, _ContentId, _CurrentPage);
        }
        /// <summary>
        /// 删除内容页
        /// </summary>
        /// <param name="_ChannelId"></param>
        /// <param name="_ContentId"></param>
        public static void DeleteContent(string _module, string _ChannelId, string _ContentId)
        {
            IMD = (IModule)Activator.CreateInstance(Type.GetType(String.Format("JumbotDLS.DAL.Module_{0}DAL", _module), true, true));
            IMD.DeleteContent(_ChannelId, _ContentId);
        }
    }
}
