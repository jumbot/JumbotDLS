﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.DBUtility;

namespace JumbotDLS.DAL
{
    public class EmployeeDAL : Common
    {
        public EmployeeDAL()
        {
            base.SetupSystemDate();
        }

        public JumbotDLS.Entity.Employee GetEntity(string _employeeid)
        {
            JumbotDLS.Entity.Employee employee = new JumbotDLS.Entity.Employee();
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.SqlCmd = "SELECT * FROM [View_Employee] WHERE [userId]=" + _employeeid;
                DataTable dt = _doh.GetDataTable();
                if (dt.Rows.Count > 0)
                {
                    employee.EmployeeId = Str2Int(dt.Rows[0]["userId"].ToString(), 0);
                    employee.AdminOpenID = dt.Rows[0]["AdminOpenID"].ToString();
                    employee.EmployeeMobile = dt.Rows[0]["EmployeeMobile"].ToString();
                    employee.EmployeeName = dt.Rows[0]["EmployeeName"].ToString();
                    employee.EmployeeAppList = dt.Rows[0]["EmployeeAppList"].ToString();
                    employee.LastTime4 = Validator.StrToDate(dt.Rows[0]["LastTime4"].ToString(), DateTime.Now);
                    employee.LastIP4 = dt.Rows[0]["LastIP4"].ToString();
                    employee.LeaveAuditUserID1 = dt.Rows[0]["LeaveAuditUserID1"].ToString();
                    employee.LeaveAuditUserName1 = dt.Rows[0]["LeaveAuditUserName1"].ToString();
                    //employee.LeaveAuditMobile1 = dt.Rows[0]["LeaveAuditMobile1"].ToString();
                    //employee.LeaveAuditWeixinID1 = dt.Rows[0]["LeaveAuditWeixinID1"].ToString();


                    employee.LeaveAuditUserID2 = dt.Rows[0]["LeaveAuditUserID2"].ToString();
                    employee.LeaveAuditUserName2 = dt.Rows[0]["LeaveAuditUserName2"].ToString();
                    //employee.LeaveAuditMobile2 = dt.Rows[0]["LeaveAuditMobile2"].ToString();
                    //employee.LeaveAuditWeixinID2 = dt.Rows[0]["LeaveAuditWeixinID2"].ToString();


                    employee.LeaveNotifyUserID = dt.Rows[0]["LeaveNotifyUserID"].ToString();
                    employee.LeaveNotifyUserName = dt.Rows[0]["LeaveNotifyUserName"].ToString();


                    employee.ReimbursementAuditUserID1 = dt.Rows[0]["ReimbursementAuditUserID1"].ToString();
                    employee.ReimbursementAuditUserName1 = dt.Rows[0]["ReimbursementAuditUserName1"].ToString();
                    employee.ReimbursementAuditUserID2 = dt.Rows[0]["ReimbursementAuditUserID2"].ToString();
                    employee.ReimbursementAuditUserName2 = dt.Rows[0]["ReimbursementAuditUserName2"].ToString();

                    employee.ReimbursementNotifyUserID = dt.Rows[0]["ReimbursementNotifyUserID"].ToString();
                    employee.ReimbursementNotifyUserName = dt.Rows[0]["ReimbursementNotifyUserName"].ToString();
                }
            }
            return employee;
        }
        /// <summary>
        /// 获得员工姓名
        /// </summary>
        /// <param name="_employeecode"></param>
        /// <returns></returns>
        public string GetEmployeeName(string _employeecode)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.ConditionExpress = "employeecode=@employeecode";
                _doh.AddConditionParameter("@employeecode", _employeecode);

                string _employeename = _doh.GetField("view_employee", "employeename").ToString();
                return _employeename;
            }

        }
        /// <summary>
        /// 获得员工微信id
        /// </summary>
        /// <param name="_userid"></param>
        /// <returns></returns>
        public string GetEmployeeWeixinID(string _userid)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.ConditionExpress = "userid=@userid";
                _doh.AddConditionParameter("@userid", _userid);

                string _employeecode = _doh.GetField("view_employee", "employeecode").ToString();
                return _employeecode;
            }

        }

        /// <summary>
        /// 验证员工登录
        /// </summary>
        /// <param name="_adminname">登录名</param>
        /// <param name="_adminpass">32位MD5密码</param>
        /// <returns></returns>
        public string ChkEmployeeLogin(string _employeecode, int iExpires)
        {
            return ChkEmployeeLogin(_employeecode, "", iExpires, true);
        }
        
            public string ChkEmployeeLogin(string _employeename, string _employeepass, int iExpires, bool useemployeecode)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                if (useemployeecode)
                {
                    _doh.Reset();
                    _doh.ConditionExpress = "employeecode=@employeecode";
                    _doh.AddConditionParameter("@employeecode", _employeename);
                }
                else
                {
                    _employeename = _employeename.Replace("\'", "");
                    string _employeepass2 = JumbotDLS.Utils.MD5.Last64(_employeepass);
                    _doh.Reset();
                    _doh.ConditionExpress = "employeename=@employeename and employeepass=@employeepass";
                    _doh.AddConditionParameter("@employeename", _employeename);
                    _doh.AddConditionParameter("@employeepass", _employeepass2);
                }


                string _employeeid = _doh.GetField("view_employee", "userid").ToString();
                if (_employeeid != "")
                {
                    JumbotDLS.Entity.Employee _Employee = GetEntity(_employeeid);

                    //设置Cookies
                    System.Collections.Specialized.NameValueCollection myCol = new System.Collections.Specialized.NameValueCollection();
                    myCol.Add("id", _employeeid);
                    myCol.Add("name", _Employee.EmployeeName);
                    JumbotDLS.Utils.Cookie.SetObj(site.CookiePrev + "employee", iExpires, myCol, site.CookieDomain, site.CookiePath);

                    //更新管理员登陆信息
                    _doh.Reset();
                    _doh.ConditionExpress = "id=@employeeid and isemployee=1";
                    _doh.AddConditionParameter("@employeeid", _employeeid);
                    _doh.AddFieldItem("LastTime4", DateTime.Now.ToString());
                    _doh.AddFieldItem("LastIP4", IPHelp.ClientIP);
                    _doh.Update("jdls_normal_user");
                    //_doh.Dispose();
                    return "ok";
                }
                else
                {
                    //_doh.Dispose();
                    return "帐号或密码不正确";
                }
            }

        }
        /// <summary>
        /// 退出登录
        /// </summary>
        public void ChkEmployeeLogout()
        {
            if (JumbotDLS.Utils.Cookie.GetValue(site.CookiePrev + "employee") != null)
            {
                JumbotDLS.Utils.Cookie.Del(site.CookiePrev + "employee", site.CookieDomain, site.CookiePath);
            }
        }
    }
}
