﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.DBUtility;

namespace JumbotDLS.DAL
{
    /// <summary>
    /// 标签表信息
    /// </summary>
    public class Normal_TagDAL : Common
    {
        public Normal_TagDAL()
        {
            base.SetupSystemDate();
        }
        /// <summary>
        /// 是否存在记录
        /// </summary>
        /// <param name="_wherestr">条件</param>
        /// <returns></returns>
        public bool Exists(string _wherestr)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                int _ext = 0;
                _doh.Reset();
                _doh.ConditionExpress = _wherestr;
                if (_doh.Exist("jdls_normal_tag"))
                    _ext = 1;
                return (_ext == 1);
            }
        }
        /// <summary>
        /// 判断重复性(标题是否存在)
        /// </summary>
        /// <param name="_title">需要检索的标题</param>
        /// <param name="_id">除外的ID</param>
        /// <param name="_wherestr">其他条件</param>
        /// <returns></returns>
        public bool ExistTitle(string _title, string _id, string _wherestr)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                int _ext = 0;
                _doh.Reset();
                _doh.ConditionExpress = "title=@title and id<>" + _id;
                if (_wherestr != "") _doh.ConditionExpress += " and " + _wherestr;
                _doh.AddConditionParameter("@title", _title);
                if (_doh.Exist("jdls_normal_tag"))
                    _ext = 1;
                return (_ext == 1);
            }
        }

        /// <summary>
        /// 自动增添Tag标签到数据库
        /// </summary>
        /// <param name="_channelid">频道ID</param>
        /// <param name="_tags">要增加的Tag，多个Tag以,隔开</param>
        public void InsertTags(string _channelid, string _tags, int _state)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                if (_tags.Length == 0) return;
                string[] tag = _tags.Split(',');
                for (int i = 0; i < tag.Length; i++)
                {
                    if (!ExistTitle(tag[i].ToString(), "0", "ChannelId=" + _channelid))
                    {
                        _doh.Reset();
                        _doh.AddFieldItem("Title", tag[i].ToString());
                        _doh.AddFieldItem("ClickTimes", "0");
                        _doh.AddFieldItem("State", _state);
                        _doh.AddFieldItem("ChannelId", _channelid);
                        _doh.Insert("jdls_normal_tag");
                    }
                }
            }
        }
        /// <summary>
        /// 增加标签点击数
        /// </summary>
        /// <param name="_channelid"></param>
        /// <param name="_tagname"></param>
        public void AddClickTimes(string _channelid, string _tagname)
        {
            if (_tagname.Length == 0) return;
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.ConditionExpress = "State=1 and Title=@Title and ChannelId=" + _channelid;
                _doh.AddConditionParameter("@Title", _tagname);
                _doh.Add("jdls_normal_tag", "ClickTimes");
            }
        }
    }
}
