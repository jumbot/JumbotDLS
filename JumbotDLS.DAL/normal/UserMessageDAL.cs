﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.DBUtility;

namespace JumbotDLS.DAL
{
    /// <summary>
    /// 会员通知表信息
    /// </summary>
    public class Normal_UserMessageDAL : Common
    {
        public Normal_UserMessageDAL()
        {
            base.SetupSystemDate();
        }
        /// <summary>
        /// 发站内短信
        /// </summary>
        /// <param name="_Title">标题</param>
        /// <param name="_Content">内容</param>
        /// <param name="_SendUserId">发送人ID</param>
        /// <param name="_ReceiveUserId">接收人ID,多个用逗号隔开</param>
        /// <param name="_ReceiveUserName">接收人用户名,多个用逗号隔开</param>
        public bool SendMessage(string _Title, string _Content, string _SendUserId, string _ReceiveUserId, string _ReceiveUserName)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                string[] _uId = _ReceiveUserId.Split(',');
                string[] _uName = _ReceiveUserName.Split(',');
                for (int i = 0; i < _uId.Length; i++)
                {
                    _doh.Reset();
                    _doh.AddFieldItem("Title", _Title);
                    _doh.AddFieldItem("AddDate", DateTime.Now.ToString());
                    _doh.AddFieldItem("Content", _Content);
                    _doh.AddFieldItem("SendIP", IPHelp.ClientIP);
                    _doh.AddFieldItem("SendUserId", _SendUserId);
                    _doh.AddFieldItem("ReceiveUserId", _uId[i]);
                    _doh.AddFieldItem("ReceiveUserName", _uName[i]);
                    _doh.AddFieldItem("State", 0);
                    _doh.Insert("jdls_normal_user_message");
                }
                return true;
            }


        }
        /// <summary>
        /// 系统发站内短信给客服
        /// </summary>
        /// <param name="_Title"></param>
        /// <param name="_Content"></param>
        public bool SendServiceMessage(string _Title, string _Content)
        {
            string strXmlFile = HttpContext.Current.Server.MapPath("~/_data/config/message.config");
            JumbotDLS.DBUtility.XmlControl XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
            string _SendUserId = XmlTool.GetText("Messages/Service/UserId");
            string _ReceiveUserId = XmlTool.GetText("Messages/Service/UserId");
            string _ReceiveUserName = XmlTool.GetText("Messages/Service/UserName");
            XmlTool.Dispose();
            return SendMessage(_Title, _Content, _SendUserId, _ReceiveUserId, _ReceiveUserName);
        }
    }
}
