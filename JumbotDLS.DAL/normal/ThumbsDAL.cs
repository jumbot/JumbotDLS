﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.DBUtility;

namespace JumbotDLS.DAL
{
    /// <summary>
    /// 缩略图表信息
    /// </summary>
    public class Normal_ThumbsDAL : Common
    {
        public Normal_ThumbsDAL()
        {
            base.SetupSystemDate();
        }
        /// <summary>
        /// 得到数据表
        /// </summary>
        /// <param name="_channelid"></param>
        /// <returns></returns>
        public DataTable GetDataTable(string _channelid)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                if (_channelid == "0")
                    _doh.SqlCmd = "SELECT ID,Title,iWidth,iHeight FROM [jdls_normal_thumbs] ORDER BY ChannelID,ID";
                else
                    _doh.SqlCmd = "SELECT ID,Title,iWidth,iHeight FROM [jdls_normal_thumbs] WHERE [ChannelId]=" + _channelid + " OR [ChannelId]=0 ORDER BY ChannelID,ID";
                DataTable dt = _doh.GetDataTable();
                return dt;
            }

        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteByID(string _id)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.ConditionExpress = "id=@id";
                _doh.AddConditionParameter("@id", _id);
                int _del = _doh.Delete("jdls_normal_thumbs");
                return (_del == 1);
            }

        }
    }
}
