﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.DBUtility;

namespace JumbotDLS.DAL
{
    /// <summary>
    /// 内容评论
    /// </summary>
    public class Normal_ReviewDAL : Common
    {
        public Normal_ReviewDAL()
        {
            base.SetupSystemDate();
        }
        /// <summary>
        /// 得到评论列表
        /// </summary>
        /// <param name="_thispage">当前页码</param>
        /// <param name="_pagesize">每页记录条数</param>
        /// <param name="_channelid">频道ID</param>
        /// <param name="_contentid">内容ID</param>
        public string GetTopList(int _thispage, int _pagesize, string _channelid, string _contentid)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                string sqlStr = "";
                int totalCount = 0;
                string whereStr = "[IsPass]=1 AND [ParentId]=0";
                if (_channelid != "0") whereStr += " AND [ChannelId]=" + _channelid;
                if (_contentid != "0") whereStr += " AND [ContentId]=" + _contentid;
                _doh.Reset();
                _doh.ConditionExpress = whereStr;
                totalCount = _doh.Count("jdls_normal_review");

                sqlStr = JumbotDLS.Utils.SqlHelp.GetSql0("Id,ChannelId,ContentId,IP,UserName,AddDate,Content", "jdls_normal_review", "id", _pagesize, _thispage, "desc", whereStr);
                _doh.Reset();
                _doh.SqlCmd = sqlStr;
                DataTable dt = _doh.GetDataTable();
                string ResponseStr = "";
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    ResponseStr += "<li><a href=\"" + site.Dir + "review/default.aspx?ccid=" + dt.Rows[j]["ChannelId"].ToString() + "&id=" + dt.Rows[j]["ContentId"].ToString() + "#c" + dt.Rows[j]["Id"].ToString() + "\" target=\"_blank\">" + dt.Rows[j]["Content"].ToString() + "</a></li>";
                }
                dt.Clear();
                dt.Dispose();
                return ResponseStr;
            }
        }
    }
}
