﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.Entity;
using JumbotDLS.DBUtility;

namespace JumbotDLS.DAL
{
    /// <summary>
    /// 内容顶客
    /// </summary>
    public class Normal_DiggDAL : Common
    {
        public Normal_DiggDAL()
        {
            base.SetupSystemDate();
        }
        /// <summary>
        /// 得到内容
        /// </summary>
        /// <param name="_channeltype"></param>
        /// <param name="_contentid"></param>
        /// <returns></returns>
        public Normal_Digg GetDigg(string _channeltype, string _contentid)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                Normal_Digg digg = new Normal_Digg();
                digg.ChannelType = _channeltype;
                digg.ContentId = Str2Int(_contentid);
                _doh.Reset();
                _doh.ConditionExpress = "channeltype=@channeltype and contentid=@contentid";
                _doh.AddConditionParameter("@channeltype", _channeltype);
                _doh.AddConditionParameter("@contentid", _contentid);
                if (!_doh.Exist("jdls_normal_digg"))
                {
                    _doh.Reset();
                    _doh.AddFieldItem("ChannelType", _channeltype);
                    _doh.AddFieldItem("ContentId", _contentid);

                    _doh.AddFieldItem("DiggNum", 0);
                    _doh.Insert("jdls_normal_digg");
                }
                _doh.Reset();
                _doh.ConditionExpress = "channeltype=@channeltype and contentid=@contentid";
                _doh.AddConditionParameter("@channeltype", _channeltype);
                _doh.AddConditionParameter("@contentid", _contentid);
                digg.DiggNum = Str2Int(_doh.GetField("jdls_normal_digg", "DiggNum").ToString());
                return digg;
            }
        }
    }
}
