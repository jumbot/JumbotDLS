﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.DBUtility;

namespace JumbotDLS.DAL
{
    /// <summary>
    /// 会员日志表信息
    /// </summary>
    public class Normal_UserLogsDAL : Common
    {
        public Normal_UserLogsDAL()
        {
            base.SetupSystemDate();
        }
        /// <summary>
        /// 保存用户日志
        /// </summary>
        /// <param name="_uid">会员ID</param>
        /// <param name="_info">保存信息</param>
        /// <param name="_type">操作类型,1=分组移动,2=扣除points,3=积分增加(2,3为系统操作),4=增加将元,5=VIP升级,6积分扣除(4,5,6为管理员操作)</param>
        public void SaveLog(string _uid, string _info, int _type)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.AddFieldItem("UserId", _uid);
                _doh.AddFieldItem("OperInfo", _info);
                _doh.AddFieldItem("OperType", _type);
                _doh.AddFieldItem("OperTime", DateTime.Now.ToString());
                _doh.AddFieldItem("OperIP", IPHelp.ClientIP);
                _doh.Insert("jdls_normal_user_logs");
            }
        }
    }
}
