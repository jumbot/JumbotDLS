﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.DBUtility;

namespace JumbotDLS.DAL
{
    /// <summary>
    /// 非法IP表信息
    /// </summary>
    public class Normal_WhiteIPDAL : Common
    {
        public Normal_WhiteIPDAL()
        {
            base.SetupSystemDate();
        }
        /// <summary>
        /// 是否存在记录
        /// </summary>
        /// <param name="_wherestr">条件</param>
        /// <returns></returns>
        public bool Exists(string _wherestr)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                int _ext = 0;
                _doh.Reset();
                _doh.ConditionExpress = _wherestr;
                if (_doh.Exist("jdls_organization_whiteip"))
                    _ext = 1;
                return (_ext == 1);
            }
        }
        /// <summary>
        /// 判断重复性(标题是否存在)
        /// </summary>
        /// <param name="_title">需要检索的标题</param>
        /// <param name="_id">除外的ID</param>
        /// <param name="_wherestr">其他条件</param>
        /// <returns></returns>
        public bool ExistTitle(string _title, string _id, string _wherestr)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                int _ext = 0;
                _doh.Reset();
                _doh.ConditionExpress = "title=@title and id<>" + _id;
                if (_wherestr != "") _doh.ConditionExpress += " and " + _wherestr;
                _doh.AddConditionParameter("@title", _title);
                if (_doh.Exist("jdls_organization_whiteip"))
                    _ext = 1;
                return (_ext == 1);
            }
        }
        /// <summary>
        /// 得到列表JSON数据
        /// </summary>
        /// <param name="_thispage">当前页码</param>
        /// <param name="_pagesize">每页记录条数</param>
        /// <param name="_wherestr">搜索条件</param>
        /// <param name="_jsonstr">返回值</param>
        public void GetListJSON(int _thispage, int _pagesize, string _wherestr, ref string _jsonstr)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.ConditionExpress = _wherestr;
                string sqlStr = "";
                int _countnum = _doh.Count("jdls_organization_whiteip");
                sqlStr = JumbotDLS.Utils.SqlHelp.GetSql0("*", "jdls_organization_whiteip", "StartIP", _pagesize, _thispage, "ASC", _wherestr);
                _doh.Reset();
                _doh.SqlCmd = sqlStr;
                DataTable dt = _doh.GetDataTable();
                _jsonstr = "{\"result\" :\"1\"," +
                    "\"returnval\" :\"操作成功\"," +
                    "\"pagebar\" :\"" + JumbotDLS.Utils.PageBar.GetPageBar(3, "js", 2, _countnum, _pagesize, _thispage, "javascript:ajaxList(<#page#>);") + "\"," +
                    JumbotDLS.Utils.dtHelper.DT2JSON(dt) +
                    "}";
                dt.Clear();
                dt.Dispose();
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteByID(string _id)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.ConditionExpress = "id=@id";
                _doh.AddConditionParameter("@id", _id);
                int _del = _doh.Delete("jdls_organization_whiteip");
                return (_del == 1);
            }
        }

        /// <summary>
        /// 获得单页内容的单条记录实体
        /// </summary>
        /// <param name="_id"></param>
        public JumbotDLS.Entity.Normal_WhiteIP GetEntity(string _id)
        {
            JumbotDLS.Entity.Normal_WhiteIP whiteip = new JumbotDLS.Entity.Normal_WhiteIP();
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.SqlCmd = "SELECT * FROM [jdls_organization_whiteip] WHERE [Id]=" + _id;
                DataTable dt = _doh.GetDataTable();
                if (dt.Rows.Count > 0)
                {
                    whiteip.Id = dt.Rows[0]["Id"].ToString();
                    whiteip.StartIP = Convert.ToInt32(dt.Rows[0]["StartIP"].ToString());
                    whiteip.EndIP = Convert.ToInt32(dt.Rows[0]["EndIP"].ToString());
                    whiteip.StartIP2 = dt.Rows[0]["StartIP2"].ToString();
                    whiteip.EndIP2 = dt.Rows[0]["EndIP2"].ToString();
                }
                return whiteip;
            }
        }
        /// <summary>
        /// 更新起/始IP整型值
        /// </summary>
        /// <param name="_id">编号</param>
        /// <param name="_startip">起始IP</param>
        /// <param name="_endip">截止IP</param>
        public bool UpdateIPData(string _id, string _startip, string _endip)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.ConditionExpress = "id=@id";
                _doh.AddConditionParameter("@id", _id);
                _doh.AddFieldItem("StartIP", JumbotDLS.Utils.IPHelp.IP2Long(System.Net.IPAddress.Parse(_startip)));
                _doh.AddFieldItem("EndIP", JumbotDLS.Utils.IPHelp.IP2Long(System.Net.IPAddress.Parse(_endip)));
                _doh.AddFieldItem("Enabled", 1);
                int _update = _doh.Update("jdls_organization_whiteip");
                return (_update == 1);
            }
        }
        /// <summary>
        /// 检测是不是合法IP，如果是就直接可以访问，否则需要登录
        /// </summary>
        /// <param name="_ip"></param>
        /// <returns></returns>
        public bool IPIsOK(string _ip, string _organizationid)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                long ip = JumbotDLS.Utils.IPHelp.IP2Long(System.Net.IPAddress.Parse(_ip));
                _doh.Reset();
                _doh.ConditionExpress = "OrganizationID=" + _organizationid + " AND ([StartIP]<=" + ip + " and [EndIP]>=" + ip + ")";
                return (_doh.Exist("jdls_organization_whiteip") == false);
            }
        }
        /// <summary>
        /// 通过IP去匹配子站
        /// </summary>
        /// <param name="_ip"></param>
        /// <param name="_channelid"></param>
        /// <returns></returns>
        public string GetOrganizationIdByIP(string _ip)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                long ip = JumbotDLS.Utils.IPHelp.IP2Long(System.Net.IPAddress.Parse(_ip));
                _doh.Reset();
                _doh.SqlCmd = "select TOP 1 OrganizationID from [jdls_organization_whiteip] where ([StartIP]<=" + ip + " and [EndIP]>=" + ip + ") and OrganizationID in (select id from [jdls_organization] where Enabled=1 and (begintime<=getdate() and getdate()<=endtime))";
                DataTable dt = _doh.GetDataTable();
                if (dt.Rows.Count > 0)
                {
                    return dt.Rows[0]["OrganizationID"].ToString();
                }
                return "0";
            }
        }
    }
}
