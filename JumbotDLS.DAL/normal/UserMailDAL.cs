﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.DBUtility;
using System.Collections;
namespace JumbotDLS.DAL
{
    /// <summary>
    /// 发邮件信息
    /// </summary>
    public class Normal_UserMailDAL : Common
    {
        public Normal_UserMailDAL()
        {
            base.SetupSystemDate();
        }

        /// <summary>
        /// 系统发邮件
        /// </summary>
        /// <param name="_To">收件人,单封邮件</param>
        /// <param name="_Title">标题</param>
        /// <param name="_Body">内容</param>
        /// <param name="_IsHtml">是否支持html</param>
        /// <param name="_MailServer">邮箱服务器列表</param>
        /// <returns></returns>
        public bool SendMails(string _To, string _Title, string _Body, string _Attach, bool _IsHtml, JumbotDLS.Entity.MailServer _MailServer)
        {
            _Body += "<br /><br />" + site.Name + "  <a href='" + site.Url + "'>" + site.Url + "</a>";
            return JumbotDLS.Common.MailHelp.SendOK(_To, _Title, _Body, _Attach, _IsHtml, _MailServer);

        }
        public bool SendMails(string _To, string _Title, string _Body, bool _IsHtml, JumbotDLS.Entity.MailServer _MailServer)
        {
            return SendMails(_To, _Title, _Body, "", _IsHtml, _MailServer);

        }
        public bool SendMails(string _To, string _Title, string _Body, JumbotDLS.Entity.MailServer _MailServer)
        {
            return SendMails(_To, _Title, _Body, "", true, _MailServer);
        }
        public bool SendMail(string _To, string _Title, string _Body)
        {
            string strXmlFile = HttpContext.Current.Server.MapPath("~/_data/config/mail.config");
            JumbotDLS.DBUtility.XmlControl XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
            string _MailFrom = XmlTool.GetText("Root/Address");
            string _MailFromName = XmlTool.GetText("Root/NickName");
            string _MailPwd = XmlTool.GetText("Root/Password");
            string _MailSmtpHost = XmlTool.GetText("Root/SmtpHost");
            int _MailSmtpPort = Str2Int(XmlTool.GetText("Root/SmtpPort"));
            XmlTool.Dispose();
            _Body += "<br /><br />" + site.Name + "  <a href='" + site.Url + "'>" + site.Url + "</a>";
            return JumbotDLS.Common.MailHelp.SendOK(_To, _Title, _Body, true, _MailFrom, _MailFromName, _MailPwd, _MailSmtpHost, _MailSmtpPort);

        }
        /// <summary>
        /// 系统发邮件给客服
        /// </summary>
        /// <param name="_Title"></param>
        /// <param name="_Body"></param>
        /// <returns></returns>
        public bool SendServiceMail(string _Title, string _Body)
        {
            string strXmlFile = HttpContext.Current.Server.MapPath("~/_data/config/message.config");
            JumbotDLS.DBUtility.XmlControl XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
            string _ServiceMail = XmlTool.GetText("Messages/Service/UserMail");
            XmlTool.Dispose();
            return SendMail(_ServiceMail, _Title, _Body);
        }
        /// <summary>
        /// 导出数据至配置文件
        /// </summary>
        /// <returns></returns>
        public bool ExportEmailServer()
        {
            string strXmlFile = HttpContext.Current.Server.MapPath("~/_data/config/jdls(emailserver).config");
            JumbotDLS.DBUtility.XmlControl XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
            try
            {
                XmlTool.RemoveAll("Mails");
                XmlTool.Save();
                using (DbOperHandler _doh = new Common().Doh())
                {
                    _doh.Reset();
                    _doh.SqlCmd = "Select * FROM [jdls_email_smtpserver] WHERE [Enabled]=1 ORDER BY id asc";
                    DataTable dt = _doh.GetDataTable();
                    string _id = string.Empty;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        _id = dt.Rows[i]["Id"].ToString();
                        XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
                        XmlTool.InsertNode("Mails", "Mail", "ID", _id);
                        XmlTool.InsertElement("Mails/Mail[ID=\"" + _id + "\"]", "FromAddress", dt.Rows[i]["FromAddress"].ToString(), false);
                        XmlTool.InsertElement("Mails/Mail[ID=\"" + _id + "\"]", "FromName", dt.Rows[i]["FromName"].ToString(), false);
                        XmlTool.InsertElement("Mails/Mail[ID=\"" + _id + "\"]", "FromPwd", dt.Rows[i]["FromPwd"].ToString(), false);
                        XmlTool.InsertElement("Mails/Mail[ID=\"" + _id + "\"]", "SmtpHost", dt.Rows[i]["SmtpHost"].ToString(), false);
                        XmlTool.InsertElement("Mails/Mail[ID=\"" + _id + "\"]", "SmtpPort", dt.Rows[i]["SmtpPort"].ToString(), false);
                        XmlTool.InsertElement("Mails/Mail[ID=\"" + _id + "\"]", "Used", DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss"), false);
                        XmlTool.Save();
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// 导入配置文件至数据库
        /// </summary>
        /// <returns></returns>
        public bool ImportEmailServer()
        {
            string strXmlFile = HttpContext.Current.Server.MapPath("~/_data/config/jdls(emailserver).config");
            JumbotDLS.DBUtility.XmlControl XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
            try
            {
                using (DbOperHandler _doh = new Common().Doh())
                {
                    _doh.Reset();
                    _doh.Delete("jdls_email_smtpserver");
                    DataTable dt = XmlTool.GetTable("Mails");
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            _doh.Reset();
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                if (dt.Columns[j].ColumnName.ToLower() != "id" && dt.Columns[j].ColumnName.ToLower() != "used")
                                    _doh.AddFieldItem(dt.Columns[j].ColumnName.ToLower(), dt.Rows[i][j].ToString());
                            }
                            _doh.Insert("jdls_email_smtpserver");
                        }
                    }
                    dt.Clear();
                    dt.Dispose();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
