﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Xml;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.Entity;
using JumbotDLS.DBUtility;
namespace JumbotDLS.DAL
{
    /// <summary>
    /// 生成html主文件
    /// </summary>
    public class TemplateEngineDAL : Common
    {
        public TemplateEngineDAL()
        {
            base.SetupSystemDate();
            this.MainChannel = new JumbotDLS.DAL.Normal_ChannelDAL().GetEntity("0");
            this.Lang = new JumbotDLS.Utils.LanguageHelp().GetEntity("cn");
        }
        public TemplateEngineDAL(string _channelid)
        {
            base.SetupSystemDate();
            if (_channelid == string.Empty)
                _channelid = "0";
            
            this.MainChannel = new JumbotDLS.DAL.Normal_ChannelDAL().GetEntity(_channelid);
            this.Lang = new JumbotDLS.Utils.LanguageHelp().GetEntity(this.MainChannel.LanguageCode);
            //_Channel = Channel;

        }
        public JumbotDLS.Entity.Normal_Channel MainChannel;//页面频道实体
        public JumbotDLS.Entity.Normal_Channel ThisChannel;//模块频道实体
        private string _pagetitle, _pagekeywords, _pagedescription, _pagenav;
        private bool m_isHtml;
        private Dictionary<string, object> m_lang;
        /// <summary>
        /// 页面默认标题
        /// </summary>
        public string PageTitle
        {
            get { return this._pagetitle; }
            set { this._pagetitle = value; }
        }
        /// <summary>
        /// 页面默认关键字
        /// </summary>
        public string PageKeywords
        {
            get { return this._pagekeywords; }
            set { this._pagekeywords = value; }
        }
        /// <summary>
        /// 页面默认简介
        /// </summary>
        public string PageDescription
        {
            get { return this._pagedescription; }
            set { this._pagedescription = value; }
        }
        /// <summary>
        /// 页面链接导航
        /// </summary>
        public string PageNav
        {
            get { return this._pagenav; }
            set { this._pagenav = value; }
        }
        /// <summary>
        /// 是否生成静态
        /// </summary>
        public bool IsHtml
        {
            get { return this.m_isHtml; }
            set { this.m_isHtml = value; }
        }
        /// <summary>
        /// 频道语言包
        /// </summary>
        public Dictionary<string, object> Lang
        {
            get { return this.m_lang; }
            set { this.m_lang = value; }
        }
        /// <summary>
        /// 判断最终页面是否静态(频道ID只能从外部传入，不支持跨频道)
        /// </summary>
        /// <returns></returns>
        public bool PageIsHtml()
        {
            if (this.MainChannel.Id == "0")//没指定频道
                return (site.IsHtml);
            else
                return (site.IsHtml && this.MainChannel.IsHtml);
        }
        private string p__getNeightor(bool isHtml, string channelType, string channelId, string classId, string contentId, int isNext)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                StringBuilder sb = new StringBuilder();
                _doh.Reset();
                if (isNext == 0)
                    _doh.SqlCmd = "SELECT TOP 1 [Id],[Title] FROM [jdls_module_" + channelType + "] WHERE [ChannelId] = " + channelId + " And [IsPass]=1 AND [ParentID]=0 AND [Id]<" + contentId + " order By [Id] DESC";
                else
                    _doh.SqlCmd = "SELECT TOP 1 [Id],[Title] FROM [jdls_module_" + channelType + "] WHERE [ChannelId] = " + channelId + " AND [IsPass]=1 AND [ParentID]=0 AND [Id]>" + contentId + " order By [Id] ASC";
                DataTable dtContent = _doh.GetDataTable();
                if (dtContent.Rows.Count > 0)
                    sb.Append("<a href=\"" + Go2View(1, this.MainChannel.IsHtml, channelId, dtContent.Rows[0]["ID"].ToString(), true) + "\">" + dtContent.Rows[0]["Title"].ToString() + "</a>");
                else
                {
                    if (classId != "0")
                        sb.Append("<a href=\"" + Go2Class(1, isHtml, channelId, classId, false) + "\">返回列表</a>");
                    else
                        sb.Append("<a href=\"" + Go2Channel(1, isHtml, channelId, false) + "\">返回频道</a>");
                }
                dtContent.Clear();
                dtContent.Dispose();
                return sb.ToString();
            }

        }

        /// <summary>
        /// 替换专题标签
        /// </summary>
        /// <param name="_pagestr"></param>
        public void ReplaceSpecialTag(ref string _pagestr, string _SpecialId)
        {
            new JumbotDLS.DAL.Normal_SpecialDAL().ExecuteTags(ref _pagestr, _SpecialId);
        }
        /// <summary>
        /// 替换频道标签
        /// </summary>
        /// <param name="_pagestr"></param>
        public void ReplaceChannelTag(ref string _pagestr, string _ChannelId)
        {
            new JumbotDLS.DAL.Normal_ChannelDAL().ExecuteTags(ref _pagestr, _ChannelId);
        }
        /// <summary>
        /// 判断内容阅读权限(频道ID只能从外部传入，不支持跨频道)
        /// 假设内容ID和栏目ID都已经正确
        /// </summary>
        /// <param name="_contentid"></param>
        /// <param name="_classid"></param>
        /// <returns></returns>
        public bool CanReadContent(string _contentid, string _classid)
        {
            if (Cookie.GetValue(site.CookiePrev + "admin") != null)//管理员直接可以看
                return true;
            int _usergroup = 0;
            if (Cookie.GetValue(site.CookiePrev + "user") != null)
                _usergroup = Str2Int(Cookie.GetValue(site.CookiePrev + "user", "groupid"));
            int _ContentReadGroup, _ClassReadGroup;
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.ConditionExpress = "id=" + _contentid;
                _ContentReadGroup = Str2Int(_doh.GetField("jdls_module_" + this.MainChannel.Type, "ReadGroup").ToString());
                _doh.Reset();
                _doh.ConditionExpress = "id=" + _classid;
                _ClassReadGroup = Str2Int(_doh.GetField("jdls_normal_class", "ReadGroup").ToString());
            }
            if (_ContentReadGroup > -1)//说明不是继承栏目
            {
                if (_ContentReadGroup > _usergroup)
                    return false;
                else
                    return true;
            }
            else
            {
                if (_ClassReadGroup > _usergroup)
                    return false;
                else
                    return true;
            }
        }
        /// <summary>
        /// 替换栏目标签
        /// </summary>
        /// <param name="_pagestr"></param>
        public void ReplaceClassTag(ref string _pagestr, string _ClassId)
        {
            executeTag_Class(ref _pagestr, _ClassId);

        }
        /// <summary>
        /// 替换单页内容标签(频道ID从外部传入)
        /// </summary>
        /// <param name="_pagestr"></param>
        public void ReplaceContentTag(ref string _pagestr, string _ContentId)
        {
            executeTag_Content(ref _pagestr, _ContentId);
        }
        /// <summary>
        /// 解析栏目循环标签(不支持跨频道)
        /// </summary>
        /// <param name="_pagestr">原始内容</param>
        /// <returns></returns>
        public void ReplaceChannelClassLoopTag(ref string _pagestr)
        {
            replaceTag_ChannelClassLoop(ref _pagestr);
            replaceTag_ChannelClass2Loop(ref _pagestr);
            replaceTag_ClassTree(ref _pagestr);//2012-02-24新增标签
        }
        /// <summary>
        /// 解析内容循环标签
        /// </summary>
        /// <param name="_pagestr">原始内容</param>
        /// <returns></returns>
        public void ReplaceContentLoopTag(ref string _pagestr)
        {
            replaceTag_ContentLoop(ref _pagestr);
            replaceTag_RecordLoop(ref _pagestr);
        }
        /// <summary>
        /// 解析aspx动态页最后的标签
        /// </summary>
        /// <param name="_pagestr"></param>
        public void ReplaceSHTMLTag(ref string _pagestr)
        {
            replaceTag_Shtml(ref _pagestr);
        }
        public void ReplaceUserTag(ref string _pagestr)
        {
            replaceTag_User(ref _pagestr);
        }
        /// <summary>
        /// 解析一般的脚本标签(2014.01.27增加)
        /// </summary>
        /// <param name="_pagestr"></param>
        public void ReplaceScriptTag(ref string _pagestr)
        {
            replaceTag_Script(ref _pagestr);
        }
        /// <summary>
        /// 解析站点信息
        /// </summary>
        /// <param name="_pagestr">原始内容</param>
        /// <returns></returns>
        public void ReplaceSiteTags(ref string _pagestr)
        {
            replaceTag_Include(ref _pagestr);
            replaceTag_SiteConfig(ref _pagestr);
            replaceTag_GetRemoteWeb(ref _pagestr);
            replaceTag_Shtml(ref _pagestr);
            replaceTag_User(ref _pagestr);
        }
        /// <summary>
        /// 解析公共标签
        /// </summary>
        /// <param name="_pagestr">原始内容</param>
        /// <returns></returns>
        public void ReplacePublicTag(ref string _pagestr)
        {
            replaceTag_Include(ref _pagestr);
            replaceTag_SiteConfig(ref _pagestr);
            replaceTag_GetRemoteWeb(ref _pagestr);
            replaceTag_ChannelLoop(ref _pagestr);
        }
        /// <summary>
        /// 替换html包含标签(解析次序：2)
        /// </summary>
        /// <param name="_pagestr"></param>
        private void replaceTag_Include(ref string _pagestr)
        {
            string RegexString = "<jdls:include (?<tagcontent>.*?)/>";
            string[] _tagcontent = JumbotDLS.Utils.Strings.GetRegValue(_pagestr, RegexString, "tagcontent", false);
            if (_tagcontent.Length > 0)//标签存在
            {
                string _loopbody = string.Empty;
                string _replacestr = string.Empty;
                string _viewstr = string.Empty;
                string _tagfile = string.Empty;
                for (int i = 0; i < _tagcontent.Length; i++)
                {
                    _loopbody = "<jdls:include " + _tagcontent[i] + "/>";
                    _tagfile = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "file");
                    if (!_tagfile.StartsWith("/") && !_tagfile.StartsWith("~/"))
                        _tagfile = site.Dir + "themes/default/include/" + _tagfile;
                    if (JumbotDLS.Utils.DirFile.FileExists(_tagfile))
                        _replacestr = JumbotDLS.Utils.DirFile.ReadFile(_tagfile);
                    else
                        _replacestr = "";
                    _pagestr = _pagestr.Replace(_loopbody, _replacestr);
                }
            }
        }
        /// <summary>
        /// 替换shtml包含标签
        /// </summary>
        /// <param name="_pagestr"></param>
        private void replaceTag_Shtml(ref string _pagestr)
        {
            string RegexString = "<!--#include (?<tagcontent>.*?)-->";
            string[] _tagcontent = JumbotDLS.Utils.Strings.GetRegValue(_pagestr, RegexString, "tagcontent", false);
            if (_tagcontent.Length > 0)//标签存在
            {
                string _loopbody = string.Empty;
                string _replacestr = string.Empty;
                string _viewstr = string.Empty;
                string _tagfile = string.Empty;
                for (int i = 0; i < _tagcontent.Length; i++)
                {
                    _loopbody = "<!--#include " + _tagcontent[i] + "-->";
                    _tagfile = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "virtual");
                    if (JumbotDLS.Utils.DirFile.FileExists(_tagfile))
                        _replacestr = JumbotDLS.Utils.DirFile.ReadFile(_tagfile);
                    else
                        _replacestr = "";
                    _pagestr = _pagestr.Replace(_loopbody, _replacestr);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_pagestr"></param>
        private void replaceTag_User(ref string _pagestr)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                string RegexString = "<jdls:user>(?<tempstr>.*?)</jdls:user>";
                string[] _tempstr = JumbotDLS.Utils.Strings.GetRegValue(_pagestr, RegexString, "tempstr", false);
                if (_tempstr.Length > 0)//标签存在
                {
                    string _loopbody = string.Empty;
                    string _replacestr = string.Empty;
                    string _viewstr = string.Empty;
                    string _tablename = string.Empty;
                    for (int i = 0; i < _tempstr.Length; i++)
                    {
                        _loopbody = "<jdls:user>" + _tempstr[i] + "</jdls:user>";
                        string _TemplateContent = _tempstr[i];
                        JumbotDLS.TEngine.TemplateManager manager = JumbotDLS.TEngine.TemplateManager.FromString(_TemplateContent);
                        JumbotDLS.Entity.Client client = new JumbotDLS.Entity.Client();
                        if (Cookie.GetValue(site.CookiePrev + "user") != null)
                        {
                            client.UserId = Str2Str(Cookie.GetValue(site.CookiePrev + "user", "id"));
                            client.UserName = Cookie.GetValue(site.CookiePrev + "user", "name");
                        }
                        else
                        {
                            client.UserId = "0";
                            client.UserName = "";
                        }


                        if (Cookie.GetValue(site.CookiePrev + "user") != null)
                        {
                            string _userid = Str2Str(Cookie.GetValue(site.CookiePrev + "user", "id"));
                            string _usercookies = Cookie.GetValue(site.CookiePrev + "user", "cookies");
                            JumbotDLS.Entity.Normal_User _User = new JumbotDLS.DAL.Normal_UserDAL().GetEntity(_userid, true, IPHelp.ClientIP);
                            if (_User.Id != null)
                            {
                                client.UserId = _userid;
                                client.UserName = _User.UserName;
                                client.UserGroupId = _User.Group;
                                client.UserPoint = _User.Points;
                                client.UserIntegral = _User.Integral;
                            }
                        }
                        string ClientUserIP = JumbotDLS.Common.Const.GetUserIp;
                        client.OrgId = new JumbotDLS.DAL.Normal_WhiteIPDAL().GetOrganizationIdByIP(ClientUserIP);
                        if (client.OrgId != "0")
                        {
                            JumbotDLS.Entity.Normal_Organization ClientRealOrgInfo = new JumbotDLS.DAL.Normal_OrganizationDAL().GetEntity(client.OrgId);
                            client.OrgName = ClientRealOrgInfo.Title;
                        }



                        manager.SetValue("client", client);
                        string _content = manager.Process();
                        _pagestr = _pagestr.Replace(_loopbody, _content);
                    }
                }
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_pagestr"></param>
        private void replaceTag_Script(ref string _pagestr)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                string RegexString = "<jdls:script>(?<tempstr>.*?)</jdls:script>";
                string[] _tempstr = JumbotDLS.Utils.Strings.GetRegValue(_pagestr, RegexString, "tempstr", false);
                if (_tempstr.Length > 0)//标签存在
                {
                    string _loopbody = string.Empty;
                    string _replacestr = string.Empty;
                    string _viewstr = string.Empty;
                    string _tablename = string.Empty;
                    for (int i = 0; i < _tempstr.Length; i++)
                    {
                        _loopbody = "<jdls:script>" + _tempstr[i] + "</jdls:script>";
                        string _TemplateContent = _tempstr[i];
                        JumbotDLS.TEngine.TemplateManager manager = JumbotDLS.TEngine.TemplateManager.FromString(_TemplateContent);
                        string _content = manager.Process();
                        _pagestr = _pagestr.Replace(_loopbody, _content);
                    }
                }
            }

        }
        /// <summary>
        /// 替换公共标签(解析次序：3)
        /// </summary>
        /// <param name="_pagestr"></param>
        private void replaceTag_SiteConfig(ref string _pagestr)
        {
            _pagestr = _pagestr.Replace("{site.Dir}", site.Dir);//老版本
            _pagestr = _pagestr.Replace("{site.Url}", site.Url);//老版本
            _pagestr = _pagestr.Replace("<jdls:site.keywords/>", site.Keywords);
            _pagestr = _pagestr.Replace("<jdls:site.description/>", site.Description);
            _pagestr = _pagestr.Replace("<jdls:site.author/>", "jumbot,子木将博");
            _pagestr = _pagestr.Replace("<jdls:site.url/>", site.Url);
            _pagestr = _pagestr.Replace("<jdls:site.dir/>", site.Dir);
            _pagestr = _pagestr.Replace("<jdls:site.home/>", site.Home);

            _pagestr = _pagestr.Replace("<jdls:site.name/>", site.Name);
            _pagestr = _pagestr.Replace("<jdls:site.name2/>", site.Name2);
            _pagestr = _pagestr.Replace("{site.ICP}", site.ICP);
            _pagestr = _pagestr.Replace("{site.ForumUrl}", site.ForumUrl);
            if (this.MainChannel.Id != "0")
                _pagestr = _pagestr.Replace("<jdls:site.page.basehref/>", site.Url + site.Dir + this.MainChannel.Dir + "/");
            else
                _pagestr = _pagestr.Replace("<jdls:site.page.basehref/>", site.Url + site.Dir);
            _pagestr = _pagestr.Replace("<jdls:site.page.nav/>", this.PageNav);
            _pagestr = _pagestr.Replace("<jdls:site.page.title/>", this.PageTitle);
            _pagestr = _pagestr.Replace("<jdls:site.page.keywords/>", this.PageKeywords);
            _pagestr = _pagestr.Replace("<jdls:site.page.description/>", this.PageDescription);
            _pagestr = _pagestr.Replace("<jdls:site.version/>", site.Version);

        }
        /// <summary>
        /// 替换远程网页内容(解析次序：3)
        /// </summary>
        /// <param name="_pagestr"></param>
        private void replaceTag_GetRemoteWeb(ref string _pagestr)
        {
            string RegexString = "<jdls:remoteweb (?<tagcontent>.*?)/>";
            string[] _tagcontent = JumbotDLS.Utils.Strings.GetRegValue(_pagestr, RegexString, "tagcontent", false);
            if (_tagcontent.Length > 0)//标签存在
            {
                string _loopbody = string.Empty;
                string _replacestr = string.Empty;
                string _viewstr = string.Empty;
                string _tagurl = string.Empty;
                string _tagcharset = string.Empty;
                System.Text.Encoding encodeType = System.Text.Encoding.Default;
                for (int i = 0; i < _tagcontent.Length; i++)
                {
                    _loopbody = "<jdls:remoteweb " + _tagcontent[i] + "/>";
                    _tagurl = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "url");
                    _tagcharset = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "charset").ToLower();
                    switch (_tagcharset)
                    {
                        case "unicode":
                            encodeType = System.Text.Encoding.Unicode;
                            break;
                        case "utf-8":
                            encodeType = System.Text.Encoding.UTF8;
                            break;
                        case "gb2312":
                            encodeType = System.Text.Encoding.GetEncoding("GB2312");
                            break;
                        case "gbk":
                            encodeType = System.Text.Encoding.GetEncoding("GB2312");
                            break;
                        default:
                            encodeType = System.Text.Encoding.Default;
                            break;
                    }
                    JumbotDLS.Common.NewsCollection nc = new JumbotDLS.Common.NewsCollection();
                    _replacestr = JumbotDLS.Utils.HttpHelper.Get_Http(_tagurl, 8000, encodeType);
                    _pagestr = _pagestr.Replace(_loopbody, _replacestr);
                }
            }
        }
        /// <summary>
        /// 替换注释标签
        /// </summary>
        /// <param name="_pagestr">已取到的模板内容</param>
        private void replaceTag_NoShow(ref string _pagestr)
        {
            System.Collections.ArrayList TagArray = JumbotDLS.Utils.Strings.GetHtmls(_pagestr, "<!--~", "~-->", false, false);
            if (TagArray.Count > 0)//标签存在
            {
                string TempStr = string.Empty;
                string ReplaceStr;
                for (int i = 0; i < TagArray.Count; i++)
                {
                    TempStr = "<!--~" + TagArray[i].ToString() + "~-->";
                    ReplaceStr = "";
                    _pagestr = _pagestr.Replace(TempStr, ReplaceStr);
                }
            }
        }
        /// <summary>
        /// 替换循环频道标签(将频道信息赋值给循环体)(解析次序：5)
        /// </summary>
        /// <param name="_pagestr"></param>
        private void replaceTag_ChannelLoop(ref string _pagestr)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                string RegexString = "<jdls:channelloop (?<tagcontent>.*?)>(?<tempstr>.*?)</jdls:channelloop>";
                string[] _tagcontent = JumbotDLS.Utils.Strings.GetRegValue(_pagestr, RegexString, "tagcontent", false);
                string[] _tempstr = JumbotDLS.Utils.Strings.GetRegValue(_pagestr, RegexString, "tempstr", false);
                if (_tagcontent.Length > 0)//标签存在
                {
                    string _loopbody = string.Empty;
                    string _replacestr = string.Empty;
                    string _viewstr = string.Empty;
                    string _tagpagesize, _tagisnav, _tagselectids, _tagorderfield, _tagordertype, _tagwherestr = string.Empty;
                    for (int i = 0; i < _tagcontent.Length; i++)
                    {
                        _loopbody = "<jdls:channelloop " + _tagcontent[i] + ">" + _tempstr[i] + "</jdls:channelloop>";
                        _tagisnav = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "isnav");
                        if (_tagisnav == "") _tagisnav = "0";
                        _tagpagesize = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "pagesize");
                        if (_tagpagesize == "") _tagpagesize = "0";
                        _tagselectids = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "selectids");
                        _tagorderfield = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "orderfield");
                        if (_tagorderfield == "") _tagorderfield = "pid";
                        _tagordertype = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "ordertype");
                        if (_tagordertype == "") _tagordertype = "asc";
                        _tagwherestr = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "wherestr");
                        _doh.Reset();
                        string _sql = "select top " + _tagpagesize + " * FROM [jdls_normal_channel] WHERE [Enabled]=1";
                        if (_tagisnav == "1")
                            _sql += " AND [IsNav]=1";
                        if (_tagselectids != "")
                            _sql += " AND id in (" + _tagselectids.Replace("|", ",") + ")";
                        if (_tagwherestr != "")
                            _sql += " and " + _tagwherestr;
                        _sql += " ORDER BY [" + _tagorderfield + "] " + _tagordertype;
                        _doh.SqlCmd = _sql;
                        DataTable _dt = _doh.GetDataTable();
                        StringBuilder sb = new StringBuilder();
                        for (int j = 0; j < _dt.Rows.Count; j++)
                        {
                            _viewstr = _tempstr[i];
                            _viewstr = _viewstr.Replace("{$ChannelNO}", (j+1).ToString());
                            DataRow dr = _dt.Rows[j];
                            JumbotDLS.Entity.Normal_Channel _Channel = new JumbotDLS.DAL.Normal_ChannelDAL().GetEntity(dr);
                            new JumbotDLS.DAL.Normal_ChannelDAL().ExecuteTags(ref _viewstr, _Channel);
                            sb.Append(_viewstr);
                        }
                        _pagestr = _pagestr.Replace(_loopbody, sb.ToString());
                        _dt.Clear();
                        _dt.Dispose();
                    }
                }
            }
        }
        /// <summary>
        /// 替换频道栏目循环标签(不支持跨频道)
        /// </summary>
        /// <param name="_pagestr"></param>
        private void replaceTag_ChannelClassLoop(ref string _pagestr)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                string RegexString = "<jdls:classloop (?<tagcontent>.*?)>(?<tempstr>.*?)</jdls:classloop>";
                string[] _tagcontent = JumbotDLS.Utils.Strings.GetRegValue(_pagestr, RegexString, "tagcontent", false);
                string[] _tempstr = JumbotDLS.Utils.Strings.GetRegValue(_pagestr, RegexString, "tempstr", false);
                if (_tagcontent.Length > 0)//标签存在
                {
                    string _loopbody = string.Empty;
                    string _replacestr = string.Empty;
                    string _viewstr = string.Empty;
                    string _tagpagesize, _tagselectids, _tagdepth, _tagparentid, _tagwherestr, _tagorderfield, _tagordertype, _hascontent = string.Empty;
                    for (int i = 0; i < _tagcontent.Length; i++)
                    {
                        _loopbody = "<jdls:classloop " + _tagcontent[i] + ">" + _tempstr[i] + "</jdls:classloop>";
                        string _tagchannelid = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "channelid");
                        if (_tagchannelid == "") _tagchannelid = "0";
                        _tagpagesize = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "pagesize");
                        _tagselectids = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "selectids");
                        _tagdepth = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "depth");
                        _tagparentid = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "parentid");
                        if (_tagparentid == "") _tagparentid = "0";
                        _tagwherestr = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "wherestr");
                        _tagorderfield = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "orderfield");
                        if (_tagorderfield == "") _tagorderfield = "code";
                        _tagordertype = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "ordertype");
                        if (_tagordertype == "") _tagordertype = "asc";
                        if (_tagpagesize == "") _tagpagesize = "0";
                        _hascontent = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "hascontent");
                        if (_hascontent == "") _hascontent = "0";
                        if (_tagdepth == "") _tagdepth = "0";
                        string pStr = " [Id],[Title],[Info],[TopicNum],[Code],[ChannelId] FROM [jdls_normal_class] WHERE [IsNav]=1 AND [ChannelId]=" + _tagchannelid;
                        string oStr = " ORDER BY code asc";
                        if (_tagorderfield.ToLower() != "code")
                            oStr = " ORDER BY " + _tagorderfield + " " + _tagordertype + ",code asc";
                        else
                            oStr = " ORDER BY " + _tagorderfield + " " + _tagordertype;
                        _doh.Reset();
                        if (_tagdepth != "-1" && _tagdepth != "0")
                            pStr += " AND Len(Code)=" + (Str2Int(_tagdepth, 0) * 4);
                        if (_tagpagesize != "0")
                            pStr = " top " + _tagpagesize + pStr;
                        if (_tagparentid != "" && _tagparentid != "0")
                            pStr += " AND [ParentId]=" + _tagparentid;
                        if (_hascontent == "1")
                            pStr += " AND [TopicNum]>0";
                        if (_tagwherestr != "")
                            pStr += " AND " + _tagwherestr.Replace("小于", "<").Replace("大于", ">").Replace("不等于", "<>");
                        if (_tagselectids != "")
                            pStr += " AND [id] IN (" + _tagselectids.Replace("|", ",") + ")";

                        _doh.SqlCmd = "select" + pStr + oStr;
                        DataTable _dt = _doh.GetDataTable();
                        StringBuilder sb = new StringBuilder();
                        for (int j = 0; j < _dt.Rows.Count; j++)
                        {
                            if (_tagdepth == "-1")
                            {
                                _doh.Reset();
                                _doh.ConditionExpress = " [ChannelId]=" + _tagchannelid + " AND [ParentId]=" + _dt.Rows[j]["Id"].ToString();
                                int totalCount = _doh.Count("jdls_normal_class");
                                if (totalCount > 1)//表示非末级栏目，直接跳过
                                    continue;
                            }
                            _viewstr = _tempstr[i];
                            _viewstr = _viewstr.Replace("{$ClassNO}", (j + 1).ToString());
                            executeTag_Class(ref _viewstr, _dt.Rows[j]["Id"].ToString());
                            sb.Append(_viewstr);
                        }
                        _pagestr = _pagestr.Replace(_loopbody, sb.ToString());
                        _dt.Clear();
                        _dt.Dispose();
                    }
                }
            }

        }
        /// <summary>
        /// 替换频道栏目循环标签(不支持跨频道)
        /// </summary>
        /// <param name="_pagestr"></param>
        private void replaceTag_ChannelClass2Loop(ref string _pagestr)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                string RegexString = "<jdls:class2loop (?<tagcontent>.*?)>(?<tempstr>.*?)</jdls:class2loop>";
                string[] _tagcontent = JumbotDLS.Utils.Strings.GetRegValue(_pagestr, RegexString, "tagcontent", false);
                string[] _tempstr = JumbotDLS.Utils.Strings.GetRegValue(_pagestr, RegexString, "tempstr", false);
                if (_tagcontent.Length > 0)//标签存在
                {
                    string _loopbody = string.Empty;
                    string _replacestr = string.Empty;
                    string _viewstr = string.Empty;
                    string _tagpagesize, _tagselectids, _tagdepth, _tagparentid, _tagwherestr, _tagorderfield, _tagordertype, _hascontent = string.Empty;
                    for (int i = 0; i < _tagcontent.Length; i++)
                    {
                        _loopbody = "<jdls:class2loop " + _tagcontent[i] + ">" + _tempstr[i] + "</jdls:class2loop>";
                        string _tagchannelid = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "channelid");
                        _tagpagesize = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "pagesize");
                        _tagselectids = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "selectids");
                        _tagdepth = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "depth");
                        _tagparentid = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "parentid");
                        _tagwherestr = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "wherestr");
                        _tagorderfield = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "orderfield");
                        if (_tagorderfield == "") _tagorderfield = "code";
                        _tagordertype = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "ordertype");
                        if (_tagordertype == "") _tagordertype = "asc";
                        if (_tagpagesize == "") _tagpagesize = "0";
                        _hascontent = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "hascontent");
                        if (_hascontent == "") _hascontent = "0";
                        if (_tagdepth == "") _tagdepth = "0";
                        string pStr = " [Id],[Title],[Info],[TopicNum],[Code],[ChannelId] FROM [jdls_normal_class] WHERE [ChannelId]=" + _tagchannelid;
                        string oStr = " ORDER BY code asc";
                        if (_tagorderfield.ToLower() != "code")
                            oStr = " ORDER BY " + _tagorderfield + " " + _tagordertype + ",code asc";
                        else
                            oStr = " ORDER BY " + _tagorderfield + " " + _tagordertype;
                        _doh.Reset();
                        if (_tagdepth != "-1" && _tagdepth != "0")
                            pStr += " AND Len(Code)=" + (Str2Int(_tagdepth, 0) * 4);
                        if (_tagpagesize != "0")
                            pStr = " top " + _tagpagesize + pStr;
                        if (_tagparentid != "" && _tagparentid != "0")
                            pStr += " AND [ParentId]=" + _tagparentid;
                        if (_hascontent == "1")
                            pStr += " AND [TopicNum]>0";
                        if (_tagwherestr != "")
                            pStr += " AND " + _tagwherestr.Replace("小于", "<").Replace("大于", ">").Replace("不等于", "<>");
                        if (_tagselectids != "")
                            pStr += " AND [id] IN (" + _tagselectids.Replace("|", ",") + ")";

                        _doh.SqlCmd = "select" + pStr + oStr;
                        DataTable _dt = _doh.GetDataTable();
                        StringBuilder sb = new StringBuilder();
                        for (int j = 0; j < _dt.Rows.Count; j++)
                        {
                            if (_tagdepth == "-1")
                            {
                                _doh.Reset();
                                _doh.ConditionExpress = " [ChannelId]=" + _tagchannelid + " AND [ParentId]=" + _dt.Rows[j]["Id"].ToString();
                                int totalCount = _doh.Count("jdls_normal_class");
                                if (totalCount > 1)//表示非末级栏目，直接跳过
                                    continue;
                            }
                            _viewstr = _tempstr[i];
                            _viewstr = _viewstr.Replace("{$Class2NO}", (j + 1).ToString());
                            executeTag_Class2(ref _viewstr, _dt.Rows[j]["Id"].ToString());
                            sb.Append(_viewstr);
                        }
                        _pagestr = _pagestr.Replace(_loopbody, sb.ToString());
                        _dt.Clear();
                        _dt.Dispose();
                    }
                }
            }

        }
        /// <summary>
        /// 解析栏目树标签(2012-02-24新增标签)
        /// </summary>
        /// <param name="_pagestr"></param>
        private void replaceTag_ClassTree(ref string _pagestr)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                string RegexString = "<jdls:classtree (?<tagcontent>.*?)>(?<tempstr>.*?)</jdls:classtree>";
                string[] _tagcontent = JumbotDLS.Utils.Strings.GetRegValue(_pagestr, RegexString, "tagcontent", false);
                string[] _tempstr = JumbotDLS.Utils.Strings.GetRegValue(_pagestr, RegexString, "tempstr", false);
                if (_tagcontent.Length > 0)//标签存在
                {
                    string _loopbody = string.Empty;
                    string _replacestr = string.Empty;
                    string _viewstr = string.Empty;
                    string _tagchannelid, _tagclassid = string.Empty;
                    bool _tagincludechild = false;
                    for (int i = 0; i < _tagcontent.Length; i++)
                    {
                        _loopbody = "<jdls:classtree " + _tagcontent[i] + ">" + _tempstr[i] + "</jdls:classtree>";
                        _tagchannelid = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "channelid");
                        _tagclassid = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "classid");
                        if (_tagclassid == "") _tagclassid = "0";
                        _tagincludechild = (JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "includechild") != "0");

                        string _TemplateContent = _tempstr[i];
                        JumbotDLS.TEngine.TemplateManager manager = JumbotDLS.TEngine.TemplateManager.FromString(_TemplateContent);
                        manager.SetValue("tree", (new JumbotDLS.DAL.Normal_ClassDAL().GetClassTree(_tagchannelid, _tagclassid, _tagincludechild)));
                        _replacestr = manager.Process();
                        _pagestr = _pagestr.Replace(_loopbody, _replacestr);
                    }
                }
            }

        }
        /// <summary>
        /// 替换内容循环标签
        /// </summary>
        /// <param name="_pagestr"></param>
        private void replaceTag_ContentLoop(ref string _pagestr)
        {
            string RegexString = "<jdls:contentloop (?<tagcontent>.*?)>(?<tempstr>.*?)</jdls:contentloop>";
            string[] _tagcontent = JumbotDLS.Utils.Strings.GetRegValue(_pagestr, RegexString, "tagcontent", false);
            string[] _tempstr = JumbotDLS.Utils.Strings.GetRegValue(_pagestr, RegexString, "tempstr", false);
            if (_tagcontent.Length > 0)//标签存在
            {
                string _loopbody = string.Empty;
                string _replacestr = string.Empty;
                string _viewstr = string.Empty;
                for (int i = 0; i < _tagcontent.Length; i++)
                {
                    _loopbody = "<jdls:contentloop " + _tagcontent[i] + ">" + _tempstr[i] + "</jdls:contentloop>";
                    _replacestr = getContentList_RL(_tagcontent[i], _tempstr[i].Replace("<#foreach>", "<#foreach collection=\"${contents}\" var=\"field\" index=\"i\">"));
                    _pagestr = _pagestr.Replace(_loopbody, _replacestr);
                }
            }
        }
        /// <summary>
        /// 提取列表供列表标签使用
        /// </summary>
        /// <param name="Parameter"></param>

        /// <returns></returns>
        private string getContentList_RL(string _tagcontent, string _tempstr)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                int _tagpagesize = Str2Int(JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "pagesize"));
                if (_tagpagesize == 0) _tagpagesize = 10;
                int _tagpage = Str2Int(JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "page"));
                if (_tagpage == 0) _tagpage = 1;
                string _tagchannelid = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "channelid");
                if (_tagchannelid == "") _tagchannelid = "0";
                string _tagchanneltype = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "channeltype");
                if (_tagchanneltype == "") _tagchanneltype = "article";
                string _tagclassid = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "classid");
                if (_tagclassid == "") _tagclassid = "0";
                string _tagfields = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "fields");
                string _tagorderfield = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "orderfield");
                if (_tagorderfield == "") _tagorderfield = "adddate";
                string _tagordertype = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "ordertype");
                if (_tagordertype == "") _tagordertype = "desc";
                string _tagistop = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "istop");
                if (_tagistop == "") _tagistop = "0";
                string _tagisfocus = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "isfocus");
                if (_tagisfocus == "") _tagisfocus = "0";
                string _tagishead = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "ishead");
                if (_tagishead == "") _tagishead = "0";
                string _tagisimg = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "isimg");
                if (_tagisimg == "") _tagisimg = "0";
                string _tagtimerange = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "timerange");
                string _tagexceptids = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "exceptids");
                string _tagwherestr = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "wherestr");
                string _tagislike = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "islike");
                string _tagkeywords = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "keywords");
                string _tagpagerlink = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent, "pagerlink");
                string _ccType = string.Empty;
                if (_tagchannelid != "0")
                {
                    _doh.Reset();
                    _doh.SqlCmd = "SELECT [Id],[Type] FROM [jdls_normal_channel] WHERE [Id]=" + _tagchannelid + " AND [Enabled]=1";
                    DataTable dtChannel = _doh.GetDataTable();
                    if (dtChannel.Rows.Count > 0)
                    {
                        _ccType = dtChannel.Rows[0]["Type"].ToString();
                    }
                    else
                    {
                        return "&nbsp;频道参数错误";
                    }
                    dtChannel.Clear();
                    dtChannel.Dispose();
                }
                else
                {
                    _ccType = _tagchanneltype;
                }
                JumbotDLS.DAL.Normal_ChannelDAL dal = new JumbotDLS.DAL.Normal_ChannelDAL();
                dal.ExecuteTags(ref _tempstr, _tagchannelid);
                if (_tagclassid != "0")
                    executeTag_Class(ref _tempstr, _tagclassid);

                string whereStr =  "[IsPass]=1";
                #region 约束搜索条件
                if (_tagchannelid != "0")
                {
                    whereStr += " AND [ChannelId]=" + _tagchannelid;
                    if (_tagclassid != "0")
                        whereStr += " And ([ClassId] in (SELECT ID FROM [jdls_normal_class] WHERE [IsOut]=0 AND [Code] Like (SELECT Code FROM [jdls_normal_class] WHERE [IsOut]=0 AND [Id]=" + _tagclassid + " AND [ChannelId]=" + _tagchannelid + ")+'%')" + " AND [ChannelId]=" + _tagchannelid + ")";
                }
                else
                {
                    if (_tagclassid != "0")
                        whereStr += " And ([ClassId] in (SELECT ID FROM [jdls_normal_class] WHERE [IsOut]=0 AND [Code] Like (SELECT Code FROM [jdls_normal_class] WHERE [IsOut]=0 AND [Id]=" + _tagclassid + ")+'%'))";
                    else
                        whereStr += " And ([ChannelId] in (SELECT ID FROM [jdls_normal_channel] WHERE [Type]='" + _ccType + "' AND [Enabled]=1))";

                }
                if (_tagistop == "1")
                    whereStr += " And [IsTop]=1";
                else if (_tagistop == "-1")
                    whereStr += " And [IsTop]=0";
                if (_tagisfocus == "1")
                    whereStr += " And [IsFocus]=1";
                else if (_tagisfocus == "-1")
                    whereStr += " And [IsFocus]=0";
                if (_tagishead == "1")
                    whereStr += " And [IsHead]=1";
                else if (_tagishead == "-1")
                    whereStr += " And [IsHead]=0";
                if (DBType == "0")
                {
                    switch (_tagtimerange)
                    {
                        case "1d":
                            whereStr += " AND datediff('d',AddDate,'" + DateTime.Now.ToShortDateString() + "')=0";
                            break;
                        case "1w":
                            whereStr += " AND datediff('ww',AddDate,'" + DateTime.Now.ToShortDateString() + "')=0";
                            break;
                        case "1m":
                            whereStr += " AND datediff('m',AddDate,'" + DateTime.Now.ToShortDateString() + "')=0";
                            break;
                        case "1y":
                            whereStr += " AND AddDate>=#" + (DateTime.Now.Year + "-1-1") + "#";
                            break;
                    }
                }
                else
                {
                    switch (_tagtimerange)
                    {
                        case "1d":
                            whereStr += " AND datediff(d,AddDate,'" + DateTime.Now.ToShortDateString() + "')=0";
                            break;
                        case "1w":
                            whereStr += " AND datediff(ww,AddDate,'" + DateTime.Now.ToShortDateString() + "')=0";
                            break;
                        case "1m":
                            whereStr += " AND datediff(m,AddDate,'" + DateTime.Now.ToShortDateString() + "')=0";
                            break;
                        case "1y":
                            whereStr += " AND AddDate>='" + (DateTime.Now.Year + "-1-1") + "'";
                            break;
                    }
                }
                if (_tagisimg == "1")
                    whereStr += " And [IsImg]=1";
                if (_tagwherestr != "")
                    whereStr += " AND " + _tagwherestr.Replace("小于", "<").Replace("大于", ">").Replace("不等于", "<>");
                if (_tagexceptids != "")
                    whereStr += " AND ID not in(" + _tagexceptids + ")";
                if (_tagislike == "1")
                {
                    if (_tagkeywords == "") _tagkeywords = "超星";
                    _tagkeywords = _tagkeywords.Replace(",", " ").Replace(";", " ").Replace("；", " ").Replace("、", " ");
                    string[] key = _tagkeywords.Split(new string[] { " " }, StringSplitOptions.None);
                    string _joinstr = " AND (1<0";//亏我想得出来
                    for (int i = 0; i < key.Length; i++)
                    {
                        if (key[i].Length > 1)
                        {
                            _joinstr += " OR [Tags] LIKE '%" + key[i].Trim() + "%'";
                        }
                    }
                    _joinstr += ")";
                    whereStr += _joinstr;
                }
                #endregion
                NameValueCollection orders = new NameValueCollection();
                if (_tagorderfield.ToLower() != "rnd")
                {
                    orders.Add(_tagorderfield, _tagordertype);
                    if (_tagorderfield.ToLower() != "adddate")
                        orders.Add("AddDate", "desc");
                    if (_tagorderfield.ToLower() != "id")
                    orders.Add("Id", "desc");
                }
                else
                    orders.Add(ORDER_BY_RND(), "");

                _doh.Reset();
                _doh.ConditionExpress = whereStr;
                int totalCount = _doh.Count("jdls_module_" + _ccType);
                string FieldList = (_tagfields + ",(select ishtml from [jdls_normal_channel] where id=[jdls_module_" + _ccType + "].channelid) as channelishtml").ToLower();
                if (!_tagfields.StartsWith("*"))//不是取所有字段
                {
                    FieldList += ",[Id],[ChannelId],[OutUrl],[Target],[ClassId],[FirstPage]";
                    if (!FieldList.Contains(",adddate"))
                        FieldList += ",adddate";
                    if (!FieldList.Contains(",module"))
                        FieldList += ",module";
                    if (!FieldList.Contains(_tagorderfield.ToLower()))
                        FieldList += "," + _tagorderfield.ToLower();
                }

                string sqlStr = JumbotDLS.Utils.SqlHelp.GetSql1(FieldList, "jdls_module_" + _ccType, totalCount, _tagpagesize, _tagpage, orders, whereStr);
                
                //return sqlStr;
                _doh.Reset();
                _doh.SqlCmd = sqlStr;
                DataTable dt = _doh.GetDataTable();
                _tempstr = _tempstr.Replace("{$This.page}", _tagpagesize.ToString());
                _tempstr = _tempstr.Replace("{$This.pagesize}", _tagpage.ToString());
                _tempstr = _tempstr.Replace("{$TotalCount}", totalCount.ToString());
                _tempstr = _tempstr.Replace("{$ContentCount}", dt.Rows.Count.ToString());
                string ReplaceStr = operateContentTag(_ccType, dt, _tempstr);

                ReplaceStr = ReplaceStr.Replace("{$PageBarHTML}", JumbotDLS.Utils.PageBar.GetPageBar(4, "js", 2, totalCount, _tagpagesize, _tagpage, _tagpagerlink));

                dt.Clear();
                dt.Dispose();
                return ReplaceStr;

            }

        }

        /// <summary>
        /// 解析栏目标签
        /// </summary>
        /// <param name="_pagestr"></param>
        /// <param name="_classid"></param>
        /// <returns></returns>
        private void executeTag_Class(ref string _pagestr, string _classid)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.SqlCmd = "SELECT [Id],[Title],LikeFieldTitle,LikeFieldValue,[IsPaging],[DefaultContentId],DefaultModuleId,[Target],[PageSize],[Info],[IconString],[Img],[Icon],[Keywords],[Content],[TopicNum],[Code],len(code) as len,[ChannelId],[ParentId] FROM [jdls_normal_class] WHERE [Id]=" + _classid;
                DataTable _dt = _doh.GetDataTable();
                if (_dt.Rows.Count > 0)
                {
                    string _channelid = _dt.Rows[0]["ChannelId"].ToString();
                    string _parentid = _dt.Rows[0]["ParentId"].ToString();
                    if (_channelid == this.MainChannel.Id)//说明是当前
                        this.ThisChannel = this.MainChannel;
                    else
                        this.ThisChannel = new JumbotDLS.DAL.Normal_ChannelDAL().GetEntity(_channelid);
                    _pagestr = _pagestr.Replace("{$ClassId}", _dt.Rows[0]["Id"].ToString());
                    _pagestr = _pagestr.Replace("{$ClassDefaultContentId}", _dt.Rows[0]["DefaultContentId"].ToString());
                    _pagestr = _pagestr.Replace("{$ClassName}", _dt.Rows[0]["Title"].ToString());
                    _pagestr = _pagestr.Replace("{$ClassLikeFieldTitle}", _dt.Rows[0]["LikeFieldTitle"].ToString());
                    _pagestr = _pagestr.Replace("{$ClassLikeFieldValue}", _dt.Rows[0]["LikeFieldValue"].ToString());
                    _pagestr = _pagestr.Replace("{$ClassTarget}", _dt.Rows[0]["Target"].ToString());
                    _pagestr = _pagestr.Replace("{$ClassInfo}", _dt.Rows[0]["Info"].ToString());
                    _pagestr = _pagestr.Replace("{$ClassKeywords}", _dt.Rows[0]["Keywords"].ToString());
                    _pagestr = _pagestr.Replace("{$ClassContent}", _dt.Rows[0]["Content"].ToString());
                    _pagestr = _pagestr.Replace("{$ClassDefaultModuleId}", _dt.Rows[0]["DefaultModuleId"].ToString());
                    _pagestr = _pagestr.Replace("{$ClassImg}", _dt.Rows[0]["Img"].ToString());
                    _pagestr = _pagestr.Replace("{$ClassPageSize}", (_dt.Rows[0]["IsPaging"].ToString() == "1") ? _dt.Rows[0]["PageSize"].ToString() : "9999999");
                    _pagestr = _pagestr.Replace("{$ClassIcon}", _dt.Rows[0]["Icon"].ToString());
                    _pagestr = _pagestr.Replace("{$ClassIconString}", _dt.Rows[0]["IconString"].ToString()); 
                    _pagestr = _pagestr.Replace("{$ClassTopicNum}", _dt.Rows[0]["TopicNum"].ToString());
                    _pagestr = _pagestr.Replace("{$ClassLink}", Go2Class(1, this.ThisChannel.IsHtml, _channelid, _classid, false));
                    _pagestr = _pagestr.Replace("{$ClassCode}", _dt.Rows[0]["Code"].ToString());
                    _pagestr = _pagestr.Replace("{$ClassCodeLeft4}", _dt.Rows[0]["Code"].ToString().Substring(0,4));
                    _pagestr = _pagestr.Replace("{$ClassDepth}", (Str2Int(_dt.Rows[0]["Len"].ToString()) / 4).ToString());
                    _pagestr = _pagestr.Replace("{$ClassParentId}", _dt.Rows[0]["ParentId"].ToString());
                    if (_dt.Rows[0]["ParentId"].ToString() != "0")
                    {
                        JumbotDLS.Entity.Normal_Class _parentclass = new JumbotDLS.DAL.Normal_ClassDAL().GetEntity(_parentid);
                        _pagestr = _pagestr.Replace("{$ClassParentName}", _parentclass.Title);
                        _pagestr = _pagestr.Replace("{$ClassParentLink}", Go2Class(1, this.ThisChannel.IsHtml, _channelid, _parentid, false));
                        _pagestr = _pagestr.Replace("{$ClassParentCode}", _parentclass.Code);
                    }
                    else
                    {
                        _pagestr = _pagestr.Replace("{$ClassParentName}", this.ThisChannel.Title);
                        _pagestr = _pagestr.Replace("{$ClassParentLink}", Go2Channel(1, this.ThisChannel.IsHtml, _channelid, false));
                        _pagestr = _pagestr.Replace("{$ClassParentCode}", "");
                    }
                }
                _dt.Clear();
                _dt.Dispose();
            }
        }
        /// <summary>
        /// 解析栏目标签
        /// </summary>
        /// <param name="_pagestr"></param>
        /// <param name="_classid"></param>
        /// <returns></returns>
        private void executeTag_Class2(ref string _pagestr, string _classid)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.SqlCmd = "SELECT [Id],[Title],[Info],[Img],[TopicNum],[Code],len(code) as len,[ChannelId],[ParentId] FROM [jdls_normal_class] WHERE [Id]=" + _classid;
                DataTable _dt = _doh.GetDataTable();
                if (_dt.Rows.Count > 0)
                {
                    _pagestr = _pagestr.Replace("{$Class2Id}", _dt.Rows[0]["Id"].ToString());
                    _pagestr = _pagestr.Replace("{$Class2Name}", _dt.Rows[0]["Title"].ToString());
                    _pagestr = _pagestr.Replace("{$Class2Info}", _dt.Rows[0]["Info"].ToString());
                    _pagestr = _pagestr.Replace("{$Class2Img}", _dt.Rows[0]["Img"].ToString());
                    _pagestr = _pagestr.Replace("{$Class2TopicNum}", _dt.Rows[0]["TopicNum"].ToString());
                    _pagestr = _pagestr.Replace("{$Class2Link}", Go2Class(1, this.MainChannel.IsHtml, _dt.Rows[0]["ChannelId"].ToString(), _dt.Rows[0]["Id"].ToString(), false));
                    _pagestr = _pagestr.Replace("{$Class2Code}", _dt.Rows[0]["Code"].ToString());
                    _pagestr = _pagestr.Replace("{$Class2Depth}", (Str2Int(_dt.Rows[0]["Len"].ToString()) / 4).ToString());
                }
                _dt.Clear();
                _dt.Dispose();
            }
        }

        /// <summary>
        /// 解析单条内容标签
        /// </summary>
        /// <param name="_pagestr"></param>
        /// <param name="_contentid"></param>
        /// <returns></returns>
        private void executeTag_Content(ref string _pagestr, string _contentid)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                string _randomstr = "1" + RandomStr(4);
                string _tempstr = string.Empty;
                _doh.Reset();
                _doh.SqlCmd = "SELECT * FROM  [jdls_module_" + this.MainChannel.Type + "] WHERE [Id]=" + _contentid;
                DataTable dtContent = _doh.GetDataTable();
                if (dtContent.Rows.Count > 0)
                {
                    _pagestr = _pagestr.Replace("{$_getNeightor(0)}", p__getNeightor(this.MainChannel.IsHtml, this.MainChannel.Type, dtContent.Rows[0]["ChannelId"].ToString(), dtContent.Rows[0]["ClassId"].ToString(), _contentid, 0));
                    _pagestr = _pagestr.Replace("{$_getNeightor(1)}", p__getNeightor(this.MainChannel.IsHtml, this.MainChannel.Type, dtContent.Rows[0]["ChannelId"].ToString(), dtContent.Rows[0]["ClassId"].ToString(), _contentid, 1));
                    for (int i = 0; i < dtContent.Columns.Count; i++)
                    {
                        if (dtContent.Rows[0]["IsImg"].ToString() == "0" || dtContent.Rows[0]["Img"].ToString().Length == 0)
                            _pagestr = _pagestr.Replace("{$_img}", site.Dir + "statics/common/nophoto.jpg");
                        else
                            _pagestr = _pagestr.Replace("{$_img}", dtContent.Rows[0]["Img"].ToString());
                        switch (dtContent.Columns[i].ColumnName.ToLower())
                        {
                            case "adddate":
                                _pagestr = _pagestr.Replace("{$_adddate}", Convert.ToDateTime(dtContent.Rows[0]["AddDate"]).ToString("yyyy-MM-dd"));
                                break;
                            case "viewnum":
                                _pagestr = _pagestr.Replace("{$_viewnum}", "<script src=\"" + site.Dir + "plus/viewcount.aspx?ccid=" + this.MainChannel.Id + "&cType=" + this.MainChannel.Type + "&id=" + _contentid + "&addit=1\"></script>");
                                break;
                            case "downnum":
                                _pagestr = _pagestr.Replace("{$_downnum}", "<script src=\"" + site.Dir + "plus/downcount.aspx?ccid=" + this.MainChannel.Id + "&cType=" + this.MainChannel.Type + "&id=" + _contentid + "\"></script>");
                                break;
                            default:
                                _pagestr = _pagestr.Replace("{$_" + dtContent.Columns[i].ColumnName.ToLower() + "}", dtContent.Rows[0][i].ToString());
                                break;
                        }
                    }
                }
                dtContent.Clear();
                dtContent.Dispose();
            }
        }
        /// <summary>
        /// 替换其他模型标签
        /// </summary>
        /// <param name="_pagestr"></param>
        private void replaceTag_RecordLoop(ref string _pagestr)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                string RegexString = "<jdls:recordloop (?<tagcontent>.*?)>(?<tempstr>.*?)</jdls:recordloop>";
                string[] _tagcontent = JumbotDLS.Utils.Strings.GetRegValue(_pagestr, RegexString, "tagcontent", false);
                string[] _tempstr = JumbotDLS.Utils.Strings.GetRegValue(_pagestr, RegexString, "tempstr", false);
                if (_tagcontent.Length > 0)//标签存在
                {
                    string _loopbody = string.Empty;
                    string _replacestr = string.Empty;
                    string _viewstr = string.Empty;
                    string _tablename = string.Empty;
                    for (int i = 0; i < _tagcontent.Length; i++)
                    {
                        _loopbody = "<jdls:recordloop " + _tagcontent[i] + ">" + _tempstr[i] + "</jdls:recordloop>";
                        string _tagetypename = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "typename");
                        string _tagpagesize = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "pagesize");
                        if (_tagpagesize == "") _tagpagesize = "0";
                        string _tagwherestr = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "wherestr");
                        string _tagfields = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "fields");
                        string _tagorderfield = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "orderfield");
                        if (_tagorderfield == "") _tagorderfield = "id";
                        string _tagordertype = JumbotDLS.Utils.Strings.AttributeValue(_tagcontent[i], "ordertype");
                        if (_tagordertype == "") _tagordertype = "desc";

                        switch (_tagetypename.ToLower())
                        {
                            case "link":
                                _tablename = "jdls_normal_link";
                                break;
                            case "tag":
                                _tablename = "jdls_normal_tag";
                                break;
                            case "module":
                                _tablename = "jdls_module_article_module";
                                break;
                            case "modulefield":
                                _tablename = "jdls_module_article_module_field";
                                break;
                            case "fielditem":
                                _tablename = "jdls_module_article_module_field_item";
                                break;
                            case "special":
                                _tablename = "jdls_normal_special";
                                break;
                            case "vote":
                                _tablename = "jdls_extends_vote";
                                break;
                        }

                        string sql = "select";
                        if (_tagpagesize != "0")
                            sql += " top " + _tagpagesize;
                        sql += " " + _tagfields + " FROM [" + _tablename + "] WHERE (1=1";
                        if (_tagwherestr != "")
                            sql += " AND " + _tagwherestr.Replace("小于", "<").Replace("大于", ">").Replace("不等于", "<>");
                        sql += ")";
                        if (_tagorderfield.ToLower() != "rnd")
                        {
                            if (_tagorderfield.ToLower() != "id")
                                switch (_tagetypename.ToLower())
                                {
                                    case "link":
                                        sql += " ORDER BY " + _tagorderfield + " " + _tagordertype + ",channelid desc,id Desc";
                                        break;
                                    case "vote":
                                        sql += " ORDER BY " + _tagorderfield + " " + _tagordertype + ",channelid desc,id Desc";
                                        break;
                                    default:
                                        sql += " ORDER BY " + _tagorderfield + " " + _tagordertype + ",id Desc";
                                        break;
                                }

                            else
                                sql += " ORDER BY " + _tagorderfield + " " + _tagordertype;
                        }
                        else
                        {
                            sql += " ORDER BY " + ORDER_BY_RND();
                        }
                        _doh.Reset();
                        _doh.SqlCmd = sql;
                        DataTable _dt = _doh.GetDataTable();
                        StringBuilder sb = new StringBuilder();
                        string _TemplateContent = _tempstr[i].Replace("<#foreach>", "<#foreach collection=\"${" + _tagetypename.ToLower() + "s}\" var=\"field\" index=\"i\">");
                        JumbotDLS.TEngine.TemplateManager manager = JumbotDLS.TEngine.TemplateManager.FromString(_TemplateContent);
                        switch (_tagetypename.ToLower())
                        {
                            case "link":
                                List<Normal_Link> links = (new Normal_Links()).DT2List(_dt);
                                manager.SetValue("links", links);
                                break;
                            case "tag":
                                List<Normal_Tag> tags = (new Normal_Tags()).DT2List(_dt);
                                manager.SetValue("tags", tags);
                                break;
                            case "module":
                                List<Module> modules = (new Modules()).DT2List(_dt);
                                manager.SetValue("modules", modules);
                                break;
                            case "modulefield":
                                List<ModuleField> modulefields = (new ModuleFields()).DT2List(_dt);
                                manager.SetValue("modulefields", modulefields);
                                break;
                            case "fielditem":
                                List<Normal_FieldItem> fielditems = (new Normal_FieldItems()).DT2List(_dt);
                                manager.SetValue("fielditems", fielditems);
                                break;
                            case "special":
                                List<Normal_Special> specials = (new Normal_Specials()).DT2List(_dt);
                                manager.SetValue("specials", specials);
                                break;
                            case "vote":
                                List<Extends_Vote> votes = new JumbotDLS.DAL.Extends_VoteDAL().GetVotes(_dt);
                                manager.SetValue("votes", votes);
                                break;

                        }
                        string _content = manager.Process();
                        _pagestr = _pagestr.Replace(_loopbody, _content);
                        _dt.Clear();
                        _dt.Dispose();
                    }
                }
            }

        }

        /// <summary>
        /// 处理最后的内容
        /// </summary>
        /// <param name="_pagestr"></param>
        public void ExcuteLastHTML(ref string _pagestr)
        {
            replaceTag_Script(ref _pagestr);
            replaceTag_NoShow(ref _pagestr);
        }
        /// <summary>
        /// 处理最后的内容并生成页面
        /// </summary>
        /// <param name="_pagestr"></param>
        /// <param name="_filepath"></param>
        /// <param name="noBom"></param>
        public void SaveHTML(string _pagestr, string _filepath, bool noBom)
        {
            ExcuteLastHTML(ref _pagestr);
            JumbotDLS.Utils.DirFile.SaveFile(_pagestr, _filepath, noBom);
        }
        public void SaveHTML(string _pagestr, string _filepath)
        {
            SaveHTML(_pagestr, _filepath, true);
        }
        #region 生成静态页面
        /// <summary>
        /// 生成首页文件
        /// </summary>

        public bool CreateDefaultFile()
        {
            string _pagestr = new JumbotDLS.DAL.Common(true).ExecuteSHTMLTags(GetSiteDefaultPage());
            JumbotDLS.Utils.DirFile.SaveFile(_pagestr, "~/" + "index" + site.StaticExt);
            return true;
        }
        /// <summary>
        /// 生成频道首页(频道ID只能从外部传入，不支持跨频道)
        /// </summary>
        public void CreateChannelFile(int _currentpage)
        {
            string _pagestr = GetSiteChannelPage(_currentpage);
            JumbotDLS.Utils.DirFile.SaveFile(_pagestr, Go2Channel(_currentpage, true, this.MainChannel.Id, true));
        }
        #endregion
        /// <summary>
        /// 获得首页内容
        /// </summary>
        /// <returns></returns>
        public string GetSiteDefaultPage()
        {
            string pId = string.Empty;
            string _pagestr = string.Empty;
            //得到首页的缺省模板：方案组ID/主题ID/模板内容
            JumbotDLS.DAL.Normal_TemplateDAL dal = new JumbotDLS.DAL.Normal_TemplateDAL();
            dal.GetTemplateContent("0", 1, ref pId, ref _pagestr);
            this.IsHtml = site.IsHtml;
            this.PageNav = site.Name + "&nbsp;&raquo;&nbsp;首页";
            this.PageTitle = site.Name + " - " + site.Description + site.TitleTail;
            this.PageKeywords = site.Keywords;
            this.PageDescription = site.Description;
            ReplacePublicTag(ref _pagestr);
            ReplaceChannelClassLoopTag(ref _pagestr);
            ReplaceContentLoopTag(ref _pagestr);
            ExcuteLastHTML(ref _pagestr);
            return JoinEndHTML(_pagestr);
        }
        #region 获得频道页内容
        /// <summary>
        /// 获得页面内容
        /// </summary>
        /// <param name="_templatefile"></param>
        /// <returns></returns>
        public string GetPageHTML(string _templatefile, Dictionary<string, string> _param)
        {
            string _pagestr = JumbotDLS.Utils.DirFile.ReadFile("~/themes/" + _templatefile);
            if (_param != null && _param.Count > 0)
            {
                foreach (KeyValuePair<string, string> a in _param)
                {
                    _pagestr = _pagestr.Replace("{$ThisPage." + a.Key.ToLower() + "}", a.Value);
                }
            }
            ReplacePublicTag(ref _pagestr);
            /*再执行一次，目的是解析include文件里标签*/
            if (_param != null && _param.Count > 0)
            {
                foreach (KeyValuePair<string, string> a in _param)
                {
                    _pagestr = _pagestr.Replace("{$ThisPage." + a.Key.ToLower() + "}", a.Value);
                }
            }
            ReplaceChannelTag(ref _pagestr, this.MainChannel.Id);
            ReplaceChannelClassLoopTag(ref _pagestr);
            ReplaceContentLoopTag(ref _pagestr);
            ExcuteLastHTML(ref _pagestr);
            return JoinEndHTML(_pagestr);
        }
        /// <summary>
        /// 获得频道页内容(频道ID只能从外部传入，不支持跨频道)
        /// </summary>
        /// <returns></returns>
        public string GetSiteChannelPage(int _currentpage)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                string pStr = " [IsPass]=1 AND [ParentID]=0 AND [ChannelId]=" + this.MainChannel.Id;
                _doh.ConditionExpress = pStr;
                int _totalcount = _doh.Count("jdls_module_" + this.MainChannel.Type);
                int _pagecount = JumbotDLS.Utils.Int.PageCount(_totalcount, this.MainChannel.PageSize);
                System.Collections.ArrayList ContentList = getChannelSinglePage(_totalcount, _pagecount, _currentpage);
                string _pagestr = ContentList[0].ToString();
                if (ContentList.Count > 2)
                {
                    string ViewStr = ContentList[1].ToString();
                    _pagestr = _pagestr.Replace(ViewStr, ContentList[2].ToString());
                }
                _pagestr = _pagestr.Replace("{$_getPageBarHTML}",
                    getPageBar(this.MainChannel.LanguageCode == "en" ? 5 : 4, "html", 2, _totalcount, this.MainChannel.PageSize, _currentpage, Go2Channel(1, this.MainChannel.IsHtml, this.MainChannel.Id.ToString(), false), Go2Channel(-1, this.MainChannel.IsHtml, this.MainChannel.Id.ToString(), false), Go2Channel(-1, false, this.MainChannel.Id.ToString(), false), site.CreatePages)
                    );
                ExcuteLastHTML(ref _pagestr);
                return JoinEndHTML(_pagestr);
            }
        }
        private System.Collections.ArrayList getChannelSinglePage(int _totalcount, int _pagecount, int _page)
        {
            string pId = string.Empty;
            string _pagestr = string.Empty;

            //得到模板方案组ID/模板内容
            new JumbotDLS.DAL.Normal_TemplateDAL().GetTemplateContent(this.MainChannel.ThemeId.ToString(), 1, ref pId, ref _pagestr);
            this.PageNav = "<a href=\"" + site.Home + "\" class=\"home\"><span>" + (string)this.Lang["home"] + "</span></a> / " + this.MainChannel.Title;
            this.PageTitle = this.MainChannel.Title + "_" + site.Name + site.TitleTail;
            this.PageKeywords = site.Keywords;
            this.PageDescription = JumbotDLS.Utils.Strings.SimpleLineSummary(this.MainChannel.Info);
            ReplacePublicTag(ref _pagestr);
            ReplacePublicTag(ref _pagestr);
            ReplaceChannelTag(ref _pagestr, this.MainChannel.Id);
            ReplaceChannelClassLoopTag(ref _pagestr);
            ReplaceContentLoopTag(ref _pagestr);
            System.Collections.ArrayList ContentList = new System.Collections.ArrayList();
            ContentList.Add(_pagestr);
            getChannelSinglePageListBody(ref ContentList, _totalcount, _pagecount, _page);

            return ContentList;
        }
        private void getChannelSinglePageListBody(ref System.Collections.ArrayList ContentList, int _totalcount, int _pagecount, int _page)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                string whereStr = string.Empty;
                string _pagestr = ContentList[0].ToString();
                whereStr = " [IsPass]=1 AND [ParentID]=0 AND [ChannelId]=" + this.MainChannel.Id;
                int _pagesize = (this.MainChannel.PageSize < 1) ? 20 : this.MainChannel.PageSize;

                System.Collections.ArrayList TagArray = JumbotDLS.Utils.Strings.GetHtmls(_pagestr, "{$jdls:channel(", "{$/jdls:channel}", false, false);
                if (TagArray.Count > 0)//标签存在
                {
                    string LoopBody = string.Empty;
                    string TempStr = string.Empty;
                    string FiledsStr = string.Empty;
                    int StartTag, EndTag;

                    StartTag = TagArray[0].ToString().IndexOf(")}", 0);
                    FiledsStr = TagArray[0].ToString().Substring(0, StartTag).ToLower();
                    if (!("," + FiledsStr + ",").Contains(",adddate,")) FiledsStr += ",adddate";
                    EndTag = TagArray[0].ToString().Length;
                    LoopBody = "{$jdls:channel(" + TagArray[0].ToString() + "{$/jdls:channel}";
                    TempStr = TagArray[0].ToString().Substring(StartTag + 2, EndTag - StartTag - 2).Replace("<#foreach>", "<#foreach collection=\"${contents}\" var=\"field\" index=\"i\">");//需要循环的部分
                    ContentList.Add(LoopBody);

                    if (_pagecount > 0)
                    {
                        if (_page == 0)
                        {
                            for (int i = 1; i < _pagecount + 1; i++)
                            {
                                NameValueCollection orders = new NameValueCollection();
                                orders.Add("AddDate", "desc");
                                orders.Add("Id", "desc");
                                string FieldList = "Id,ChannelId,ClassId,[IsPass],[FirstPage],[OutUrl]," + FiledsStr;
                                string wStr = JumbotDLS.Utils.SqlHelp.GetSql1(FieldList, "jdls_module_" + this.MainChannel.Type, _totalcount, _pagesize, i, orders, whereStr);
                                if (site.SiteDataSize > 300000)
                                    wStr = JumbotDLS.Utils.SqlHelp.GetSql0(FieldList, "jdls_module_" + this.MainChannel.Type, "id", _pagesize, i, "desc", whereStr);

                                _doh.Reset();
                                _doh.SqlCmd = wStr;
                                DataTable dtContent = _doh.GetDataTable();


                                ContentList.Add(operateContentTag(this.MainChannel.Type, dtContent, TempStr.Replace("{$TotalCount}", _totalcount.ToString())));
                                dtContent.Clear();
                                dtContent.Dispose();

                            }
                        }
                        else
                        {
                            _page = _page == 0 ? 1 : _page;
                            NameValueCollection orders = new NameValueCollection();
                            orders.Add("AddDate", "desc");
                            orders.Add("Id", "desc");
                            string FieldList = "Id,ChannelId,ClassId,[IsPass],[FirstPage],[OutUrl]," + FiledsStr;
                            string wStr = JumbotDLS.Utils.SqlHelp.GetSql1(FieldList, "jdls_module_" + this.MainChannel.Type, _totalcount, _pagesize, _page, orders, whereStr);
                            if (site.SiteDataSize > 300000)
                                wStr = JumbotDLS.Utils.SqlHelp.GetSql0(FieldList, "jdls_module_" + this.MainChannel.Type, "id", _pagesize, _page, "desc", whereStr);

                            _doh.Reset();
                            _doh.SqlCmd = wStr;
                            DataTable dtContent = _doh.GetDataTable();
                            ContentList.Add(operateContentTag(this.MainChannel.Type, dtContent, TempStr.Replace("{$TotalCount}", _totalcount.ToString())));
                            dtContent.Clear();
                            dtContent.Dispose();
                        }
                    }
                    else
                        ContentList.Add("  ");
                }
            }

        }
        #endregion
        #region 获得栏目页内容
        /// <summary>
        /// 获得栏目页内容(频道ID只能从外部传入,频道ID不能为0)
        /// </summary>
        /// <param name="_classid"></param>
        /// <param name="_currentpage"></param>
        /// <returns></returns>
        public string GetSiteClassPage(string _classid, int _currentpage)
        {
            Normal_Class _class = new JumbotDLS.DAL.Normal_ClassDAL().GetEntity(_classid, "a.[IsOut]=0 AND a.[ChannelId]=" + this.MainChannel.Id);
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                string pStr = " [ClassId] in (Select id FROM [jdls_normal_class] WHERE [IsOut]=0 AND [Code] LIKE '" + _class.Code + "%') and [IsPass]=1 AND [ParentID]=0 AND [ChannelId]=" + this.MainChannel.Id;
                _doh.ConditionExpress = pStr;
                int _totalcount = _doh.Count("jdls_module_" + this.MainChannel.Type);
                int _pagecount = JumbotDLS.Utils.Int.PageCount(_totalcount, _class.PageSize);
                System.Collections.ArrayList ContentList = getClassSinglePage(_class, _totalcount, _pagecount, _currentpage);
                string _pagestr = ContentList[0].ToString();
                if (ContentList.Count > 2)
                {
                    string ViewStr = ContentList[1].ToString();
                    _pagestr = _pagestr.Replace(ViewStr, ContentList[2].ToString());
                }
                _pagestr = _pagestr.Replace("{$_getPageBarHTML}",
                    getPageBar(this.MainChannel.LanguageCode == "en" ? 5 : 4, "html", 2, _totalcount, _class.PageSize, _currentpage, Go2Class(1, this.MainChannel.IsHtml, this.MainChannel.Id.ToString(), _classid, false), Go2Class(-1, this.MainChannel.IsHtml, this.MainChannel.Id.ToString(), _classid, false), Go2Class(-1, false, this.MainChannel.Id.ToString(), _classid, false), site.CreatePages)
                    );
                ExcuteLastHTML(ref _pagestr);
                return JoinEndHTML(_pagestr);
            }
        }

        private System.Collections.ArrayList getClassSinglePage(Normal_Class _class, int _totalcount, int _pagecount, int _page)
        {
            string pId = string.Empty;
            string _pagestr = string.Empty;

            //得到模板方案组ID/模板内容
            new JumbotDLS.DAL.Normal_TemplateDAL().GetTemplateContent(_class.ThemeId, _class.IsLastClass, ref pId, ref _pagestr);
            if (site.SiteDataSize > 10000)
                this.PageNav = "<script type=\"text/javascript\" src=\"" + site.Dir + this.MainChannel.Dir + "/js/classnav_" + _class.Id + ".js\"></script>";
            else
                this.PageNav = ClassFullNavigateHtml(this.MainChannel.Id, _class.Id);
            if (this.MainChannel.IsTop)
                this.PageTitle = _class.Title + "_" + this.MainChannel.Title + "_" + site.Name + site.TitleTail;
            else
                this.PageTitle = _class.Title + "_" + site.Name + site.TitleTail;
            if (_class.Keywords == "")
                this.PageKeywords = site.Keywords;
            else
                this.PageKeywords = _class.Keywords;
            this.PageDescription = JumbotDLS.Utils.Strings.SimpleLineSummary(_class.Info);
            ReplacePublicTag(ref _pagestr);
            ReplaceChannelTag(ref _pagestr, this.MainChannel.Id);
            _pagestr = _pagestr.Replace("{$ChannelClassId}", _class.Id);
            _pagestr = _pagestr.Replace("{$ChannelClassCodeLeft4}", _class.CodeLeft4);
            _pagestr = _pagestr.Replace("{$ChannelClassParentId}", _class.ParentId.ToString());
            ReplaceChannelClassLoopTag(ref _pagestr);
            ReplaceClassTag(ref _pagestr, _class.Id);
            ReplaceContentLoopTag(ref _pagestr);
            System.Collections.ArrayList ContentList = new System.Collections.ArrayList();
            ContentList.Add(_pagestr);
            getClassSinglePageListBody(_class, ref ContentList, _totalcount, _pagecount, _page);

            return ContentList;
        }
        private void getClassSinglePageListBody(Normal_Class _class, ref System.Collections.ArrayList ContentList, int _totalcount, int _pagecount, int _page)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                string whereStr = string.Empty;
                string _pagestr = ContentList[0].ToString();
                whereStr = " [ClassId] in (SELECT ID FROM [jdls_normal_class] WHERE [IsOut]=0 AND [Code] LIKE '" + _class.Code + "%')";
                whereStr += " AND [IsPass]=1 AND [ParentID]=0 AND [ChannelId]=" + this.MainChannel.Id;

                int _pagesize = (_class.PageSize < 1) ? 20 : _class.PageSize;

                System.Collections.ArrayList TagArray = JumbotDLS.Utils.Strings.GetHtmls(_pagestr, "{$jdls:class(", "{$/jdls:class}", false, false);
                if (TagArray.Count > 0)//标签存在
                {
                    string LoopBody = string.Empty;
                    string TempStr = string.Empty;
                    string FiledsStr = string.Empty;
                    int StartTag, EndTag;

                    StartTag = TagArray[0].ToString().IndexOf(")}", 0);
                    FiledsStr = TagArray[0].ToString().Substring(0, StartTag).ToLower();
                    if (!("," + FiledsStr + ",").Contains(",adddate,")) FiledsStr += ",adddate";
                    EndTag = TagArray[0].ToString().Length;
                    LoopBody = "{$jdls:class(" + TagArray[0].ToString() + "{$/jdls:class}";
                    TempStr = TagArray[0].ToString().Substring(StartTag + 2, EndTag - StartTag - 2).Replace("<#foreach>", "<#foreach collection=\"${contents}\" var=\"field\" index=\"i\">");//需要循环的部分
                    ContentList.Add(LoopBody);

                    if (_pagecount > 0)
                    {
                        if (_page == 0)
                        {
                            for (int i = 1; i < _pagecount + 1; i++)
                            {
                                NameValueCollection orders = new NameValueCollection();
                                orders.Add("AddDate", "desc");
                                orders.Add("Id", "desc");
                                string FieldList = "Id,ChannelId,ClassId,[IsPass],[FirstPage],[OutUrl]," + FiledsStr;
                                string wStr = JumbotDLS.Utils.SqlHelp.GetSql1(FieldList, "jdls_module_" + this.MainChannel.Type, _totalcount, _pagesize, i, orders, whereStr);
                                if (site.SiteDataSize > 300000)
                                    wStr = JumbotDLS.Utils.SqlHelp.GetSql0(FieldList, "jdls_module_" + this.MainChannel.Type, "id", _pagesize, i, "desc", whereStr);

                                _doh.Reset();
                                _doh.SqlCmd = wStr;
                                DataTable dtContent = _doh.GetDataTable();
                                ContentList.Add(operateContentTag(this.MainChannel.Type, dtContent, TempStr.Replace("{$TotalCount}", _totalcount.ToString())));
                                dtContent.Clear();
                                dtContent.Dispose();

                            }
                        }
                        else
                        {
                            _page = _page == 0 ? 1 : _page;
                            NameValueCollection orders = new NameValueCollection();
                            orders.Add("AddDate", "desc");
                            orders.Add("Id", "desc");
                            string FieldList = "Id,ChannelId,ClassId,[IsPass],[FirstPage],[OutUrl]," + FiledsStr;
                            string wStr = JumbotDLS.Utils.SqlHelp.GetSql1(FieldList, "jdls_module_" + this.MainChannel.Type, _totalcount, _pagesize, _page, orders, whereStr);
                            if (site.SiteDataSize > 300000)
                                wStr = JumbotDLS.Utils.SqlHelp.GetSql0(FieldList, "jdls_module_" + this.MainChannel.Type, "id", _pagesize, _page, "desc", whereStr);

                            _doh.Reset();
                            _doh.SqlCmd = wStr;
                            DataTable dtContent = _doh.GetDataTable();
                            ContentList.Add(operateContentTag(this.MainChannel.Type, dtContent, TempStr.Replace("{$TotalCount}", _totalcount.ToString())));
                            dtContent.Clear();
                            dtContent.Dispose();
                        }
                    }
                    else
                        ContentList.Add("  ");
                }
            }

        }

        /// <summary>
        /// 获得更多页内容
        /// </summary>
        /// <param name="_channelid"></param>
        /// <param name="_classid"></param>
        /// <param name="_currentpage"></param>
        /// <param name="_pagesize"></param>
        /// <param name="_wherestr"></param>
        /// <param name="_templatefile"></param>
        /// <param name="_scriptfile"></param>
        /// <returns></returns>

        public string GetSiteListPage(string _channelid, string _classid, int _currentpage, int _pagesize, string _wherestr, string _templatefile, string _scriptfile)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                string whereStr = string.Empty;
                whereStr += "[IsPass]=1 AND [ParentID]=0 AND [ChannelId]=" + this.MainChannel.Id;
                this.PageNav = "<a href=\"" + site.Home + "\" class=\"home\"><span>" + (string)this.Lang["home"] + "</span></a> / " + this.MainChannel.Title;
                this.PageTitle = this.MainChannel.Title + "_" + site.Name + site.TitleTail;
                if (_classid != "0")
                {
                    Normal_Class _class = new JumbotDLS.DAL.Normal_ClassDAL().GetEntity(_classid, "a.[IsOut]=0 AND a.[ChannelId]=" + this.MainChannel.Id);
                    whereStr = " [ClassId] in (SELECT ID FROM [jdls_normal_class] WHERE [IsOut]=0 AND [Code] LIKE '" + _class.Code + "%')";
                    if (_pagesize == 0)
                        _pagesize = (_class.PageSize < 1) ? 20 : _class.PageSize;
                    if (site.SiteDataSize > 10000)
                        this.PageNav = "<script type=\"text/javascript\" src=\"" + site.Dir + this.MainChannel.Dir + "/js/classnav_" + _class.Id + ".js\"></script>";
                    else
                        this.PageNav = ClassFullNavigateHtml(this.MainChannel.Id, _class.Id);
                    if (this.MainChannel.IsTop)
                        this.PageTitle = _class.Title + "_" + this.MainChannel.Title + "_" + site.Name + site.TitleTail;
                    else
                        this.PageTitle = _class.Title + "_" + site.Name + site.TitleTail;
                }
                if (_pagesize == 0) _pagesize = 20;
                if (_wherestr != "")
                    whereStr += " and " + _wherestr;
                _doh.Reset();
                _doh.ConditionExpress = whereStr;
                int _totalcount = _doh.Count("jdls_module_" + this.MainChannel.Type);
                int _pagecount = JumbotDLS.Utils.Int.PageCount(_totalcount, _pagesize);
                string _pagestr = JumbotDLS.Utils.DirFile.ReadFile("~/themes/" + _templatefile);
                this.PageKeywords = site.Keywords;

                ReplacePublicTag(ref _pagestr);
                ReplaceChannelTag(ref _pagestr, this.MainChannel.Id);
                _pagestr = _pagestr.Replace("{$ChannelClassId}", _classid);
                ReplaceChannelClassLoopTag(ref _pagestr);
                if (_classid != "0")
                    ReplaceClassTag(ref _pagestr, _classid);
                ReplaceContentLoopTag(ref _pagestr);
                System.Collections.ArrayList ContentList = new System.Collections.ArrayList();
                ContentList.Add(_pagestr);

                System.Collections.ArrayList TagArray = JumbotDLS.Utils.Strings.GetHtmls(_pagestr, "{$jdls:class(", "{$/jdls:class}", false, false);
                if (TagArray.Count > 0)//标签存在
                {
                    string LoopBody = string.Empty;
                    string TempStr = string.Empty;
                    string FiledsStr = string.Empty;
                    int StartTag, EndTag;

                    StartTag = TagArray[0].ToString().IndexOf(")}", 0);
                    FiledsStr = TagArray[0].ToString().Substring(0, StartTag).ToLower();
                    if (!("," + FiledsStr + ",").Contains(",adddate,")) FiledsStr += ",adddate";
                    EndTag = TagArray[0].ToString().Length;
                    LoopBody = "{$jdls:class(" + TagArray[0].ToString() + "{$/jdls:class}";
                    TempStr = TagArray[0].ToString().Substring(StartTag + 2, EndTag - StartTag - 2).Replace("<#foreach>", "<#foreach collection=\"${contents}\" var=\"field\" index=\"i\">");//需要循环的部分
                    ContentList.Add(LoopBody);

                    if (_pagecount > 0)
                    {
                        if (_currentpage == 0)
                        {
                            for (int i = 1; i < _pagecount + 1; i++)
                            {
                                NameValueCollection orders = new NameValueCollection();
                                orders.Add("AddDate", "desc");
                                orders.Add("Id", "desc");
                                string FieldList = "Id,ChannelId,ClassId,[IsPass],[FirstPage],[OutUrl]," + FiledsStr;
                                string wStr = JumbotDLS.Utils.SqlHelp.GetSql1(FieldList, "jdls_module_" + this.MainChannel.Type, _totalcount, _pagesize, i, orders, whereStr);
                                if (site.SiteDataSize > 300000)
                                    wStr = JumbotDLS.Utils.SqlHelp.GetSql0(FieldList, "jdls_module_" + this.MainChannel.Type, "id", _pagesize, i, "desc", whereStr);

                                _doh.Reset();
                                _doh.SqlCmd = wStr;
                                DataTable dtContent = _doh.GetDataTable();
                                ContentList.Add(operateContentTag(this.MainChannel.Type, dtContent, TempStr.Replace("{$TotalCount}", _totalcount.ToString())));
                                dtContent.Clear();
                                dtContent.Dispose();

                            }
                        }
                        else
                        {
                            _currentpage = _currentpage == 0 ? 1 : _currentpage;
                            NameValueCollection orders = new NameValueCollection();
                            orders.Add("AddDate", "desc");
                            orders.Add("Id", "desc");
                            string FieldList = "Id,ChannelId,ClassId,[IsPass],[FirstPage],[OutUrl]," + FiledsStr;
                            string wStr = JumbotDLS.Utils.SqlHelp.GetSql1(FieldList, "jdls_module_" + this.MainChannel.Type, _totalcount, _pagesize, _currentpage, orders, whereStr);
                            if (site.SiteDataSize > 300000)
                                wStr = JumbotDLS.Utils.SqlHelp.GetSql0(FieldList, "jdls_module_" + this.MainChannel.Type, "id", _pagesize, _currentpage, "desc", whereStr);

                            _doh.Reset();
                            _doh.SqlCmd = wStr;
                            DataTable dtContent = _doh.GetDataTable();
                            ContentList.Add(operateContentTag(this.MainChannel.Type, dtContent, TempStr.Replace("{$TotalCount}", _totalcount.ToString())));
                            dtContent.Clear();
                            dtContent.Dispose();
                        }
                    }
                    else
                        ContentList.Add("  ");
                }
                if (ContentList.Count > 2)
                {
                    string ViewStr = ContentList[1].ToString();
                    _pagestr = _pagestr.Replace(ViewStr, ContentList[2].ToString());
                }
                _scriptfile = string.Format(_scriptfile, _channelid, _classid);
                _pagestr = _pagestr.Replace("{$_getPageBarHTML}",
                    getPageBar(this.MainChannel.LanguageCode == "en" ? 5 : 4, "js", 2, _totalcount, _pagesize, _currentpage, _scriptfile, _scriptfile, _scriptfile, 0)
                    );
                return _pagestr;
            }
        }
        #endregion
        /// <summary>
        /// 处理内容标签(频道ID不固定，所以不能直接继承本类channel)
        /// </summary>
        /// <param name="_channeltype">唯一模型 要么article 要么soft</param>
        ///  <param name="_dt">获得的数据表</param>
        /// <param name="_tempstr">循环模版</param>
        /// <returns></returns>
        private string operateContentTag(string _channeltype, DataTable _dt, string _tempstr)
        {
            string _replacestr = _tempstr;
            _replacestr = _replacestr.Replace("$_{title}", "<#formattitle title=\"${field.title}\" />");
            _replacestr = _replacestr.Replace("$_{contentlink}", "<#contentlink channelid=\"${field.channelid}\" contentid=\"${field.id}\" outurl=\"${field.outurl}\" contenturl=\"${field.firstpage}\" />");

            _replacestr = _replacestr.Replace("$_{img}", "<#imgurl sitedir=\"" + site.Dir + "\"  isimg=\"${field.isimg}\" img=\"${field.img}\" />");
            _replacestr = _replacestr.Replace("$_{classname}", "<#classname classid=\"${field.classid}\" />");
            _replacestr = _replacestr.Replace("$_{classlink}", "<#classlink channelid=\"${field.channelid}\" channelishtml=\"${field.channelishtml}\" classid=\"${field.classid}\" />");
            _replacestr = _replacestr.Replace("$_{channelname}", "<#channelname channelid=\"${field.channelid}\" />");
            _replacestr = _replacestr.Replace("$_{channellink}", "<#channellink channelid=\"${field.channelid}\" channelishtml=\"${field.channelishtml}\" />");
            _replacestr = _replacestr.Replace("$_{viewnum}", "<#viewnum sitedir=\"" + site.Dir + "\" channeltype=\"" + _channeltype + "\" channelid=\"${field.channelid}\" contentid=\"${field.id}\" />");

            string _TemplateContent = _replacestr;
            JumbotDLS.TEngine.TemplateManager manager = JumbotDLS.TEngine.TemplateManager.FromString(_TemplateContent);
            string _content = "";
            manager.RegisterCustomTag("contentlink", new TemplateTag_GetContentLink());
            manager.RegisterCustomTag("formattitle", new TemplateTag_GetFormatTitle());
            manager.RegisterCustomTag("imgurl", new TemplateTag_GetImgurl());
            manager.RegisterCustomTag("classname", new TemplateTag_GetClassName());
            manager.RegisterCustomTag("classlink", new TemplateTag_GetClassLink());
            manager.RegisterCustomTag("channelname", new TemplateTag_GetChannelName());
            manager.RegisterCustomTag("channellink", new TemplateTag_GetChannelLink());
            manager.RegisterCustomTag("cutstring", new TemplateTag_GetCutstring());
            manager.RegisterCustomTag("viewnum", new TemplateTag_GetViewnum());
            manager.SetValue("contents", (new JumbotDLS.Entity.Module_Articles()).DT2List(_dt));
            manager.SetValue("site", site);
            _content = manager.Process();
            return _content;
        }
    }
}