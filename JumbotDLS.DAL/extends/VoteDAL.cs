﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.Entity;
using JumbotDLS.DBUtility;

namespace JumbotDLS.DAL
{
    /// <summary>
    /// 友情链接插件
    /// </summary>
    public class Extends_VoteDAL : Common
    {
        public Extends_VoteDAL()
        {
            base.SetupSystemDate();
        }
        /// <summary>
        /// 得到列表
        /// </summary>
        public List<Extends_Vote> GetVotes(DataTable dtVote)
        {
            List<Extends_Vote> votes = new List<Extends_Vote>();
            for (int i = 0; i < dtVote.Rows.Count; i++)
            {
                Extends_Vote vote = new Extends_Vote();
                vote.Id = dtVote.Rows[i]["Id"].ToString();
                vote.Title = dtVote.Rows[i]["Title"].ToString();
                vote.VoteTotal = Str2Int(dtVote.Rows[i]["VoteTotal"].ToString());
                string[] itemtext = dtVote.Rows[i]["VoteText"].ToString().Split('|');
                string[] itemclicks = dtVote.Rows[i]["VoteNum"].ToString().Split('|');
                List<Extends_VoteItem> voteitems = new List<Extends_VoteItem>();
                for (int m = 0; m < itemtext.Length; m++)
                {
                    voteitems.Add(new Extends_VoteItem(itemtext[m], Str2Int(itemclicks[m])));
                }
                vote.Item = voteitems;
                vote.Type = Str2Int(dtVote.Rows[i]["Type"].ToString());
                votes.Add(vote);
            }
            return votes;
        }
        public Extends_Vote GetVote()
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                Extends_Vote vote = new Extends_Vote();
                _doh.Reset();
                _doh.SqlCmd = "SELECT TOP 1 [Id],[Title],[VoteText],[VoteNum],[VoteTotal],[Type] FROM [jdls_extends_vote] WHERE [Lock]=0 ORDER BY Id Desc";
                DataTable dtVote = _doh.GetDataTable();
                if (dtVote.Rows.Count > 0)
                {
                    vote.Id = dtVote.Rows[0]["Id"].ToString();
                    vote.Title = dtVote.Rows[0]["Title"].ToString();
                    vote.VoteTotal = Str2Int(dtVote.Rows[0]["VoteTotal"].ToString());
                    string[] itemtext = dtVote.Rows[0]["VoteText"].ToString().Split('|');
                    string[] itemclicks = dtVote.Rows[0]["VoteNum"].ToString().Split('|');
                    List<Extends_VoteItem> voteitems = new List<Extends_VoteItem>();
                    for (int i = 0; i < itemtext.Length; i++)
                    {
                        voteitems.Add(new Extends_VoteItem(itemtext[i], Str2Int(itemclicks[i])));
                    }
                    vote.Item = voteitems;
                    vote.Type = Str2Int(dtVote.Rows[0]["Type"].ToString());
                }
                else
                    vote.Id = "0";
                dtVote.Clear();
                dtVote.Dispose();
                return vote;
            }
        }
    }
}
