﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.Entity;
using JumbotDLS.DBUtility;

namespace JumbotDLS.DAL
{
    /// <summary>
    /// 友情链接插件
    /// </summary>
    public class Extends_QQOnlineDAL : Common
    {
        public Extends_QQOnlineDAL()
        {
            base.SetupSystemDate();
        }
        /// <summary>
        /// 得到列表
        /// </summary>
        public List<Extends_QQOnline> QQOnlineList()
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                List<Extends_QQOnline> qqonlines;
                qqonlines = new List<Extends_QQOnline>();
                _doh.Reset();
                _doh.SqlCmd = "SELECT * FROM [jdls_extends_qqonline] Where State=1 ORDER BY OrderNum Desc,Id Desc";
                DataTable dtQQOnline = _doh.GetDataTable();
                if (dtQQOnline.Rows.Count > 0)
                {
                    for (int i = 0; i < dtQQOnline.Rows.Count; i++)
                    {
                        qqonlines.Add(new Extends_QQOnline(dtQQOnline.Rows[i]["Id"].ToString(),
                            dtQQOnline.Rows[i]["QQID"].ToString(),
                            dtQQOnline.Rows[i]["Title"].ToString(),
                            dtQQOnline.Rows[i]["TColor"].ToString(),
                            dtQQOnline.Rows[i]["face"].ToString()
                            ));
                    }
                }
                dtQQOnline.Clear();
                dtQQOnline.Dispose();
                return qqonlines;
            }
        }
    }
}
