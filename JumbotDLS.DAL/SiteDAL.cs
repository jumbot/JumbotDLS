﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.DBUtility;
using JumbotDLS.Common;

namespace JumbotDLS.DAL
{
    /// <summary>
    /// 网站参数
    /// </summary>
    public class SiteDAL
    {
        public SiteDAL()
        { }
        /// <summary>
        /// 获得网站参数
        /// </summary>
        /// <returns></returns>
        public JumbotDLS.Entity.Site GetEntity()
        {
            JumbotDLS.Entity.Site eSite = new JumbotDLS.Entity.Site();
            eSite.Name = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "Name");
            eSite.Name2 = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "Name2");
            eSite.Url = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "Url");
            if (eSite.Url == "")
                eSite.Url = JumbotDLS.Utils.App.Url;
            else
                eSite.Url = eSite.Url.TrimEnd('/');
            //if (eSite.Url.EndsWith("/"))
            //    eSite.Url = eSite.Url.Substring(0, eSite.Url.Length - 1);
            eSite.Dir = JumbotDLS.Utils.App.Path;
            eSite.ICP = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "ICP");
            eSite.SiteID = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "SiteID");
            eSite.Keywords = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "Keywords");
            eSite.Description = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "Description");
            eSite.AllowReg = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "AllowReg") == "1";
            eSite.CheckReg = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "CheckReg") == "1";
            eSite.IsHtml = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "IsHtml") == "1";
            eSite.StaticExt = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "StaticExt").ToLower();
            #region 网站首页
            string TempUrl = JumbotDLS.Common.PageFormat.Site(eSite.Dir, eSite.IsHtml);
            TempUrl = TempUrl.Replace("<#SiteDir#>", eSite.Dir);
            TempUrl = TempUrl.Replace("<#SiteStaticExt#>", eSite.StaticExt);
            eSite.Home = TempUrl;
            #endregion
            eSite.TitleTail = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "TitleTail");
            eSite.AdminGroupId = JumbotDLS.Utils.Validator.StrToInt(JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "AdminGroupId"), 5);
            eSite.CookieDomain = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "CookieDomain");
            eSite.CookiePath = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "CookiePath");
            eSite.CookiePrev = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "CookiePrev");
            eSite.CookieKeyCode = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "CookieKeyCode");
            eSite.MainDomain = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "MainDomain");
            eSite.UrlReWriter = (JumbotDLS.Utils.Validator.StrToInt(JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "UrlReWriter"), 0) == 1);
            eSite.ExecuteSql = (JumbotDLS.Utils.Validator.StrToInt(JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "ExecuteSql"), 0) == 1);
            eSite.CreatePages = JumbotDLS.Utils.Validator.StrToInt(JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "CreatePages"), 20);
            eSite.ForumAPIKey = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/discuz", "ForumAPIKey");
            eSite.ForumUrl = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/discuz", "ForumUrl");
            eSite.ForumIP = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/discuz", "ForumIP");
            eSite.ForumAutoRegister = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/discuz", "ForumAutoRegister") == "1";

            eSite.DebugKey = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "DebugKey");
            if (eSite.DebugKey.Length == 0) eSite.DebugKey = "1111-2222-3333-4444";
            eSite.MailOnceCount = JumbotDLS.Utils.Validator.StrToInt(JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "MailOnceCount"), 15);
            eSite.MailTimeCycle = JumbotDLS.Utils.Validator.StrToInt(JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "MailTimeCycle"), 300);
            eSite.MailPrivateKey = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "MailPrivateKey");
            eSite.AdminCheckUserState = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "AdminCheckUserState") == "1";//(add:2011-03-07)
            eSite.MainSite = (JumbotDLS.Utils.Validator.StrToInt(JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "MainSite"), 0) == 1);
            eSite.WanSite = (JumbotDLS.Utils.Validator.StrToInt(JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "WanSite"), 0) == 1);
            eSite.ProductMaxBuyCount = JumbotDLS.Utils.Validator.StrToInt(JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "ProductMaxBuyCount"), 20);
            eSite.ProductMaxCartCount = JumbotDLS.Utils.Validator.StrToInt(JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "ProductMaxCartCount"), 20);
            eSite.ProductMaxOrderCount = JumbotDLS.Utils.Validator.StrToInt(JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "ProductMaxOrderCount"), 5);
            eSite.ProductPaymentUsingPoints = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "ProductPaymentUsingPoints") == "1";
            eSite.PassportTheme = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "PassportTheme");
            eSite.SiteDataSize = JumbotDLS.Utils.Validator.StrToInt(JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "SiteDataSize"), 10000);
            eSite.SiteStartYear = JumbotDLS.Utils.Validator.StrToInt(JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "SiteStartYear"), 2007);

            eSite.ComputerCode = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "ComputerCode");
            eSite.CompanyName = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "CompanyName");
            eSite.ValidDate = JumbotDLS.Utils.Validator.StrToInt(JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "ValidDate"), 20200101);
            eSite.ValidateCode = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "ValidateCode");
            eSite.SiteIsGenuine = (eSite.ComputerCode == JumbotDLS.Utils.SoftReg.GetMNum()) && (eSite.ValidateCode == JumbotDLS.Utils.SoftReg.GetRNum(eSite.ComputerCode, eSite.ValidDate, eSite.CompanyName));
            
            return eSite;
        }
        public void CreateSiteFiles()
        {
            JumbotDLS.Entity.Site site = GetEntity();
            //生成配置文件
            string TempStr = string.Empty;
            TempStr = "var site = new Object();\r\n" +
                "site.Name = '" + site.Name + "';\r\n" +
                "site.Name2 = '" + site.Name2 + "';\r\n" +
                "site.Url = '" + site.Url + "';\r\n" +
                "site.SiteID = '" + site.SiteID + "';\r\n" +
                "site.Dir = '" + site.Dir + "';\r\n" +
                "site.CookieDomain = '" + site.CookieDomain + "';\r\n" +
                "site.CookiePrev = '" + site.CookiePrev + "';\r\n" +
                "site.AllowReg = " + site.AllowReg.ToString().ToLower() + ";\r\n" +
                "site.CheckReg = " + site.CheckReg.ToString().ToLower() + ";\r\n";
            if (site.MainDomain.Length > 0)
            {
                TempStr += "document.domain='" + site.MainDomain + "';\r\n";
            }
            string _globalJS = JumbotDLS.Utils.DirFile.ReadFile("~/_data/global.js");
            string _strBegin = "//<!--网站参数begin";
            string _strEnd = "//-->网站参数end";
            System.Collections.ArrayList TagArray = JumbotDLS.Utils.Strings.GetHtmls(_globalJS, _strBegin, _strEnd, true, true);
            if (TagArray.Count > 0)//标签存在
            {
                _globalJS = _globalJS.Replace(TagArray[0].ToString(), _strBegin + "\r\n\r\n" + TempStr + "\r\n\r\n" + _strEnd);
            }
            JumbotDLS.Utils.DirFile.SaveFile(_globalJS, "~/_data/global.js");
        }
    }
}
