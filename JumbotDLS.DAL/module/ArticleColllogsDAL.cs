﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.DBUtility;

namespace JumbotDLS.DAL
{
    /// <summary>
    /// 采集日志表信息
    /// </summary>
    public class Module_ArticleColllogsDAL : Common
    {
        public Module_ArticleColllogsDAL()
        {
            base.SetupSystemDate();
        }
        /// <summary>
        /// 得到列表JSON数据
        /// </summary>
        /// <param name="_thispage">当前页码</param>
        /// <param name="_pagesize">每页记录条数</param>
        /// <param name="_joinstr">关联条件</param>
        /// <param name="_wherestr1">外围条件(带A.)</param>
        /// <param name="_wherestr2">分页条件(不带A.)</param>
        /// <param name="_jsonstr">返回值</param>
        public void GetListJSON(int _thispage, int _pagesize, string _joinstr, string _wherestr1, string _wherestr2, ref string _jsonstr)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.ConditionExpress = _wherestr2;
                string sqlStr = "";
                int _totalcount = _doh.Count("jdls_module_article_colllogs");
                sqlStr = JumbotDLS.Utils.SqlHelp.GetSql0("A.*,B.[Title] as ItemName", "jdls_module_article_colllogs", "jdls_module_article_collitem", "Id", _pagesize, _thispage, "desc", _joinstr, _wherestr1, _wherestr2);
                _doh.Reset();
                _doh.SqlCmd = sqlStr;
                DataTable dt = _doh.GetDataTable();
                _jsonstr = "{\"result\" :\"1\"," +
                    "\"returnval\" :\"操作成功\"," +
                    "\"pagerbar\" :\"" + JumbotDLS.Utils.PageBar.GetPageBar(3, "js", 2, _totalcount, _pagesize, _thispage, "javascript:ajaxList($#page#$);") + "\"," +
                    JumbotDLS.Utils.dtHelper.DT2JSON(dt, (_pagesize * (_thispage - 1))) +
                    "}";
                dt.Clear();
                dt.Dispose();
            }
        }
        /// <summary>
        /// 清空日志
        /// </summary>
        public void DeleteLogs()
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.ConditionExpress = "1=1";
                _doh.Delete("jdls_module_article_colllogs");
            }
        }
    }
}
