﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI;
using JumbotDLS.Utils;
using JumbotDLS.DBUtility;

namespace JumbotDLS.DAL
{
    public interface IModule
    {
        /// <summary>
        /// 得到内容页地址
        /// </summary>
        /// <param name="_page"></param>
        /// <param name="_ishtml"></param>
        /// <param name="_channelid"></param>
        /// <param name="_contentid"></param>
        /// <returns></returns>
        string GetContentLink(int _page, bool _ishtml, string _channelid, string _contentid, bool _truefile);
        /// <summary>
        /// 生成内容页
        /// </summary>
        /// <param name="_ChannelId"></param>
        /// <param name="_ContentId"></param>
        /// <param name="_CurrentPage"></param>
        void CreateContent(string _ChannelId, string _ContentId, int _CurrentPage);
        /// <summary>
        /// 得到内容页
        /// </summary>
        /// <param name="_ChannelId"></param>
        /// <param name="_ContentId"></param>
        /// <param name="_CurrentPage"></param>
        string GetContent(string _ChannelId, string _ContentId, int _CurrentPage);
        /// <summary>
        /// 删除内容页
        /// </summary>
        /// <param name="_ChannelId"></param>
        /// <param name="_ContentId"></param>
        void DeleteContent(string _ChannelId, string _ContentId);
    }
}
