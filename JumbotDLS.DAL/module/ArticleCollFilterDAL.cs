﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI;
using JumbotDLS.Utils;
using JumbotDLS.DBUtility;

namespace JumbotDLS.DAL
{
    public class Module_ArticleCollFilterDAL : Common
    {
        public Module_ArticleCollFilterDAL()
        {
            base.SetupSystemDate();
        }
        public string FilterBody(string PageBody, DataTable dtCollItemFilters)
        {
            for (int i = 0; i < dtCollItemFilters.Rows.Count; i++)
            {
                string fId = dtCollItemFilters.Rows[i]["Id"].ToString();
                string fType = dtCollItemFilters.Rows[i]["Filter_Type"].ToString();
                string fContent = dtCollItemFilters.Rows[i]["Filter_Content"].ToString();
                string fsString = dtCollItemFilters.Rows[i]["FisString"].ToString();
                string foString = dtCollItemFilters.Rows[i]["FioString"].ToString();
                string fReplace = dtCollItemFilters.Rows[i]["Filter_Rep"].ToString();
                if (fType == "0")//简单替换
                    PageBody = PageBody.Replace(fContent, fReplace);
                else
                {
                    JumbotDLS.Common.NewsCollection nc = new JumbotDLS.Common.NewsCollection();
                    System.Collections.ArrayList replaceArray = nc.GetArray(PageBody, fsString, foString);
                    if (replaceArray.Count == 0)
                        continue;
                    if (replaceArray[0].ToString() == "$StartFalse$" || replaceArray[0].ToString() == "$EndFalse$" || replaceArray[0].ToString() == "$NoneBody$")
                        continue;
                    for (int j = 0; j < replaceArray.Count; j++)
                    {
                        PageBody = PageBody.Replace(replaceArray[j].ToString(), fReplace);
                    }
                }
            }
            return PageBody;
        }
        /// <summary>
        /// 过滤内容
        /// </summary>
        /// <param name="doh"></param>
        /// <param name="PageBody">原文内容</param>
        /// <param name="ItemId">项目ID</param>
        /// <param name="FilterObject">Title或Body</param>
        /// <returns></returns>
        public string FilterBody(string PageBody, string ItemId, string FilterObject)
        {
            DataTable dtCollItemFilters = GetFilterDT(ItemId, FilterObject);
            PageBody = FilterBody(PageBody, dtCollItemFilters);
            dtCollItemFilters.Clear();
            dtCollItemFilters.Dispose();
            return PageBody;
        }
        /// <summary>
        /// 或得过滤规则表
        /// </summary>
        /// <param name="ItemId">项目ID</param>
        /// <param name="FilterObject">title表示标题，body表示内容</param>
        /// <returns></returns>
        public DataTable GetFilterDT(string ItemId, string FilterObject)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                string sqlStr = "Select * FROM [jdls_module_article_collfilters] WHERE ([ItemId]=" + ItemId + " OR [PublicTf]=1)";
                if (FilterObject.ToLower() == "title")
                    sqlStr += " AND [Filter_Object]=0";
                else
                    sqlStr += " AND [Filter_Object]=1";
                sqlStr += " AND [Flag]=1 ORDER BY Id";
                _doh.Reset();
                _doh.SqlCmd = sqlStr;
                return _doh.GetDataTable();
            }
        }
    }
}
