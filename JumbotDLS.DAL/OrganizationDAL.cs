﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.DBUtility;

namespace JumbotDLS.DAL
{
    /// <summary>
    /// 机构表信息
    /// </summary>
    public class Normal_OrganizationDAL : Common
    {
        public Normal_OrganizationDAL()
        {
            base.SetupSystemDate();
        }
        /// <summary>
        /// 是否存在记录
        /// </summary>
        /// <param name="_wherestr">条件</param>
        /// <returns></returns>
        public bool Exists(string _wherestr)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                int _ext = 0;
                _doh.Reset();
                _doh.ConditionExpress = _wherestr;
                if (_doh.Exist("jdls_organization"))
                    _ext = 1;
                return (_ext == 1);
            }

        }
        /// <summary>
        /// 判断重复性(标题是否存在)
        /// </summary>
        /// <param name="_title">需要检索的标题</param>
        /// <param name="_id">除外的ID</param>
        /// <param name="_wherestr">其他条件</param>
        /// <returns></returns>
        public bool ExistTitle(string _title, string _id, string _wherestr)
        {
            int _ext = 0;
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.ConditionExpress = "title=@title and id<>" + _id;
                if (_wherestr != "") _doh.ConditionExpress += " and " + _wherestr;
                _doh.AddConditionParameter("@title", _title);
                if (_doh.Exist("jdls_organization"))
                    _ext = 1;
            }
            return (_ext == 1);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteByID(string _id)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.ConditionExpress = "id=@id";
                _doh.AddConditionParameter("@id", _id);
                int _del = _doh.Delete("jdls_organization");
                return (_del == 1);
            }

        }
        /// <summary>
        /// 绑定记录至频道实体
        /// </summary>
        /// <param name="_id"></param>
        public JumbotDLS.Entity.Normal_Organization GetEntity(DataRow dr)
        {
            JumbotDLS.Entity.Normal_Organization organization = new JumbotDLS.Entity.Normal_Organization();
            organization.Id = dr["Id"].ToString();
            organization.Title = dr["Title"].ToString();
            organization.Info = dr["Info"].ToString();
            organization.Logo = dr["Logo"].ToString();
            organization.ErWei = dr["ErWei"].ToString() == "" ? site.Dir + "plus/qrcode.aspx" : dr["ErWei"].ToString();
            organization.Address = dr["Address"].ToString();
            organization.ZipCode = dr["ZipCode"].ToString();
            organization.Phone = dr["Phone"].ToString();
            organization.Email = dr["Email"].ToString();
            organization.pId = Validator.StrToInt(dr["pId"].ToString(), 0);
            organization.Enabled = Validator.StrToInt(dr["Enabled"].ToString(), 0) == 1;
            organization.TotalCount = Validator.StrToInt(dr["TotalCount"].ToString(), 0);
            organization.UsedCount = Validator.StrToInt(dr["UsedCount"].ToString(), 0);
            organization.DefaultYYbao = Validator.StrToInt(dr["DefaultYYbao"].ToString(), 0);
            organization.BeginTime = Validator.StrToDate(dr["BeginTime"].ToString(), DateTime.Now);
            organization.EndTime = Validator.StrToDate(dr["EndTime"].ToString(), DateTime.Now);
            return organization;
        }
        /// <summary>
        /// 获得单页内容的单条记录实体
        /// </summary>
        /// <param name="_id"></param>
        public JumbotDLS.Entity.Normal_Organization GetEntity(string _id)
        {

            using (DbOperHandler _doh = new Common().Doh())
            {
                JumbotDLS.Entity.Normal_Organization organization = new JumbotDLS.Entity.Normal_Organization();
                _doh.Reset();
                _doh.SqlCmd = "SELECT * FROM [jdls_organization] WHERE [Id]=" + _id;
                DataTable dt = _doh.GetDataTable();
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    organization = GetEntity(dr);
                }
                else
                {
                    organization.Id = "0";
                    organization.Title = site.Name;
                }
                dt.Clear();
                dt.Dispose();
                return organization;
            }

        }
        /// <summary>
        /// 解析频道标签
        /// </summary>
        /// <param name="pagestr">原内容</param>
        /// <param name="_organizationid">OrganizationId不能为0</param>
        public void ExecuteTags(ref string PageStr, string _organizationid)
        {
            JumbotDLS.Entity.Normal_Organization _Organization = GetEntity(_organizationid);
            ExecuteTags(ref PageStr, _Organization);
        }
        public void ExecuteTags(ref string PageStr, JumbotDLS.Entity.Normal_Organization _Organization)
        {
            PageStr = PageStr.Replace("{$OrganizationId}", _Organization.Id.ToString());
            PageStr = PageStr.Replace("{$OrganizationName}", _Organization.Title);
            PageStr = PageStr.Replace("{$OrganizationInfo}", _Organization.Info);
            PageStr = PageStr.Replace("{$OrganizationLogo}", _Organization.Logo);
            PageStr = PageStr.Replace("{$OrganizationErWei}", _Organization.ErWei);
            PageStr = PageStr.Replace("{$OrganizationAddress}", _Organization.Address);
            PageStr = PageStr.Replace("{$OrganizationZipCode}", _Organization.ZipCode);
            PageStr = PageStr.Replace("{$OrganizationPhone}", _Organization.Phone);
            PageStr = PageStr.Replace("{$OrganizationEmail}", _Organization.Email);
        }
    }
}
