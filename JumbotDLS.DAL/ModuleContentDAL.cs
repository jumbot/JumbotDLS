﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.DBUtility;

namespace JumbotDLS.DAL
{
    /// <summary>
    /// 模型内容业务类
    /// </summary>
    public class ModuleContentDAL : Common
    {
        public ModuleContentDAL()
        {
            base.SetupSystemDate();
        }
        /// <summary>
        /// 获得内容的某些属性(第一个是时间，第二个是内容页另名)
        /// </summary>
        /// <param name="_channelid">频道ID</param>
        /// <param name="_channeltype">频道模型</param>
        /// <param name="_contentid">内容ID</param>
        /// <returns></returns>
        public object[] GetSome(string _channelid, string _channeltype, string _contentid)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.ConditionExpress = "ChannelId=" + _channelid + " and Id=" + _contentid;
                return _doh.GetFields("jdls_module_" + _channeltype, "AddDate,FirstPage,AliasPage");
            }
        }
        /// <summary>
        /// 获得默认缩略图尺寸
        /// </summary>
        /// <param name="_module"></param>
        /// <param name="iWidth"></param>
        /// <param name="iHeight"></param>
        /// <returns></returns>
        public bool GetThumbsSize(string _module, ref int iWidth, ref int iHeight)
        {
            using (DbOperHandler _doh = new Common().Doh())
            {
                iWidth = 0;
                iHeight = 0;
                _doh.Reset();
                _doh.SqlCmd = "select iWidth,iHeight from [jdls_normal_thumbs] where id =(select DefaultThumbs from [jdls_module_article_module] where [Type]='" + _module + "')";
                DataTable dtThumbs = _doh.GetDataTable();
                if (dtThumbs.Rows.Count == 1)
                {
                    iWidth = Str2Int(dtThumbs.Rows[0]["iWidth"].ToString());
                    iHeight = Str2Int(dtThumbs.Rows[0]["iHeight"].ToString());
                }
                dtThumbs.Clear();
                dtThumbs.Dispose();
                return true;
            }
        }
    }
}
