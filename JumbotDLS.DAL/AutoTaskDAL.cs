﻿

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.DBUtility;
using System.Data.SqlClient;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
namespace JumbotDLS.Common
{
    /// <summary>
    /// 自动任务
    /// </summary>
    public class AutoTaskDAL
    {
        public static string SiteName = "某某站";
        public static string WebRoot = System.Configuration.ConfigurationManager.AppSettings["JumbotDLS:WebRoot"];
        public static string CryptoPassword = "THIS1IS2CRYPTO3PASSWORD";//文件加密密码，记得别瞎搞

        public static Thread Thread_Archive_Fulltext = null;
        #region common
        /// <summary>
        /// 将c# DateTime时间格式转换为Unix时间戳格式
        /// </summary>
        /// <param name="time">时间</param>
        /// <returns>long</returns>
        public static long ConvertDateTimeInt(System.DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0));
            long t = (time.Ticks - startTime.Ticks) / 10000;            //除10000调整为13位
            return t;
        }
        /// <summary>
        /// 读取Config参数
        /// </summary>
        public static string ReadConfig(string name, string key)
        {
            System.Xml.XmlDocument XmlDoc = new System.Xml.XmlDocument();
            string _file = (WebRoot + name + ".config").Replace("\\\\", "\\");
            XmlDoc.Load(_file);
            System.Xml.XmlNodeList xnl = XmlDoc.GetElementsByTagName(key);
            if (xnl.Count == 0)
                return "";
            else
            {
                System.Xml.XmlNode mNode = xnl[0];
                return mNode.InnerText;
            }
        }
        /// <summary>
        /// 保存Config参数
        /// </summary>
        public static void UpdateConfig(string name, string key, string nValue)
        {
            if (ReadConfig(name, key) != "")
            {
                string _file = (WebRoot + name + ".config").Replace("\\\\", "\\");
                try
                {
                    System.Xml.XmlDocument XmlDoc = new System.Xml.XmlDocument();
                    XmlDoc.Load(_file);
                    System.Xml.XmlNodeList elemList = XmlDoc.GetElementsByTagName(key);
                    System.Xml.XmlNode mNode = elemList[0];
                    mNode.InnerText = nValue;
                    System.Xml.XmlTextWriter xw = new System.Xml.XmlTextWriter(new System.IO.StreamWriter(_file));
                    xw.Formatting = System.Xml.Formatting.Indented;
                    XmlDoc.WriteTo(xw);
                    xw.Close();
                }
                catch { }
            }
        }
        public static SqlConnection _conn
        {
            get
            {
                string dbConnStr = ReadConfig("\\_data\\config\\conn", "dbConnStr");
                return new SqlConnection(dbConnStr);
            }
        }
        #endregion

        #region 全文归档
        /// <summary>
        /// 全文归档
        /// </summary>
        /// <returns></returns>
        public static void Archive_Fulltext()
        {
            int _repeatcount = 0;
            while (true)
            {
                if (_repeatcount < 10000)
                    _repeatcount++;
                else
                    _repeatcount = 0;
                Thread.Sleep(1 * 1000);//做1秒钟的准备工作
                string _time2 = System.DateTime.Now.ToString("HH:mm:ss");
                System.Diagnostics.Stopwatch oTime = new System.Diagnostics.Stopwatch();
                oTime.Start(); //记录起始时间
                using (DbOperHandler _doh = new SqlDbOperHandler(_conn))
                {
                    try
                    {
                        _doh.Reset();
                        _doh.SqlCmd = "select TOP 50 id,FulltextUrl FROM [jdls_module_article] where archived=0 order by newid()";
                        DataTable dt = _doh.GetDataTable();
                        if (dt.Rows.Count < 1)
                        {
                            oTime.Stop();   //记录结束时间
                            Thread.Sleep(60 * 1000);
                        }
                        else
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                #region 循环开始

                                DataRow dr = dt.Rows[i];
                                string _id = dr["id"].ToString();
                                string _inFile = dr["FulltextUrl"].ToString();
                                string _inFile_physics = (WebRoot + _inFile).Replace("/", "\\").Replace("\\\\", "\\");
                                string _outFile = "/uploads_archived/" + JumbotDLS.Utils.MD5.Upper32(_id).Substring(0, 2) + "/" + JumbotDLS.Utils.MD5.Upper32(_id) + System.IO.Path.GetExtension(_inFile) + ".mimi";
                                string _outFile_physics = (WebRoot + _outFile).Replace("/", "\\").Replace("\\\\", "\\");
                                var mimiDir = _outFile_physics.Substring(0, _outFile_physics.LastIndexOf("\\") + 1);
                                if (!System.IO.Directory.Exists(mimiDir))
                                {
                                    System.IO.Directory.CreateDirectory(mimiDir);
                                }
                                if (System.IO.File.Exists(_outFile_physics))
                                {
                                    System.IO.File.Delete(_outFile_physics);
                                }
                                bool _Encrypt = JumbotDLS.Utils.CryptoHelp.EncryptFile(_inFile_physics, _outFile_physics, CryptoPassword);

                                if (_Encrypt)
                                {
                                    _doh.Reset();
                                    _doh.ConditionExpress = "id=" + _id;
                                    _doh.AddFieldItem("FulltextUrl", _outFile);
                                    _doh.AddFieldItem("archived", 1);
                                    _doh.Update("jdls_module_article");
                                }
                                else
                                {
                                    _doh.Reset();
                                    _doh.ConditionExpress = "id=" + _id;
                                    _doh.AddFieldItem("archived", -1);
                                    _doh.Update("jdls_module_article");
                                }
                                Thread.Sleep(500);//先休息一下
                                #endregion
                            }
                        }
                        dt.Clear();
                        dt.Dispose();
                    }
                    catch (Exception ex)
                    {
                        JumbotDLS.Utils.Logs.Error("Archive_Fulltext", "" + ex);
                    }
                    oTime.Stop();   //记录结束时间
                    Thread.Sleep(2 * 1000);

                }

            }
        }
        #endregion
    }
}
