﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.DBUtility;

namespace JumbotDLS.DAL
{
    /// <summary>
    /// 管理员表信息
    /// </summary>
    public class AdminDAL : Common
    {
        public AdminDAL()
        {
            base.SetupSystemDate();
        }

        public JumbotDLS.Entity.Admin GetEntity(string _adminid)
        {
            JumbotDLS.Entity.Admin admin = new JumbotDLS.Entity.Admin();
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.SqlCmd = "SELECT * FROM [View_Admin] WHERE [AdminId]=" + _adminid;
                DataTable dt = _doh.GetDataTable();
                if (dt.Rows.Count > 0)
                {
                    admin.AdminId = Str2Int(dt.Rows[0]["AdminId"].ToString(), 0);
                    admin.AdminName = dt.Rows[0]["AdminName"].ToString();
                    admin.AdminPass = dt.Rows[0]["AdminPass"].ToString();
                    admin.AdminSign = dt.Rows[0]["AdminSign"].ToString();
                    admin.AdminSetting = dt.Rows[0]["AdminSetting"].ToString();
                    admin.LastTime2 = Validator.StrToDate(dt.Rows[0]["LastTime2"].ToString(), DateTime.Now);
                    admin.LastIP2 = dt.Rows[0]["LastIP2"].ToString();
                    admin.Cookiess = dt.Rows[0]["Cookiess"].ToString();
                    admin.AdminState = Str2Int(dt.Rows[0]["AdminState"].ToString(), 0);
                }
            }
            return admin;
        }
        /// <summary>
        /// 验证管理员登录
        /// </summary>
        /// <param name="_adminname">登录名</param>
        /// <param name="_adminpass">32位MD5密码</param>
        /// <returns></returns>
        public string ChkAdminLogin(string _adminname, string _adminpass, int iExpires)
        {
            _adminname = _adminname.Replace("\'", "");
            string _adminpass2 = JumbotDLS.Utils.MD5.Last64(_adminpass);
            bool _cmsisold = false;//如果检测密码小于64位就认定为旧系统
            using (DbOperHandler _doh = new Common().Doh())
            {
                if (_adminname.Length == 36)
                {
                    _doh.Reset();
                    _doh.ConditionExpress = "[AdminOpenid]='" + _adminname + "'  and adminstate=1";
                }
                else
                {
                    _doh.Reset();
                    _doh.ConditionExpress = "adminname=@adminname and adminstate=1";
                    _doh.AddConditionParameter("@adminname", _adminname);
                }
                string _adminid = _doh.GetField("jdls_normal_user", "adminid").ToString();
                if (_adminid != "0" && _adminid != "")
                {
                    JumbotDLS.Entity.Admin _Admin = GetEntity(_adminid);
                    if (_adminname.Length < 36)
                    {
                        if (_Admin.AdminPass.Length < 64)
                        {
                            _cmsisold = true;
                            if (_Admin.AdminPass.ToLower() != _adminpass)
                            {
                                return "密码错误";
                            }
                        }
                        else
                        {
                            if (_Admin.AdminPass.ToLower() != _adminpass2)
                            {
                                return "密码错误";
                            }
                        }
                    }
                    string _adminCookiess = "c" + (new Random().Next(10000000, 99999999)).ToString();
                    //设置Cookies
                    System.Collections.Specialized.NameValueCollection myCol = new System.Collections.Specialized.NameValueCollection();
                    myCol.Add("id", _adminid);
                    myCol.Add("name", _Admin.AdminName);
                    myCol.Add("cookiess", _adminCookiess);
                    JumbotDLS.Utils.Cookie.SetObj(site.CookiePrev + "admin", iExpires, myCol, site.CookieDomain, site.CookiePath);

                    //更新管理员登陆信息
                    _doh.Reset();
                    _doh.ConditionExpress = "adminid=@adminid and adminstate=1";
                    _doh.AddConditionParameter("@adminid", _adminid);
                    _doh.AddFieldItem("Cookiess", _adminCookiess);
                    _doh.AddFieldItem("LastTime2", DateTime.Now.ToString());
                    _doh.AddFieldItem("LastIP2", IPHelp.ClientIP);
                    _doh.AddFieldItem("AdminSign", Guid.NewGuid().ToString().Replace("-", ""));//登录后赋值一个32位的字符串
                    if (_cmsisold)
                        _doh.AddFieldItem("AdminPass", _adminpass2);
                    _doh.Update("jdls_normal_user");
                    //_doh.Dispose();
                    return "ok";
                }
                else
                {
                    //_doh.Dispose();
                    return "帐号不存在";
                }
            }

        }
        /// <summary>
        /// 管理员退出登录
        /// </summary>
        public void ChkAdminLogout()
        {
            if (JumbotDLS.Utils.Cookie.GetValue(site.CookiePrev + "admin") != null)
            {
                JumbotDLS.Utils.Cookie.Del(site.CookiePrev + "admin", site.CookieDomain, site.CookiePath);
            }
        }
        /// <summary>
        /// 判断adminsign是否正确
        /// </summary>
        /// <param name="_adminid"></param>
        /// <param name="_adminsign">长度一定是32位</param>
        /// <returns></returns>
        public bool ChkAdminSign(string _adminid, string _adminsign)
        {
            if (_adminsign.Length < 32 || _adminid == "")
            {
                return false;
            }
            using (DbOperHandler _doh = new Common().Doh())
            {
                _doh.Reset();
                _doh.ConditionExpress = "adminid=@adminid and adminsign=@adminsign and adminstate=1";
                _doh.AddConditionParameter("@adminid", _adminid);
                _doh.AddConditionParameter("@adminsign", _adminsign);
                return (_doh.Exist("jdls_normal_user"));
            }
        }
    }
}
