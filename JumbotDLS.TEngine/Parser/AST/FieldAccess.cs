﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

#region Using directives

using System;
using System.Collections.Generic;
using System.Text;

#endregion

namespace JumbotDLS.TEngine.Parser.AST
{
	public class FieldAccess : Expression
	{
		Expression exp;
		string field;

		public FieldAccess(int line, int col, Expression exp, string field)
			:base(line, col)
		{
			this.exp = exp;
			this.field = field;
		}

		public Expression Exp
		{
			get { return this.exp; }
		}

		public string Field
		{
			get { return this.field; }
		}

	}
}
