﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

#region Using directives

using System;
using System.Collections.Generic;
using System.Text;

#endregion

namespace JumbotDLS.TEngine.Parser.AST
{
	public class Text : Element
	{
		string data;

		public Text(int line, int col, string data)
			:base(line, col)
		{
			this.data = data;
		}

		public string Data
		{
			get { return this.data; }
		}
	}
}
