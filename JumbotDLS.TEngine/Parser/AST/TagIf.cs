﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

#region Using directives

using System;
using System.Collections.Generic;
using System.Text;

#endregion

namespace JumbotDLS.TEngine.Parser.AST
{


	public class TagIf : Tag
	{
		Tag falseBranch;
		Expression test;

		public TagIf(int line, int col, Expression test)
			:base(line, col, "if")
		{
			this.test = test;
		}

		public Tag FalseBranch
		{
			get { return this.falseBranch; }
			set { this.falseBranch = value; }
		}

		public Expression Test
		{
			get { return test; }
		}
	}
}
