﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

#region Using directives

using System;
using System.Collections.Generic;
using System.Text;

#endregion

namespace JumbotDLS.TEngine.Parser.AST
{
	public class MethodCall : Expression
	{
		string name;
		Expression obj;
		Expression[] args;

		public MethodCall(int line, int col, Expression obj, string name, Expression[] args)
			:base(line, col)
		{
			this.name = name;
			this.args = args;
			this.obj = obj;
		}

		public Expression CallObject
		{
			get { return this.obj; }
		}

		public Expression[] Args
		{
			get { return this.args; }
		}

		public string Name
		{
			get { return this.name; }
		}
	}
}
