﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

#region Using directives

using System;
using System.Collections.Generic;
using System.Text;

#endregion

namespace JumbotDLS.TEngine.Parser.AST
{
	public class StringExpression : Expression
	{
		List<Expression> exps;

		public StringExpression(int line, int col)
			:base(line, col)
		{
			exps = new List<Expression>();
		}

		public int ExpCount
		{
			get { return exps.Count; }
		}

		public void Add(Expression exp)
		{
			exps.Add(exp);
		}

		public Expression this[int index]
		{
			get { return exps[index]; }
		}

		public IEnumerable<Expression> Expressions
		{
			get
			{
				for (int i = 0; i < exps.Count; i++)
					yield return exps[i];
			}
		}
	}
}
