﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

#region Using directives

using System;
using System.Collections.Generic;
using System.Text;

#endregion

namespace JumbotDLS.TEngine.Parser.AST
{
	public class TagAttribute
	{
		string name;
		Expression expression;

		public TagAttribute(string name, Expression expression)
		{
			this.name = name;
			this.expression = expression;
		}

		public Expression Expression
		{
			get { return this.expression; }
		}

		public string Name
		{
			get { return this.name; }
		}
	}
}
