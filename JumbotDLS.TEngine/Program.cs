﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

#region Using directives

using System;
using System.Collections.Generic;
using System.Text;

#endregion
using JumbotDLS.TEngine.Parser;

namespace TemplateEngine
{
    class Program
    {
        static void Main(string[] args)
        {
        Start:
            Console.Write("> ");
            string data = Console.ReadLine();

            TemplateLexer lexer = new TemplateLexer(data);
            do
            {
                Token token = lexer.Next();
                Console.WriteLine("{0} ({1}, {2}): {3}", token.TokenKind.ToString(), token.Line, token.Col
                    , token.Data);
                if (token.TokenKind == TokenKind.EOF)
                    break;
            } while (true);

            goto Start;


        }
    }
}
