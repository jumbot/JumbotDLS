﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace JumbotDLS.TEngine
{
    public static class Util
    {
        static object syncObject = new object();

        static Regex regExVarName;

        public static bool ToBool(object obj)
        {
            if (obj is bool)
                return (bool)obj;
            else if (obj is string)
            {
                string str = (string)obj;
                if (string.Compare(str, "true", true) == 0)
                    return true;
                else if (string.Compare(str, "yes", true) == 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public static bool IsValidVariableName(string name)
        {
            return RegExVarName.IsMatch(name);
        }

        private static Regex RegExVarName
        {
            get
            {
                if ((regExVarName == null))
                {
                    System.Threading.Monitor.Enter(syncObject);
                    if (regExVarName == null)
                    {
                        try
                        {
                            regExVarName = new Regex("^[a-zA-Z_][a-zA-Z0-9_]*$", RegexOptions.Compiled);
                        }
                        finally
                        {
                            System.Threading.Monitor.Exit(syncObject);
                        }
                    }
                }

                return regExVarName;
            }
        }
    }
}
