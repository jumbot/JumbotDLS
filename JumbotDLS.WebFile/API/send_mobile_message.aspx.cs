﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JumbotDLS.WebFile.API
{
    public partial class send_mobile_message:JumbotDLS.UI.FrontHtml
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string _username = q("username");
            string _mobile = q("mobile");
            string _t = q("t");
            string _sn = JumbotDLS.Utils.MD5.Upper32(_mobile + "_" + _t);
            if (q("sn") != _sn)
            {
                Response.Write(JsonResult(0, "签名失败"));
                Response.End();
            }

            List<string> phoneNumbers = new List<string>();
            phoneNumbers.Add(_mobile);
            bool _send_success = false;
            if (JumbotDLS.Common.SMS.SendCloudSMS.NewTask(_username, _mobile))
                _send_success = true;
            else
                _send_success = false;
            if (_send_success)
                Response.Write(JsonResult(1,"通知成功"));
            else
                Response.Write(JsonResult(0, "通知失败"));
        }
    }
}