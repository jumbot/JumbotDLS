﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.API
{
    public partial class _submit : JumbotDLS.UI.UserCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            User_Load("", "json");
            string payWay = f("payway");
            int points = Str2Int(f("txtPoints"));
            string productName = f("txtProductName");
            string productDesc = f("txtProductDesc");
            string orderNum = JumbotDLS.Utils.Strings.FilterSymbol(f("txtOrderNum"));
            orderNum = new JumbotDLS.DAL.Normal_RechargeDAL().NewOrder(UserId, points, payWay, orderNum);//订单号
            Response.Write("<script>top.location.href='" + site.Dir + "api/" + payWay + "/default.aspx"
                + "?userid=" + UserId
                + "&payerName=" + System.Web.HttpUtility.UrlEncode(UserName)
                + "&orderNum=" + orderNum
                + "&orderAmount=" + points
                + "&productName=" + System.Web.HttpUtility.UrlEncode(productName)
                + "&productDesc=" + System.Web.HttpUtility.UrlEncode(productDesc)
                + "';</script>");
        }
    }
}