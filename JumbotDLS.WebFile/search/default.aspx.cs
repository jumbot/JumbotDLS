﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace JumbotDLS.WebFile.Search
{
    public partial class _default : JumbotDLS.UI.UserCenter
    {
        public string Keywords, Keywords_Fen, ChannelId, ClassId, Year, Publisher, Mode, SearchType = "all", SearchField = "", PageBarHTML = "";
        public int CurrentPage = 1, PageSize = 10, TotalCount = 0;
        public int AllCount1 = 0, AllCount2 = 0, AllCount3 = 0, AllCount4 = 0,AllCount5 = 0;
        public double EventTime = 0;
        public List<JumbotDLS.Utils.LuceneHelp.SearchItem> SearchResult = null;
        public Dictionary<string, int> channelAggregate = new Dictionary<string, int>();
        public Dictionary<string, int> classAggregate = new Dictionary<string, int>();
        public Dictionary<string, int> moduleAggregate = new Dictionary<string, int>();
        public Dictionary<string, int> yearAggregate = new Dictionary<string, int>();
        public Dictionary<string, int> publisherAggregate = new Dictionary<string, int>();
        public string UrlName;
        public string LeftMenuTitle1 = "全部", LeftMenuTitle2 = "全部", LeftMenuTitle3 = "全部", LeftMenuTitle4 = "全部", LeftMenuTitle5 = "全部", LeftMenuBody1, LeftMenuBody2, LeftMenuBody3, LeftMenuBody4 = "", LeftMenuBody5 = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 8;//脚本过期时间
            string[] path = Request.FilePath.Split(new Char[] { '/' });
            UrlName = path[path.Length - 1];

            GetLoginedInfo();
            CurrentPage = Int_ThisPage();
            string _switch = q("switch");
            Keywords = q("k").Trim();
            if (Keywords == "")
            {
                Response.Redirect("/");
                Response.End();
            }
            if (q("field").Trim() != "")
                SearchField = q("field").Trim();
            Mode = q("mode");
            Year = Str2Str(q("year"));
            ChannelId = Str2Str(q("channelid"));
            ClassId = Str2Str(q("classid"));
            Publisher = q("publisher");
            if (_switch == "equals")
            {
                doh.Reset();
                doh.ConditionExpress = "title=@title or title2=@title2";
                doh.AddConditionParameter("@title", Keywords);
                doh.AddConditionParameter("@title2", Keywords);
                object[] _value = doh.GetFields("jdls_module_article", "channelid,id");
                if (_value != null)
                {
                    Response.Redirect(Go2View(1, false, _value[0].ToString(), _value[1].ToString(), false));
                    Response.End();
                }
                else
                {
                    //Keywords = "\"" + Keywords + "\"";
                }
            }
            
            SearchType = q("type");
            if (!ModuleIsOK(SearchType))
                SearchType = "all";
            string ModuleList = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "ModuleList");
            string IndexType = (SearchType == "all") ? ModuleList : SearchType;

            int PSize = Str2Int(q("pagesize"), 20);
            if (Keywords.Length > 2)
                Keywords = System.Text.RegularExpressions.Regex.Replace(Keywords, "\\s{2,}", " ");
            Keywords_Fen = Keywords.Length < 2 ? Keywords : JumbotDLS.Utils.WordSpliter.GetKeyword(Keywords);//分词
            if (_switch != "tag")
            {
                if (Mode == "1") Keywords = Keywords_Fen;//自动分词
                //else
                //    Keywords = "\"" + Keywords + "\"";
            }
            else//表示统计标签检索
            {
                if (ModuleIsOK(SearchType))
                {
                    new JumbotDLS.DAL.Normal_TagDAL().AddClickTimes(ChannelId, Keywords);
                }
            }
            string _LasteKeywords = Keywords;//最后的关键词
            if(!JumbotDLS.Utils.Validator.IsChinese(Keywords)){//如果不是纯中文
                if (!_LasteKeywords.StartsWith("\""))
                    _LasteKeywords = "\"" + _LasteKeywords;
                if (!_LasteKeywords.EndsWith("\""))
                    _LasteKeywords = _LasteKeywords + "\"";
            }
            SearchResult = JumbotDLS.Utils.LuceneHelp.SearchIndex.Search(IndexType, SearchField,ChannelId, ClassId, Year, Publisher, _LasteKeywords, PSize, CurrentPage, out TotalCount, out EventTime);
            PageBarHTML = AutoPageBar(6, 4, TotalCount, PSize, CurrentPage);
            AllCount1 = JumbotDLS.Utils.LuceneHelp.SearchIndex.GetCount(ModuleList,SearchField, "0", "0", "0", Publisher, _LasteKeywords, "channelid", ref channelAggregate);//不按频道分组
            //if (ModuleIsOK(SearchType))
            //{
            //    doh.Reset();
            //    doh.ConditionExpress = "type=@type";
            //    doh.AddConditionParameter("@type", SearchType);
            //    LeftMenuTitle1 = doh.GetField("jdls_module_article_module", "Title").ToString();
            //}
            LeftMenuBody1 = "<dt id=\"channel0\" class=\"" + ("" == q("channelid") ? "active" : "") + "\"><i class=\"iconfont ic-tag\"></i><a href=\""+UrlName+"?channelid=0&publisher=" + Server.UrlEncode(Publisher) + "&k=" + Server.UrlEncode(Keywords) + "\"><em class=\"val\">" + LeftMenuTitle1 + "(" + AllCount1 + ")</em></a></dt>";
            doh.Reset();
            doh.SqlCmd = "SELECT ID,Title FROM [jdls_normal_channel] WHERE IsIndex=1 ORDER BY PID";
            DataTable dt = doh.GetDataTable();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string _channelid = dt.Rows[i]["ID"].ToString();
                string _channelname = dt.Rows[i]["Title"].ToString();
                Dictionary<string, int> typeAggregate = new Dictionary<string, int>();
                int _count1 = JumbotDLS.Utils.LuceneHelp.SearchIndex.GetCount(ModuleList, SearchField, _channelid, "0", "0", Publisher, _LasteKeywords, "", ref typeAggregate);
                if (_count1 > 0)
                    LeftMenuBody1 += "<dt id=\"channel" + _channelid + "\" class=\"" + (_channelid == q("channelid") ? "active" : "") + "\"><i class=\"iconfont ic-tag\"></i><a href=\"" + UrlName + "?channelid=" + _channelid + "&publisher=" + Server.UrlEncode(Publisher) + "&k=" + Server.UrlEncode(Keywords) + "\"><em class=\"val\">" + _channelname + "(" + _count1 + ")</em></a></dt>";
            }
            dt.Clear();
            dt.Dispose();
            AllCount2 = JumbotDLS.Utils.LuceneHelp.SearchIndex.GetCount(IndexType, SearchField, ChannelId, "0", "0", Publisher, _LasteKeywords, "year", ref yearAggregate);
            LeftMenuBody2 = "<dt id=\"year0\" class=\"" + ("" == q("year") ? "active" : "") + "\"><i class=\"iconfont ic-tag\"></i><a href=\"" + UrlName + "?year=&channelid=" + ChannelId + "&type=" + SearchType + "&field=" + SearchField + "&publisher=" + Server.UrlEncode(Publisher) + "&k=" + Server.UrlEncode(Keywords) + "\"><em class=\"val\">" + LeftMenuTitle2 + "(" + AllCount2 + ")</em></a></dt>";
            if (yearAggregate != null && yearAggregate.Count > 0)
            {
                JumbotDLS.Utils.dicHelp.Order(ref yearAggregate, 3);
                foreach (KeyValuePair<string, int> a in yearAggregate)
                {
                    if (a.Key != "")
                    {
                        string _url = "" + UrlName + "?year=" + a.Key + "&channelid=" + ChannelId + "&type=" + SearchType + "&field=" + SearchField + "&publisher=" + Server.UrlEncode(Publisher) + "&k=" + Server.UrlEncode(Keywords);
                        LeftMenuBody2 += "<dt id=\"year" + a.Key + "\" class=\"" + (a.Key == q("year") ? "active" : "") + "\"><i class=\"iconfont ic-tag\"></i><a href=\"" + _url + "\"><em class=\"val\">" + a.Key + "(" + a.Value + ")</em></a></dt>";
                    }
                }
            }
            AllCount3 = JumbotDLS.Utils.LuceneHelp.SearchIndex.GetCount(ModuleList, SearchField, "0", "0", "0", Publisher, _LasteKeywords, "module", ref moduleAggregate);//不按类型分组
            LeftMenuBody3 = "<dt id=\"module\" class=\"" + ("" == q("type") ? "active" : "") + "\"><i class=\"iconfont ic-tag\"></i><a href=\"" + UrlName + "?k=" + Server.UrlEncode(Keywords) + "&publisher=" + Server.UrlEncode(Publisher) + "\"><em class=\"val\">" + LeftMenuTitle3 + "(" + AllCount3 + ")</em></a></dt>";

            doh.Reset();
            doh.SqlCmd = "SELECT ID,Title,[Type] FROM [jdls_module_article_module] WHERE Enabled=1 and parentid=0 ORDER BY GroupId ASC";
            DataTable dt2 = doh.GetDataTable();
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                string _moduleid = dt2.Rows[i]["ID"].ToString();
                string _modulename = dt2.Rows[i]["Title"].ToString();
                string _type = dt2.Rows[i]["Type"].ToString();
                Dictionary<string, int> typeAggregate = new Dictionary<string, int>();
                int _count3 = JumbotDLS.Utils.LuceneHelp.SearchIndex.GetCount(_type, SearchField, "0", "0", "0", Publisher, _LasteKeywords, "", ref typeAggregate);
                if (_count3 > 0)
                    LeftMenuBody3 += "<dt id=\"module" + _type + "\" class=\"" + (_type == q("type") ? "active" : "") + "\"><i class=\"iconfont ic-tag\"></i><a href=\"" + UrlName + "?type=" + _type + "&k=" + Server.UrlEncode(Keywords) + "&publisher=" + Server.UrlEncode(Publisher) + "\"><em class=\"val\">" + _modulename + "(" + _count3 + ")</em></a></dt>";
            }
            dt2.Clear();
            dt2.Dispose();

            AllCount4 = JumbotDLS.Utils.LuceneHelp.SearchIndex.GetCount(IndexType, SearchField, ChannelId, "0", Year, "", _LasteKeywords, "publisher", ref publisherAggregate);
            LeftMenuBody4 = "<dt id=\"publisher\" class=\"" + ("" == q("publisher") ? "active" : "") + "\"><i class=\"iconfont ic-tag\"></i><a href=\"" + UrlName + "?year=" + Year + "&channelid=" + ChannelId + "&type=" + SearchType + "&field=" + SearchField + "&publisher=&k=" + Server.UrlEncode(Keywords) + "\"><em class=\"val\">" + LeftMenuTitle4 + "(" + AllCount4 + ")</em></a></dt>";
            if (publisherAggregate != null && publisherAggregate.Count > 0)
            {
                foreach (KeyValuePair<string, int> a in publisherAggregate)
                {
                    if (a.Key != "")
                    {
                        string _url = "" + UrlName + "?year=" + Year + "&channelid=" + ChannelId + "&type=" + SearchType + "&field=" + SearchField + "&publisher=" + Server.UrlEncode(a.Key) + "&k=" + Server.UrlEncode(Keywords);
                        LeftMenuBody4 += "<dt id=\"publisher" + a.Key + "\" class=\"" + (a.Key == q("publisher") ? "active" : "") + "\"><i class=\"iconfont ic-tag\"></i><a href=\"" + _url + "\"><em class=\"val\">" + a.Key + "(" + a.Value + ")</em></a></dt>";
                    }
                }
            }

            //栏目
            doh.Reset();
            doh.SqlCmd = "select ID,Title from jdls_normal_class WHERE  ChannelId=" + ChannelId + "  ORDER BY code";
            dt = doh.GetDataTable();
            AllCount5 = JumbotDLS.Utils.LuceneHelp.SearchIndex.GetCount(IndexType, SearchField, ChannelId, "0", Year, "", _LasteKeywords, "classid", ref classAggregate);
            LeftMenuBody5 = "<dt id=\"class\" class=\"" + ("" == q("classid") ? "on" : "") + "\"><i class=\"iconfont ic-tag\"></i><a href=\"" + UrlName + "?year=" + Year + "&channelid=" + ChannelId + "&type=" + SearchType + "&field=" + SearchField + "&publisher=&classid=&k=" + Server.UrlEncode(Keywords) + "\"><em class=\"val\">" + LeftMenuTitle5 + "(" + AllCount5 + ")</em></a></dt>";
            if (classAggregate != null && classAggregate.Count > 0)
            {
                foreach (KeyValuePair<string, int> a in classAggregate)
                {
                    if (a.Key != "")
                    {
                        var rows = dt.Select("ID=" + a.Key);
                        if (rows.Length>0)
                        {
                            var row = rows[0];
                            string _url = "" + UrlName + "?year=" + Year + "&channelid=" + ChannelId + "&type=" + SearchType + "&field=" + SearchField + "&publisher="+Publisher+"&classid=" + Server.UrlEncode(a.Key) + "&k=" + Server.UrlEncode(Keywords);
                            LeftMenuBody5 += "<dt id=\"class" + a.Key + "\" class=\"" + (a.Key == q("classid") ? "on" : "") + "\"><i class=\"iconfont ic-tag\"></i><a href=\"" + _url + "\"><em class=\"val\">" + row["Title"] + "(" + a.Value + ")</em></a></dt>";
                        }
                       
                    }
                }
            }

        }
        public string HotTagList(string _ccid, int _count)
        {
            string _listHTML = "";
            doh.Reset();
            if (_ccid == "0")
                doh.SqlCmd = "Select TOP " + _count + " ChannelId,Title FROM [jdls_normal_tag] WHERE State=1";
            else
                doh.SqlCmd = "Select TOP " + _count + " ChannelId,Title FROM [jdls_normal_tag] WHERE State=1 AND [ChannelID]=" + _ccid;
            DataTable dt = doh.GetDataTable();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string _channelid = dt.Rows[i]["ChannelId"].ToString();
                string _tag = dt.Rows[i]["title"].ToString();
                _listHTML += "<li><a href=\"default.aspx?channelid=" + _channelid + "&k=" + Server.UrlEncode(_tag) + "&switch=tag\">" + _tag + "</a></li>";

            }
            dt.Clear();
            dt.Dispose();
            return _listHTML;
        }
    }
}
