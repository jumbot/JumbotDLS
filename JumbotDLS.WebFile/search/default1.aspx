﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default1.aspx.cs" Inherits="JumbotDLS.WebFile.Search._default" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title><%= Keywords%> - 资源检索 - <%= site.Name%></title>
    <link rel="stylesheet" href="/skins/default/css/public.css">
    <link rel="stylesheet" href="/skins/default/css/css.css">
    <script src="/skins/default/js/jquery-1.7.2.js"></script>
</head>
<body>
<div class="header">
    <!--#include virtual="/themes/default/include/header_aspx.htm-->
    <!--#include virtual="/themes/default/include/searchform_aspx.htm-->
    
</div>
<script type="text/javascript">showSearch(<%= ChannelId%>)</script>
<div id="keyword" style="display: none;"><%= Keywords%></div>
<script type="text/javascript">
    $("#txtKeyword").val($("#keyword").text());
</script>
    <!-- main -->
    <div class="main wrap">
        <div class="crumbs ">首页>找到“<em><%= Keywords%></em>”相关的<em><%=TotalCount%></em>条结果</div>
        <div class="main-content cls">
            <div id="menus" class="main-left good-menu ">
                <div class="good-menu-item">
                    <h3 class="item-hd">
                        <i class="ic-rec"></i><em class="val">栏目导航</em> <i class="iconfont ic-tag">&#xe90c;</i>
                    </h3>
                    <div class="item-bd">
                        <dl class="item-child">
                            <%=LeftMenuBody5%>
                        </dl>
                    </div>
                </div>
            </div>

            <div class="main-right">
                <!--     <ul class="sort-box cls selectAction">
                    <li class="active"><em class="val">时间</em><i class="iconfont">&#xe90e;</i></li>
                    <li><em class="val">相关度</em><i class="iconfont">&#xe90e;</i></li>
                    <li><em class="val">下载量</em><i class="iconfont">&#xe90e;</i></li>
                </ul>-->
                <div class="good-box">
                    <ul class="good1-list base-list cls" style="border-top: 1px solid #e5e5e5;">
                       
    <%            if (SearchResult != null)
                  {
                      for (int i = 0; i < SearchResult.Count; i++)
                      {
                          %>

            <li class="cls good1-item">
                <a href="<%=SearchResult[i].Url %>"> <img class="img" src="<%=SearchResult[i].Img%>" alt=""></a>
                <div class="content ">
                    <div class="name">
                        <a href="<%=SearchResult[i].Url %>">
                            <%=SearchResult[i].Title%></a>
                    </div>
                         <% if (!string.IsNullOrEmpty(SearchResult[i].Author))
                                       {%>
                                    <span>作者：<%=SearchResult[i].Author %></span>
                                    <% }%>
                    <p class="date">
                        更新时间：<%=SearchResult[i].AddDate %></p>
                    <p class="info">
                        <a href="<%=SearchResult[i].Url %>"><%=SearchResult[i].Summary%> </a>
                    </p>
                </div>          
            </li>
<%
                      }
                  } %>


                    </ul>
                </div>
                <div class="page">
                    <%=PageBarHTML%>
                </div>
            </div>
        </div>
    </div>
    <!-- e main -->
    <!-- footer -->
<!--#include virtual="/themes/default/include/footer_aspx.htm"-->
    <!-- e footer -->
</body>
</html>
