﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
namespace JumbotDLS.WebFile.Review
{
    public partial class _review_toplist : JumbotDLS.UI.FrontHtml
    {
        private string _response = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            string ccid = Str2Str(q("ccid"));
            string id = Str2Str(q("id"));
            int PSize = (Str2Int(q("pagesize"), 0) < 1 || Str2Int(q("pagesize"), 0) > 20) ? 10 : Str2Int(q("pagesize"), 0);
            int page = Int_ThisPage();
            string HtmlStr = (new JumbotDLS.DAL.Normal_ReviewDAL()).GetTopList(page, PSize, ccid, id);
            Response.Write(JumbotDLS.Utils.Strings.Html2Js(HtmlStr));

        }
    }
}
