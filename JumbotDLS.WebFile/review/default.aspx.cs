﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
namespace JumbotDLS.WebFile.Review
{
    public partial class _review_index : JumbotDLS.UI.FrontHtml
    {
        protected void Page_Unload(object sender, EventArgs e)
        {
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string ContentId = Str2Str(q("id"));
            if (ContentId == "0")
            {
                FinalMessage("参数错误!", site.Home, 0, 8);
                Response.End();
            }
            string ChannelId = Str2Str(q("ccid"));
            doh.Reset();
            doh.ConditionExpress = "id=@id and Enabled=1";
            doh.AddConditionParameter("@id", ChannelId);
            if (!doh.Exist("jdls_normal_channel"))
            {
                FinalMessage("频道不存在或被禁用!", site.Home, 0, 8);
                Response.End();
            }
            JumbotDLS.DAL.TemplateEngineDAL teDAL = new JumbotDLS.DAL.TemplateEngineDAL(ChannelId);
            doh.Reset();
            doh.SqlCmd = "SELECT * FROM [jdls_module_" + teDAL.MainChannel.Type + "] WHERE [ChannelId]=" + ChannelId + " and [IsPass]=1 and [Id]=" + ContentId;
            DataTable dtContent = doh.GetDataTable();
            string PageStr = JumbotDLS.Utils.DirFile.ReadFile("~/themes/system_review_index.htm");
            ReplaceSiteTags(ref PageStr);
            teDAL.ReplaceContentTag(ref PageStr, ContentId);
            dtContent.Clear();
            dtContent.Dispose();
            Response.Write(PageStr);//直接输出
        }
    }
}
