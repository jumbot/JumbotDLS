﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Web;
using System.Data;
using JumbotDLS.Common;

namespace JumbotDLS.WebFile.User
{
    public partial class _member_avatar : JumbotDLS.UI.UserCenter
    {
        public string ServiceUrl = string.Empty;
        public string FileFilter = "*.jpg;*.bmp;*.png;";
        public string MaxSize = string.Empty;
        public string FlashVars = string.Empty;
        private string _operType = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            User_Load("", "html");
            ServiceUrl = ResolveUrl("ajax.aspx");
            MaxSize = "" + (1 * 1024) + "";
        }
    }
}
