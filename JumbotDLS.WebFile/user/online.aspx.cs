﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using JumbotDLS.Common;
using JumbotDLS.Utils;
namespace JumbotDLS.WebFile.User
{
    public partial class _online : JumbotDLS.UI.UserCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string _now = DateTime.Now.ToString();
            string _username = q("username");
            if (!(q("oper") == "check"))
            {
                User_Load("", "test");
                if (UserId != "0")
                {
                    doh.Reset();
                    doh.ConditionExpress = "id=@userid";
                   doh.AddConditionParameter("@userid", UserId);
                    doh.AddFieldItem("LastOnlineTime", _now);
                    doh.Update("jdls_normal_user");
                }
                return;
            }
            doh.Reset();
            doh.ConditionExpress = "username=@username and datediff(second, LastOnlineTime, getdate())<=120";
            doh.AddConditionParameter("@username", _username);
            if (doh.Exist("jdls_normal_user"))
            {
                Response.Write(Strings.Html2Js("专家在线"));
                return;
            }
            Response.Write(Strings.Html2Js("专家离线"));
        }
    }
}
