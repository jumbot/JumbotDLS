﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using JumbotDLS.Common;

namespace JumbotDLS.WebFile.User
{
    public partial class _member_third : JumbotDLS.UI.UserCenter
    {
        public bool Bind_Sina = false;
        public bool Bind_Tencent = false;
        public bool Bind_Renren = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            User_Load("", "html");
            doh.Reset();
            doh.ConditionExpress = "id=" + UserId;
            object[] value = doh.GetFields("jdls_normal_user", "Token_Sina,Token_Tencent,Token_Renren");
            Bind_Sina = (value[0].ToString().Length > 0);
            Bind_Tencent = (value[1].ToString().Length > 0);
            Bind_Renren = (value[2].ToString().Length > 0);
        }
    }
}
