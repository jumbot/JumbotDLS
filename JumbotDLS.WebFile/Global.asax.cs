﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Timers;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
namespace JumbotDLS.WebFile
{
    public class Global : System.Web.HttpApplication
    {

        private static string AutoArchive_Fulltext = System.Configuration.ConfigurationManager.AppSettings["JumbotDLS:AutoArchive_Fulltext"];

        protected void Application_Start(object sender, EventArgs e)
        {

            if (AutoArchive_Fulltext == "1")
            {
                JumbotDLS.Common.AutoTaskDAL.Thread_Archive_Fulltext = new Thread(new ThreadStart(JumbotDLS.Common.AutoTaskDAL.Archive_Fulltext));
                JumbotDLS.Common.AutoTaskDAL.Thread_Archive_Fulltext.Start();
            }

        }
        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            if (!Request.Browser.Crawler)//如果不是爬虫
            {
                Exception objExp = HttpContext.Current.Server.GetLastError();
                HttpException erroy = Server.GetLastError() as HttpException;
                if (erroy != null && erroy.GetHttpCode() == 404)
                {
                    JumbotDLS.Utils.Logs.Error("404", "客户机IP:" + Request.UserHostAddress + "\t访问地址:" + Request.Url, false);
                    Server.ClearError();
                    string path = "~/404.html";
                    Server.Transfer(path);
                }
                if (erroy != null && erroy.GetHttpCode() == 500)
                {
                    JumbotDLS.Utils.Logs.Error("500", "客户机IP:" + Request.UserHostAddress + "\t访问地址:" + Request.Url + "\r\n异常信息:" + Server.GetLastError().Message, false);
                    Server.ClearError();
                    string path = "~/500.html";
                    Server.Transfer(path);
                }
            }

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            try
            {
                JumbotDLS.Common.AutoTaskDAL.Thread_Archive_Fulltext.Abort();
            }
            catch (Exception ex)
            {
                JumbotDLS.Utils.Logs.Error("Application_End", ex.ToString());
            }
        }
    }
}