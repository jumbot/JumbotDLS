﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using JumbotDLS.Common;

namespace JumbotDLS.WebFile
{
    public partial class _register : JumbotDLS.UI.BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.txtComputerCode.Text = JumbotDLS.Utils.SoftReg.GetMNum();
                this.txtValidDate.Text = "20991231";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.txtValidateCode.Text == JumbotDLS.Utils.SoftReg.GetRNum(this.txtComputerCode.Text, Str2Int(this.txtValidDate.Text), this.txtCompanyName.Text))
            {
                string strXmlFile = Server.MapPath("~/_data/config/site.config");
                JumbotDLS.DBUtility.XmlControl XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
                XmlTool.Update("Root/ComputerCode", this.txtComputerCode.Text);
                XmlTool.Update("Root/ValidDate", this.txtValidDate.Text);
                XmlTool.Update("Root/CompanyName", this.txtCompanyName.Text);
                XmlTool.Update("Root/ValidateCode", this.txtValidateCode.Text);
                XmlTool.Save();
                XmlTool.Dispose();
                SetupSystemDate();
                this.lblResult.Text = "授权成功";
                Response.Redirect(site.Dir);
            }
            else
                this.lblResult.Text = "授权信息有误";
        }
    }
}
