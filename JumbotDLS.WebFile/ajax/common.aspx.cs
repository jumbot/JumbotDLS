﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Ajax
{
    public partial class _common : JumbotDLS.UI.FrontAjax
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 8;//脚本过期时间
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxAddFavorite":
                    ajaxAddFavorite();
                    break;
                case "ajaxGetValidateCode":
                    ajaxGetValidateCode();
                    break;
                case "ajaxGetLoginbar":
                    ajaxGetLoginbar();
                    break;
                case "ajaxGetUserInfo":
                    ajaxGetUserInfo();
                    break;
                case "ajaxGetSpecialContentList":
                    ajaxGetSpecialContentList();
                    break;
                case "ajaxGetSpecialList":
                    ajaxGetSpecialList();
                    break;
                default:
                    DefaultResponse();
                    break;
            }

            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxAddFavorite()
        {
            if (!ModuleIsOK(q("cType")))
            {
                this._response = JsonResult(0, "error");
                return;
            }
            string _channelid = Str2Str(q("ccid"));
            string _contentid = Str2Str(q("id"));
            string _channeltype = q("cType");
            string _userid;
            string _usercookies;
            int favCount = 0;
            if (Cookie.GetValue(site.CookiePrev + "user") != null)
            {
                _userid = Str2Str(Cookie.GetValue(site.CookiePrev + "user", "id"));
                _usercookies = Cookie.GetValue(site.CookiePrev + "user", "cookies");
                JumbotDLS.Entity.Normal_User _User = new JumbotDLS.DAL.Normal_UserDAL().GetEntity(_userid, true, IPHelp.ClientIP);
                string[] usersetting = (string[])_User.UserSetting.Split(',');
                bool _CanFavorite = (usersetting[12] == "1");
                favCount = Str2Int(usersetting[14]);
                if (!_CanFavorite)
                {
                    this._response = JsonResult(0, "您所在的组不允许收藏");
                    return;
                }

                doh.Reset();
                doh.ConditionExpress = "userid=@userid and channelid=@channelid and contentid=@contentid";
                doh.AddConditionParameter("@userid", _userid);
                doh.AddConditionParameter("@channelid", _channelid);
                doh.AddConditionParameter("@contentid", _contentid);
                if (doh.Exist("jdls_normal_user_favorite"))
                {
                    this._response = JsonResult(0, "此内容已被你收藏");
                    return;
                }
                if (favCount > 0)
                {
                    doh.Reset();
                    doh.ConditionExpress = "[UserId]=" + _userid;
                    int aleadyFav = doh.Count("jdls_normal_user_favorite");
                    if (aleadyFav >= favCount)
                    {
                        this._response = JsonResult(0, "收藏的内容已满");
                        return;
                    }
                }
                doh.Reset();
                doh.ConditionExpress = "channelid=@channelid and id=@id";
                doh.AddConditionParameter("@channelid", _channelid);
                doh.AddConditionParameter("@id", _contentid);
                string _title = doh.GetField("jdls_module_" + _channeltype, "Title").ToString();
                doh.Reset();
                doh.AddFieldItem("Title", _title);
                doh.AddFieldItem("ChannelId", _channelid);
                doh.AddFieldItem("ContentId", _contentid);
                doh.AddFieldItem("ModuleType", _channeltype);
                doh.AddFieldItem("AddDate", DateTime.Now.ToString());
                doh.AddFieldItem("UserId", _userid);
                doh.Insert("jdls_normal_user_favorite");
                this._response = JsonResult(1, "成功收藏");
            }
            else
            {
                this._response = JsonResult(0, "请登录后再收藏");
            }
        }
        private void ajaxGetValidateCode()
        {
            this._response = JsonResult(1, JumbotDLS.Utils.Session.Get("ValidateCode"));
        }
        private void ajaxGetUserInfo()
        {
            string tempBody = string.Empty;
            string _userid = "0";
            string _usercookies = "";
            string _groupname = string.Empty;
            if (Cookie.GetValue(site.CookiePrev + "user") != null)
            {
                _userid = Str2Str(Cookie.GetValue(site.CookiePrev + "user", "id"));
                _usercookies = Cookie.GetValue(site.CookiePrev + "user", "cookies");
                _groupname = Cookie.GetValue(site.CookiePrev + "user", "groupname");
                tempBody = "{\"result\" :\"1\",";
                JumbotDLS.Entity.Normal_User _User = new JumbotDLS.DAL.Normal_UserDAL().GetEntity(_userid, true, IPHelp.ClientIP);
                if (_User.Id != null)
                {
                    doh.Reset();
                    doh.ConditionExpress = "ReceiveUserId=@userid and state=0";
                    doh.AddConditionParameter("@userid", _userid);
                    int _newmessage = doh.Count("jdls_normal_user_message");
                    doh.Reset();
                    doh.ConditionExpress = "UserId=@userid and state=0";
                    doh.AddConditionParameter("@userid", _userid);
                    int _newnotice = doh.Count("jdls_normal_user_notice");
                    int _newcart = new JumbotDLS.DAL.Normal_UserCartDAL().GetNewGoods(_userid);
                    tempBody += "\"userid\" :\"" + _User.Id + "\"," +
                        "\"username\" :\"" + _User.UserName + "\"," +
                        "\"nickname\" :\"" + _User.NickName + "\"," +
                        "\"signature\" :\"" + _User.Signature + "\"," +
                        "\"userpass\" :\"" + _User.UserPass + "\"," +
                        "\"userkey\" :\"" + _User.UserPass.Substring(4, 8) + "\"," +
                        "\"email\" :\"" + _User.Email + "\"," +
                        "\"sex\" :\"" + _User.Sex + "\"," +
                        "\"isvip\" :\"" + _User.IsVIP + "\"," +
                        "\"vipdate\" :\"" + _User.VIPDate + "\"," +
                        "\"truename\" :\"" + _User.TrueName + "\"," +
                        "\"idtype\" :\"" + _User.IDType + "\"," +
                        "\"idcard\" :\"" + _User.IDCard + "\"," +
                        "\"points\" :\"" + _User.Points + "\"," +
                        "\"integral\" :\"" + _User.Integral + "\"," +
                        "\"groupname\" :\"" + _groupname + "\"," +
                        "\"newmessage\" :\"" + _newmessage + "\"," +
                        "\"newnotice\" :\"" + _newnotice + "\"," +
                        "\"newcart\" :\"" + _newcart + "\"," +
                        "\"birthday\" :\"" + _User.BirthDay + "\"," +
                        "\"provincecity\" :\"" + _User.ProvinceCity + "\"," +
                        "\"workunit\" :\"" + _User.WorkUnit + "\"," +
                        "\"address\" :\"" + _User.Address + "\"," +
                        "\"zipcode\" :\"" + _User.ZipCode + "\"," +
                        "\"qq\" :\"" + _User.QQ + "\"," +
                        "\"msn\" :\"" + _User.MSN + "\"," +
                        "\"mobiletel\" :\"" + _User.MobileTel + "\"," +
                        "\"telephone\" :\"" + _User.Telephone + "\"," +
                        "\"homepage\" :\"" + _User.HomePage + "\"," +
                        "\"adminid\" :\"" + _User.AdminId + "\"," +
                        "\"adminlogined\" :\"" + ((Cookie.GetValue(site.CookiePrev + "admin") != null) ? "1" : "0") + "\"," +
                        "\"adminname\" :\"" + _User.AdminName + "\"," +
                        "\"forumname\" :\"" + _User.ForumName + "\"" +
                        "}";
                    this._response = tempBody;
                    return;
                }
                else
                    new JumbotDLS.DAL.Normal_UserDAL().ChkUserLogout("");
            }
            this._response = "{\"result\" :\"0\",\"userid\" :\"0\"," +
                "\"username\" :\"\"," +
                "\"nickname\" :\"\"," +
                "\"userpass\" :\"\"," +
                "\"userkey\" :\"\"," +
                "\"email\" :\"\"," +
                "\"sex\" :\"0\"," +
                "\"isvip\" :\"0\"," +
                "\"vipdate\" :\"\"," +
                "\"truename\" :\"\"," +
                "\"idtype\" :\"\"," +
                "\"idcard\" :\"\"," +
                "\"points\" :\"\"," +
                "\"integral\" :\"\"," +
                "\"groupname\" :\"\"," +
                "\"newmessage\" :\"0\"," +
                "\"newnotice\" :\"0\"," +
                "\"newcart\" :\"0\"," +
                "\"birthday\" :\"\"," +
                "\"provincecity\" :\"\"," +
                "\"workunit\" :\"\"," +
                "\"address\" :\"\"," +
                "\"zipcode\" :\"\"," +
                "\"qq\" :\"\"," +
                "\"msn\" :\"\"," +
                "\"mobiletel\" :\"\"," +
                "\"telephone\" :\"\"," +
                "\"homepage\" :\"\"," +
                "\"adminid\" :\"0\"," +
                "\"adminlogined\" :\"" + ((Cookie.GetValue(site.CookiePrev + "admin") != null) ? "1" : "0") + "\"," +
                "\"adminname\" :\"\"," +
                "\"forumname\" :\"\"" +
                "}";
        }
        private void ajaxGetLoginbar()
        {
            string tempBody = string.Empty;
            string returninfo = string.Empty;
            if (f("state") == "1")
            {
                string uName = f("name");
                string uPass = f("pass");
                returninfo = new JumbotDLS.DAL.Normal_UserDAL().ChkUserLogin(uName, uPass, 1);
            }
            if (Cookie.GetValue(site.CookiePrev + "user") != null)
            {
                string _userid = Str2Str(Cookie.GetValue(site.CookiePrev + "user", "id"));
                string _usercookies = Cookie.GetValue(site.CookiePrev + "user", "cookies");
                string _username = Cookie.GetValue(site.CookiePrev + "user", "name");
                string _userkey = Cookie.GetValue(site.CookiePrev + "user", "userkey");
                string _groupname = Cookie.GetValue(site.CookiePrev + "user", "groupname");

                JumbotDLS.Entity.Normal_User _User = new JumbotDLS.DAL.Normal_UserDAL().GetEntity(_userid, true, IPHelp.ClientIP);
                if (_User.Id != null)
                {
                    doh.Reset();
                    doh.ConditionExpress = "ReceiveUserId=@userid and state=0";
                    doh.AddConditionParameter("@userid", _userid);
                    int _newmessage = doh.Count("jdls_normal_user_message");
                    doh.Reset();
                    doh.ConditionExpress = "UserId=@userid and state=0";
                    doh.AddConditionParameter("@userid", _userid);
                    int _newnotice = doh.Count("jdls_normal_user_notice");
                    int _newcart = new JumbotDLS.DAL.Normal_UserCartDAL().GetNewGoods(_userid);
                    tempBody = "{\"result\" :\"1\"," +
                        "\"userid\" :\"" + _userid + "\"," +
                        "\"username\" :\"" + _username + "\"," +
                        "\"nickname\" :\"" + _User.NickName + "\"," +
                        "\"userkey\" :\"" + _userkey + "\"," +
                        "\"points\" :\"" + _User.Points + "\"," +
                        "\"integral\" :\"" + _User.Integral + "\"," +
                        "\"isvip\" :\"" + _User.IsVIP + "\"," +
                        "\"vipdate\" :\"" + _User.VIPDate + "\"," +
                        "\"groupname\" :\"" + _groupname + "\"," +
                        "\"newmessage\" :\"" + _newmessage + "\"," +
                        "\"newnotice\" :\"" + _newnotice + "\"," +
                        "\"newcart\" :\"" + _newcart + "\"," +
                        "\"adminid\" :\"" + _User.AdminId + "\"" +
                        "}";
                    this._response = tempBody;
                    return;
                }
                else
                    new JumbotDLS.DAL.Normal_UserDAL().ChkUserLogout("");
            }
            this._response = "{\"result\" :\"0\"";
            if (f("state") == "1")
                this._response += ",\"returnval\" :\"" + returninfo + "\"";
            this._response += "}";
        }
        private void ajaxGetSpecialList()
        {
            int page = Int_ThisPage();
            int PSize = Str2Int(q("pagesize"), 20);
            string whereStr = "1=1";
            string jsonStr = "";
            new JumbotDLS.DAL.Normal_SpecialDAL().GetListJSON(page, PSize, whereStr, ref jsonStr);
            this._response = jsonStr;
        }
        private void ajaxGetSpecialContentList()
        {
            string sId = Str2Str(q("sid"));
            int page = Int_ThisPage();
            int PSize = Str2Int(q("pagesize"), 20);
            string joinStr = "A.[ChannelId]=B.Id";
            string whereStr1 = "A.[sId]=" + sId;//外围条件(带A.)
            string whereStr2 = "[sId]=" + sId;//分页条件(不带A.)
            string jsonStr = string.Empty;
            new JumbotDLS.DAL.Normal_SpecialContentDAL().GetListJSON(page, PSize, joinStr, whereStr1, whereStr2, ref jsonStr);
            this._response = jsonStr;
        }
    }
}