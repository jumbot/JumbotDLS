﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="organization_edit.aspx.cs" Inherits="JumbotDLS.WebFile.Admin._organization_edit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta   name="robots" content="noindex, nofollow" />
<meta   http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title></title>
<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js?v20150402"></script>
<link type="text/css" rel="stylesheet" href="../_data/global.css" />
<link type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
<script type="text/javascript" src="../statics/admin/js/common.js"></script>
<script type="text/javascript" src="../_libs/my97datepicker4.8/WdatePicker.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.tip-r').jtip({gravity: 'r',fade: false});
	$.formValidator.initConfig({onError:function(msg){alert(msg);}});
	$("#txtTitle").formValidator({ tipid: "tipTitle", onshow: "请输入机构简称", onfocus: "请输入机构简称", oncorrect: "OK" }).InputValidator({ min: 4, onerror: "请输入机构简称" });
	$("#txtInfo").formValidator({ tipid: "tipInfo", onshow: "请输入机构全称", onfocus: "请输入机构全称", oncorrect: "OK" }).InputValidator({ min: 2, onerror: "请输入机构全称" });

	$("#txtBeginTime").formValidator({ tipid: "tipBeginTime", onshow: "格式：2001-01-01", onfocus: "格式：2001-01-01" }).RegexValidator({ regexp: "date", datatype: "enum", onerror: "格式：2001-01-01" });
	$("#txtEndTime").formValidator({ tipid: "tipEndTime", onshow: "格式：2001-01-01", onfocus: "格式：2001-01-01" }).RegexValidator({ regexp: "date", datatype: "enum", onerror: "格式：2001-01-01" });
	$("#txtTotalCount").formValidator({ tipid: "tipTotalCount", onshow: "请您填写数字", onfocus: "请您填写数字" }).RegexValidator({ regexp: "^\([0-9]+)$", onerror: "请您填写数字" });
});
function AttachmentOperater1(path, type, size) {
    $('#txtLogo').val(path);
}
function AttachmentOperater2(path, type, size) {
    $('#txtErWei').val(path);
}
    </script>
</head>
<body>
<form id="form1" runat="server" onsubmit="return $.formValidator.PageIsValid('1')">
	<table class="formtable">
		<tr>
			<th> 机构简称 </th>
			<td><asp:TextBox ID="txtTitle" runat="server" MaxLength="9" Width="225px" CssClass="ipt"></asp:TextBox>
				<span id="tipTitle" style="width:200px;"> </span></td>
		</tr>
		<tr>
			<th> 机构全称</th>
			<td><asp:TextBox ID="txtInfo" runat="server" MaxLength="20" Width="225px" CssClass="ipt"></asp:TextBox>
            <span id="tipInfo" style="width:200px;"></span>
			</td>
		</tr>

        <tr>
          <th> 机构地址 </th>
          <td><asp:TextBox ID="txtAddress" runat="server" MaxLength="60" Width="500px" CssClass="ipt"></asp:TextBox>
          </td>
        </tr>
        <tr>
          <th> 机构邮编 </th>
          <td><asp:TextBox ID="txtZipCode" runat="server" MaxLength="10" Width="500px" CssClass="ipt"></asp:TextBox>
          </td>
        </tr>
        <tr>
          <th> 联系电话 </th>
          <td><asp:TextBox ID="txtPhone" runat="server" MaxLength="30" Width="500px" CssClass="ipt"></asp:TextBox>
          </td>
        </tr>
        <tr>
          <th> 联系邮箱 </th>
          <td><asp:TextBox ID="txtEmail" runat="server" MaxLength="60" Width="500px" CssClass="ipt"></asp:TextBox>
          </td>
        </tr>
		<tr style="display:none;">
			<th> 禁用本机构 </th>
			<td><asp:RadioButtonList ID="rblEnabled" runat="server" EnableViewState="False" RepeatColumns="2">
					<asp:ListItem Selected="True" Value="1">否</asp:ListItem>
					<asp:ListItem Value="0">是</asp:ListItem>
				</asp:RadioButtonList>
			</td>
		</tr>
		<tr>
			<th> 开通时间 </th>
			<td><asp:TextBox ID="txtBeginTime" runat="server" Width="120px" CssClass="ipt" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,maxDate:'#F{$dp.$D(\'txtEndTime\')}'})"></asp:TextBox>
				<span id="tipBeginTime" style="width:200px;"> </span>
			</td>
		</tr>
		<tr>
			<th> 结束时间 </th>
			<td><asp:TextBox ID="txtEndTime" runat="server" Width="120px" CssClass="ipt" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'#F{$dp.$D(\'txtBeginTime\')}'})"></asp:TextBox>
				<span id="tipEndTime" style="width:200px;"> </span>
			</td>
		</tr>
		<tr>
			<th> 总使用次数 </th>
			<td><asp:TextBox ID="txtTotalCount" runat="server" Width="120px" CssClass="ipt">10000</asp:TextBox>
				<span id="tipTotalCount" style="width:200px;"> </span>
			</td>
		</tr>
	</table>
	<div class="buttonok">
		<asp:Button ID="btnSave" runat="server" Text="确定" CssClass="btnsubmit" 
            onclick="btnSave_Click" />
		<input id="btnReset" type="button" value="取消" class="btncancel" OnClick="parent.JumbotDLS.Popup.hide();" />
	</div>
</form>
<script type="text/javascript">_jdls_SetDialogTitle();</script>
</body>
</html>
