﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="admin_list.aspx.cs" Inherits="JumbotDLS.WebFile.Admin._admin_list" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta   name="robots" content="noindex, nofollow" />
<meta   http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title></title>
<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js"></script>
<link type="text/css" rel="stylesheet" href="../_data/global.css" />
<link type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
<script type="text/javascript" src="../statics/admin/js/common.js"></script>

<script type="text/javascript">
var pagesize=15;
var page=thispage();
$(document).ready(function(){
	ajaxList(page);
});
function ajaxList(currentpage)
{
	if(currentpage!=null) page=currentpage;
	$.ajax({
		type:		"get",
		dataType:	"json",
		data:		"page="+currentpage+"&pagesize="+pagesize+"&clienttime="+Math.random(),
		url:		"admin_ajax.aspx?oper=ajaxGetList",
		error:		function(XmlHttpRequest,textStatus, errorThrown){alert(XmlHttpRequest.responseText); },
		success:	function(d){
			switch (d.result)
			{
			case '-1':
				top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
				break;
			case '0':
				window.JumbotDLS.Alert(d.returnval, "0");
				break;
			case '1':
				$("#ajaxList").setTemplateElement("tplList", null, {filter_data: true});
				$("#ajaxList").processTemplate(d);
				ActiveCoolTable();
				$("#ajaxPageBar").html(d.pagebar);
				break;
			}
		}
	});
}
function ConfirmDel(id){
	window.JumbotDLS.Confirm("确定要删除吗?", "ajaxDel("+id+")");
}
function ajaxDel(id){
	$.ajax({
		type:		"post",
		dataType:	"json",
		data:		"id="+id,
		url:		"admin_ajax.aspx?oper=ajaxDel&clienttime="+Math.random(),
		error:		function(XmlHttpRequest,textStatus, errorThrown){alert(XmlHttpRequest.responseText); },
		success:	function(d){
			switch (d.result)
			{
			case '-1':
				window.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
				break;
			case '0':
				window.JumbotDLS.Alert(d.returnval, "0");
				break;
			case '1':
				ajaxList(page);
				break;
			}
		}
	});
}
    </script>

</head>
<body>
<div class="topnav"> <span class="preload1"></span><span class="preload2"></span>
	<ul id="topnavbar">
		<li class="topmenu"><a href="javascript:void(0);" onclick="top.JumbotDLS.Popup.show('admin_add.aspx?id=0',650,380,false)" class="top_link"><span>添加管理员</span></a></li>
	</ul>
	<script>
	topnavbarStuHover();
        </script>
</div>
<table class="helptable mrg10T">
	<tr>
		<td>
			<ul>
				<li>此处添加的管理员无权操作“系统管理”和“用户管理”</li>
				<li>只有site.config中Founders节点上的用户才是系统管理员</li>
			</ul>
		</td>
	</tr>
</table>
<textarea class="template" id="tplList" style="display:none">
<table class="cooltable">
<thead>
	<tr>
		<th scope="col" style="width:40px;">编号</th>
		<th scope="col" width="*">登录名</th>
        <th scope="col" style="width:90px;">职工名</th>
		<th scope="col" style="width:50px;">性别</th>
        <th scope="col" style="width:300px;">职工代码(微信标识)</th>
		<th scope="col" style="width:150px;">上次登录时间</th>
		<th scope="col" style="width:140px;">上次登录IP</th>

        <th scope="col" style="width:90px;">所属角色</th>
		<th scope="col" style="width:150px;">操作</th>
	</tr>
</thead>
<tbody>
	{#foreach $T.table as record}
	<tr>
		<td align="center">{$T.record.no}</td>
		<td align="center">{$T.record.adminname}</td>
		<td align="center">
			{#if $T.record.isemployee == "1"}
			{$T.record.employeename}
			{#/if}
		</td>
		<td align="center">
			{#if $T.record.sex == "1"}
			<font color='blue'>男</font>
			{#else}
			<font color='red'>女</font>
			{#/if}
		</td>
		<td align="center">
			{#if $T.record.isemployee == "1"}
			{$T.record.employeecode}
			{#/if}
		</td>
		<td align="left">{$T.record.lasttime2}</td>
		<td align="center">{$T.record.lastip2}</td>

		<td align="center">{$T.record.rolename}</td>
		<td align="center" class="oper">
           		<a href="javascript:void(0);" onclick="top.JumbotDLS.Popup.show('admin_edit.aspx?id={$T.record.userid}',850,380,true)">修改</a>
			<a href="javascript:void(0);" onclick="top.JumbotDLS.Popup.show('admin_editpower.aspx?id={$T.record.userid}',-1,-1,true)">附加权限</a>
			<a href="javascript:void(0);" onclick="ConfirmDel({$T.record.userid})">删除</a>
		</td>
	</tr>
	{#/for}
</tbody>
</table>
</textarea>
    <div id="ajaxList" class="mrg10T">
    </div>
    <div id="ajaxPageBar" class="pages">
    </div>
</body>
</html>
