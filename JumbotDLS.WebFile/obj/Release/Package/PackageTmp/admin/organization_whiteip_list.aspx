﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="organization_whiteip_list.aspx.cs" Inherits="JumbotDLS.WebFile.Admin._organization_whiteip_list" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta   name="robots" content="noindex, nofollow" />
<meta   http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title>IP权限</title>
<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js"></script>
<link type="text/css" rel="stylesheet" href="../_data/global.css" />
<link type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
<script type="text/javascript" src="../statics/admin/js/common.js"></script>

<script type="text/javascript">
    var organizationid = joinValue('organizationid');
var pagesize=200;
var page=thispage();
$(document).ready(function(){
	ajaxList(page);
});
function ajaxList(currentpage)
{
	if(currentpage!=null) page=currentpage;
	JumbotDLS.Loading.show("正在加载数据,请等待...");
	$.ajax({
		type:		"get",
		dataType:	"json",
		data:		"page="+currentpage+"&pagesize="+pagesize+"&visittime="+(new Date().getTime()),
		url: "organization_whiteip_ajax.aspx?oper=ajaxGetList" + organizationid,
		error:		function(XmlHttpRequest,textStatus, errorThrown){JumbotDLS.Loading.hide();alert(XmlHttpRequest.responseText); },
		success:	function(d){
			switch (d.result)
			{
			case '-1':
				JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
				break;
			case '0':
				JumbotDLS.Alert(d.returnval, "0");
				break;
			case '1':
				JumbotDLS.Loading.hide();
				$("#ajaxList").setTemplateElement("tplList", null, {filter_data: true});
				$("#ajaxList").processTemplate(d);
				ActiveCoolTable();
				break;
			}
		}
	});
}
function operater(act){
	var ids = JoinSelect("selectID");
	if(ids=="")
	{
		top.JumbotDLS.Alert("请先勾选要操作的内容", "0"); 
		return;
	}
	JumbotDLS.Loading.show("正在处理...");
	$.ajax({
		type:		"post",
		dataType:	"json",
		data:		"ids="+ids,
		url: "organization_whiteip_ajax.aspx?oper=ajaxBatchOper&act=" + act + "&visittime=" + (new Date().getTime())+organizationid,
		error:		function(XmlHttpRequest,textStatus, errorThrown){JumbotDLS.Loading.hide();alert(XmlHttpRequest.responseText); },
		success:	function(d){
			switch (d.result)
			{
			case '-1':
				JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
				break;
			case '0':
				JumbotDLS.Alert(d.returnval, "0");
				break;
			case '1':
				JumbotDLS.Message(d.returnval, "1");
				ajaxList(page);
				break;
			}
		}
	});
}
function ConfirmDel(id){
	JumbotDLS.Confirm("确定要删除吗?", "ajaxDel("+id+")");
}
function ajaxDel(id){
	$.ajax({
		type:		"post",
		dataType:	"json",
		data:		"id="+id,
		url: "organization_whiteip_ajax.aspx?oper=ajaxDel&visittime=" + (new Date().getTime()) + organizationid,
		error:		function(XmlHttpRequest,textStatus, errorThrown){alert(XmlHttpRequest.responseText); },
		success:	function(d){
			switch (d.result)
			{
			case '-1':
				JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
				break;
			case '0':
				JumbotDLS.Alert(d.returnval, "0");
				break;
			case '1':
				ajaxList(page);
				break;
			}
		}
	});
}
    </script>

</head>
<body>
    <div class="topnav">
        <span class="preload1"></span><span class="preload2"></span>
        <ul id="topnavbar">
            <li class="topmenu"><a href="javascript:void(0);" onclick="JumbotDLS.Popup.show('organization_whiteip_edit.aspx?id=0'+organizationid,500,320,false)" id="operater2" class="top_link"><span>添加白名单</span></a>
            </li>
        </ul>

        <script>
	topnavbarStuHover();
        </script>

    </div>
    <table class="helptable">
        <tr>
            <td>
                <ul>
                    <li>白名单之内的IP用户直接可以访问前台</li>
                    <li>白名单之外的IP用户将需要以读者的身份登录</li>
                    <li>增加的IP范围规则尽量不要超过20条，以免影响性能</li>
                    <li><span style="color:Red;">添加的IP范围不允许与其他子站的IP范围冲突</span></li>
                </ul>
            </td>
        </tr>
    </table>
<textarea id="tplList" style="display:none">
<table class="cooltable">
<thead>
	<tr>
		<th scope="col" style="width:40px;">序号</th>
		<th scope="col" style="width:200px;">起始IP</th>
		<th scope="col" style="width:200px;">截止IP</th>
		<th scope="col" width="*">描述</th>
		<th scope="col" style="width:90px;">操作</th>
	</tr>
</thead>
<tbody>
	{#foreach $T.table as record}
	<tr>
		<td align="center"><span title="id:{$T.record.id}">{$T.record.no}</span></td>
		<td align="center">{$T.record.startip2}</td>
		<td align="center">{$T.record.endip2}</td>
		<td align="center">{$T.record.title}</td>
		<td align="center" class="oper">
           		<a href="javascript:void(0);" onclick="JumbotDLS.Popup.show('organization_whiteip_edit.aspx?id={$T.record.id}'+organizationid,500,320,false)">修改</a> <a href="javascript:void(0);" onclick="ConfirmDel({$T.record.id})">删除</a>
		</td>
	</tr>
	{#/for}
</tbody>
</table>
</textarea>
<div id="ajaxList" style="width:100%;margin:0;padding:0" class="mrg10T"></div>
<script type="text/javascript">    _fisp_SetDialogTitle();</script>
</body>
</html>
