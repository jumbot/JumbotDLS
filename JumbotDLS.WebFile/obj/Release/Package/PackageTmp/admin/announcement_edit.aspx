﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="announcement_edit.aspx.cs" Inherits="JumbotDLS.WebFile.Admin._announcement_edit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta   http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title></title>
<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js"></script>
<link type="text/css" rel="stylesheet" href="../_data/global.css" />
<link type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
<link rel="stylesheet" href="../_libs/kindeditor-4.1.10/themes/default/default.css" />
<link rel="stylesheet" href="../_libs/kindeditor-4.1.10/plugins/code/prettify.css" />
<script charset="utf-8" src="../_libs/kindeditor-4.1.10/kindeditor.js"></script>
<script charset="utf-8" src="../_libs/kindeditor-4.1.10/lang/zh_CN.js"></script>
<script charset="utf-8" src="../_libs/kindeditor-4.1.10/plugins/code/prettify.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $.formValidator.initConfig({ onError: function (msg) { alert(msg) } });
	
        $("#txtTitle").formValidator({ tipid: "tipTitle", onshow: "请输入标题，单引号之类的将自动过滤", onfocus: "至少输入2个字符" }).InputValidator({ min: 2, onerror: "至少输入2个字符,请确认" });
    <%=MyFormFieldRequiredList %>
    
    
    
    });
    </script>
</head>
<body>
<form id="form1" runat="server" onsubmit="return jQuery.formValidator.PageIsValid('1')">
	<table cellspacing="0" cellpadding="0" width="100%" class="formtable">
		<tr>
			<th> 标题 </th>
			<td><asp:TextBox ID="txtTitle" runat="server" MaxLength="150" Width="500px" CssClass="ipt"></asp:TextBox>
				<span id="tipTitle" style="width:250px"></span>
			</td>
		</tr>
        <%=MyFormFieldList %>
	</table>
	<div class="buttonok">
		<asp:Button ID="btnSave" runat="server" Text="确定" CssClass="btnsubmit" />
		<input id="btnReset" type="button" value="取消" class="btncancel" onclick="parent.JumbotDLS.Popup.hide();" />
	</div>
</form>
<script type="text/javascript">jdls__SetDialogTitle();</script>
</body>
</html>
