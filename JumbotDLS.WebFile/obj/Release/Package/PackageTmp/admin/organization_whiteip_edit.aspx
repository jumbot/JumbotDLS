﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="organization_whiteip_edit.aspx.cs" Inherits="JumbotDLS.WebFile.Admin._organization_whiteip_edit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta   name="robots" content="noindex, nofollow" />
<meta   http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title>IP白名单维护</title>
<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js"></script>
<link type="text/css" rel="stylesheet" href="../_data/global.css" />
<link type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
<script type="text/javascript" src="../statics/admin/js/common.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$.formValidator.initConfig({onError:function(msg){alert(msg);}});
	$("#txtStartIP").formValidator({tipid:"tipStartIP",onshow:"输入标准IP格式",onfocus:"例如：100.20.1.1",oncorrect:"OK"}).RegexValidator({regexp:"^(((2[0-4]{1}[0-9]{1})|(25[0-5]{1}))|(1[0-9]{2})|([1-9]{1}[0-9]{1})|([0-9]{1})).(((2[0-4]{1}[0-9]{1})|(25[0-5]{1}))|(1[0-9]{2})|([1-9]{1}[0-9]{1})|([0-9]{1})).(((2[0-4]{1}[0-9]{1})|(25[0-5]{1}))|(1[0-9]{2})|([1-9]{1}[0-9]{1})|([0-9]{1})).(((2[0-4]{1}[0-9]{1})|(25[0-5]{1}))|(1[0-9]{2})|([1-9]{1}[0-9]{1})|([0-9]{1}))$",onerror:"IP输入有误"});
	$("#txtEndIP").formValidator({tipid:"tipEndIP",onshow:"输入标准IP格式",onfocus:"例如：100.20.1.255",oncorrect:"OK"}).RegexValidator({regexp:"^(((2[0-4]{1}[0-9]{1})|(25[0-5]{1}))|(1[0-9]{2})|([1-9]{1}[0-9]{1})|([0-9]{1})).(((2[0-4]{1}[0-9]{1})|(25[0-5]{1}))|(1[0-9]{2})|([1-9]{1}[0-9]{1})|([0-9]{1})).(((2[0-4]{1}[0-9]{1})|(25[0-5]{1}))|(1[0-9]{2})|([1-9]{1}[0-9]{1})|([0-9]{1})).(((2[0-4]{1}[0-9]{1})|(25[0-5]{1}))|(1[0-9]{2})|([1-9]{1}[0-9]{1})|([0-9]{1}))$",onerror:"IP输入有误"});
});

/*最后的表单验证*/
function CheckFormSubmit(){
	if($.formValidator.PageIsValid('1'))
	{
	    JumbotDLS.Loading.show("正在处理，请等待...");
		return true;
	}else{
		return false;
	}
}
    </script>
</head>
<body>
<form id="form1" runat="server" onsubmit="return CheckFormSubmit()">
	<table class="formtable">
		<tr>
			<th> 起始IP </th>
			<td><asp:TextBox ID="txtStartIP" runat="server" Width="150px" MaxLength="20" CssClass="ipt"></asp:TextBox>
				<span id="tipStartIP" style="width:200px;"> </span></td>
		</tr>
		<tr>
			<th> 截止IP </th>
			<td><asp:TextBox ID="txtEndIP" runat="server" Width="150px" MaxLength="20" CssClass="ipt"></asp:TextBox>
				<span id="tipEndIP" style="width:200px;"> </span></td>
		</tr>
		<tr>
			<th> 描述 </th>
			<td><asp:TextBox ID="txtTitle" runat="server" MaxLength="50" Width="97%" CssClass="ipt"></asp:TextBox>
				<span id="tipTitle" style="width:200px;"> </span></td>
		</tr>
	</table>
	<div class="buttonok">
		<asp:Button ID="btnSave" runat="server" Text="确定" CssClass="btnsubmit" />
		<input id="btnReset" type="button" value="取消" class="btncancel" OnClick="parent.JumbotDLS.Popup.hide();" />
	</div>
</form>
<script type="text/javascript">_fisp_SetDialogTitle();</script>
</body>
</html>
