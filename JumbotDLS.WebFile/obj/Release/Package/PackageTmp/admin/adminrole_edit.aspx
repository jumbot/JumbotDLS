﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminrole_edit.aspx.cs" Inherits="JumbotDLS.WebFile.Admin._adminrole_edit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta   name="robots" content="noindex, nofollow" />
<meta   http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title>编辑管理角色</title>
<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js"></script>
<link type="text/css" rel="stylesheet" href="../_data/global.css" />
<link type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
<script type="text/javascript" src="../statics/admin/js/common.js"></script>
</head>
<body>
<form id="form1" runat="server">
	<table class="formtable mrg10T">
		<tr>
			<th> 角色名 </th>
			<td><asp:TextBox ID="txtRoleName" runat="server" Width="120px" MaxLength="20" ReadOnly="True" CssClass="ipt"></asp:TextBox>
			</td>
		</tr>
	</table>
	<div class="buttonok">
		<asp:Button ID="btnSave" runat="server" Text="确定" CssClass="btnsubmit" />
		<input id="btnReset" type="button" value="取消" class="btncancel" onclick="parent.JumbotDLS.Popup.hide();" />
	</div>
</form>
<script type="text/javascript">_jdls_SetDialogTitle();</script>
</body>
</html>
