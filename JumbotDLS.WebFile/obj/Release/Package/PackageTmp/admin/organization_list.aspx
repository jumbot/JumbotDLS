﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="organization_list.aspx.cs" Inherits="JumbotDLS.WebFile.Admin._organization_list" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta   name="robots" content="noindex, nofollow" />
<meta   http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title></title>
<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js?v20150402"></script>
<link type="text/css" rel="stylesheet" href="../_data/global.css" />
<link type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
<script type="text/javascript" src="../statics/admin/js/common.js"></script>
<script type="text/javascript">
var pagesize=15;
var page=thispage();
$(document).ready(function(){
	ajaxList(page);
});
function ajaxList(currentpage)
{
	if(currentpage!=null) page=currentpage;
	$.ajax({
		type:		"get",
		dataType:	"json",
		data:		"page="+currentpage+"&pagesize="+pagesize+"&time="+(new Date().getTime()),
		url:		"organization_ajax.aspx?oper=ajaxGetList",
		error:		function(XmlHttpRequest,textStatus, errorThrown){if(XmlHttpRequest.responseText.length>0){alert(XmlHttpRequest.responseText);} },
		success:	function(d){
			switch (d.result)
			{
			case '-1':
				JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
				break;
			case '0':
				JumbotDLS.Alert(d.returnval, "0");
				break;
			case '1':
				$("#ajaxList").setTemplateElement("tplList", null, {filter_data: true});
				$("#ajaxList").processTemplate(d);
				ActiveCoolTable();
				break;
			}
		}
	});
}
function operater(act){
	var ids = JoinSelect("selectID");
	if(ids=="")
	{
		JumbotDLS.Alert("请先勾选要操作的内容", "0"); 
		return;
	}
	top.JumbotDLS.Loading.show("正在处理...");
	$.ajax({
		type:		"post",
		dataType:	"json",
		data:		"ids="+ids,
		url:		"organization_ajax.aspx?oper=ajaxBatchOper&act="+act+"&time="+(new Date().getTime()),
		error: function (XmlHttpRequest, textStatus, errorThrown) { top.JumbotDLS.Loading.hide(); if (XmlHttpRequest.responseText.length > 0) { alert(XmlHttpRequest.responseText); } },
		success:	function(d){
			switch (d.result)
			{
			case '-1':
				JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
				break;
			case '0':
				JumbotDLS.Alert(d.returnval, "0");
				break;
			case '1':
				top.JumbotDLS.Message(d.returnval, "1");
				ajaxList(page);
				break;
			}
		}
	});
}
function move(id,isUp){
	$.ajax({
		type:		"post",
		dataType:	"json",
		data:		"id="+id+"&up="+isUp,
		url:		"organization_ajax.aspx?oper=move&time="+(new Date().getTime()),
		error:		function(XmlHttpRequest,textStatus, errorThrown){if(XmlHttpRequest.responseText.length>0){alert(XmlHttpRequest.responseText);} },
		success:	function(d){
			switch (d.result)
			{
			case '-1':
				JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
				break;
			case '0':
				JumbotDLS.Alert(d.returnval, "0");
				break;
			case '1':
				top.JumbotDLS.Message(d.returnval, "1");
				ajaxList(page);
				break;
			}
		}
	});
}
function ConfirmDel(id){
	JumbotDLS.Confirm("确定要删除ID为"+id+"的机构吗?", "ajaxDel("+id+")");
}
function ajaxDel(id){
	$.ajax({
		type:		"post",
		dataType:	"json",
		data:		"id="+id,
		url:		"organization_ajax.aspx?oper=ajaxDel&time="+(new Date().getTime()),
		error:		function(XmlHttpRequest,textStatus, errorThrown){if(XmlHttpRequest.responseText.length>0){alert(XmlHttpRequest.responseText);} },
		success:	function(d){
			switch (d.result)
			{
			case '-1':
				JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
				break;
			case '0':
				JumbotDLS.Alert(d.returnval, "0");
				break;
			case '1':
				top.JumbotDLS.Message(d.returnval, "1");
				ajaxList(page);
				break;
			}
		}
	});
}
</script>
</head>
<body>
<div class="topnav">
    <span class="preload1"></span><span class="preload2"></span>
    <ul id="topnavbar">
        <li class="topmenu"><a href="javascript:void(0);" id="operater" class="top_link"><span
            class="down">批量操作</span></a>
            <ul class="sub">
                <li><a href="javascript:void(0);" onclick="operater('pass')" title="启用后的机构才会在前台可用">启用机构</a></li>
                <li><a href="javascript:void(0);" onclick="operater('nopass')" title="禁用后的机构不会在前台可用">禁用机构</a></li>
            </ul>
        </li>
        <li class="topmenu"><a href="javascript:void(0);" onclick="JumbotDLS.Popup.show('organization_edit.aspx',-1,-1,true)" class="top_link"><span>增加机构</span></a></li>
    </ul>
    <script>
	topnavbarStuHover();
    </script>
</div>
<textarea id="tplList" style="display:none">
<table class="cooltable">
<thead>
	<tr>
		<th align="center" scope="col" style="width:40px;"><input onclick="checkAllLine()" id="checkedAll" name="checkedAll" type="checkbox" title="全部选择/全部不选" /></th>
		<th scope="col" style="width:60px;">ID</th>
		<th scope="col" style="width:180px;">机构名称</th>
		<th scope="col" width="*">机构Email</th>
		<th scope="col" style="width:110px;">开通时间</th>
		<th scope="col" style="width:110px;">结束时间</th>
		<th scope="col" style="width:90px;">总使用次数</th>
		<th scope="col" style="width:90px;">已使用次数</th>
		<th scope="col" style="width:90px;">IP白名单</th>
		<th scope="col" style="width:90px;">操作</th>
	</tr>
</thead>
<tbody>
	{#foreach $T.table as record}
	<tr>
		<td align="center"><input class="checkbox" name="selectID" type="checkbox" value='{$T.record.id}' />
		</td>
		<td align="center">{$T.record.id}</td>
		<td align="center">
			{$T.record.title}
		</td>
		<td align="left">
			{$T.record.email}
		</td>
		<td align="center">
			{formatDate($T.record.begintime,'yyyy-MM-dd')}
		</td>
		<td align="center">
			{formatDate($T.record.endtime,'yyyy-MM-dd')}
		</td>
		<td align="center">{$T.record.totalcount}</td>
		<td align="center">{$T.record.usedcount}</td>
		<td align="center" class="oper">
           	<a href="javascript:void(0);" onclick="JumbotDLS.Popup.show('organization_whiteip_list.aspx?organizationid={$T.record.id}',-1,-1,true)">管理</a>
		</td>
		<td align="center" class="oper">
           	<a href="javascript:void(0);" onclick="JumbotDLS.Popup.show('organization_edit.aspx?ccid={$T.record.id}',-1,-1,true)">修改</a> <a href="javascript:void(0);" onclick="ConfirmDel({$T.record.id})">删除</a>
		</td>
	</tr>
	{#/for}
</tbody>
</table>
</textarea>
<div id="ajaxList" style="width:100%;margin:0;padding:0" class="mrg10T"></div>
</body>
</html>
