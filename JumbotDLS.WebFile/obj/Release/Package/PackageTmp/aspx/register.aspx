﻿<%@ Page Language="C#" AutoEventWireup="true" Codebehind="register.aspx.cs" Inherits="JumbotDLS.WebFile._register" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta   name="robots" content="noindex, nofollow" />
<meta   http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title>系统正版注册</title>
<script type="text/javascript" src="/_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="/_data/global.js"></script>
<link type="text/css" rel="stylesheet" href="/_data/global.css" />
<link type="text/css" rel="stylesheet" href="/statics/admin/css/common.css" />
</head>
<body>
<div style="margin:0 auto;width:780px;;margin-top:120px;">
<table class="helptable">
	<tr>
		<td>
			<ul>
				<li>机器码由系统自动获取,请输入正版系统的授权码</li>
			</ul>
		</td>
	</tr>
</table>
<form id="form1" runat="server">
	<table class="formtable">
		<tr>
			<th> 机器码 </th>
			<td><asp:TextBox ID="txtComputerCode" runat="server" Width="300px" MaxLength="60" ReadOnly="True" CssClass="ipt"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<th> 授权单位 </th>
			<td><asp:TextBox ID="txtCompanyName" runat="server" Width="300px" MaxLength="60" CssClass="ipt"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<th> 有效期 </th>
			<td><asp:TextBox ID="txtValidDate" runat="server" Width="300px" MaxLength="6" CssClass="ipt"></asp:TextBox>
			<br />格式为20130101</td>
		</tr>
		<tr>
			<th> 授权码 </th>
			<td><asp:TextBox ID="txtValidateCode" runat="server" Width="300px" MaxLength="60" CssClass="ipt"></asp:TextBox>
                <asp:Label ID="lblResult" runat="server" Text=""></asp:Label></td>
		</tr>
	</table>
	<div class="buttonok">
		<asp:Button ID="btnSave" runat="server" Text="注册" CssClass="btnsubmit" 
            onclick="btnSave_Click" />
	</div>
</form>
</div>
</body>
</html>