﻿$(document).ready(function () {
    $("#login_mpweixin_img").click(function () {
        login_qrcode_get();
    });
    login_qrcode_get();

});
var _c_t_i = null; //记录过期
var _v_t_i = null; //验证
function login_qrcode_get() {
    $("#login_mpweixin_img").attr('src', '/weixin/images/qrcode_loading.gif');
    if (_v_t_i) {
        clearTimeout(_v_t_i);
    }
    if (_c_t_i) {
        clearTimeout(_c_t_i);
    }
    $.ajax({
        type: 'get',
        url: '/weixin/ajax.aspx?oper=login_ticket',
        dataType: 'json',
        async: false,
        success: function (data) {
            if (data.result == 1) {
                var _t = new Date().getTime();
                var img_url = 'https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=' + data.returnval+'&t=' + _t;
                
                setTimeout(function() { $("#login_mpweixin_img").attr('src', img_url); },500);


                _c_t_i = setTimeout("login_core_expire();", 30000);
                _v_t_i = setTimeout("login_verify();", 2000);
            } else {
                $("#login_mpweixin_img").attr('src', '/weixin/images/qrcode_invalid.jpg');
            }
        },
        error: function () {
            $("#login_mpweixin_img").attr('src', '/weixin/images/qrcode_invalid.jpg');
        }
    });
}
function login_core_expire() {
    if (_v_t_i) {
        clearTimeout(_v_t_i);
    }
    login_qrcode_get();
}
function login_core_success() {
    if (_v_t_i) {
        clearTimeout(_v_t_i);
    }
    if (_c_t_i) {
        clearTimeout(_c_t_i);
    }
    loginSuccess();
}
function login_verify() {
    var _t = new Date().getTime();
    $.ajax({
        type: 'get',
        url: '/weixin/ajax.aspx?oper=login_verify&t=' + _t,
        dataType: 'json',
        async: false,
        success: function (data) {
            if (data.result == 1) {
                login_core_success();
            } else {
                _v_t_i = setTimeout("login_verify();", 2000);
            }
        },
        error: function () {
            _v_t_i = setTimeout("login_verify();", 2000);
        }
    });
}