﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="other_upload.aspx.cs" Inherits="JumbotDLS.WebFile.Admin._other_upload" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>附件上传</title>
<script type="text/javascript" src="/_libs/jquery.tools.pack.js?ver=20200909"></script>
<script type="text/javascript" src="/_data/global.js?ver=<%=site.Version %>"></script>
<link type="text/css" rel="stylesheet" href="/_data/global.css?ver=<%=site.Version %>" />
<link type="text/css" rel="stylesheet" href="/statics/admin/css/common.css?ver=<%=site.Version %>" />
<script type="text/javascript" src="/statics/admin/js/common.js"></script>
<style type="text/css">
*{ margin:0; padding:0; }
body {margin: 0px;padding: 0; font-size: 12px; background-color: #FFFFFF;color: #333333;}
td,input {font-size: 12px;}
.uploadBtn 
{
    width: 90px;
    display: block;
    height: 22px;
    line-height: 22px; 
    background-color:#3391FF; 
    font-size: 12px;
    color: #ffffff; 
    border:none;
    margin: 0 auto;
    cursor:pointer;
    }
.uploadFile{cursor:pointer;height: 22px;top: 0; font-size:50px;right: 0;opacity: 0;filter: alpha(opacity=0);position: absolute;}
#uploadInfo{margin:0 10px;width: 320px;height: 26px;color:red;line-height:26px; float:left;display:block;}
</style>
<style type="text/css">
body{margin: 0px;}
</style>
<script type="text/javascript">
    function ajaxFileUpload() {
        $("#uploadInfo").html("正在上传...");
        $.ajaxFileUpload({
            url: "/admin/other_upfile.aspx?adminid=<%=AdminId%>&adminsign=<%=AdminSign %>&type=<%=UploadType %>",
            secureuri: false, //是否需要安全协议，一般设置为false
            fileElementId: "file1",
            dataType: "json",
            success: function (data, status) {
                if (data.result != '1') {
                    $("#uploadInfo").html(data.returnval);
                } else {
                    var callBack = data.returnval;
                    var filename = callBack.split('|')[0];
                    var filesize = callBack.split('|')[1];
                    var s = filename.lastIndexOf(".");
                    var e = filename.substring(s + 1).toLowerCase();
                    parent.AttachmentOperater<%=N %>(filename, e, filesize, '<%=BindField %>',data.body);
                    //$("#uploadInfo").html("文件上传成功");
                    window.location.reload();
                }
            },
            error: function (data, status, e) {
                JumbotDLS.Loading.hide();
                alert(e);
            }
        });

        return false;

    }
</script>
</head>
<body>
<div style="margin:0;width: 90px;height: 22px;overflow: hidden;position: relative; float:left;"> 
  <input id="btnUpload1" type="button" value="上传文件" onclick="$('#file1').click()"  class="uploadBtn" />
  <input id="file1" name="file1"  type="file" onchange="ajaxFileUpload();" class="uploadFile" />
</div><span id="uploadInfo"></span>
</body>
</html>