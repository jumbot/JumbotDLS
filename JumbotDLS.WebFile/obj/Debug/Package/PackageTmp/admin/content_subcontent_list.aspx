﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="content_subcontent_list.aspx.cs" Inherits="JumbotDLS.WebFile.Admin._content_subcontent_list" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta   http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title></title>
<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js"></script>
<link   type="text/css" rel="stylesheet" href="../_data/global.css" />
<script type="text/javascript" src="../statics/admin/js/common.js"></script>
<link   type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
<script type="text/javascript">
    var ccid = joinValue('ccid'); //频道ID
    var parentid = joinValue('parentid');
    var moduleid = joinValue('moduleid');
    var pagesize = 50;
    var page = thispage();
    $(document).ready(function () {
        ajaxList(page);
    });
    function ajaxList(currentpage) {
        if (currentpage != null) page = currentpage;
        $("#ajaxList").html();
        $.ajax({
            type: "get",
            dataType: "json",
            data: "page=" + currentpage + "&pagesize=" + pagesize + "&clienttime=" + Math.random(),
            url: "content_subcontent_ajax.aspx?oper=ajaxGetList" + ccid + parentid + moduleid,
            error: function (XmlHttpRequest, textStatus, errorThrown) { alert(XmlHttpRequest.responseText); },
            success: function (d) {
                switch (d.result) {
                    case '-1':
                        JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                        break;
                    case '0':
                        JumbotDLS.Alert(d.returnval, "0");
                        break;
                    case '1':
                        $("#ajaxList").setTemplateElement("tplList", null, { filter_data: false });
                        $("#ajaxList").processTemplate(d);
                        $("#ajaxPageBar").html(d.pagebar);
                        ActiveCoolTable();
                        break;
                }
            }
        });
    }
    function ConfirmDel(id) {
        JumbotDLS.Confirm("确定要删除吗?", "ajaxDel(" + id + ")");
    }
    function ajaxDel(id) {
        $.ajax({
            type: "post",
            dataType: "json",
            data: "id=" + id,
            url: "content_subcontent_ajax.aspx?oper=ajaxDel&clienttime=" + Math.random() + ccid + parentid + moduleid,
            error: function (XmlHttpRequest, textStatus, errorThrown) { alert(XmlHttpRequest.responseText); },
            success: function (d) {
                switch (d.result) {
                    case '-1':
                        JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                        break;
                    case '0':
                        JumbotDLS.Alert(d.returnval, "0");
                        break;
                    case '1':
                        ajaxList(page);
                        break;
                }
            }
        });
    }
</script>
</head>
<body>
<div class="topnav"> <span class="preload1"></span><span class="preload2"></span>
  <ul id="topnavbar">
    <li class="topmenu"><a href="javascript:void(0);" onclick="JumbotDLS.Popup.show('content_subcontent_edit.aspx?id=0'+ccid + parentid + moduleid,-1,-1,true)" class="top_link"><span>添加内容</span></a></li>
     <li class="topmenu"><a href="javascript:void(0);" onclick="JumbotDLS.Popup.show('content_subcontent_import_form.aspx?id=0'+ccid + parentid + moduleid,-1,-1,true)" class="top_link"><span>批量导入</span></a></li>
 </ul>
</div>
<script type="text/javascript">    topnavbarStuHover();</script>
<textarea class="template" id="tplList" style="display:none">
<table class="cooltable">
<thead>
	<tr>
    <%if (ModuleId==107)
      { %>
		<th scope="col" width="*">标题</th>
		<th scope="col" style="width:80px;">操作</th>
<%} %>
    <%if (ModuleId==108)
      { %>
		<th scope="col" width="*">标题</th>
		<th scope="col" style="width:80px;">操作</th>
<%} %>
    <%if (ModuleId==109)
      { %>
		<th scope="col" style="width:120px;">开始时间</th>
		<th scope="col" style="width:120px;">结束时间</th>
		<th scope="col" width="*">机构</th>
		<th scope="col" style="width:70px;">学位</th>
		<th scope="col" style="width:80px;">操作</th>
<%} %>
    <%if (ModuleId==110)
      { %>
		<th scope="col" style="width:120px;">开始时间</th>
		<th scope="col" style="width:120px;">结束时间</th>
		<th scope="col" width="*">单位</th>
		<th scope="col" style="width:250px;">部门</th>
		<th scope="col" style="width:100px;">职位</th>
		<th scope="col" style="width:80px;">操作</th>
<%} %>
	</tr>
	</thead>
<tbody>
	{#foreach $T.table as record}
	<tr>
    <%if (ModuleId==107)
      { %>
		<td align="center">{$T.record.title}</td>
<%} %>
    <%if (ModuleId==108)
      { %>
		<td align="center">{$T.record.title}</td>
<%} %>
    <%if (ModuleId==109)
      { %>
		<td align="center">{$T.record.title}</td>
		<td align="center">{$T.record.title2}</td>
		<td align="center">{$T.record.customfield01}</td>
		<td align="center">{$T.record.customfield02}</td>
<%} %>
    <%if (ModuleId==110)
      { %>
		<td align="center">{$T.record.title}</td>
		<td align="center">{$T.record.title2}</td>
		<td align="center">{$T.record.customfield01}</td>
		<td align="center">{$T.record.customfield02}</td>
		<td align="center">{$T.record.customfield03}</td>
<%} %>
		<td align="center" class="oper">
			<a href="javascript:void(0);" onclick="JumbotDLS.Popup.show('<%=EditFile %>?id={$T.record.id}'+ccid + parentid + moduleid,-1,-1,true)">修改</a>
			<a href="javascript:void(0);" onclick="ConfirmDel({$T.record.id})">删除</a>
		</td>
	</tr>
	{#/for}
</tbody>
</table></textarea>
<div id="ajaxList" style="width:100%;margin:0;padding:0" class="mrg10T"></div>
<div id="ajaxPageBar" class="pages"></div>
</body>
</html>
