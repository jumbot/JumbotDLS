﻿function ajaxSetVersion() {
    $.ajax({
        type: "get",
        dataType: "json",
        data: "clienttime=" + Math.random(),
        url: "ajax.aspx?oper=ajaxSetVersion",
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            if (XmlHttpRequest.responseText != "") {
                alert(XmlHttpRequest.responseText)
            }
        },
        success: function (d) { }
    })
};

function ajaxChkVersion() {
    $.ajax({
        type: "get",
        dataType: "json",
        data: "clienttime=" + Math.random(),
        url: "ajax.aspx?oper=ajaxChkVersion",
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            if (XmlHttpRequest.responseText != "") {
                alert(XmlHttpRequest.responseText)
            }
        },
        success: function (d) {
            switch (d.result) {
                case '0':
                    top.location.href = "login.aspx";
                    break
            }
        }
    })
};

function chkLogout() {
    $.ajax({
        type: "get",
        dataType: "json",
        url: "ajax.aspx?oper=logout&clienttime=" + Math.random(),
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            alert(XmlHttpRequest.responseText)
        },
        success: function (d) {
            if (d.result == "1") top.location.href = 'login.aspx'
        }
    })
};

function ajaxClearSystemCache() {
    top.JumbotDLS.Loading.show("正在更新，请等待...");
    $.ajax({
        type: "get",
        dataType: "json",
        data: "oper=ajaxClearSystemCache",
        url: "ajax.aspx?clienttime=" + Math.random(),
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            top.JumbotDLS.Loading.hide();
            alert(XmlHttpRequest.responseText)
        },
        success: function (d) {
            switch (d.result) {
                case '-1':
                    top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                    break;
                case '0':
                    top.JumbotDLS.Alert(d.returnval, "0");
                    break;
                case '1':
                    top.JumbotDLS.Message(d.returnval, "1");
                    break
            }
        }
    })
};

function ajaxCreateSystemCount() {
    top.JumbotDLS.Loading.show("正在更新，时间可能会比较长...");
    $.ajax({
        type: "get",
        dataType: "json",
        data: "oper=ajaxCreateSystemCount",
        url: "ajax.aspx?clienttime=" + Math.random(),
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            top.JumbotDLS.Loading.hide();
            alert(XmlHttpRequest.responseText)
        },
        success: function (d) {
            switch (d.result) {
                case '-1':
                    top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                    break;
                case '0':
                    top.JumbotDLS.Alert(d.returnval, "0");
                    break;
                case '1':
                    top.JumbotDLS.Message(d.returnval, "1");
                    break
            }
        }
    })
};

function ajaxCreateIndexPage() {
    top.JumbotDLS.Loading.show("正在更新，请等待...");
    $.ajax({
        type: "get",
        dataType: "json",
        data: "oper=ajaxCreateIndexPage",
        url: "ajax.aspx?clienttime=" + Math.random(),
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            top.JumbotDLS.Loading.hide();
            alert(XmlHttpRequest.responseText)
        },
        success: function (d) {
            switch (d.result) {
                case '-1':
                    top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                    break;
                case '0':
                    top.JumbotDLS.Alert(d.returnval, "0");
                    break;
                case '1':
                    top.JumbotDLS.Message(d.returnval, "1");
                    break
            }
        }
    })
};

function ajaxEmailServerExport() {
    top.JumbotDLS.Loading.show("正在导出，请等待...", 260, 80);
    $.ajax({
        type: "get",
        dataType: "json",
        data: "oper=ajaxEmailServerExport",
        url: "email_smtpserver_ajax.aspx?clienttime=" + Math.random(),
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            top.JumbotDLS.Loading.hide();
            alert(XmlHttpRequest.responseText)
        },
        success: function (d) {
            switch (d.result) {
                case '-1':
                    top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                    break;
                case '0':
                    top.JumbotDLS.Alert(d.returnval, "0");
                    break;
                case '1':
                    top.JumbotDLS.Message(d.returnval, "1");
                    ajaxList(1);
                    break
            }
        }
    })
};

function ajaxEmailServerImport() {
    top.JumbotDLS.Loading.show("正在导出，请等待...", 260, 80);
    $.ajax({
        type: "get",
        dataType: "json",
        data: "oper=ajaxEmailServerImport",
        url: "email_smtpserver_ajax.aspx?clienttime=" + Math.random(),
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            top.JumbotDLS.Loading.hide();
            alert(XmlHttpRequest.responseText)
        },
        success: function (d) {
            switch (d.result) {
                case '-1':
                    top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                    break;
                case '0':
                    top.JumbotDLS.Alert(d.returnval, "0");
                    break;
                case '1':
                    top.JumbotDLS.Message(d.returnval, "1");
                    ajaxList(1);
                    break
            }
        }
    })
};

function ajaxCreateJavascript() {
    top.JumbotDLS.Loading.show("正在更新，请等待...");
    $.ajax({
        type: "get",
        dataType: "json",
        data: "oper=ajaxCreateJavascript",
        url: "javascript_ajax.aspx?clienttime=" + Math.random(),
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            top.JumbotDLS.Loading.hide();
            alert(XmlHttpRequest.responseText)
        },
        success: function (d) {
            switch (d.result) {
                case '-1':
                    top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                    break;
                case '0':
                    top.JumbotDLS.Alert(d.returnval, "0");
                    break;
                case '1':
                    top.JumbotDLS.Message(d.returnval, "1");
                    break
            }
        }
    })
};

function ajaxTemplateIncludeUpdateFore(pid, source) {
    if (source == null) source = "";
    top.JumbotDLS.Loading.show("正在更新，请等待...");
    $.ajax({
        type: "get",
        dataType: "json",
        data: "clienttime=" + Math.random(),
        url: "templateinclude_ajax.aspx?oper=updatefore&pid=" + pid + "&source=" + source,
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            top.JumbotDLS.Loading.hide();
            alert(XmlHttpRequest.responseText)
        },
        success: function (d) {
            switch (d.result) {
                case '-1':
                    top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                    break;
                case '0':
                    top.JumbotDLS.Alert(d.returnval, "0");
                    break;
                case '1':
                    top.JumbotDLS.Message(d.returnval, "1");
                    break
            }
        }
    })
};

function ajaxPageUpdateFore(id) {
    top.JumbotDLS.Loading.show("正在生成...");
    $.ajax({
        type: "post",
        dataType: "json",
        data: "id=" + id,
        url: "page_ajax.aspx?oper=ajaxCreatePage&clienttime=" + Math.random(),
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            top.JumbotDLS.Loading.hide();
            alert(XmlHttpRequest.responseText)
        },
        success: function (d) {
            switch (d.result) {
                case '-1':
                    top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                    break;
                case '0':
                    top.JumbotDLS.Alert(d.returnval, "0");
                    break;
                case '1':
                    top.JumbotDLS.Message(d.returnval, "1");
                    break
            }
        }
    })
};

function ajaxModuleUpdateFore() {
    top.JumbotDLS.Loading.show("正在更新，请等待...");
    $.ajax({
        type: "get",
        dataType: "json",
        data: "clienttime=" + Math.random(),
        url: "modules_ajax.aspx?oper=updatefore",
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            top.JumbotDLS.Loading.hide();
            alert(XmlHttpRequest.responseText)
        },
        success: function (d) {
            switch (d.result) {
                case '-1':
                    top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                    break;
                case '0':
                    top.JumbotDLS.Alert(d.returnval, "0");
                    break;
                case '1':
                    top.JumbotDLS.Message(d.returnval, "1");
                    break
            }
        }
    })
};

function ajaxCreateSearchIndex(create, num) {
    top.JumbotDLS.Loading.show("正在对第" + num + "批数据做索引，请耐心等待...");
    $.ajax({
        type: "get",
        dataType: "json",
        data: "oper=ajaxCreateSearchIndex&create=" + create,
        url: "ajax.aspx?clienttime=" + Math.random(),
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            top.JumbotDLS.Loading.hide();
            alert(XmlHttpRequest.responseText)
        },
        success: function (d) {
            switch (d.result) {
                case '-1':
                    top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                    break;
                case '0':
                    top.JumbotDLS.Alert(d.returnval, "0");
                    break;
                case '1':
                    top.JumbotDLS.Message(d.returnval, "1");
                    break;
                case '2':
                    ajaxCreateSearchIndex(0, num + 1);
                    break
            }
        }
    })
}