﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="content_subcontent_import_form.aspx.cs" Inherits="JumbotDLS.WebFile.Admin._content_subcontent_import_form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title>批量导入数据</title>
<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js"></script>
<link   type="text/css" rel="stylesheet" href="../_data/global.css" />
<link   type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
<style>
#ajaxResult p {line-height:20px;}
</style>
<script type="text/javascript">
    var ccid = joinValue('ccid'); //频道ID
$(document).ready(function(){
	$.formValidator.initConfig({onError:function(msg){alert(msg);}});
	$("#txtDatabase").formValidator({ tipid: "tipDatabase", onshow: "请上传mdb|xls文件", onfocus: "请上传mdb|xls文件" }).InputValidator({ min: 1, onerror: "请上传mdb|xls文件" });
});
function AttachmentOperater(path, type, size) {
    $("#txtDatabase").val(path);
    $("#lblDatabase").text(path);
}
function ajaxSubmit(page) {
    if (!$.formValidator.PageIsValid('1')) {
        return;
    }
    var _data = {
        orderby: $("#ddlOrderBy").val(),
        checkrepeat: $("#ddlCheckRepeat").val(),
        database: $("#txtDatabase").val()
    };

    if (_data.database == "") {
        $("#txtDatabase").focus();
        return;
    }
    info = "正在处理第" + page + "批数据，请等待...";
    JumbotDLS.Loading.show(info);
    $.ajax({
        type: "post",
        dataType: "json",
        data: _data,
        url: "content_subcontent_import_ajax.aspx?oper=ajaxImport&parentid=<%=_ParentId %>&moduleid=<%=_ModuleId %>&page=" + page + "&visittime=" + (new Date().getTime()) + ccid,
        error: function (XmlHttpRequest, textStatus, errorThrown) { JumbotDLS.Loading.hide(); alert(XmlHttpRequest.responseText); },
        success: function (d) {
            switch (d.result) {
                case '-1':
                    JumbotDLS.Alert(d.returnval, "0", "top.location.href='index.aspx';");
                    break;
                case '0':
                    JumbotDLS.Alert(d.returnval, "0");
                    break;
                case '1':
                    $('#ajaxResult').append("第" + page + "批结果：" + d.returnval + "<br />");
                    parent.ajaxList(1);
                    JumbotDLS.Loading.hide();
                    break;
                case '2': //说明还要继续
                    $('#ajaxResult').append("第" + page + "批结果：" + d.returnval + "<br />");
                    ajaxSubmit(page + 1);
                    break;
            }
        }
    });
}
</script>
</head>
<body>
<form id="form1" runat="server">
	<table class="formtable">
		<tr>
			<th> 导入方式 </th>
			<td><asp:DropDownList ID="ddlOrderBy" runat="server">
            <asp:ListItem Value="desc">mdb|xls表数据按时间倒序</asp:ListItem><asp:ListItem Value="asc">mdb|xls表数据按时间顺序</asp:ListItem></asp:DropDownList>
			</td>
		</tr>
			<tr style="display:none;">
				<th> mdb|xls文件 </th>
				<td align="left"><span style="display:none;"><asp:TextBox ID="txtDatabase" runat="server" Width="97%" CssClass="ipt"></asp:TextBox></span>
				</td>
			</tr>
		<tr>
			<th>上传文件</th>
			<td><iframe id="frm_upload" src="other_upload.aspx?type=database" width="100%" height="30" scrolling="no" frameborder="0"></iframe><br />
            <asp:Label ID="lblDatabase" runat="server" Text="请上传mdb|xls文档"></asp:Label></td>
		</tr>
			<tr style="display:none;">
				<th> 去重检查 </th>
				<td><asp:DropDownList ID="ddlCheckRepeat" runat="server">
                <asp:ListItem Value="1">是</asp:ListItem>
                <asp:ListItem Value="0" Selected="True">否</asp:ListItem>
                </asp:DropDownList></td>
			</tr>
	</table>
	<div class="buttonok">
		<input type="button" name="btnSave" value="确定" id="btnSave" class="btnsubmit" onclick="ajaxSubmit(1);"  />
		<input id="btnReset" type="button" value="返回" class="btncancel" onclick="parent.JumbotDLS.Popup.hide();" />
	</div>
	<table class="formtable">
		<tr>
			<th> 导入结果 </th>
			<td><p id="ajaxResult"></p></td>
		</tr>
	</table>
</form>
<script type="text/javascript">    _jdls_SetDialogTitle();</script>
</body>
</html>
