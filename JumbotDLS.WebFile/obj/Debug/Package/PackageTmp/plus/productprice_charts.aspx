﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="productprice_charts.aspx.cs" Inherits="JumbotDLS.WebFile.plus.productprice_charts" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<script type="text/javascript" src="/_libs/echarts/asset/js/esl/esl.js"></script>
<head runat="server">
<style>
*{margin:0;padding:0}
</style>
<title></title>
</head>
<body>
<div id="echarts" style="width:680px;height:325px;"></div>

<script type="text/javascript">
                require.config({
                    paths: {
                        'echarts': '/_libs/echarts/www/js/echarts-map',
                        'echarts/chart/pie': '/_libs/echarts/www/js/echarts-map'
                    }
                });
                require(['echarts', 'echarts/chart/pie'], function (ec) {
                    var myChart = ec.init(document.getElementById('echarts'));
                    var option = {
                        title: {
                            text: '单位：<%=PriceUnit %>',subtext:''
                        },
                        tooltip: {
                            trigger: 'axis'
                        },
                        toolbox: {
                            show: true,
                            feature : {saveAsImage : {show: true}}
                        },
                        calculable: true,
                        xAxis: [
                        {
                            type: 'category',
                            boundaryGap: false,
                            data: [<%=xList %>]
                        }],
                        yAxis: [
                        {
                            type: 'value',
                            axisLabel : {
                            formatter: '{value}'
                            }
                        }],
                        grid: { x: 60, y: 50, x2: 30, y2: 50, borderWidth: 0 },

                        series: [
                        {
                            name: '发展趋势',
                            type: 'line',
                            data: [<%=yList %>]
                        }]
                    };
                    myChart.setOption(option);
                });
    </script>
</body>
</html>
