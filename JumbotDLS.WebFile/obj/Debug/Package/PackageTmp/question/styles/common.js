﻿function ajaxQuestionAdd() {
    var uName = $("#questionName").val();
    var uPhone = $("#questionPhone").val();
    var uEmail = $("#questionEmail").val();
    var uCode = $("#questionCode").val();
    var title = ajaxQuestionSiftSymbol($("#questionTitle").val());
    var content = ajaxQuestionSiftSymbol($("#questionContent").val());
    var contentid = $("#questionContentId").val();
    if (!uName) {
        alert("姓名不能为空!");
        return;
    }
    if (!uEmail) {
        alert("邮箱不能为空!");
        return;
    }
    if (uCode == "") {
        alert("请填写验证码!");
        return;
    }
    if (!title) {
        alert("请填写标题!");
        return;
    }
    if (!content || content.length < 5) {
        alert("留言字符不能少于5个!");
        return;
    }
    if (content.length > 200) {
        alert("留言字符太多!");
        return;
    }
    $.ajax({
        type: "post",
        dataType: "json",
        data: "contentid=" + contentid + "&classid=" + classid + "&name=" + encodeURIComponent(uName) + "&phone=" + encodeURIComponent(uPhone) + "&email=" + encodeURIComponent(uEmail) + "&code=" + encodeURIComponent(uCode) + "&title=" + encodeURIComponent(title) + "&content=" + encodeURIComponent(content),
        url: site.Dir + "question/ajax.aspx?oper=ajaxQuestionAdd&clienttime=" + Math.random(),
        error: function (XmlHttpRequest, textStatus, errorThrown) { if (XmlHttpRequest.responseText != "") { alert(XmlHttpRequest.responseText); } },
        success: function (d) {
            if (d.result == "1") {
                $("#questionName").val("");
                $("#questionPhone").val("");
                $("#questionEmail").val("");
                $("#questionTitle").val("");
                $("#questionContent").val("");
                $("#questionCode").val("");
                alert(d.returnval);
                ajaxQuestionList(1);
            }
            else {
                alert(d.returnval);
            }
            _jdls_GetRefreshCode('imgCode');
        }
    });
}
function ajaxQuestionDelete(contentid, questionid) {
    $.ajax({
        type: "post",
        dataType: "json",
        data: "contentid=" + contentid + "&questionid=" + questionid,
        url: site.Dir + "question/ajax.aspx?oper=ajaxQuestionDelete&clienttime=" + Math.random(),
        error: function (XmlHttpRequest, textStatus, errorThrown) { if (XmlHttpRequest.responseText != "") { alert(XmlHttpRequest.responseText); } },
        success: function (d) {
            if (d.result == "1") {
                alert(d.returnval);
                ajaxQuestionList(page);
            }
            else {
                alert(d.returnval);
            }
        }
    });
}
function ajaxQuestionSiftSymbol(str) {
    str = str.replace(/[\-]{10}/g, "").replace(/[,]{10}/g, "").replace(/[\.]{10}/g, "");
    return (str);
}
function ajaxQuestionReply2(questionid) {
    $(".replyarea").hide();
    $("#replyarea" + questionid).show();
}
function ajaxQuestionReply(contentid,questionid) {
    var replycontent = ajaxQuestionSiftSymbol($("#replycontent" + questionid).val());
    if (!replycontent) {
        alert("请填写回复内容!");
        return;
    }
    $.ajax({
        type: "post",
        dataType: "json",
        data: "contentid=" + contentid + "&questionid=" + questionid + "&replycontent=" + encodeURIComponent(replycontent),
        url: site.Dir + "question/ajax.aspx?oper=ajaxQuestionReply&clienttime=" + Math.random(),
        error: function (XmlHttpRequest, textStatus, errorThrown) { if (XmlHttpRequest.responseText != "") { alert(XmlHttpRequest.responseText); } },
        success: function (d) {
            if (d.result == "1") {
                alert(d.returnval);
                ajaxQuestionList(page);
            }
            else {
                alert(d.returnval);
            }
        }
    });
}