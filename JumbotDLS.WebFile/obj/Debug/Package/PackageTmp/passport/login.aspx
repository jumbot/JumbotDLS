﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="login.aspx.cs" Inherits="JumbotDLS.WebFile.Passport._login" %>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>用户登录 <%=site.Name%></title>
    <link rel="stylesheet" href="/skins/default/css/public.css">
    <link rel="stylesheet" href="/skins/default/css/css.css">
    <script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
    <script type="text/javascript" src="../_data/global.js"></script>
    <script src="/skins/default/js/public.js"></script>
</head>
<body class="login-page">
    <div class="hd-top">
        <div class="hd-top-bg">
        </div>
        <div class="content">
            <div class="wrap cls">
                <div class="hd-top-right fr">
                    <a href="/">首页</a> | <a href="#">帮助</a> | <a href="register.aspx"><i class="iconfont"></i>
                        <em>注册</em></a>
                </div>
            </div>
        </div>
    </div>
    <div class="login-box" style="z-index: 999">
        <div class="hd">
            欢迎注册</div>
        <div class="bd">
            <form id="loginform" class="psp-form">
            <div class="inner">
                <div class="input-div">
                    <input name="txtUserName" id="txtUserName" class="input" type="text" placeholder="用户名：">
                </div>
                <div class="input-div">
                    <input type="password" name="txtUserPass" id="txtUserPass" class="input" placeholder="密&nbsp;&nbsp;码：">
                </div>
                <div class="input-div">
                    <input id="txtUserCode" name="txtUserCode" maxlength="4" size="4" onpaste="return false"  style="width: 30%" class="input" type="text" placeholder="验证码">
                    <img class="vcode-img" id="imgCode" onclick="_jdls_GetRefreshCode('imgCode',36);"
                        src="" align="absmiddle" /><a href="javascript:void(0);" id="_vcode_txt" onclick="_jdls_GetRefreshCode('imgCode',36);return false;">换一张</a>
                    <img id="imgRead" src="../statics/common/maintb.gif" align="absmiddle" style="cursor: pointer"
                        alt="收听验证码" title="收听验证码" onclick="ajaxPlayCodeVoice('player');" /><span id="player"></span>

                </div>
                <div class="other cls">
                    <div class="fl checkbox ver toggleActive">
                        <i class="iconfont noActive">&#xe6ad;</i> <i class="iconfont active">&#xe6ac;</i>
                        <em>记住我</em>
                    </div>
                    <a href="getpassword.aspx" class="fr forget-pass">忘记密码？</a>
                    <input type="hidden" id="txtRefer" name="txtRefer" value="<%=Referer%>" />
                    <input type="hidden" id="txtLoginType" name="LoginType" value="7" />
                </div>
            </div>
            <div class="input-div">
                <input class="btn input" onclick="ajaxLogin();" id="btnLogin" type="submit" value="登  录">
            </div>
            </form>
        </div>
    </div>
    <div class="footer2" style="z-index: 100">
        <div class="ft-inner wrap">
            <div class="ft-top ">
                <div class="nav ">
                    <a href="#">关于我们</a> | <a href="#">联系我们</a> | <a href="#">帮助</a> | <a href="#">合作</a>
                </div>
            </div>
            <div class="copyright">
                <span>北京子木将博科技有限公司</span> <span>版权所有Copyright &copy; 2016-2020 </span><span>www.junbot.net</span>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            if (site.AllowReg) { _jdls_SetOAuthBar(); }
            _jdls_GetRefreshCode('imgCode', 36);
            var act = q('act');
            if (act != "logout")
                sendRequest(1);
            else
                chkLogout();

        });
        var gSubmitTimes = 0;
        function oLogin() {
        }
        function ajaxLogin() {
            if (gSubmitTimes > 4) {
                alert('请想想密码再来');
                return;
            }
            var uName = $("#txtUserName").val();
            var uPass = "";
            var uCode = $("#txtUserCode").val();
            var oPass = $('#txtUserPass').val();
            var typeNum = $("#txtLoginType").val();
            if (uName == "") {
                alert('请填写用户名');
                $("#txtUserName").focus();
                $("#txtUserPass").val(oPass);
                return;
            }
            if (oPass == "") {
                alert('请填写密码');
                $("#txtUserPass").focus();
                return;
            }
            if (uCode == "") {
                alert("请填写验证码");
                $("#txtUserPass").val(oPass);
                $("#txtUserCode").focus();
                return;
            }
            uPass = JumbotDLS.MD5(oPass);
            $('#txtUserPass').val(uPass);
            $.ajax({
                type: "post",
                dataType: "html",
                url: "ajax.aspx?oper=ajaxLogin&clienttime=" + Math.random(),
                data: "name=" + encodeURIComponent(uName) + "&pass=" + encodeURIComponent(uPass) + "&code=" + encodeURIComponent(uCode) + "&type=" + typeNum,
                error: function (XmlHttpRequest, textStatus, errorThrown) { alert(XmlHttpRequest.responseText); },
                success: function (d) {
                    if (d == "ok")
                        top.location.href = "logining.aspx?url=" + encodeURIComponent($("#txtRefer").val());
                    else {
                        gSubmitTimes = gSubmitTimes + 1;
                        alert(d);
                        $('#txtUserPass').val(oPass);
                        _jdls_GetRefreshCode('imgCode', 36);
                    }
                }
            });
        }
        function chkLogout() {
            top.location.href = "logout.aspx";
        }
        function sendRequest(p) {
            if (p == 0) {
                oLogin();
                return;
            }
            $.ajax({
                url: site.Dir + "user/ajax.aspx?clienttime=" + Math.random(),
                type: "get",
                dataType: "json",
                success: function (d) {
                    if (d.result == "1")
                        if (q('refer') != "")
                            top.location.href = decodeURIComponent(q('refer'));
                        else
                            top.location.href = site.Dir + 'user/default.aspx';
                    else
                        oLogin();
                }
            });
        }
        function SavePassword() {
            if (document.getElementById("savePassword").checked) {
                var username = $("#username").val();
                var password = $("#password").val();
                window.sessionStorage.username = username;
                window.sessionStorage.password = password;
                localStorage.rmbPassword = true;
            }
            else {
                localStorage.rmbPassword = false;
            }
        }
    </script>
</body>
</html>
