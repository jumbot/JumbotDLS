var myChart;
//var domCode = document.getElementById('sidebar-code');
//var domGraphic = document.getElementById('graphic');
var domMain = document.getElementById('main');
//var domMessage = document.getElementById('wrong-message');
var iconResize = document.getElementById('icon-resize');
var needRefresh = false;

function autoResize() {
    if (iconResize.className == 'icon-resize-full') {
        focusCode();
        iconResize.className = 'icon-resize-small';
    }
    else {
        focusGraphic();
        iconResize.className = 'icon-resize-full';
    }
}

function focusCode() {
    domCode.className = 'span8 ani';
    domGraphic.className = 'span4 ani';
}

function focusGraphic() {
    //domCode.className = 'span4 ani';
    //domGraphic.className = 'span8 ani';
    if (needRefresh) {
        myChart.showLoading();
        setTimeout(refresh, 1000);
    }
}


function refresh(isBtnRefresh){
    if (isBtnRefresh) {
        needRefresh = true;
        focusGraphic();
        return;
    }
    needRefresh = false;
    if (myChart && myChart.dispose) {
        myChart.dispose();
    }
    myChart = echarts.init(domMain);
    window.onresize = myChart.resize;
    //(new Function(editor.doc.getValue()))();
    //data();
    myChart.setOption(option, true);
    window.onresize = myChart.resize;

    //domMessage.innerHTML = '';
}

var echarts;
var developMode = false;

var fileLocation = '/statics/echarts/www/js/echarts-map';
    require.config({
        paths:{ 
            echarts: fileLocation,
            'echarts/chart/line': fileLocation,
            'echarts/chart/bar': fileLocation,
            'echarts/chart/scatter': fileLocation,
            'echarts/chart/k': fileLocation,
            'echarts/chart/pie': fileLocation,
            'echarts/chart/radar': fileLocation,
            'echarts/chart/map': fileLocation,
            'echarts/chart/chord': fileLocation,
            'echarts/chart/force': fileLocation
        }
    });

require(
    [
        'echarts',
        'echarts/chart/line',
        'echarts/chart/bar',
        'echarts/chart/scatter',
        'echarts/chart/k',
        'echarts/chart/pie',
        'echarts/chart/radar',
        'echarts/chart/force',
        'echarts/chart/chord',
        'echarts/chart/map'
    ],
    requireCallback
);

function requireCallback (ec) {
    echarts = ec;
    if (myChart && myChart.dispose) {
        myChart.dispose();
    }
    myChart = echarts.init(domMain);
    
//ajaxGetContent(1);


}