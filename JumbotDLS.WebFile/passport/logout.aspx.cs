﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using JumbotDLS.Common;

namespace JumbotDLS.WebFile.Passport
{
    public partial class _logout : JumbotDLS.UI.UserCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (new JumbotDLS.DAL.Normal_UserDAL().ChkUserLogout(q("userkey")))
            {
                if(q("refer") != "")
                    FinalMessage("已清除您的登录信息", q("refer"), 0);
                else
                    FinalMessage("已清除您的登录信息", Request.ServerVariables["HTTP_REFERER"].ToString(), 0);
            }
            else
                FinalMessage("无法确定您的身份", site.Home, 0);
        }
    }
}
