﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using JumbotDLS.Common;

namespace JumbotDLS.WebFile.Passport
{
    public partial class _active : JumbotDLS.UI.BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string uUserName = q("username");
            string uEmail = q("email");
            string uUserSign = q("usersign");
            doh.Reset();
            doh.ConditionExpress = "username=@username and usersign=@usersign";
            doh.AddConditionParameter("@username", uUserName);
            doh.AddConditionParameter("@usersign", uUserSign);
            doh.AddFieldItem("State", 1);
            doh.AddFieldItem("UserSign", "");
            if (doh.Update("jdls_normal_user") == 1)
                Response.Write("<script>alert('您的帐号已激活成功');window.location.href='" + site.Dir + "passport/login.aspx';</script>");
            else
                Response.Write("<script>alert('参数失败');window.close();</script>");
        }
    }
}
