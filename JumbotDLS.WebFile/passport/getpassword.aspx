﻿<%@ Page Language="C#" AutoEventWireup="True" Codebehind="getpassword.aspx.cs" Inherits="JumbotDLS.WebFile.Passport._getpassword" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>用户登录 <%=site.Name%></title>
    <link   type="text/css" rel="stylesheet" href="../_data/global.css" />
    <link rel="stylesheet" href="/skins/default/css/public.css">
    <link rel="stylesheet" href="/skins/default/css/css.css">
    <script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
    <script type="text/javascript" src="../_data/global.js"></script>
    <script src="/skins/default/js/public.js"></script>
</head>
<body class="login-page">
    <div class="hd-top">
        <div class="hd-top-bg">
        </div>
        <div class="content">
            <div class="wrap cls">
                <div class="hd-top-right fr">
                    <a href="/">首页</a> | <a href="#">帮助</a> | <a href="login.aspx"><i class="iconfont"></i>
                        <em>登录</em></a>
                </div>
            </div>
        </div>
    </div>
    <div class="login-box" style="height: 350px">
        <div class="hd">
            找回密码</div>
        <div class="bd">
            <form action="ajax.aspx?oper=ajaxResetPassword" method="post" class="psp-form" id="resetPwdForm" name="lostpassForm">
            <div class="inner">
                <div class="rediv">
                    <input name="txtEmail" id="txtEmail" class="input" type="text" placeholder="邮箱："/>
                     <span id="tipEmail" class="tip"></span>
                </div>
                <div class="rediv">
                    <input type="text" name="txtUserName" id="txtUserName" class="input" placeholder="用户名："/>
                     <span id="tipUserName" class="tip"></span>
                </div>
              
            </div>
            <div class="input-div">
                <input class="btn input" id="btnLogin" type="submit" value="确定">
            </div>
            </form>
        </div>
    </div>
    <div class="footer2">
        <div class="ft-inner wrap">
            <div class="ft-top ">
                <div class="nav ">
                    <a href="#">关于我们</a> | <a href="#">联系我们</a> | <a href="#">帮助</a> | <a href="#">合作</a>
                </div>
            </div>
            <div class="copyright">
                <span>北京子木将博科技有限公司</span> <span>版权所有Copyright &copy; 2016-2020 </span><span>www.junbot.net</span>
            </div>
        </div>
    </div>
   <script type="text/javascript">
       $(document).ready(function () {
           $('#resetPwdForm').ajaxForm({
               beforeSubmit: JumbotDLS.AjaxFormSubmit,
               success: function (data) {
                   JumbotDLS.Eval(data);
               }
           });
           $.formValidator.initConfig({ onError: function (msg) { /*alert(msg);*/ } });
           $("#txtEmail")
		.formValidator({ tipid: "tipEmail", onshow: "请输入注册时的邮箱", onfocus: "请输入注册时的邮箱" })
		.InputValidator({ min: 8, max: 50, onerror: "你输入的邮箱非法,请确认" })
		.RegexValidator({ regexp: "email", datatype: "enum", onerror: "格式不正确" });
           $("#txtUserName")
		.formValidator({ tipid: "tipUserName", onshow: "输入注册时的用户名", onfocus: "输入注册时的用户名" })
		.InputValidator({ min: 4, max: 20, onerror: "你输入的用户名非法,请确认" })
		.RegexValidator({ regexp: "username", datatype: "enum", onerror: "输入不正确" });
       });
       //Form验证
       JumbotDLS.AjaxFormSubmit = function (item) {
           try {
               if ($.formValidator.PageIsValid('1')) {
                   JumbotDLS.Loading.show("正在处理，请稍等...");
                   return true;
               } else {
                   return false;
               }
           } catch (e) {
               return false;
           }
       }
</script>
</body>
</html>
