﻿<%@ Page Language="C#" AutoEventWireup="True" Codebehind="register.aspx.cs" Inherits="JumbotDLS.WebFile.Passport._register" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>新用户注册
        <%=site.Name%></title>
    <link   type="text/css" rel="stylesheet" href="../_data/global.css" />
    <link rel="stylesheet" href="/skins/default/css/public.css">
    <link rel="stylesheet" href="/skins/default/css/css.css">
 <script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js"></script>
</head>
<body class="login-page">
    <div class="hd-top">
        <div class="hd-top-bg"></div>
        <div class="content">
            <div class="wrap cls">
                <div class="hd-top-right fr">
                    <a href="/">首页</a> |
                    <a href="#">帮助</a> |
                    <a href="login.aspx"><i class="iconfont">&#xe677;</i> <em>登录</em></a>
                </div>
            </div>
        </div>
    </div>

    <div class="login-box" style="height: 575px; top: 38%;">
        <div class="hd">
            欢迎注册</div>
        <div class="bd">
            <form id="registerForm" action="ajax.aspx?oper=ajaxRegister" method="post" class="psp-form">
            <div class="inner">
                <div class="rediv" >
                    <input name="txtEmail" id="txtEmail" class="input error" type="text" placeholder="Email邮箱："/>
                    <span id="tipEmail" class="tip"></span>
                </div>
                <div class="rediv">
                    <input name="txtUserName" id="txtUserName" class="input" type="text" placeholder="用户名："/>
                     <span id="tipUserName" class="tip"></span>
                </div>
                <div class="rediv">
                    <input name="txtPass1" id="txtPass1" class="input" type="password" placeholder="密&nbsp;&nbsp;码："/>
                     <span id="tipPass1" class="tip"></span>
                </div>
                <div class="rediv">
                    <input name="txtPass2" id="txtPass2" class="input" type="password" placeholder="确认密码："/>
                     <span id="tipPass2" class="tip"></span>
                </div>
                <div class="rediv">
                    <input id="txtCode" name="txtCode" style="width: 30%" class="input" type="text" placeholder="验证码"/>
                    <img class="vcode-img" id="imgCode" onclick="_jdls_GetRefreshCode('imgCode',36);" src="" align="absmiddle" /><a href="javascript:void(0);" id="_vcode_txt" onclick="_jdls_GetRefreshCode('imgCode',36);return false;">换一张</a> <img id="imgRead" src="../statics/common/maintb.gif" align="absmiddle" style="cursor: pointer" alt="收听验证码" title="收听验证码" onclick="ajaxPlayCodeVoice('player');" /><br/>
                    <span id="player"></span><em id="tipCode"></em></span>
                </div>
            </div>
            <div class="input-div">
                <input class="btn input" onclick="SetMD5(true);" type="submit" value="注  册"/>
            </div>
            </form>
        </div>
    </div>
    <div class="footer2" style="display: none;">
        <div class="ft-inner wrap">
            <div class="ft-top ">
                <div class="nav ">
                    <a href="#">关于我们</a> | <a href="#">联系我们</a> | <a href="#">帮助</a> | <a href="#">合作</a>
                </div>
            </div>
            <div class="copyright">
                <span>北京子木将博科技有限公司</span> <span>版权所有Copyright &copy; 2016-2020 </span><span>www.junbot.net</span>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var pass1 = '';
        var pass2 = '';
        $(document).ready(function () {
            if (site.AllowReg) { _jdls_SetOAuthBar(); }
            _jdls_GetRefreshCode('imgCode', 36);
            $('#registerForm').ajaxForm({
                beforeSubmit: JumbotDLS.AjaxFormSubmit,
                success: function (data) {
                    JumbotDLS.Eval(data);
                }
            });
            $.formValidator.initConfig({ onError: function (msg) { /*alert(msg);*/ } });
            $("#txtEmail")
		.formValidator({ tipid: "tipEmail", onshow: "请输入常用的邮箱", onfocus: "请输入常用的邮箱" })
		.InputValidator({ min: 8, max: 50, onerror: "邮箱非法,请确认" })
		.RegexValidator({ regexp: "email", datatype: "enum", onerror: "格式不正确" })
		.AjaxValidator({
		    type: "get",
		    url: "ajax.aspx?oper=checkemail&id=0&clienttime=" + Math.random(),
		    datatype: "json",
		    success: function (d) {
		        if (d.result == "1")
		            return true;
		        else
		            return false;
		    },
		    buttons: $("#btnSave"),
		    error: function () { alert("服务器繁忙，请稍后再试"); },
		    onerror: "邮箱已被占用",
		    onwait: "正在校验邮箱的合法性，请稍候..."
		});
            $("#txtUserName")
		.formValidator({ tipid: "tipUserName", onfocus: "支持中英文、数字、下划线" })
		.InputValidator({ min: 4, max: 20, onerror: "4-20个字符" })
		.RegexValidator({ regexp: "username", datatype: "enum", onerror: "必须以汉字或字母开头" })
		.AjaxValidator({
		    type: "get",
		    url: "ajax.aspx?oper=checkname&id=0&clienttime=" + Math.random(),
		    datatype: "json",
		    success: function (d) {
		        if (d.result == "1")
		            return true;
		        else
		            return false;
		    },
		    buttons: $("#btnSave"),
		    error: function () { alert("服务器繁忙，请稍后再试"); },
		    onerror: "用户名已被注册",
		    onwait: "正在校验用户名的合法性，请稍候..."
		});
            $("#txtPass1").formValidator({ tipid: "tipPass1", onfocus: "请输入6-14位密码" }).InputValidator({ min: 6, max: 32, onerror: "密码6-14位" });
            $("#txtPass2").formValidator({ tipid: "tipPass2", onfocus: "两次密码必须一致" }).InputValidator({ min: 6, max: 32, onerror: "密码6-14位,请确认" }).CompareValidator({ desID: "txtPass1", operateor: "=", onerror: "两次密码不一致" });
            $("#txtCode").formValidator({ tipid: "tipCode", onfocus: "验证码必须填写" }).InputValidator({ min: 4, max: 4, onerror: "4位验证码" });
        });
        //Form验证
        JumbotDLS.AjaxFormSubmit = function (item) {
            try {

                if ($.formValidator.PageIsValid('1')) {
                    JumbotDLS.Loading.show("正在处理，请稍等...");
                    return true;
                } else {
                    SetMD5(false);
                    return false;
                }
            } catch (e) {
                SetMD5(false);
                return false;
            }
        }
        function SetMD5(n) {
            if (n) {
                pass1 = $('#txtPass1').val();
                pass2 = $('#txtPass2').val();
                $('#txtPass1').val(JumbotDLS.MD5(pass1));
                $('#txtPass2').val(JumbotDLS.MD5(pass2));
            } else {
                $('#txtPass1').val(pass1);
                $('#txtPass2').val(pass2);
            }
        }
</script>

</body>
</html>