﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using JumbotDLS.Utils;
using JumbotDLS.Common;

namespace JumbotDLS.WebFile.Passport
{
    public partial class _register_step2 : JumbotDLS.UI.FrontPassport
    {
        public string _UserName = "";
        public string _Email = "";
        public string _UserSign = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            _UserName = q("username");
            _Email = q("email");
            _UserSign = q("usersign");

            if (Cookie.GetValue(site.CookiePrev + "user") != null)
            {
                FinalMessage("请先注销当前用户再进行注册!", site.Home, 0);
                Response.End();
            }
            if (!site.AllowReg)
            {
                FinalMessage("对不起，本站暂停注册!", site.Home, 0);
                Response.End();
            }
            if (JumbotDLS.Utils.Session.Get("jdls_user_register") != "1")
            {
                Response.Write("请勿随便试这个功能");
                Response.End();
            }
        }

    }
}
