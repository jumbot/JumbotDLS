﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile
{
    public partial class _default : JumbotDLS.UI.FrontHtml
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 8;//脚本过期时间
           string TxtStr = string.Empty;
            JumbotDLS.DAL.TemplateEngineDAL teDAL = new JumbotDLS.DAL.TemplateEngineDAL();
            TxtStr = teDAL.GetSiteDefaultPage();
            teDAL.ReplaceSHTMLTag(ref TxtStr);
            teDAL.ReplaceUserTag(ref TxtStr);
            Response.Write(TxtStr);//直接输出
        }
    }
}
