﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
namespace JumbotDLS.WebFile.Plus
{
    public partial class _codevoice : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "audio/mpeg";
            Response.WriteFile("../statics/sound/begin.mp3");
            string checkCode = q("code");
            if (checkCode.Length > 0)
                for (int i = 0; i < checkCode.Length; i++)
                {
                    Response.WriteFile("../statics/sound/" + checkCode[i] + ".mp3");
                }
            Response.WriteFile("../statics/sound/end.mp3");
        }
        /// <summary>
        /// 获取querystring
        /// </summary>
        /// <param name="s">参数名</param>
        /// <returns>返回值</returns>
        public string q(string s)
        {
            if (HttpContext.Current.Request.QueryString[s] != null && HttpContext.Current.Request.QueryString[s] != "")
            {
                return JumbotDLS.Utils.Strings.SafetyQueryS(HttpContext.Current.Request.QueryString[s].ToString());
            }
            return string.Empty;
        }
    }
}
