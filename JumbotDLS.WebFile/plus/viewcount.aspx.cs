﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Plus
{
    public partial class _viewcount : JumbotDLS.UI.FrontHtml
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 8;//脚本过期时间
            if (!CheckFormUrl(false))//不可直接在url下访问
            {
                Response.End();
            }
            string _channeltype = "article";//固定别改
            string _contentid = Str2Str(q("id"));
            int _viewnum = 0;
            if (JumbotDLS.Utils.Cookie.GetValue(_channeltype + "ViewNum" + _contentid) == null && Str2Int(q("addit")) == 1)
            {
                doh.Reset();
                doh.ConditionExpress = "id=@id";
                doh.AddConditionParameter("@id", _contentid);
                _viewnum = doh.Add("jdls_module_" + _channeltype, "ViewNum");
                JumbotDLS.Utils.Cookie.SetObj(_channeltype + "ViewNum" + _contentid, 10, "ok");
            }
            else
            {
                doh.Reset();
                doh.ConditionExpress = "id=@id";
                doh.AddConditionParameter("@id", _contentid);
                _viewnum = Str2Int(doh.GetField("jdls_module_" + _channeltype, "ViewNum").ToString(), 0);
            }
            Response.Write(JumbotDLS.Utils.Strings.Html2Js(_viewnum.ToString()));
        }
    }
}