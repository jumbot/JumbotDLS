﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Plus
{
    public partial class _download_file : JumbotDLS.UI.UserCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string _file = q("file");
            string _name = JumbotDLS.Utils.Strings.FilterFFSymbol(q("name"));
            DownloadFile(_file, _name);
        }
    }
}
