﻿namespace JumbotDLS.WebFile.Plus {
    /// <summary>
    /// Rss 类。
    /// </summary>
    /// <remarks>
    /// 自动生成的类。
    /// </remarks>
    public partial class _rss {

        /// <summary>
        /// lblChannelId 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChannelId;

        /// <summary>
        /// lblClassId 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblClassId;
    }
}
