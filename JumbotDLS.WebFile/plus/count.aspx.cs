﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Web;
using System.Data;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
namespace JumbotDLS.WebFile.Plus
{
    public partial class _count : JumbotDLS.UI.BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //防止网页后退--禁止缓存    
            Response.Expires = 0;
            Response.Buffer = true;
            Response.ExpiresAbsolute = DateTime.Now.AddSeconds(-1);
            Response.AddHeader("pragma", "no-cache");
            Response.CacheControl = "no-cache";

            string img1 = site.Dir + "_data/powered_by_jumbotcms.jpg";
            string img2 = site.Dir + "_data/powered_by_jumbotcms.jpg";
            if (HttpContext.Current.Request.UrlReferrer != null)
            {
                if (JumbotDLS.Utils.Validator.IsCommonDomain(HttpContext.Current.Request.UrlReferrer.Host))
                {
                    string _domain = HttpContext.Current.Request.UrlReferrer.Host;
                    string _fromurl = q("url") == "" ? HttpContext.Current.Request.UrlReferrer.AbsoluteUri : q("url");
                    if (JumbotDLS.Utils.Cookie.GetValue("kjcxV1") == null)
                    {
                        doh.Reset();
                        doh.ConditionExpress = "domain=@domain";
                        doh.AddConditionParameter("@domain", _domain);
                        doh.AddFieldItem("LastLoginTime", DateTime.Now.ToString());
                        doh.AddFieldItem("FromUrl", _fromurl);
                        if (doh.Update("jdls_official_user") == 0)
                        {
                            doh.Reset();
                            doh.AddFieldItem("Domain", _domain);
                            doh.AddFieldItem("LastLoginTime", DateTime.Now.ToString());
                            doh.AddFieldItem("FromUrl", _fromurl);
                            doh.Insert("jdls_official_user");
                        }
                        JumbotDLS.Utils.Cookie.SetObj("kjcxV1", 0, "ok");
                    }
                    img2 = site.Dir + "_data/cache/site/" + _domain + ".jpg";
                    JumbotDLS.Utils.DirFile.CopyFile(img1, img2, false);
                }
            }
            ShowImage(img2);
        }
        public void ShowImage(string _url)
        {
            try
            {
                System.Drawing.Image img = System.Drawing.Image.FromFile(Server.MapPath(_url));
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                img.Save(ms, JumbotDLS.Utils.ImageHelp.ImgFormat(_url));
                Response.ClearContent();
                Response.BinaryWrite(ms.ToArray());
                Response.ContentType = "image/jpeg";//指定输出格式为图形
                img.Dispose();
                Response.End();
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }
    }
}
