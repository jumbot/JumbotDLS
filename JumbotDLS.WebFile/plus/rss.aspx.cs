﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Plus
{
    public partial class _rss : JumbotDLS.UI.FrontHtml
    {
        protected void Page_Unload(object sender, EventArgs e)
        {
            SavePageLog(1);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 8;//脚本过期时间
            int CurrentPage = Int_ThisPage();
            string ClassId = this.lblClassId.Text == "{$ClassId}" ? Str2Str(q("id")) : this.lblClassId.Text;
            string ChannelId = this.lblChannelId.Text == "{$ChannelId}" ? Str2Str(q("ChannelId")) : this.lblChannelId.Text;
            doh.Reset();
            if (q("preview") == "1")
                doh.ConditionExpress = "id=@id";
            else
                doh.ConditionExpress = "id=@id and Enabled=1";
            doh.AddConditionParameter("@id", ChannelId);
            if (!doh.Exist("jdls_normal_channel"))
            {
                FinalMessage("频道不存在或被禁用!", site.Home, 0, 8);
                Response.End();
            }
            JumbotDLS.Entity.Normal_Channel _Channel = new JumbotDLS.DAL.Normal_ChannelDAL().GetEntity(ChannelId);
            string ChannelName = _Channel.Title;
            bool ChannelIsHtml =_Channel.IsHtml;
            string ClassName = string.Empty;
            string ClassCode = string.Empty;
            int PSize = Str2Int(q("pagesize"), 20);
            if (PSize > 100)
                PSize = 100;
            if (ClassId == "0")
            {
                ClassName = "全部栏目";
            }
            else
            {
                doh.Reset();
                doh.SqlCmd = "SELECT * FROM [jdls_normal_class] WHERE [IsOut]=0 AND [ChannelId]=" + ChannelId + " and [Id]=" + ClassId;
                DataTable dtClass = doh.GetDataTable();
                if (dtClass.Rows.Count == 0)
                {
                    FinalMessage("栏目不存在或已被删除!", site.Home, 0, 8);
                    Response.End();
                }
                ClassName = dtClass.Rows[0]["Title"].ToString();
                ClassCode = dtClass.Rows[0]["code"].ToString();
                dtClass.Clear();
                dtClass.Dispose();
            }
            JumbotDLS.DAL.TemplateEngineDAL teDAL = new JumbotDLS.DAL.TemplateEngineDAL(ChannelId);
            teDAL.IsHtml = site.IsHtml;
            Response.Charset = "utf-8";
            Response.ContentType = "text/xml";
            Response.Expires = 0;
            Response.Buffer = true;
            Response.ExpiresAbsolute = DateTime.Now.AddSeconds(-1);
            Response.AddHeader("pragma", "no-cache");
            Response.CacheControl = "no-cache";
            StringBuilder strCode = new StringBuilder();
            strCode.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n");
            strCode.Append("<?xml-stylesheet type=\"text/css\" href=\"http://img.china.alibaba.com/news/rsstyle.css\" ?>\r\n");
            strCode.Append("<rss version=\"2.0\">\r\n");
            strCode.Append("    <channel>\r\n");
            if (ClassId != "0")
            {
                strCode.Append("        <title>" + ClassName + "_" + ChannelName + "_" + site.Name + "</title>\r\n");
                strCode.Append("        <link><![CDATA[" + GetURL(Go2Class(1, ChannelIsHtml, ChannelId, ClassId, false)) + "]]></link>\r\n");
            }
            else
            {
                strCode.Append("        <title>" + ChannelName + "_" + site.Name + "</title>\r\n");
                strCode.Append("        <link><![CDATA[" + GetURL(Go2Channel(1, ChannelIsHtml, ChannelId, false)) + "]]></link>\r\n");
            }
            strCode.Append("        <description><![CDATA[" + site.Description + "]]></description>\r\n");
            strCode.Append("        <language>zh-cn</language>\r\n");
            strCode.Append("        <copyright><![CDATA[Copyright (C) " + site.Name + "]]></copyright>\r\n");
            strCode.Append("        <pubDate><![CDATA[" + System.DateTime.Now.ToUniversalTime().ToString("r") + "]]></pubDate>\r\n");
            strCode.Append("        <category><![CDATA[]]></category>\r\n");


            string whereStr = "";
            if (ClassId != "0")
                whereStr = " [ClassID] in (Select id FROM [jdls_normal_class] WHERE [Code] LIKE '" + ClassCode + "%') and [IsPass]=1 AND [ChannelId]=" + ChannelId;
            else
                whereStr = " [ChannelId]=" + ChannelId;
            int page = Int_ThisPage();
            string sqlStr = "";
            sqlStr = JumbotDLS.Utils.SqlHelp.GetSql0("id,title,Author,AddDate,Img,ChannelId,ClassId,Summary", "jdls_module_" + teDAL.MainChannel.Type, "id", PSize, page, "desc", whereStr);
            doh.Reset();
            doh.SqlCmd = sqlStr;
            DataTable dtContent = doh.GetDataTable();
            for (int i = 0; i < dtContent.Rows.Count; i++)
            {
                string aId = dtContent.Rows[i]["Id"].ToString();
                string aTitle = dtContent.Rows[i]["Title"].ToString();
                string aAuthor = dtContent.Rows[i]["Author"].ToString();
                string aAddDate = dtContent.Rows[i]["AddDate"].ToString();
                string aImg = dtContent.Rows[i]["Img"].ToString();
                string aSummary = dtContent.Rows[i]["Summary"].ToString();
                string aClassId = dtContent.Rows[i]["ClassId"].ToString();
                strCode.Append("        <item>\r\n");
                strCode.Append("            <title><![CDATA[" + aTitle + "]]></title>\r\n");
                strCode.Append("            <description><![CDATA[" + aSummary + "]]></description>\r\n");
                strCode.Append("            <link><![CDATA[" + GetURL(Go2View(1, ChannelIsHtml, ChannelId, aId, false)) + "]]></link>\r\n");
                strCode.Append("            <author><![CDATA[" + aAuthor + "]]></author>\r\n");
                strCode.Append("            <guid><![CDATA[" + GetURL(Go2View(1, ChannelIsHtml, ChannelId, aId, false)) + "]]></guid>\r\n");
                strCode.Append("            <category><![CDATA[" + (new JumbotDLS.DAL.Normal_ClassDAL().GetClassName(aClassId)) + "]]></category>\r\n");
                if(aImg.Length>0)
                    strCode.Append("            <subImagePath><![CDATA[" + GetURL(aImg) + "]]></subImagePath>\r\n");
                strCode.Append("            <pubDate><![CDATA[" + Convert.ToDateTime(aAddDate).ToUniversalTime().ToString("r") + "]]></pubDate>\r\n");

                strCode.Append("            <comments></comments>\r\n");
                strCode.Append("            <source></source>\r\n");
                strCode.Append("        </item>\r\n");
            }
            strCode.Append("    </channel>\r\n");
            strCode.Append("</rss>\r\n");
            Response.Write(strCode.ToString());
            dtContent.Clear();
            dtContent.Dispose();
        }
        private string GetURL(string _url)
        {
            if (_url.StartsWith("http://") || _url.StartsWith("https://"))
                return _url;
            return site.Url + _url;
        }
    }
}
