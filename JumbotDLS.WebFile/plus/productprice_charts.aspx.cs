﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfoSoftGlobal;
namespace JumbotDLS.WebFile.plus
{
    public partial class productprice_charts : JumbotDLS.UI.BasicPage
    {
        public string xList = "";
        public string yList = "";
        public string PriceUnit = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string _id = Str2Str(q("id"));
            doh.Reset();
            doh.ConditionExpress = "id=" + _id + " and module='productprice'";
            object[] _values  = doh.GetFields("jdls_module_article", "CustomField01,CustomField02");
            string _PriceList = JumbotDLS.Utils.Strings.HtmlDecode(_values[0].ToString()).Replace("\r\n", "\r");
            PriceUnit = _values[1].ToString();
            string[] PriceArr = _PriceList.Split(new string[] { "\r" }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 1; i < PriceArr.Length; i++)
            {
                string[] _PriceArrs = PriceArr[i].Split('@');
                if (_PriceArrs.Length > 1)
                {
                    if (xList != "")
                    {
                        xList += ",";
                        yList += ",";
                    }
                    xList += "'" + _PriceArrs[0] + "'";
                    yList += "'" + _PriceArrs[1] + "'";
                }
            }
        }
    }
}