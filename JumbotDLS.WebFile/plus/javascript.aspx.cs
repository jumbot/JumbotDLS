﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Common;

namespace JumbotDLS.WebFile.Plus
{
    public partial class _javascript : JumbotDLS.UI.FrontHtml
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string _code = q("code");
            if (_code.Length != 64)
                Response.Write("参数有误");
            string strXmlFile = HttpContext.Current.Server.MapPath("~/_data/config/javascript.config");
            JumbotDLS.DBUtility.XmlControl XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
            string _TemplateContent = XmlTool.GetText("Lis/Li[Code=\"" + _code + "\"]/TemplateContent");
            XmlTool.Dispose();
            JumbotDLS.DAL.TemplateEngineDAL teDAL = new JumbotDLS.DAL.TemplateEngineDAL("0");
            string fileStr = ExecuteTags(_TemplateContent);
            Response.Write(JumbotDLS.Utils.Strings.Html2Js(fileStr));
        }
    }
}
