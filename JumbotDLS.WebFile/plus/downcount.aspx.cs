﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Plus
{
    public partial class _downcount : JumbotDLS.UI.FrontHtml
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 8;//脚本过期时间
            if (!ModuleIsOK(q("cType")))
            {
                this._response = "error";
                return;
            }
            string ChannelType = q("cType");
            string ContentId = Str2Str(q("id"));
            doh.Reset();
            doh.ConditionExpress = "id=@id";
            doh.AddConditionParameter("@id", ContentId);
            Response.Write(JumbotDLS.Utils.Strings.Html2Js(Str2Str(doh.GetField("jdls_module_" + ChannelType, "DownNum").ToString())));
        }
    }
}