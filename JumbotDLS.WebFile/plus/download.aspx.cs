﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Plus
{
    public partial class _download : JumbotDLS.UI.UserCenter
    {
        public string Referer = "";
        public int NO = 0;
        public string UserChecked = "0";
        public string FileUrl = "";
        public string FileTitle = "";
        public string TipInfo = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 4;//脚本过期时间
            if (JumbotDLS.Utils.Session.Get("lastdowntime") != null)
            {
                if (Convert.ToInt32(JumbotDLS.Utils.Session.Get("lastdowntime")) + 2 > Convert.ToInt32(System.DateTime.Now.ToString("HHmmss")))
                {
                    FinalMessage("服务器繁忙，请稍后再试!", site.Dir, 0, 8);
                    Response.End();
                }
            }
            JumbotDLS.Utils.Session.Add("lastdowntime", System.DateTime.Now.ToString("HHmmss"));

            User_Load("", "html");
            id = Str2Str(q("id"));
            ChannelId = Str2Str(q("ChannelId"));
            ChannelType = q("ChannelType");
            string _modulelist2 = JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "ModuleList2");
            if (!_modulelist2.Contains("." + ChannelType + "."))
            {
                Response.Write("请不要恶意修改提交参数!");
                Response.End();
            }
            if (id == "0")
            {
                Response.Write("请不要恶意修改提交参数!");
                Response.End();
            }
            NO = Str2Int(q("NO"));
            bool _candown = false;
            doh.Reset();
            doh.ConditionExpress = "ChannelId=" + ChannelId + " AND id=" + id;
            if (JumbotDLS.Utils.Cookie.GetValue(site.CookiePrev + "admin") == null)
                doh.ConditionExpress += " AND [IsPass]=1";
            object[] _obj = doh.GetFields("jdls_module_" + ChannelType, "Title,Points," + ChannelType + "Url");
            if (_obj == null)
            {
                Response.Write("参数有误!");
                Response.End();
            }
            string _SourceTitle = _obj[0].ToString();
            int _Points = Str2Int(_obj[1].ToString(), 0);
            string downUrl = _obj[2].ToString().Replace("\r\n", "\r");
            bool _isvip = new JumbotDLS.DAL.Normal_UserDAL().IsVIPUser(UserId);
            if (!_isvip)//给用户扣除将元,VIP不扣除
            {
                TipInfo = "下载本资源需要" + _Points + "将元，您当前账户剩余" + UserPoints + "将元。";
                if (_Points > 0)
                {
                    doh.Reset();
                    doh.ConditionExpress = "ChannelId=" + ChannelId + " and [" + ChannelType + "Id]=" + id + " and UserId=" + UserId;
                    if (doh.Exist("jdls_module_" + ChannelType + "_downlogs"))//说明已经扣过
                    {
                        TipInfo = "您已经支付过本资源，本次下载为免费。";
                        _candown = true;
                    }
                }
                else
                {
                    _candown = true;
                }
            }
            else
            {
                TipInfo = "您是本站VIP会员，本次下载部会额外扣除将元。";
                _candown = true;
            }
            if (_candown)
            {
                if (downUrl == "")
                {
                    Response.Write("当前下载地址为空!");
                    Response.End();
                }
                string[] _DownUrl = downUrl.Split(new string[] { "\r" }, StringSplitOptions.None);
                if ((NO > _DownUrl.Length - 1) || NO < 0)
                {
                    Response.Write("请不要恶意修改提交参数!");
                    Response.End();
                }
                string _url = _DownUrl[NO];
                if (_url.Contains("|||"))
                    _url = _url.Substring(_url.IndexOf("|||") + 3, (_url.Length - _url.IndexOf("|||") - 3));
                if (!JumbotDLS.Utils.DirFile.FileExists(_url))
                {
                    Response.Write("下载文件不存在!");
                    Response.End();
                }
                DownloadFile(_url, JumbotDLS.Utils.Strings.FilterFFSymbol(_SourceTitle));
            }
            else
            {
                Response.Redirect("~/errordown.aspx");
                Response.End();
            }

        }
    }
}
