﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Plus
{
    public partial class _autotask : JumbotDLS.UI.BasicPage
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;
        private string _spliter = "'";
        protected void Page_Load(object sender, EventArgs e)
        {
            this._operType = q("oper");
            if (base.DBType == "0") _spliter = "#";
            switch (this._operType)
            {
                case "DeleteNotice":
                    DeleteNotice();
                    break;
                case "DeleteUnactivedUser":
                    DeleteUnactivedUser();
                    break;
                case "CreateDefaultPage":
                    CreateDefaultPage();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }
        private void DefaultResponse()
        {
            this._response = "未知操作";
        }
        private void DeleteNotice()
        {
            string _password = q("password");
            if (_password != System.Configuration.ConfigurationManager.AppSettings["AutoTask:Password"])
            {
                this._response = "密码错误";
                return;
            }
            doh.Reset();
            doh.ConditionExpress = "[State]=1 AND [ReadTime]<=" + _spliter + System.DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd") + _spliter;
            int _doCount = doh.Delete("jdls_normal_user_notice");
            doh.Reset();
            doh.ConditionExpress = "[State]=1 AND [ReadTime]<=" + _spliter + System.DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd") + _spliter;
            int _doCount2 = doh.Delete("jdls_normal_user_message");
            this._response = "有" + _doCount + "条已阅读的站内信被删除；有" + _doCount2 + "条已阅读的通知被删除";
        }
        private void DeleteUnactivedUser()
        {
            string _password = q("password");
            if (_password != System.Configuration.ConfigurationManager.AppSettings["AutoTask:Password"])
            {
                this._response = "密码错误";
                return;
            }
            doh.Reset();
            doh.ConditionExpress = "[State]=0 AND [RegTime]<=" + _spliter + System.DateTime.Now.AddDays(-3).ToString("yyyy-MM-dd") + _spliter;
            int _doCount = doh.Delete("jdls_normal_user");
            this._response = "有" + _doCount + "个未激活的会员被删除";
        }
        /// <summary>
        /// 生成首页
        /// </summary>
        private void CreateDefaultPage()
        {
            string _password = q("password");
            if (_password != System.Configuration.ConfigurationManager.AppSettings["AutoTask:Password"])
            {
                this._response = "密码错误";
                return;
            }
            JumbotDLS.DAL.TemplateEngineDAL teDAL = new JumbotDLS.DAL.TemplateEngineDAL("0");
            teDAL.CreateDefaultFile();
            this._response = "成功生成";
        }
    }
}