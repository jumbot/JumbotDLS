﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Plus
{
    public partial class _video_play : JumbotDLS.UI.FrontHtml
    {
        protected void Page_Unload(object sender, EventArgs e)
        {
            SavePageLog(1);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 5;//脚本过期时间
            int vWidth = Str2Int(q("w")) == 0 ? 400 : Str2Int(q("w"));
            int vHeight = Str2Int(q("h")) == 0 ? 300 : Str2Int(q("h"));
            string vAutoStart = Str2Int(q("auto")) != 0 ? "true" : "false";
            string vAutoPlay = Str2Int(q("auto")) != 0 ? "1" : "0";
            bool vFull = Str2Int(q("full")) != 0 ? true : false;
            if (vFull)
                vHeight -= 55;
            string _html = string.Empty;
            string _img = q("img");
            string _url = q("url");
            string _ext = JumbotDLS.Utils.DirFile.GetFileExt(_url.ToLower());
            if (!_url.ToLower().StartsWith("http://"))
                _url = site.Url + _url;
            switch (_ext)
            {
                case "asf":
                    _html = "<object classid=\"clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95\" codebase=\"http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,0,02,902\" type=\"application/x-oleobject\" standby=\"Loading...\" width=\"" + vWidth + "\" height=\"" + vHeight + "\"><param name=\"FileName\" VALUE=\"" + _url + "\" /><param name=\"ShowStatusBar\" value=\"-1\" /><param name=\"AutoStart\" value=\"" + vAutoStart + "\" /><embed type=\"application/x-mplayer2\" pluginspage=\"http://www.microsoft.com/Windows/MediaPlayer/\" src=\"" + _url + "\" autostart=\"" + vAutoStart + "\" width=\"" + vWidth + "\" height=\"" + vHeight + "\" /></object>";
                    break;
                case "avi":
                    _html = "<object classid=\"clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95\" codebase=\"http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,0,02,902\" type=\"application/x-oleobject\" standby=\"Loading...\" width=\"" + vWidth + "\" height=\"" + vHeight + "\"><param name=\"FileName\" VALUE=\"" + _url + "\" /><param name=\"ShowStatusBar\" value=\"-1\" /><param name=\"AutoStart\" value=\"" + vAutoStart + "\" /><embed type=\"application/x-mplayer2\" pluginspage=\"http://www.microsoft.com/Windows/MediaPlayer/\" src=\"" + _url + "\" autostart=\"" + vAutoStart + "\" width=\"" + vWidth + "\" height=\"" + vHeight + "\" /></object>";
                    break;
                case "wmv":
                    _html = "<object classid=\"clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95\" codebase=\"http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,0,02,902\" type=\"application/x-oleobject\" standby=\"Loading...\" width=\"" + vWidth + "\" height=\"" + vHeight + "\"><param name=\"FileName\" VALUE=\"" + _url + "\" /><param name=\"ShowStatusBar\" value=\"-1\" /><param name=\"AutoStart\" value=\"" + vAutoStart + "\" /><embed type=\"application/x-mplayer2\" pluginspage=\"http://www.microsoft.com/Windows/MediaPlayer/\" src=\"" + _url + "\" autostart=\"" + vAutoStart + "\" width=\"" + vWidth + "\" height=\"" + vHeight + "\" /></object>";
                    break;
                case "swf":
                    _html = "<object codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0\" classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" width=\"" + vWidth + "\" height=\"" + vHeight + "\"><param name=\"movie\" value=\"" + _url + "\" /><param name=\"quality\" value=\"high\" /><param name=\"AllowScriptAccess\" value=\"never\" /><embed src=\"" + _url + "\" quality=\"high\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" type=\"application/x-shockwave-flash\" width=\"" + vWidth + "\" height=\"" + vHeight + "\" /></object>";
                    break;
                default:
                    if (_url.ToLower().Contains(".swf"))
                        _html = "<object codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0\" classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" width=\"" + vWidth + "\" height=\"" + vHeight + "\"><param name=\"movie\" value=\"" + _url + "\" /><param name=\"quality\" value=\"high\" /><param name=\"AllowScriptAccess\" value=\"never\" /><embed src=\"" + _url + "\" quality=\"high\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" type=\"application/x-shockwave-flash\" width=\"" + vWidth + "\" height=\"" + vHeight + "\" /></object>";
                    else
                        _html = "<object type=\"application/x-shockwave-flash\" data=\"" + site.Dir + "statics/flash/mediaplayer.swf\" width=\"" + vWidth + "\" height=\"" + vHeight + "\" id=\"videoPlayer\"><param name=\"movie\" value=\"" + site.Dir + "statics/flash/mediaplayer.swf\"/><param name=\"play\" value=\"true\" /><param name=\"loop\" value=\"true\" /><param name=\"allowFullScreen\" value=\"true\" /><param name=\"wmode\" value=\"transparent\" /><param name=\"FlashVars\" value=\"file=" + _url + "&amp;width=" + vWidth + "&amp;height=" + vHeight + "&amp;image=" + _img + "&amp;autostart=" + vAutoStart + "\"/></object>";
                    break;

            }
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head></head><body style=\"margin:0px;padding:0px;\"><div style=\"margin:0px;padding-top:0px;\">" + _html + "</div></body></html>");

        }
    }
}