﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Controls
{
    public partial class _index : JumbotDLS.UI.FrontHtml
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 8;//脚本过期时间
            int CurrentPage = Int_ThisPage();
            string ChannelId = (this.lblChannelId.Text == "{$ChannelId}") ? Str2Str(q("ChannelId")) : Str2Str(this.lblChannelId.Text);
            string TxtStr = string.Empty;
            JumbotDLS.DAL.TemplateEngineDAL teDAL = new JumbotDLS.DAL.TemplateEngineDAL(ChannelId);
            TxtStr = teDAL.GetSiteChannelPage(CurrentPage);
            teDAL.ReplaceSHTMLTag(ref TxtStr);
            teDAL.ReplaceUserTag(ref TxtStr);
            Response.Write(TxtStr);//直接输出
        }
    }
}
