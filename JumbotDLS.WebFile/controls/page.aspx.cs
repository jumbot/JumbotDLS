﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Controls
{
    public partial class _page : JumbotDLS.UI.FrontHtml
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 8;//脚本过期时间
            string TxtStr = string.Empty;
            string ChannelId = (this.lblChannelId.Text == "{$ChannelId}") ? Str2Str(q("ChannelId")) : Str2Str(this.lblChannelId.Text);
            string ClassId = Str2Str(q("classid"));
            string ContentId = Str2Str(q("id"));
            string TemplateFile = this.lblTemplateFile.Text;
            JumbotDLS.DAL.TemplateEngineDAL teDAL = new JumbotDLS.DAL.TemplateEngineDAL(ChannelId);
            Dictionary<string, string> _param = new Dictionary<string, string>();
            _param.Add("title", q("title"));
            if (q("sf") == "")
                _param.Add("sf", "title");
            else
                _param.Add("sf", JumbotDLS.Utils.Strings.DelSymbol(q("sf")));
            _param.Add("sw", JumbotDLS.Utils.Strings.DelSymbol(q("sw")));
            _param.Add("field", q("field"));
            _param.Add("year", Str2Str(q("year")));
            _param.Add("channelid", ChannelId);
            _param.Add("classid", ClassId);
            _param.Add("moduleid", Str2Str(q("moduleid")));
            //效率问题，只支持三个扩展模糊查询
            string _otherwherestr = " AND (1=1";
            if (JumbotDLS.Utils.Validator.QuickValidate("^[a-zA-Z0-9_]+$", q("likefieldtitle1")) && q("likefieldvalue1") != "")
            {
                _otherwherestr += " AND ','+" +  q("likefieldtitle1") + "+',' LIKE '%," + JumbotDLS.Utils.Strings.FilterSymbol(q("likefieldvalue1")) + ",%'";
            }
            if (JumbotDLS.Utils.Validator.QuickValidate("^[a-zA-Z0-9_]+$", q("likefieldtitle2")) && q("likefieldvalue2") != "")
            {
                _otherwherestr += " AND ','+" + q("likefieldtitle2") + "+',' LIKE '%," + JumbotDLS.Utils.Strings.FilterSymbol(q("likefieldvalue2")) + ",%'";
            }
            if (JumbotDLS.Utils.Validator.QuickValidate("^[a-zA-Z0-9_]+$", q("likefieldtitle3")) && q("likefieldvalue3") != "")
            {
                _otherwherestr += " AND ','+" + q("likefieldtitle3") + "+',' LIKE '%," + JumbotDLS.Utils.Strings.FilterSymbol(q("likefieldvalue3")) + ",%'";
            }
            if (JumbotDLS.Utils.Validator.QuickValidate("^[a-zA-Z0-9_]+$", q("likefieldtitle4")) && q("likefieldvalue4") != "")
            {
                _otherwherestr += " AND ','+" + q("likefieldtitle4") + "+',' LIKE '%," + JumbotDLS.Utils.Strings.FilterSymbol(q("likefieldvalue4")) + ",%'";
            }
            if (JumbotDLS.Utils.Validator.QuickValidate("^[a-zA-Z0-9_]+$", q("likefieldtitle5")) && q("likefieldvalue5") != "")
            {
                _otherwherestr += " AND ','+" + q("likefieldtitle5") + "+',' LIKE '%," + JumbotDLS.Utils.Strings.FilterSymbol(q("likefieldvalue5")) + ",%'";
            }
            if (JumbotDLS.Utils.Validator.QuickValidate("^[a-zA-Z0-9_]+$", q("likefieldtitle6")) && q("likefieldvalue6") != "")
            {
                _otherwherestr += " AND ','+" + q("likefieldtitle6") + "+',' LIKE '%," + JumbotDLS.Utils.Strings.FilterSymbol(q("likefieldvalue6")) + ",%'";
            }
            _otherwherestr += ")";
            _param.Add("otherwherestr", _otherwherestr);
            string sort = JumbotDLS.Utils.Validator.QuickValidate("^[a-zA-Z0-9]+_(desc|asc)$", q("sort")) ? q("sort") : "ishead_desc";
            string orderfield = (sort).Split('_')[0];
            string ordertype = (sort).Split('_')[1];
            _param.Add("sort", sort);
            _param.Add("orderfield", orderfield);
            _param.Add("ordertype", ordertype);
            _param.Add("module", ModuleIsOK(q("module")) == false ? "" : q("module"));
            _param.Add("ajaxfunction", q("ajaxfunction") == "" ? "ajaxGetContent" : q("ajaxfunction"));
            _param.Add("id", Str2Str(q("id")));
            _param.Add("pagesize", Str2Int(q("pagesize"), 20).ToString());
            _param.Add("page", Int_ThisPage().ToString());
            _param.Add("pagerlink", GetUrlPrefix() + "$#page#$");
            TxtStr = teDAL.GetPageHTML(TemplateFile, _param);
            Response.Write(TxtStr);//直接输出
        }
    }
}
