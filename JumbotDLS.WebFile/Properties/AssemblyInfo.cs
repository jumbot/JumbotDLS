﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("JumbotDLS.WebFile")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("北京子木将博科技有限公司")]
[assembly: AssemblyProduct("JumbotDLS.WebFile")]
[assembly: AssemblyCopyright("jumbot.net (C) 2014-2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("7D57B41E-5FF5-78B0-83D1-51CAEECB6204")]
[assembly: AssemblyVersion("1.2.1.0214")]
