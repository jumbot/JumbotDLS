﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _myinfo_ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("", "json");
            this._operType = q("oper");
            switch (this._operType)
            {
                case "changepass":
                    ajaxChangePass();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxChangePass()
        {
            string _oldPass = f("oldpass");
            string _NewPass = f("newpass");
            doh.Reset();
            doh.ConditionExpress = "adminid=@adminid and adminstate=1";
            doh.AddConditionParameter("@adminid", AdminId);
            object pass = doh.GetField("jdls_normal_user", "AdminPass");
            if (pass != null)
            {
                if (pass.ToString().ToLower() == JumbotDLS.Utils.MD5.Last64(_oldPass)) //验证旧密码
                {
                    doh.Reset();
                    doh.ConditionExpress = "adminid=@adminid and adminstate=1";
                    doh.AddConditionParameter("@adminid", AdminId);
                    doh.AddFieldItem("AdminPass", JumbotDLS.Utils.MD5.Last64(_NewPass));
                    doh.AddFieldItem("LastIP2", Const.GetUserIp);
                    doh.Update("jdls_normal_user");
                    this._response = JsonResult(1, "密码修改成功");
                }
                else
                {
                    this._response = JsonResult(0, "旧密码错误");
                }
            }
            else
            {
                this._response = JsonResult(0, "未登录");
            }
        }
    }
}