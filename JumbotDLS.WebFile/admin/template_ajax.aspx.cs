﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _template_ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;
        public string pId = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("master", "json");
            pId = Str2Str(q("pid"));
            id = Str2Str(q("id"));
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxGetList":
                    ajaxGetList();
                    break;
                case "ajaxDel":
                    ajaxDel();
                    break;
                case "ajaxDef":
                    ajaxDef();
                    break;
                case "checkname":
                    ajaxCheckName();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxCheckName()
        {
            doh.Reset();
            doh.ConditionExpress = "title=@title and id<>" + id;
            doh.AddConditionParameter("@title", q("txtTitle"));
            if (doh.Exist("jdls_normal_theme"))
                this._response = JsonResult(0, "不可录入");
            else
                this._response = JsonResult(1, "可以录入");
        }
        private void ajaxGetList()
        {
            doh.Reset();
            doh.SqlCmd = "Select [ID],[Title],[stype],[IsDefault],[Type],[source],(select title from [jdls_module_article_module] where id=[jdls_normal_theme].ModuleId) as modulename FROM [jdls_normal_theme] WHERE [pId]= " + pId + " ORDER BY type desc,stype,Title asc";
            DataTable dt = doh.GetDataTable();
            this._response = "{\"result\" :\"1\",\"returnval\" :\"操作成功\"," + JumbotDLS.Utils.dtHelper.DT2JSON(dt) + "}";
        }
        private void ajaxDel()
        {
            string tId = f("id");

            string IsDefault = "1";
            string TempSType = string.Empty;
            bool isUsing = false;
            doh.Reset();
            doh.ConditionExpress = "id=" + tId;
            TempSType = doh.GetField("jdls_normal_theme", "sType").ToString();
            doh.Reset();
            doh.ConditionExpress = "id=" + tId;
            IsDefault = doh.GetField("jdls_normal_theme", "IsDefault").ToString();
            if (IsDefault == "1")//缺省模板
                isUsing = true;
            else
            {
                if (TempSType.ToLower() == "channel")
                {
                    doh.Reset();
                    doh.SqlCmd = "SELECT ID FROM [jdls_normal_channel] WHERE [ThemeId]=" + tId;
                    if (doh.GetDataTable().Rows.Count > 0)
                        isUsing = true;
                }
                else
                {
                    doh.Reset();
                    doh.SqlCmd = "SELECT ID FROM [jdls_normal_class] WHERE [IsOut]=0 AND [ThemeId]=" + tId + " or [ContentTheme]=" + tId;
                    if (doh.GetDataTable().Rows.Count > 0)
                        isUsing = true;
                }
            }
            if (isUsing)
                this._response = JsonResult(0, "正在使用或缺省模板不允许删除");
            else
            {
                doh.Reset();
                doh.ConditionExpress = "id=" + tId;
                doh.Delete("jdls_normal_theme");
                this._response = JsonResult(1, "成功删除");
            }
        }
        private void ajaxDef()
        {
            string tId = f("id");
            string sType = string.Empty;
            string Type = string.Empty;
            doh.Reset();
            doh.ConditionExpress = "id=" + tId;
            sType = doh.GetField("jdls_normal_theme", "sType").ToString();
            doh.Reset();
            doh.ConditionExpress = "id=" + tId;
            Type = doh.GetField("jdls_normal_theme", "Type").ToString();
            doh.Reset();
            doh.ConditionExpress = "stype=@stype and type=@type and IsDefault=1";
            doh.AddConditionParameter("@stype", sType);
            doh.AddConditionParameter("@type", Type);
            doh.AddFieldItem("IsDefault", 0);
            doh.Update("jdls_normal_theme");
            doh.Reset();
            doh.ConditionExpress = "id=" + tId;
            doh.AddFieldItem("IsDefault", 1);
            doh.Update("jdls_normal_theme");
            doh.Reset();
            this._response = JsonResult(1, "成功设置");
        }
    }
}