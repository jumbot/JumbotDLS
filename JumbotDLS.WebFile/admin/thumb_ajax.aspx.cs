﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Specialized;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _thumb_ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ccid"));
            if (!CheckFormUrl())
            {
                Response.End();
            }
            Admin_Load("master", "json", true);
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxGetList":
                    ajaxGetList();
                    break;
                case "ajaxDel":
                    ajaxDel();
                    break;
                case "ajaxUpdate":
                    ajaxUpdate();
                    break;
                case "ajaxInsert":
                    ajaxInsert();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxGetList()
        {
            int page = Int_ThisPage();
            int PSize = Str2Int(q("pagesize"), 20);
            int countNum = 0;
            string whereStr = "ChannelId=" + ChannelId;

            doh.Reset();
            doh.ConditionExpress = whereStr;
            countNum = doh.Count("jdls_normal_thumbs");
            NameValueCollection orders = new NameValueCollection();
            orders.Add("Id", "asc");
            string FieldList = "*";
            string sqlStr = JumbotDLS.Utils.SqlHelp.GetSql1(FieldList,
                "jdls_normal_thumbs",
                countNum,
                PSize,
                page,
                orders,
                whereStr);
            doh.Reset();
            doh.SqlCmd = sqlStr;

            DataTable dt = doh.GetDataTable();
            this._response = "{\"result\" :\"1\"," +
                "\"returnval\" :\"操作成功\"," +
                "\"pagebar\" :\"" + JumbotDLS.Utils.PageBar.GetPageBar(3, "js", 3, countNum, PSize, page, "javascript:ajaxList($#page#$);") + "\"," +
                JumbotDLS.Utils.dtHelper.DT2JSON(dt, (PSize * (page - 1))) +
                "}";
            dt.Clear();
            dt.Dispose();
        }
        private void ajaxDel()
        {
            string tId = f("id");
            if (MainChannel.DefaultThumbs != Str2Int(tId))
            {
                doh.Reset();
                doh.ConditionExpress = "id=@id";
                doh.AddConditionParameter("@id", tId);
                int _delCount = doh.Delete("jdls_normal_thumbs");
                if (_delCount == 1)
                    this._response = JsonResult(1, "成功删除");
                else
                    this._response = JsonResult(0, "删除有误");
            }
            else
                this._response = JsonResult(0, "频道的默认缩略图不能删");
        }
        private void ajaxUpdate()
        {
            string pId = f("id");
            try
            {
                doh.Reset();
                doh.ConditionExpress = "id=" + pId;
                doh.AddFieldItem(f("field"), f("value"));
                doh.Update("jdls_normal_thumbs");
                this._response = JsonResult(1, "成功修改");
            }
            catch
            {
                this._response = JsonResult(0, "格式有误");
            }
        }
        private void ajaxInsert()
        {
            try
            {
                doh.Reset();
                doh.AddFieldItem("ChannelId", ChannelId);
                doh.AddFieldItem("Title", f("title"));
                doh.AddFieldItem("iWidth", f("iwidth"));
                doh.AddFieldItem("iHeight", f("iheight"));
                doh.Insert("jdls_normal_thumbs");
                this._response = JsonResult(1, "成功添加");
            }
            catch
            {
                this._response = JsonResult(0, "格式有误");
            }
        }
    }
}