﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _template_edittemplate : JumbotDLS.UI.AdminCenter
    {
        public string tpPath = string.Empty;
        private string _Source = string.Empty;
        private string _tempFile1 = string.Empty;
        private string _tempFile2 = string.Empty;
        public bool UsingAsterisk = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("master", "html");
            id = Str2Str(q("id"));
            string pid = Str2Str(q("pid"));

            doh.Reset();
            doh.ConditionExpress = "id=@id";
            doh.AddConditionParameter("@id", pid);
            tpPath = doh.GetField("jdls_normal_themeproject", "Dir").ToString();
            if (tpPath.Length == 0)
            {
                Response.Write("HTML模板方案选择有误!");
                Response.End();
            }
            JumbotDLS.DAL.Normal_TemplateDAL sTempate = new JumbotDLS.DAL.Normal_TemplateDAL();
            _Source = sTempate.GetSource(id);//可能带星号
            if (_Source.Contains("*"))
            {
                UsingAsterisk = true;
                _tempFile1 = site.Dir + "themes/" + tpPath + "/" + _Source.Replace("*", "0");
                _tempFile2 = site.Dir + "themes/" + tpPath + "/" + _Source.Replace("*", "1");

            }
            else
            {
                _tempFile1 = site.Dir + "themes/" + tpPath + "/" + _Source;
                _tempFile2 = site.Dir + "themes/" + tpPath + "/" + _Source;
            }
            this.lblTemplateFile1.Text = _tempFile1;
            this.lblTemplateFile2.Text = _tempFile2;
            if (!IsPostBack)
            {
                this.txtTemplateContent1.Text = JumbotDLS.Utils.DirFile.ReadFile(_tempFile1);
                this.txtTemplateContent2.Text = JumbotDLS.Utils.DirFile.ReadFile(_tempFile2);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            JumbotDLS.Utils.DirFile.SaveFile(this.txtTemplateContent1.Text, _tempFile1);
            if (UsingAsterisk)
                JumbotDLS.Utils.DirFile.SaveFile(this.txtTemplateContent2.Text, _tempFile2);
            FinalMessage("成功保存", "close.htm", 0);
        }
    }
}
