﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _user_searchform : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("master", "html");
            string UserGroupId = Str2Str(q("gid"));
            if (!Page.IsPostBack)
            {
                if (q("keys") != "")
                    this.txtKeyword.Text = q("keys");
                if (this.ddlUserGroup.Items.Count < 1)
                {
                    doh.Reset();
                    doh.SqlCmd = "SELECT ID,GroupName FROM [jdls_normal_usergroup] ORDER BY Id";
                    DataTable dtUserGroup = doh.GetDataTable();
                    this.ddlUserGroup.Items.Clear();
                    this.ddlUserGroup.Items.Add(new ListItem("不指定分组", "0"));
                    ListItem li;
                    for (int i = 0; i < dtUserGroup.Rows.Count; i++)
                    {
                        li = new ListItem();
                        li.Value = dtUserGroup.Rows[i]["Id"].ToString();
                        li.Text = dtUserGroup.Rows[i]["GroupName"].ToString();
                        if (UserGroupId == li.Value)
                            li.Selected = true;
                        else
                            li.Selected = false;
                        this.ddlUserGroup.Items.Add(li);
                    }
                    dtUserGroup.Clear();
                    dtUserGroup.Dispose();
                }
            }

        }

    }
}
