﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */
using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
using System.Data.OleDb;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _content_import_ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;
        public int _ModuleId = 0;
        public string _ModuleName = "";
        public string _ModuleType = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ccid"));
            _ModuleId = Str2Int(q("moduleid"));
            Admin_Load("", "json", true);
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxImport":
                    ajaxImport();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxImport()
        {
            Server.ScriptTimeout = 999999;//脚本过期时间
            doh.Reset();
            doh.ConditionExpress = "id=@id";
            doh.AddConditionParameter("@id", _ModuleId);
            object[] _value = doh.GetFields("jdls_module_article_module", "title,type");
            if (_value == null)
            {
                this._response = JsonResult(0, "参数有误");
                return;
            }
            _ModuleName = _value[0].ToString();
            _ModuleType = _value[1].ToString();
            string classid = Str2Str(f("classid"));
            string orderby = (f("orderby") == "desc") ? "desc" : "asc";
            string themeid = Str2Str(f("themeid"));
            string database = f("database").ToLower();
            int page = Int_ThisPage();
            int PSize = 100;
            int countNum = 0;
            doh.Reset();
            doh.SqlCmd = "Select * FROM [jdls_module_article_module_field] WHERE [State]=1 AND  [ModuleId]=" + _ModuleId + " ORDER BY pid";
            DataTable dtModuleField = doh.GetDataTable();
            Dictionary<string, string> _param = new Dictionary<string, string>();
            for (int i = 0; i < dtModuleField.Rows.Count; i++)
            {
                _param.Add(dtModuleField.Rows[i]["FieldName"].ToString().ToLower(), dtModuleField.Rows[i]["FieldTitle"].ToString());
            }
            dtModuleField.Clear();
            dtModuleField.Dispose();
            DataTable dt = null;
            string dbPath = Server.MapPath(database);
            int _totalpage = 1;
            if (database.EndsWith("mdb"))
            {
                JumbotDLS.DBUtility.DbOperHandler MyDoh = new JumbotDLS.DBUtility.OleDbOperHandler(new System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + dbPath));
                MyDoh.Reset();
                string whereStr = "1=1";
                MyDoh.ConditionExpress = whereStr;
                countNum = MyDoh.Count("Content");
                _totalpage = Utils.Int.PageCount(countNum, PSize);
                MyDoh.SqlCmd = JumbotDLS.Utils.SqlHelp.GetSql0("*", "Content", "id", PSize, page, orderby, whereStr);
                dt = MyDoh.GetDataTable();
                MyDoh.Dispose();
            }
            else
            {
                dt = XlsToDataTable(dbPath, "sheet1");
            }
            foreach (KeyValuePair<string, string> a in _param)
            {
                if (!dt.Columns.Contains(a.Value))
                {
                    this._response = JsonResult(0, "缺少【" + a.Value + "】字段");
                    return;
                }
            }
            int _totalcount = dt.Rows.Count;
            int _success = 0, _repeat = 0, _error = 0;
            bool CheckRepeat = Str2Int(f("checkrepeat")) == 1;
            for (int i = 0; i < _totalcount; i++)
            {
                //int j = (orderby == "desc") ? (_totalcount - i - 1) : i;
                int j = i;
                string _title = dt.Rows[j][_param["title"].ToString()].ToString();
                if (_title.Length < 2)
                    continue;
                bool _isrepeat = false;
                if (CheckRepeat)
                {
                    doh.Reset();
                    doh.ConditionExpress = "title=@title and Module=@module and classid=@classid";
                    doh.AddConditionParameter("@title", _title);
                    doh.AddConditionParameter("@module", _ModuleType);
                    doh.AddConditionParameter("@classid", classid);
                    _isrepeat = doh.Exist("jdls_module_article");
                }
                if (!_isrepeat)
                {
                    try
                    {
                        doh.Reset();
                        foreach (KeyValuePair<string, string> a in _param)
                        {
                            if (dt.Rows[j][a.Value].ToString() != "NULL")
                                doh.AddFieldItem(a.Key, dt.Rows[j][a.Value].ToString());
                            else
                                doh.AddFieldItem(a.Key, "");
                        }
                        if (dt.Rows[j]["子分类"] != null && dt.Rows[j]["子分类"].ToString() != "NULL")
                        {
                            doh.AddFieldItem("subclasslist", dt.Rows[j]["子分类"].ToString());
                        }
                        //下列为默认
                        doh.AddFieldItem("ChannelId", ChannelId);
                        doh.AddFieldItem("Classid", classid);
                        doh.AddFieldItem("Module", _ModuleType);
                        doh.AddFieldItem("IsImg", 0);
                        doh.AddFieldItem("IsPass", 1);
                        doh.AddFieldItem("AddDate", DateTime.Now.ToString());
                        doh.AddFieldItem("themeid", themeid);
                        doh.AddFieldItem("UserID", AdminId);
                        doh.AddFieldItem("AdminID", AdminId);
                        int _insertID = doh.Insert("jdls_module_article");
                        _success++;
                    }
                    catch
                    {
                        _error++;
                    }
                }
                else
                    _repeat++;
            }
            if (page >= _totalpage)//说明可以结束了
                this._response = JsonResult(1, "成功[" + _success + "],重复[" + _repeat + "],失败[" + _error + "]");
            else
                this._response = JsonResult(2, "成功[" + _success + "],重复[" + _repeat + "],失败[" + _error + "]");
            dt.Clear();
            dt.Dispose();
        }
        /// xls文件的某个sheet转成表 
        /// 备注：此方法不会启动服务端的Excel进程
        /// </summary>
        /// <param name="strExcelFileName">xls路径</param>
        /// <param name="strSheetName">xls中的某张sheetName</param>
        /// <returns></returns>
        public DataTable XlsToDataTable(string strExcelFileName, string strSheetName)
        {
            //源的定义
            strExcelFileName = strExcelFileName.ToLower();
            string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strExcelFileName + ";Extended Properties='Excel 8.0;HDR=yes;IMEX=1';";
            if (strExcelFileName.EndsWith(".xlsx"))
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strExcelFileName + ";Extended Properties='Excel 12.0 Xml;HDR=YES;'";

            //Sql语句
            string strExcel = string.Format("select * from [{0}$]", strSheetName);
            //定义存放的数据表
            DataSet ds = new DataSet();
            //连接数据源
            OleDbConnection conn = new OleDbConnection(strConn);
            conn.Open();
            //适配到数据源
            OleDbDataAdapter adapter = new OleDbDataAdapter(strExcel, strConn);
            adapter.Fill(ds, strSheetName);
            conn.Close();
            return ds.Tables[strSheetName];
        }
    }
}
