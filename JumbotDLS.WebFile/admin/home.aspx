﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="home.aspx.cs" Inherits="JumbotDLS.WebFile.Admin._home" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta   name="robots" content="noindex, nofollow" />
<meta   http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title>后台首页</title>
<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js"></script>
<link type="text/css" rel="stylesheet" href="../_data/global.css" />
<link type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
<script type="text/javascript" src="../statics/admin/js/common.js"></script>
<script type="text/javascript" src="scripts/admin.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.tip-t').jtip({ gravity: 't', fade: false });
        $('.tip-r').jtip({ gravity: 'r', fade: false });
        $('.tip-b').jtip({ gravity: 'b', fade: false });
        $('.tip-l').jtip({ gravity: 'l', fade: false });
        //ajaxIncludeList();
        //ajaxPageList();
        $SetTab('tab', 1, 2);
    });
    function ajaxIncludeList() {
        $.ajax({
            type: "get",
            dataType: "json",
            data: "page=1&pagesize=100&pid=1&clienttime=" + Math.random(),
            url: "templateinclude_ajax.aspx?oper=ajaxGetList",
            error: function (XmlHttpRequest, textStatus, errorThrown) { alert(XmlHttpRequest.responseText); },
            success: function (d) {
                switch (d.result) {
                    case '-1':
                        top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                        break;
                    case '0':
                        top.JumbotDLS.Alert(d.returnval, "0");
                        break;
                    case '1':
                        $("#ajaxIncludeList").setTemplateElement("tplIncludeList", null, { filter_data: true });
                        $("#ajaxIncludeList").processTemplate(d);
                        break;
                }
            }
        });
    }
    function ajaxPageList() {
        $.ajax({
            type: "get",
            dataType: "json",
            data: "page=1&pagesize=100&clienttime=" + Math.random(),
            url: "page_ajax.aspx?oper=ajaxGetList",
            error: function (XmlHttpRequest, textStatus, errorThrown) { alert(XmlHttpRequest.responseText); },
            success: function (d) {
                switch (d.result) {
                    case '-1':
                        top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                        break;
                    case '0':
                        top.JumbotDLS.Alert(d.returnval, "0");
                        break;
                    case '1':
                        $("#ajaxPageList").setTemplateElement("tplPageList", null, { filter_data: true });
                        $("#ajaxPageList").processTemplate(d);
                        break;
                }
            }
        });
    }
</script>
</head>
<body>
    <table class="formtable mrg10T">
      <tr>
        <th>更新索引
          <span class="tip-r" tip="如果有新增的内容，需要更新" /></th>
        <td><input type="button" value="更新" class="btnsubmit" onclick="ajaxCreateSearchIndex(0,1);" />
        </td>
        <th>全新索引
          <span class="tip-l" tip="如果对旧的内容进行了修改或删除，需要更新" /></th>
        <td><input type="button" value="更新" class="btnsubmit" onclick="ajaxCreateSearchIndex(1,1);" />
        </td>
      </tr>
    </table>
</body>
</html>
