﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _module_article_collectitem_setcontent : JumbotDLS.UI.AdminCenter
    {
        private string _err = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 99999999;
            ChannelId = Str2Str(q("ccid"));
            id = Str2Str(q("id"));
            Admin_Load(ChannelId + "-01", "stop", true);
            if (!Page.IsPostBack && !GetTest())
            {
                this.btnSave.Enabled = false;
                doh.Reset();
                doh.ConditionExpress = "id=" + id;
                doh.AddFieldItem("flag", 0);
                doh.AddFieldItem("ErrorListRule", 1);
                doh.Update("jdls_module_article_collitem");
                FinalMessage(this._err, site.Dir + "admin/close.htm", 0);
            }
            else
            {
                JumbotDLS.DBUtility.WebFormHandler wh = new JumbotDLS.DBUtility.WebFormHandler(doh, "jdls_module_article_collitem", btnSave);
                wh.AddBind(ddlContentWebEncode, "ContentWebEncode", false);
                wh.AddBind(txtTitleStart, "TitleStart", true);
                wh.AddBind(txtTitleEnd, "TitleEnd", true);
                wh.AddBind(txtPubTimeStart, "PubTimeStart", true);
                wh.AddBind(txtPubTimeEnd, "PubTimeEnd", true);
                wh.AddBind(txtSourceFromStart, "SourceFromStart", true);
                wh.AddBind(txtSourceFromEnd, "SourceFromEnd", true);
                wh.AddBind(txtContentStart, "ContentStart", true);
                wh.AddBind(txtContentEnd, "ContentEnd", true);
                wh.AddBind(txtNPageStart, "NPageStart", true);
                wh.AddBind(txtNPageEnd, "NPageEnd", true);
                wh.ConditionExpress = "id=" + id;
                wh.Mode = JumbotDLS.DBUtility.OperationType.Modify;
                wh.ModifyOk += new EventHandler(save_ok);
                wh.validator = chkForm;
            }
        }
        /// <summary>
        /// 绑定数据后的处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bind_ok(object sender, EventArgs e)
        {
        }
        protected bool chkForm()
        {
            if (!CheckFormUrl())
                return false;
            if (!Page.IsValid)
                return false;
            return true;
        }
        protected void save_ok(object sender, EventArgs e)
        {
            FinalMessage("成功保存", "module_article_collitem_test.aspx?ccid=" + ChannelId + "&id=" + id, 0);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
        }
        private bool GetTest()
        {
            if (id == "0")
            {
                this._err = "参数错误，项目ID不能为 0";
                return false;
            }
            doh.Reset();
            doh.SqlCmd = "SELECT * FROM [jdls_module_article_collitem] WHERE [Id]=" + id;
            DataTable dtCollItem = doh.GetDataTable();
            if (dtCollItem.Rows.Count == 0)
            {
                this._err = "ID为 " + id + " 的项目不存在!";
                return false;
            }
            int CollecNewsNum = Str2Int(dtCollItem.Rows[0]["CollecNewsNum"].ToString());
            string ListStr = dtCollItem.Rows[0]["ListStr"].ToString();
            string ListStart = dtCollItem.Rows[0]["ListStart"].ToString();
            string ListEnd = dtCollItem.Rows[0]["ListEnd"].ToString();
            string ListWebEncode = dtCollItem.Rows[0]["ListWebEncode"].ToString();
            string LinkBaseHref = dtCollItem.Rows[0]["LinkBaseHref"].ToString();
            if (LinkBaseHref == "")
                LinkBaseHref = ListStr;
            string LinkStart = dtCollItem.Rows[0]["LinkStart"].ToString();
            string LinkEnd = dtCollItem.Rows[0]["LinkEnd"].ToString();
            string CollecOrder = dtCollItem.Rows[0]["CollecOrder"].ToString();
            dtCollItem.Clear();
            dtCollItem.Dispose();
            System.Text.Encoding LencodeType = System.Text.Encoding.Default;
            switch (ListWebEncode)
            {
                case "3":
                    LencodeType = System.Text.Encoding.Unicode;
                    break;
                case "2":
                    LencodeType = System.Text.Encoding.UTF8;
                    break;
                case "1":
                    LencodeType = System.Text.Encoding.GetEncoding("GB2312");
                    break;
            }
            NewsCollection nc = new NewsCollection();
            string testList = JumbotDLS.Utils.HttpHelper.Get_Http(ListStr, 10000, LencodeType);
            if (testList == "$UrlIsFalse$")
            {
                this._err = "列表地址设置错误";
                return false;
            }
            if (testList == "$GetFalse$")
            {
                this._err = "无法连接列表页或连接超时";
                return false;
            }
            testList = nc.GetBody(testList, ListStart, ListEnd);
            if (testList == "$StartFalse$")
            {
                this._err = "列表开始前标记设置错误,请重新设置";
                return false;
            }
            if (testList == "$EndFalse$")
            {
                this._err = "列表结束后标记设置错误,请重新设置";
                return false;
            }
            System.Collections.ArrayList linkArray = nc.GetArray(testList, LinkStart, LinkEnd);
            if (linkArray.Count == 0)
            {
                this._err = "未取到链接,请检查链接设置";
                return false;
            }
            if (linkArray[0].ToString() == "$StartFalse$")
            {
                this._err = "链接开始前标记设置错误,请重新设置";
                return false;
            }
            if (linkArray[0].ToString() == "$EndFalse$")
            {
                this._err = "链接结束后标记设置错误,请重新设置";
                return false;
            }
            if (linkArray[0].ToString() == "$NoneBody$")
            {
                this._err = "未取到链接,请检查链接设置";
                return false;
            }
            if (CollecOrder == "1")
                linkArray.Reverse();
            if (CollecNewsNum > 0 && linkArray.Count > CollecNewsNum)
                linkArray.RemoveRange(CollecNewsNum, linkArray.Count - CollecNewsNum);

            string linkStr = string.Empty;
            for (int i = 0; i < linkArray.Count; i++)
            {
                linkStr = nc.DefiniteUrl(linkArray[i].ToString(), LinkBaseHref);
                if (linkStr != "$False$")
                {
                    linkStr = "<a href='" + linkStr + "' target=_blank>" + linkStr + "</a><br />\r\n\r\n";
                    ltLinkTest.Text += linkStr;
                }
            }
            return true;
        }
    }
}
