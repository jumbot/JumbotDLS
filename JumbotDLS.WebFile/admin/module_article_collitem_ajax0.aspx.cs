﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _module_article_collectitem_ajax0 : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ccid"));
            id = Str2Str(q("id"));
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxGetList":
                    ajaxGetList();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxGetList()
        {
            Admin_Load("", "json");
            int page = Int_ThisPage();
            int PSize = Str2Int(q("pagesize"), 20);
            int Flag = Str2Int(q("flag"));
            string Auto = q("auto");
            int totalCount = 0;
            string sqlStr = "SELECT a.id as id,a.Title as ItemName,webname,a.ErrorListRule,a.ErrorPageRule,a.ChannelId as Channelid,ClassId,b.Title as ClassName,flag,isrunning,lasttime,(getdate()) as thistime,CollecNewsNum,ListStr FROM [jdls_module_article_collitem] a LEFT JOIN [jdls_normal_class] b on a.classid=b.id WHERE a.deleted=0";
            switch (Auto)
            {
                case "1":
                    sqlStr += " AND [AutoCollect]=1";
                    break;
                case "-1":
                    sqlStr += " AND [AutoCollect]=0";
                    break;
                default:
                    break;
            }
            if (Flag == 1)
                sqlStr += " and a.flag=1 ORDER BY a.lasttime asc,a.id asc";
            doh.Reset();
            doh.SqlCmd = sqlStr;
            DataTable dt = doh.GetDataTable();
            totalCount = dt.Rows.Count;
            this._response = "{\"result\" :\"1\"," +
                "\"returnval\" :\"操作成功\"," +
                "\"pagerbar\" :\"" + JumbotDLS.Utils.PageBar.GetPageBar(3, "js", 2, totalCount, PSize, page, "javascript:ajaxList($#page#$);") + "\"," +
                JumbotDLS.Utils.dtHelper.DT2JSON(dt) +
                "}";
            dt.Clear();
            dt.Dispose();
        }
    }
}