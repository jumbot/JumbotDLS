﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _module_article_collectitem_list1 : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ccid"));
            if (ChannelId == "0")
            {
                doh.Reset();
                doh.SqlCmd = "SELECT TOP 1 ID FROM [jdls_normal_channel] WHERE [Type]='article' ORDER BY SubsiteID asc,PID asc";
                DataTable dt = doh.GetDataTable();
                if (dt.Rows.Count == 1)
                    ChannelId = dt.Rows[0][0].ToString();
                dt.Clear();
                dt.Dispose();
                Response.Redirect("module_article_collitem_list1.aspx?ccid=" + ChannelId);
            }
            else
                Admin_Load("", "html", true);
        }
    }
}