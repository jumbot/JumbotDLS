﻿/*
 * 程序名称: JumbotDLS(知远统一资源平台)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _admin_add : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("master", "html");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string _password = this.txtAdminPass1.Text;
            if (_password == "")
            {
                FinalMessage("请填写密码", "", 1);
            }
            doh.Reset();
            doh.SqlCmd = "SELECT Id FROM [jdls_normal_user] WHERE [UserName]='" + txtAdminName.Text + "'";
            if (doh.GetDataTable().Rows.Count > 0)
            {
                FinalMessage("用户名重复", "", 1);
            }
            int _uID = new JumbotDLS.DAL.Normal_UserDAL().Register(txtAdminName.Text, txtAdminName.Text, JumbotDLS.Utils.MD5.Lower32(_password), 0, GetRandomNumberString(12) + "@domain.com", "1980-1-1", GetRandomNumberString(32), "", "", "", "", false);
            doh.Reset();
            doh.ConditionExpress = "id=" + _uID;
            doh.AddFieldItem("AdminName", txtAdminName.Text);
            doh.AddFieldItem("AdminState", 1);
            doh.AddFieldItem("AdminId", _uID);
            doh.AddFieldItem("AdminPass", JumbotDLS.Utils.MD5.Last64(JumbotDLS.Utils.MD5.Lower32(_password)));
            doh.AddFieldItem("Group", site.AdminGroupId);
            doh.AddFieldItem("State", 1);
            doh.Update("jdls_normal_user");
            FinalMessage("成功保存", "close.htm", 0);
        }
    }
}
