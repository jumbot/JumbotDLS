﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Text;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _admin_edit : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("master", "html");
            id = Str2Str(q("id"));

            if (!Page.IsPostBack)
            {
                doh.Reset();
                doh.SqlCmd = "SELECT [ID],[RoleName] FROM [jdls_normal_adminrole] ORDER BY id asc";
                DataTable dtAdminRole = doh.GetDataTable();
 
                for (int i = 0; i < dtAdminRole.Rows.Count; i++)
                {
                    this.ddlAdminRoleID.Items.Add(new ListItem(dtAdminRole.Rows[i]["RoleName"].ToString(), dtAdminRole.Rows[i]["ID"].ToString()));
                }
                dtAdminRole.Clear();
                dtAdminRole.Dispose();
            }


            JumbotDLS.DBUtility.WebFormHandler wh = new JumbotDLS.DBUtility.WebFormHandler(doh, "jdls_normal_user", btnSave);
            wh.AddBind(txtAdminName, "AdminName", true);
            wh.AddBind(rbtnSex, "SelectedValue", "Sex", false);
            wh.AddBind(ddlAdminRoleID, "AdminRoleID", false);
            wh.AddBind(rbtnAdminState, "SelectedValue", "AdminState", false);
            this.txtAdminName.ReadOnly = true;
            wh.ConditionExpress = "id=" + id;
            wh.Mode = JumbotDLS.DBUtility.OperationType.Modify;
            wh.validator = chkForm;
            wh.ModifyOk += new EventHandler(save_ok);

        }
        protected void bind_ok(object sender, EventArgs e)
        {
        }
        protected bool chkForm()
        {
            if (!CheckFormUrl())
                return false;
            if (!Page.IsValid)
                return false;
            return true;
        }
        protected void save_ok(object sender, EventArgs e)
        {

            if (this.txtAdminPass.Text.Length > 0)
            {
                doh.Reset();
                doh.ConditionExpress = "id=@id";
                doh.AddConditionParameter("@id", id);
                doh.AddFieldItem("AdminPass", JumbotDLS.Utils.MD5.Last64(JumbotDLS.Utils.MD5.Lower32(this.txtAdminPass.Text)));
                doh.Update("jdls_normal_user");
            }
    
            FinalMessage("成功保存", "close.htm", 0);
        }
    }
}
