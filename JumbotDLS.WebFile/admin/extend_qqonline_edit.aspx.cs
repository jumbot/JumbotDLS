﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _extend_qqonline_edit : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("master", "html");
            id = Str2Str(q("id"));
            doh.Reset();
            JumbotDLS.DBUtility.WebFormHandler wh = new JumbotDLS.DBUtility.WebFormHandler(doh, "jdls_extends_qqonline", btnSave);
            wh.AddBind(txtTitle, "Title", true);
            wh.AddBind(txtQQID, "QQID", true);
            wh.AddBind(txtTColor, "TColor", true);
            wh.AddBind(txtOrderNum, "OrderNum", false);
            wh.AddBind(rblState, "SelectedValue", "State", false);
            wh.AddBind(rblFace, "SelectedValue", "Face", false);
            if (id != "0")
            {
                wh.ConditionExpress = "id=" + id;
                wh.Mode = JumbotDLS.DBUtility.OperationType.Modify;
            }
            else
                wh.Mode = JumbotDLS.DBUtility.OperationType.Add;
            wh.BindBeforeModifyOk += new EventHandler(bind_ok);
            wh.BindBeforeAddOk += new EventHandler(bind_ok);
            wh.AddOk += new EventHandler(save_ok);
            wh.ModifyOk += new EventHandler(save_ok);
            wh.validator = chkForm;
        }
        /// <summary>
        /// 绑定数据后的处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bind_ok(object sender, EventArgs e)
        {
        }
        protected bool chkForm()
        {
            if (!CheckFormUrl())
                return false;
            if (!Page.IsValid)
                return false;
            return true;
        }
        protected void save_ok(object sender, EventArgs e)
        {
            FinalMessage("成功保存", "close.htm", 0);
        }
    }
}
