﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _urlrewriter_ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!CheckFormUrl())
            {
                Response.End();
            }
            Admin_Load("master", "json");
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxSaveRules":
                    ajaxSaveRules();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxSaveRules()
        {
            string _RulesContent = f("txtRulesContent");
            if (_RulesContent.Length == 0)
            {
                this._response = "top.JumbotDLS.Alert('规则有误', '0');";
                return;
            }
            string _tmpFile = site.Dir + "_data/UrlRewriter.config";
            JumbotDLS.Utils.DirFile.SaveFile(_RulesContent, _tmpFile);
            this._response = "top.JumbotDLS.Alert('规则修改成功', '1');";
        }
    }
}