﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _forbidip_edit : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("master", "html");
            id = Str2Str(q("id"));
            this.txtExpireDate.Attributes.Add("onFocus", "WdatePicker({isShowClear:false,readOnly:true,skin:'blue',startDate:'" + System.DateTime.Now.AddYears(1).ToShortDateString() + "'})");
            if (id == "0")
                this.txtExpireDate.Attributes.Add("value", System.DateTime.Now.AddYears(1).ToShortDateString());
            this.txtExpireDate.Attributes.Add("readonly", "readonly");

            JumbotDLS.DBUtility.WebFormHandler wh = new JumbotDLS.DBUtility.WebFormHandler(doh, "jdls_normal_forbidip", btnSave);
            wh.AddBind(txtStartIP, "StartIP2", true);
            wh.AddBind(txtEndIP, "EndIP2", true);
            wh.AddBind(txtExpireDate, "ExpireDate", true);
            if (id == "0")
            {
                wh.Mode = JumbotDLS.DBUtility.OperationType.Add;
            }
            else
            {
                wh.ConditionExpress = "id=" + id;
                wh.Mode = JumbotDLS.DBUtility.OperationType.Modify;
            }
            wh.validator = chkForm;
            wh.AddOk += new EventHandler(save_ok);
            wh.ModifyOk += new EventHandler(save_ok);
        }
        protected void bind_ok(object sender, EventArgs e)
        {
        }
        protected bool chkForm()
        {
            if (!CheckFormUrl())
                return false;
            if (!Page.IsValid)
                return false;
            return true;
        }
        /// <summary>
        /// 格式化为IP字符串
        /// </summary>
        /// <param name="ipDoubleStr"></param>
        /// <returns></returns>
        protected string GetIPStr(string ipDoubleStr)
        {
            long ipLong = long.Parse(ipDoubleStr);
            System.Net.IPAddress ip = JumbotDLS.Utils.IPHelp.Long2IP(ipLong);
            return ip.ToString();
        }
        protected void save_ok(object sender, EventArgs e)
        {
            if (id == "0")
            {
                JumbotDLS.DBUtility.DbOperEventArgs de = (JumbotDLS.DBUtility.DbOperEventArgs)e;
                id = de.id.ToString();
            }
            if (new JumbotDLS.DAL.Normal_ForbidipDAL().UpdateIPData(id, this.txtStartIP.Text, this.txtEndIP.Text))
                FinalMessage("保存成功", "close.htm", 0);
            else
                FinalMessage("保存错误", "close.htm", 0);
        }
    }
}
