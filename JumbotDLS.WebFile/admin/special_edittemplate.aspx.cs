﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _special_edittemplate : JumbotDLS.UI.AdminCenter
    {
        private string _Source = string.Empty;
        private string _tempFile = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("special-mng", "stop");
            id = Str2Str(q("id"));
            JumbotDLS.Entity.Normal_Special eSpecial = new JumbotDLS.DAL.Normal_SpecialDAL().GetEntity(id);
            _Source = eSpecial.Source;
            _tempFile = site.Dir + "_data/special/_" + _Source;
            if (!IsPostBack)
            {
                if (JumbotDLS.Utils.DirFile.FileExists(_tempFile))
                    this.txtTemplateContent.Text = JumbotDLS.Utils.DirFile.ReadFile(_tempFile);
                else
                    this.txtTemplateContent.Text = "";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string PageStr = this.txtTemplateContent.Text;
            JumbotDLS.Utils.DirFile.SaveFile(PageStr, _tempFile);
            if (this.chkSavaDefault.Checked)
                JumbotDLS.Utils.DirFile.SaveFile(PageStr, site.Dir + "themes/special_index.htm");
            FinalMessage("成功保存", "close.htm", 0);
        }
    }
}
