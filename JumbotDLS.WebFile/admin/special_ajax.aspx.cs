﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _special_ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!CheckFormUrl())
            {
                Response.End();
            }

            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxGetList":
                    ajaxGetList();
                    break;
                case "ajaxDel":
                    ajaxDel();
                    break;
                case "ajaxCreateSpecial":
                    ajaxCreateSpecial();
                    break;
                case "checkname":
                    ajaxCheckName();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxCheckName()
        {
            Admin_Load("special-mng", "json");
            JumbotDLS.DAL.Normal_SpecialDAL dal = new JumbotDLS.DAL.Normal_SpecialDAL();
            if (dal.ExistTitle(q("txtTitle"), q("id"), ""))
                this._response = JsonResult(0, "不可添加");
            else
                this._response = JsonResult(1, "可以添加");
        }
        private void ajaxGetList()
        {
            Admin_Load("special-mng", "json");
            int page = Int_ThisPage();
            int PSize = Str2Int(q("pagesize"), 20);
            string whereStr = "1=1";
            string jsonStr = "";
            new JumbotDLS.DAL.Normal_SpecialDAL().GetListJSON(page, PSize, whereStr, ref jsonStr);
            this._response = jsonStr;
        }
        private void ajaxDel()
        {
            Admin_Load("special-mng", "json");
            string sId = f("id");
            if (new JumbotDLS.DAL.Normal_SpecialDAL().DeleteByID(sId))
                this._response = JsonResult(1, "删除成功");
            else
                this._response = JsonResult(0, "删除失败");
        }
        private void ajaxCreateSpecial()
        {
            Admin_Load("special-mng", "json");
            string sId = f("id");
            JumbotDLS.Entity.Normal_Special eSpecial = new JumbotDLS.DAL.Normal_SpecialDAL().GetEntity(sId);
            string PageStr = JumbotDLS.Utils.DirFile.ReadFile("~/_data/special/_" + eSpecial.Source);
            JumbotDLS.DAL.TemplateEngineDAL teDAL = new JumbotDLS.DAL.TemplateEngineDAL("0");
            teDAL.IsHtml = site.IsHtml;
            teDAL.PageNav = "<a href=\"" + site.Home + "\" class=\"home\">平台首页</a> / 专题 / " + eSpecial.Title;
            teDAL.PageTitle = eSpecial.Title + " - 专题 - " + site.Name + site.TitleTail;
            teDAL.PageKeywords = site.Keywords;
            teDAL.PageDescription = JumbotDLS.Utils.Strings.SimpleLineSummary(eSpecial.Info);
            teDAL.ReplacePublicTag(ref PageStr);
            teDAL.ReplaceSpecialTag(ref PageStr, sId);
            PageStr = PageStr.Replace("<@textarea ", "<textarea ");
            PageStr = PageStr.Replace("</textarea@>", "</textarea>");
            teDAL.SaveHTML(PageStr, site.Dir + "special/" + eSpecial.Source);
            this._response = JsonResult(1, "生成成功");
        }
        /// <summary>
        /// 生成特约专题list页
        /// </summary>
        private void CreateSpecialListPage()
        {
            string PageStr = JumbotDLS.Utils.DirFile.ReadFile("~/themes/speciallist_index.htm");
            JumbotDLS.DAL.TemplateEngineDAL teDAL = new JumbotDLS.DAL.TemplateEngineDAL("0");
            teDAL.IsHtml = site.IsHtml;
            teDAL.PageNav = "<a href=\"" + site.Home + "\" class=\"home\">平台首页</a> / 过往专题列表";
            teDAL.PageTitle = "过往专题列表 - " + site.Name + site.TitleTail;
            teDAL.PageKeywords = site.Keywords;
            teDAL.PageDescription = site.Description;
            teDAL.ReplacePublicTag(ref PageStr);
            teDAL.SaveHTML(PageStr, site.Dir + "speciallist" + site.StaticExt);
        }
    }
}