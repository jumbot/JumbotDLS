﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _module_article_list : JumbotDLS.UI.AdminCenter
    {
        public string EditFile = "content_edit.aspx";
        public DataTable dtModule;
        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ccid"));
            Admin_Load(ChannelId + "-00", "stop", true);
            doh.Reset();
            string wherestr = "[Enabled]=1 AND (',+" + MainChannel.ModuleIdList + "+,' LIKE '%,' + ltrim(id) + ',%')";
            //doh.SqlCmd = "Select [Id],[Title],[Type],[pId],[Locked],[Enabled] FROM [jdls_module_article_module] WHERE (ParentID IN (select id from jdls_module_article_module where " + wherestr + ")) OR (ID IN (select id from jdls_module_article_module where " + wherestr + ")) ORDER BY pid";
            doh.SqlCmd = "Select [Id],[Title],[Type],[pId],[Locked],[Enabled] FROM [jdls_module_article_module] WHERE (ID IN (select id from jdls_module_article_module where " + wherestr + ")) ORDER BY pid"; 
            dtModule = doh.GetDataTable();
            if(ChannelId=="7")
                EditFile = "content_edit_version.aspx";
        }
    }
}