﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _templateinclude_ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;
        public string pId, tpPath = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Str2Str(q("id"));
            pId = Str2Str(q("pid"));
            doh.Reset();
            doh.ConditionExpress = "id=@id";
            doh.AddConditionParameter("@id", pId);
            tpPath = doh.GetField("jdls_normal_themeproject", "Dir").ToString();
            if (tpPath.Length == 0)
            {
                Response.Write("HTML模板方案选择有误!");
                Response.End();
                return;
            }
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxGetList":
                    ajaxGetList();
                    break;
                case "ajaxDel":
                    ajaxDel();
                    break;
                case "checkname":
                    ajaxCheckName();
                    break;
                case "updatefore":
                    ajaxUpdateFore();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxCheckName()
        {
            Admin_Load("", "json");
            doh.Reset();
            doh.ConditionExpress = "title=@title and id<>" + id;
            doh.AddConditionParameter("@title", q("txtTitle"));
            if (doh.Exist("jdls_normal_themeinclude"))
                this._response = JsonResult(0, "不可录入");
            else
                this._response = JsonResult(1, "可以录入");
        }
        private void ajaxGetList()
        {
            Admin_Load("", "json");
            int _NeedBuild = Str2Int(q("needbuild"));
            doh.Reset();
            if (_NeedBuild == 1)
                doh.SqlCmd = "SELECT a.id as id,a.title as title,b.title as pTitle,a.info as info,Sort,a.source as source,a.needbuild FROM [jdls_normal_themeinclude] as a,[jdls_normal_themeproject] as b where a.needbuild=1 and a.pId = b.id and a.pId=" + pId;
            else
                doh.SqlCmd = "SELECT a.id as id,a.title as title,b.title as pTitle,a.info as info,Sort,a.source as source,a.needbuild FROM [jdls_normal_themeinclude] as a,[jdls_normal_themeproject] as b where a.pId = b.id and a.pId=" + pId;

            DataTable dt = doh.GetDataTable();
            this._response = "{\"result\" :\"1\",\"returnval\" :\"操作成功\"," + JumbotDLS.Utils.dtHelper.DT2JSON(dt) + "}";
        }
        private void ajaxDel()
        {
            Admin_Load("templateinclude-mng", "json");
            string lId = f("id");
            doh.Reset();
            doh.ConditionExpress = "id=" + lId;
            doh.Delete("jdls_normal_themeinclude");
            this._response = JsonResult(1, "成功删除");
        }
        private void ajaxUpdateFore()
        {
            Admin_Load("templateinclude-mng", "json");
            CreateIncludeFiles();
            this._response = JsonResult(1, "更新完成,前台页面需要刷新");
        }
        /// <summary>
        /// 生成包含文件
        /// </summary>
        private void CreateIncludeFiles()
        {
            string _source = q("source");
            doh.Reset();
            if (_source == "")
                doh.SqlCmd = "SELECT * FROM [jdls_normal_themeinclude] ORDER BY [Sort]";
            else
                doh.SqlCmd = "SELECT * FROM [jdls_normal_themeinclude] where [Source]='" + _source + "'";
            DataTable dt = doh.GetDataTable();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string TempStr = JumbotDLS.Utils.DirFile.ReadFile(site.Dir + "themes/" + tpPath + "/include/" + dt.Rows[i]["Source"].ToString());
                    JumbotDLS.DAL.TemplateEngineDAL teDAL = new JumbotDLS.DAL.TemplateEngineDAL("0");
                    if (dt.Rows[i]["NeedBuild"].ToString() == "1")
                    {

                        teDAL.IsHtml = site.IsHtml;
                        teDAL.ReplacePublicTag(ref TempStr);
                        teDAL.ReplaceChannelClassLoopTag(ref TempStr);
                        teDAL.ReplaceContentLoopTag(ref TempStr);
                    }
                    teDAL.SaveHTML(TempStr, "~/_data/shtm/" + dt.Rows[i]["Source"].ToString(), true);//shtm引用
                    teDAL.SaveHTML(TempStr, "~/_data/html/" + dt.Rows[i]["Source"].ToString(), false);//aspx引用
                }
            }
            dt.Clear();
            dt.Dispose();
        }
    }
}