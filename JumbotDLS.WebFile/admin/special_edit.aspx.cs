﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _special_edit : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("special-mng", "stop");
            id = Str2Str(q("id"));
            JumbotDLS.DBUtility.WebFormHandler wh = new JumbotDLS.DBUtility.WebFormHandler(doh, "jdls_normal_special", btnSave);
            wh.AddBind(txtTitle, "Title", true);
            wh.AddBind(txtOrderNum, "OrderNum", false);
            wh.AddBind(txtSource, "Source", true);
            wh.AddBind(txtInfo, "Info", true);
            if (id == "0")
            {
                wh.Mode = JumbotDLS.DBUtility.OperationType.Add;
            }
            else
            {
                wh.ConditionExpress = "id=" + id;
                this.txtSource.Enabled = false;
                wh.Mode = JumbotDLS.DBUtility.OperationType.Modify;
            }
            wh.validator = chkForm;
            wh.AddOk += new EventHandler(save_ok);
            wh.ModifyOk += new EventHandler(save_ok);
        }
        protected void bind_ok(object sender, EventArgs e)
        {
        }
        protected bool chkForm()
        {
            if (!CheckFormUrl())
                return false;
            if (!Page.IsValid)
                return false;
            //判断重复性
            JumbotDLS.DAL.Normal_SpecialDAL dal = new JumbotDLS.DAL.Normal_SpecialDAL();
            if (dal.ExistTitle(this.txtTitle.Text, id, ""))
            {
                FinalMessage("专题名重复!", "", 1);
                return false;
            }
            if (dal.ExistSource(this.txtSource.Text, id, ""))
            {
                FinalMessage("文件名重复!", "", 1);
                return false;
            }
            return true;

        }
        protected void save_ok(object sender, EventArgs e)
        {
            string _tempFile = site.Dir + "_data/special/_" + this.txtSource.Text;
            if (!JumbotDLS.Utils.DirFile.FileExists(_tempFile))
            {
                string _defaultTemplate = JumbotDLS.Utils.DirFile.ReadFile("~/themes/special_index.htm");
                JumbotDLS.Utils.DirFile.SaveFile(_defaultTemplate, _tempFile);
            }
            FinalMessage("成功保存", "close.htm", 0);
        }
    }
}
