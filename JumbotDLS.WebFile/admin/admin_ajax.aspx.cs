﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _masterajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!CheckFormUrl())
            {
                Response.End();
            }
            Admin_Load("master", "json");
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxGetList":
                    ajaxGetList();
                    break;
                case "ajaxDel":
                    ajaxDel();
                    break;
                case "checkusername":
                    ajaxCheckUserName();
                    break;
                case "checkadminname":
                    ajaxCheckAdminName();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }
        private void ajaxCheckUserName()
        {
            doh.Reset();
            doh.ConditionExpress = "username=@username";
            doh.AddConditionParameter("@username", q("txtUserName"));
            if (doh.Exist("jdls_normal_user"))
                this._response = JsonResult(1, "有此用户");
            else
                this._response = JsonResult(0, "帐号不存在");
        }

        private void ajaxCheckAdminName()
        {
            if (q("id") == "0")
            {
                doh.Reset();
                doh.ConditionExpress = "adminname=@adminname";
                doh.AddConditionParameter("@adminname", q("txtAdminName"));
                if (doh.Exist("jdls_normal_user"))
                    this._response = JsonResult(0, "不可添加");
                else
                    this._response = JsonResult(1, "可以添加");
            }
            else
                this._response = JsonResult(0, "不可修改");
        }
        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxGetList()
        {
            doh.Reset();
            doh.SqlCmd = "Select * FROM [View_Admin] ORDER BY adminid desc";
            DataTable dt = doh.GetDataTable();
            this._response = "{\"result\" :\"1\",\"returnval\" :\"操作成功\"," + JumbotDLS.Utils.dtHelper.DT2JSON(dt, (200 * (1 - 1))) + "}";
        }
        private void ajaxDel()
        {
            string aId = f("id");
            if (JumbotDLS.Utils.Cookie.GetValue(site.CookiePrev + "admin", "id") == aId) //不能删除自己
                this._response = JsonResult(0, "不能删除自己");
            else
            {
                doh.Reset();
                doh.ConditionExpress = "id=" + aId;
                doh.AddFieldItem("AdminId", 0);
                doh.AddFieldItem("AdminName", "");
                doh.AddFieldItem("AdminRoleID", 0);
                doh.AddFieldItem("AdminState", 0);
                doh.AddFieldItem("Group", 1);
                doh.Update("jdls_normal_user");
                this._response = JsonResult(1, "成功删除");
            }
        }
    }
}