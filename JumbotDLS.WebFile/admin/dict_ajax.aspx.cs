﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JumbotDLS.WebFile.admin
{
    public partial class dict_ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!CheckFormUrl())
            {
                Response.End();
            }
            Admin_Load("master", "json");
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxGetList":
                    AjaxGetList();
                    break;
                case "ajaxDel":
                    ajaxDel();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }

        private void ajaxDel()
        {
            string uId = f("id");
            doh.Reset();
            doh.ConditionExpress = "id=@id";
            doh.AddConditionParameter("@id", uId);
            int _delCount = doh.Delete("jdls_module_article_module_field_item");
            if (_delCount > 0)
                this._response = JsonResult(1, "成功删除");
            else
                this._response = JsonResult(0, "删除失败");
        }

        private void AjaxGetList()
        {
            int page = Int_ThisPage();
            int PSize = Str2Int(q("pagesize"), 20);
            int totalCount = 0;
            string groupname = q("groupname");
            string sqlStr = "";
            string joinStr = "B.name=A.groupname";
            string whereStr1 = "";
            string whereStr2 = "";
            if (groupname!="0"&&groupname!="")
            {
                whereStr1 += "groupname='" + groupname + "'";
                whereStr2 += "groupname='" + groupname + "'";
            }
            doh.Reset();
            doh.ConditionExpress = whereStr1;
            totalCount = doh.Count("jdls_module_article_module_field_item");
            sqlStr = JumbotDLS.Utils.SqlHelp.GetSql0("B.name,B.TITLE,A.*", "jdls_module_article_module_field_item", "jdls_module_article_module_field_item_group", "Id", PSize, page, "desc", joinStr, whereStr1, whereStr2);
            doh.Reset();
            doh.SqlCmd = sqlStr;
            DataTable dt = doh.GetDataTable();
            this._response = "{\"result\" :\"1\"," +
                "\"returnval\" :\"操作成功\"," +
                "\"pagerbar\" :\"" + JumbotDLS.Utils.PageBar.GetPageBar(3, "js", 2, totalCount, PSize, page, "javascript:ajaxList($#page#$);") + "\"," +
                JumbotDLS.Utils.dtHelper.DT2JSON(dt) +
                "}";
            dt.Clear();
            dt.Dispose();
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
    }
}