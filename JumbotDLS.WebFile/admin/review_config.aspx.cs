﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.IO;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _review_config : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("review-mng", "stop");
            if (!Page.IsPostBack)
            {
                string strXmlFile1 = HttpContext.Current.Server.MapPath("~/_data/config/review.config");
                JumbotDLS.DBUtility.XmlControl XmlTool1 = new JumbotDLS.DBUtility.XmlControl(strXmlFile1);
                this.txtPageSize.Text = XmlTool1.GetText("Root/PageSize");
                this.txtPostTimer.Text = XmlTool1.GetText("Root/PostTimer");
                this.rblGuestPost.SelectedValue = XmlTool1.GetText("Root/GuestPost");
                this.rblNeedCheck.SelectedValue = XmlTool1.GetText("Root/NeedCheck");
                XmlTool1.Dispose();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string strXmlFile1 = HttpContext.Current.Server.MapPath("~/_data/config/review.config");
            JumbotDLS.DBUtility.XmlControl XmlTool1 = new JumbotDLS.DBUtility.XmlControl(strXmlFile1);
            XmlTool1.Update("Root/PageSize", Str2Str(this.txtPageSize.Text));
            XmlTool1.Update("Root/PostTimer", Str2Str(this.txtPostTimer.Text));
            XmlTool1.Update("Root/GuestPost", Str2Str(this.rblGuestPost.SelectedValue));
            XmlTool1.Update("Root/NeedCheck", Str2Str(this.rblNeedCheck.SelectedValue));
            XmlTool1.Save();
            XmlTool1.Dispose();
            FinalMessage("成功保存", "close.htm", 0);
        }
    }
}
