﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _extend_pagevisit_ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!CheckFormUrl())
            {
                Response.End();
            }
            Admin_Load("master", "json");
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxGetList":
                    ajaxGetList();
                    break;
                case "ajaxExport":
                    ajaxExport();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxGetList()
        {
            int page = Int_ThisPage();
            int PSize = Str2Int(q("pagesize"), 20);
            string whereStr = "1=1";
            doh.Reset();
            doh.ConditionExpress = whereStr;
            string sqlStr = "";
            int _countnum = doh.Count("jdls_extends_visitlogs");
            sqlStr = JumbotDLS.Utils.SqlHelp.GetSql0("*", "jdls_extends_visitlogs", "Id", PSize, page, "desc", whereStr);
            doh.Reset();
            doh.SqlCmd = sqlStr;
            DataTable dt = doh.GetDataTable();
            this._response = "{\"result\" :\"1\"," +
                "\"returnval\" :\"操作成功\"," +
                "\"pagebar\" :\"" + JumbotDLS.Utils.PageBar.GetPageBar(3, "js", 2, _countnum, PSize, page, "javascript:ajaxList($#page#$);") + "\"," +
                JumbotDLS.Utils.dtHelper.DT2JSON(dt, (PSize * (page - 1))) +
                "}";
            dt.Clear();
            dt.Dispose();
        }
        private void ajaxExport()
        {
            string sFileName = "网站访问日志(" + DateTime.Now.ToString("yyyy-MM-dd") + ").xls";
            string FullPath = "/_data/tempfiles/" + sFileName;
            if (JumbotDLS.Utils.DirFile.FileExists(FullPath))
            {
                this._response = "{result :\"0\", returnval :\"今天已经备份过了\"}";
                return;
            }
            string whereStr = "1=1";
            doh.Reset();
            doh.ConditionExpress = whereStr;
            int countNum = doh.Count("jdls_extends_visitlogs");
            if (countNum == 0)
            {
                this._response = JsonResult(0, "无需备份");
                return;
            }
            doh.Reset();
            doh.SqlCmd = "SELECT CountIp as 来访IP,CountReferer as 访问地址,CountCountry as 访问地区,countiplocal as 来访ISP,CountTime as 访问时间 FROM [jdls_extends_visitlogs] WHERE " + whereStr + " ORDER BY 访问时间 ASC";
            DataTable dt = doh.GetDataTable();
            int _successcount = 0;
            int _failurecount = 0;
            if (JumbotDLS.Utils.ExcelManage.OutputToExcel(true, dt, Server.MapPath(FullPath), ref _successcount, ref _failurecount))
            {
                doh.Reset();
                doh.ConditionExpress = "1=1";
                doh.Delete("jdls_extends_visitlogs");
                this._response = JsonResult(1, FullPath);
            }
            else
            {
                this._response = JsonResult(0, "备份失败");
            }
        }
    }
}