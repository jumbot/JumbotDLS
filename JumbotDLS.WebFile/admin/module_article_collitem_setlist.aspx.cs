﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _module_article_collectitem_setlist : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 99999999;
            ChannelId = Str2Str(q("ccid"));
            id = Str2Str(q("id"));
            Admin_Load(ChannelId + "-01", "stop", true);
            JumbotDLS.DBUtility.WebFormHandler wh = new JumbotDLS.DBUtility.WebFormHandler(doh, "jdls_module_article_collitem", btnSave);
            wh.AddBind(txtListStr, "ListStr", true);
            wh.AddBind(ddlListWebEncode, "ListWebEncode", false);
            wh.AddBind(txtListStart, "ListStart", true);
            wh.AddBind(txtListEnd, "ListEnd", true);
            wh.ConditionExpress = "id=" + id;
            wh.Mode = JumbotDLS.DBUtility.OperationType.Modify;
            wh.ModifyOk += new EventHandler(save_ok);
            wh.validator = chkForm;
        }
        /// <summary>
        /// 绑定数据后的处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bind_ok(object sender, EventArgs e)
        {
        }
        protected bool chkForm()
        {
            if (!CheckFormUrl())
                return false;
            if (!Page.IsValid)
                return false;
            return true;
        }
        protected void save_ok(object sender, EventArgs e)
        {
            FinalMessage("成功保存", "module_article_collitem_setlink.aspx?ccid=" + ChannelId + "&id=" + id, 0);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
        }
    }
}
