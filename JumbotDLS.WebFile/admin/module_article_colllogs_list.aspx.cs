﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
using JumbotDLS.Utils;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _module_article_colllogs_list : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("", "html");
            string sqlStr = "SELECT id,Title FROM [jdls_module_article_collitem] WHERE flag=1";
            doh.Reset();
            doh.SqlCmd = sqlStr;
            DataTable dt = doh.GetDataTable();
            this.ddlCollitemList.Items.Clear();
            this.ddlCollitemList.Items.Add(new ListItem("==不限制==", "0"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                this.ddlCollitemList.Items.Add(new ListItem(dt.Rows[i]["Title"].ToString(), dt.Rows[i]["Id"].ToString()));
            }
            dt.Clear();
            dt.Dispose();
        }
    }
}