﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _executesql_ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!CheckFormUrl())
            {
                Response.End();
            }
            Admin_Load("master", "json");
            this._operType = q("oper");
            switch (this._operType)
            {
                case "executesql":
                    ajaxExecuteSql();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxExecuteSql()
        {
            int _canExecuteSQL = 0;
            if (Const.GetUserIp == Request.ServerVariables["LOCAl_ADDR"])
                _canExecuteSQL = 1;
            else
                _canExecuteSQL = Str2Int(JumbotDLS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "ExecuteSql"));
            if (_canExecuteSQL == 1)
            {
                string _SQLContent = f("txtSQLContent");
                if (_SQLContent.Length == 0)
                {
                    this._response = "top.JumbotDLS.Alert('脚本有误', '0');";
                    return;
                }
                string _tmpFile = site.Dir + "_data/sql/" + DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".sql";
                JumbotDLS.Utils.DirFile.SaveFile(_SQLContent, _tmpFile);
                if (ExecuteSqlInFile(Server.MapPath(_tmpFile)))
                    this._response = "top.JumbotDLS.Message('脚本执行成功', '1');";
                else
                    this._response = "top.JumbotDLS.Alert('脚本执行错误', '0');";
            }
            else
                this._response = "top.JumbotDLS.Alert('客户端" + Const.GetUserIp + "不可执行服务器" + Request.ServerVariables["LOCAl_ADDR"] + "SQL', '0');";
        }
    }
}