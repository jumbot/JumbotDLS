﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JumbotDLS.WebFile.admin
{
    public partial class dict_list : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("", "html");
            string sqlStr = "SELECT * FROM [jdls_module_article_module_field_item_group]";
            doh.Reset();
            doh.SqlCmd = sqlStr;
            DataTable dt = doh.GetDataTable();
            this.ddlCollitemList.Items.Clear();
            this.ddlCollitemList.Items.Add(new ListItem("==全部==", "0"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                this.ddlCollitemList.Items.Add(new ListItem(dt.Rows[i]["Title"].ToString(), dt.Rows[i]["name"].ToString()));
            }
            dt.Clear();
            dt.Dispose();
        }
    }
}