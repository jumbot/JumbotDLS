﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="content_edit_version.aspx.cs" Inherits="JumbotDLS.WebFile.Admin._content_edit_version" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta   http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title><%=_ModuleName%>管理</title>
<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js"></script>
<link   type="text/css" rel="stylesheet" href="../_data/global.css" />
<link   type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
<link rel="stylesheet" href="../_libs/kindeditor-4.1.10/themes/default/default.css" />
<link rel="stylesheet" href="../_libs/kindeditor-4.1.10/plugins/code/prettify.css" />
<script charset="utf-8" src="../_libs/kindeditor-4.1.10/kindeditor.js"></script>
<script charset="utf-8" src="../_libs/kindeditor-4.1.10/lang/zh_CN.js"></script>
<script charset="utf-8" src="../_libs/kindeditor-4.1.10/plugins/code/prettify.js"></script>
<script type="text/javascript">
var ccid = joinValue('ccid');
$(document).ready(function(){
	$.formValidator.initConfig({ onError: function (msg) { alert(msg); } });
	<%=MyFormFieldRequiredList %>
	$("#txtAliasPage").formValidator({empty:true,tipid:"tipAliasPage",onshow:"指定文件路径(第1页)，不输入即为默认。如/aa/bb/cc.html",onfocus:"动态频道只支持aspx结尾，请慎重输入"}).RegexValidator({regexp:"^\(/[_\-a-zA-Z0-9\.]+(/[_\-a-zA-Z0-9\.]+)*\.(aspx|htm(l)?|shtm(l)?))$",onerror:"以“/”开头，后缀支持aspx|htm(l)|shtm(l)，如/aa/bb/cc.html"});

});
/*最后的表单验证*/
function CheckFormSubmit(){
	if($.formValidator.PageIsValid('1'))
	{
	    JumbotDLS.Loading.show("正在处理，请等待...");
		return true;
	}else{
		return false;
	}
}
window.isIE = (navigator.appName == "Microsoft Internet Explorer");
//插入预览附件地址
function AttachmentSelected(path,elementid)
{
	$("#"+elementid).val(path);
}
//处理价格excel
function ajaxExecutePriceFile(path, field) {
	$.ajax({
		type:		"post",
		dataType:	"json",
		data:		"database="+path,
		url:		"ajax.aspx?oper=ajaxExecutePriceFile&clienttime="+Math.random(),
		error:		function(XmlHttpRequest,textStatus, errorThrown){alert(XmlHttpRequest.responseText); },
		success:	function(d){
			switch (d.result)
			{
			case '-1':
				top.JumbotDLS.Alert(d.returnval, "0", "top.location.href='login.aspx';");
				break;
			case '0':
				top.JumbotDLS.Alert(d.returnval, "0");
				break;
			case '1':
            	var returnval = d.returnval.replace(/[$]{3}/g, '\r');
                $('#'+field).val(returnval);
				break;
			}
		}
	});
}
var PhotoInput = 'txtImg';
function FillPhoto(photo){
	$('#'+PhotoInput).val(photo);
}
</script>
   <style type="text/css">
        .chartOptionsFlowTrend ul li
        {
            list-style: none;
            padding: 0px;
            margin: 0px;
        }
        
        .select_checkBox
        {
            border: 0px solid red;
            position: relative;
            display: inline-block;
        }
        .chartQuota
        {
            height: 23px;
            float: left;
            display: inline-block;
            border: 0px solid black;
            position: relative;
        }
        
        .chartOptionsFlowTrend
        {
            z-index: 300;
            background-color: #fff;
            border: 1px solid #ccc;
            display: none;
            position: absolute;
            left: 0px;
            top: 23px;
            width: 300px;
        }
        .chartOptionsFlowTrend ul
        {
            float: left;
            padding: 0px;
            margin: 5px;
        }
        .chartOptionsFlowTrend li
        {
            /* float:left; */
            display: block;
            position: relative;
            left: 0px;
            margin: 0px;
            clear: both;
            line-height:30px;
        }
        .chartOptionsFlowTrend li *
        {
            float: left;
            line-height:30px;
        }
         .chartOptionsFlowTrend li input
        {
            margin-top:9px;
            margin-right:3px;
        }
        a:-webkit-any-link
        {
            color: -webkit-link;
            text-decoration: underline;
            cursor: auto;
        }
        .chartQuota p a
        {
            float: left;
            height: 21px;
            outline: 0 none;
            border: 1px solid #ccc;
            line-height: 22px;
            padding: 0 5px;
            overflow: hidden;
            background: #eaeaea;
            color: #313131;
            text-decoration: none;
        }
        
        .chartQuota p
        {
            margin: 0px;
            float: left;
            overflow: hidden;
            height: 23px;
            line-height: 24px;
            display: inline-block;
        }
    </style>
</head>
<body>
<form id="form1" runat="server" onsubmit="return CheckFormSubmit()">
	<table class="formtable">
		<tr>
			<th> 一级分类 </th>
			<td><asp:DropDownList ID="ddlClassId" runat="server"><asp:ListItem Text="===未分类===" Value="0"></asp:ListItem></asp:DropDownList></td>
		</tr>
		<tr>
			<th>子分类</th>
			<td>
            <asp:TextBox ID="txtSubClassList" runat="server" Width="97%" CssClass="ipt"></asp:TextBox>
            <br />
				<div class="select_checkBox">
        <div class="chartQuota">
            <p>
                <a href="javascript:;" title="请选择子分类"><span>选择子分类</span> <b></b></a>
            </p>
            <span style="display:none;">
            
            </span>
        </div>
        <div class="chartOptionsFlowTrend">
            <ul id="subclasslist"></ul>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $(".select_checkBox").hover(function () {
                var _classid = $('#ddlClassId').val();
                if (_classid == "0") return;
                var _subclasslist_my = $("#txtSubClassList").val().split(';');
                var _subclasslist_all = [];
                $.ajax({
                    type: "get",
                    async: false,
                    dataType: "json",
                    data: "classid=" + _classid + "&clienttime=" + Math.random() + ccid,
                    url: "class_ajax.aspx?oper=ajaxGetSubClassList",
                    error: function (XmlHttpRequest, textStatus, errorThrown) { alert(XmlHttpRequest.responseText); },
                    success: function (d) {
                        switch (d.result) {
                            case '-1':
                                JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                                break;
                            case '0':
                                JumbotDLS.Alert(d.returnval, "0");
                                break;
                            case '1':
                                _subclasslist_all = d.returnval.split(';');
                                break;
                        }
                    }
                });
                if (_subclasslist_all.length > 0) {
                    $('#subclasslist').html('');
                    for (var i = 0; i < _subclasslist_all.length; i++) {
                        if (_subclasslist_all[i].length > 1)//两个字符才有效
                        {
                            $('#subclasslist').append('<li><input id="subclass_' + i + '" type="checkbox" ' + ($.inArray(_subclasslist_all[i], _subclasslist_my) > -1 ? 'checked' : '') + ' onclick="selectSubClass()" value="' + _subclasslist_all[i] + '"><label for="subclass_' + i + '">' +_subclasslist_all[i] + '</label> </li>');
                        }
                    }
                    $(this).find(".chartOptionsFlowTrend").show();
                }
            }, function () {
                $(this).find(".chartOptionsFlowTrend").hide();
            });
        });
        function selectSubClass() {
            var result = new Array();
            $("#subclasslist input:checkbox").each(function () {
                if ($(this).is(":checked")) {
                    result.push($(this).attr("value"));
                }
            });
            $("#txtSubClassList").val(result.join(";"));
        }
    </script>
                
                </td>
		</tr>
		<tr style="display:none;">
			<th> 最低浏览权限 </th>
			<td><asp:DropDownList ID="ddlReadGroup" runat="server"> </asp:DropDownList>
			(此功能在静态生成的频道无效)
			</td>
		</tr>
		<tr>
			<th> 原文编码 </th>
			<td><asp:TextBox ID="txtSourceContentCode" runat="server" CssClass="ipt" Width="180px"></asp:TextBox> 每一篇原文有且只有一个唯一的编码
			</td>
		</tr>
        <%=MyFormFieldList %>
		<tr>
			<th> 发布时间 </th>
			<td><asp:TextBox ID="txtAddDate" runat="server" CssClass="ipt" Width="180px"></asp:TextBox>
			<span style="display:none;">
				<asp:TextBox ID="txtEditor" runat="server" CssClass="ipt"></asp:TextBox>
				<asp:TextBox ID="txtUserId" runat="server" CssClass="ipt">0</asp:TextBox>
				</span></td>
		</tr>

		<tr style="display:none;">
			<th> 推荐 </th>
			<td><asp:RadioButtonList ID="rblIsTop" runat="server" RepeatColumns="2">
					<Items>
						<asp:ListItem Value="0" Selected="True">否</asp:ListItem>
						<asp:ListItem Value="1">是</asp:ListItem>
					</Items>
				</asp:RadioButtonList>
			</td>
		</tr>
        <tr>
          <th> 内容页模板 </th>
          <td><asp:DropDownList ID="ddlThemeId" runat="server"> </asp:DropDownList>
          </td>
        </tr>
        <tr style="display:none;">
          <th>HTML代码<br /><span style="color:red">模板开发人员专用</span></th>
          <td><asp:TextBox ID="txtHtmlCode" runat="server" Height="97px" TextMode="MultiLine" Width="97%" CssClass="ipt"></asp:TextBox>
          <br />用“@@@”隔开，前台直接使用{$_htmlcode(1)}、{$_htmlcode(2)}</td>
        </tr>
		<tr style="display:<%=AdminIsFounder? "" : "none"%>;">
			<th> 锁定 </th>
			<td><asp:RadioButtonList ID="rblLocked" runat="server" RepeatColumns="2">
					<Items>
						<asp:ListItem Value="0" Selected="True">否</asp:ListItem>
						<asp:ListItem Value="1">是</asp:ListItem>
					</Items>
				</asp:RadioButtonList>
			</td>
		</tr>
		<tr style="display:<%=AdminIsFounder? "" : "none"%>;">
			<th> 指定文件路径(第1页) </th>
			<td><asp:TextBox ID="txtAliasPage" runat="server" Width="97%" CssClass="ipt"></asp:TextBox>
				<br /><span id="tipAliasPage" style="width:600px;"></span></td>
		</tr>
	</table>
	<div class="buttonok">
		<asp:CheckBox ID="chkIsEdit" runat="server" Text="立即发布" Visible="false" />
		<asp:Button ID="btnSave" runat="server" Text="确定" CssClass="btnsubmit" 
            OnClick="btnSave_Click" />
		<input id="btnReset" type="button" value="返回" class="btncancel" onclick="parent.JumbotDLS.Popup.hide();" />
	</div>
</form>
<script type="text/javascript">_jdls_SetDialogTitle();</script>
</body>
</html>
