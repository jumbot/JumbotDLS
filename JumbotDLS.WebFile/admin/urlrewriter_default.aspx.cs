﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _urlrewriter_index : JumbotDLS.UI.AdminCenter
    {
        public string _rules;
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("master", "html");
            string _tmpFile = site.Dir + "_data/UrlRewriter.config";
            _rules = Server.HtmlEncode(JumbotDLS.Utils.DirFile.ReadFile(_tmpFile));
        }
    }
}
