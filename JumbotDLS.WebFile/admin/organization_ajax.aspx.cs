﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Text;
using System.Web;
using System.IO;
using JumbotDLS.Utils;
using JumbotDLS.Common;
using System.Linq;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _organization_ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!CheckFormUrl())
            {
                Response.End();
            }
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxGetList":
                    ajaxGetList();
                    break;
                case "ajaxBatchOper":
                    ajaxBatchOper();
                    break;
                case "move":
                    ajaxMove();
                    break;
                case "ajaxDel":
                    ajaxDel();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxGetList()
        {
            //不要加权限控制
            doh.Reset();
            doh.SqlCmd = "Select * FROM [jdls_organization] ORDER BY pid";
            DataTable dt = doh.GetDataTable();
            this._response = "{\"result\" :\"1\",\"returnval\" :\"操作成功\"," + JumbotDLS.Utils.dtHelper.DT2JSON(dt) + "}";
        }
        private void ajaxMove()
        {
            Admin_Load("master", "json");
            string id = f("id");
            string isUp = f("up");
            if (id == "0")
            {
                this._response = JsonResult(0, "ID错误");
                return;
            }
            doh.Reset();
            doh.ConditionExpress = "id=@id";
            doh.AddConditionParameter("@id", id);
            string pId = doh.GetField("jdls_organization", "pId").ToString();

            string temp;
            doh.Reset();
            if (isUp == "1")
            {
                doh.ConditionExpress = "pId<@pId ORDER BY pId desc";
                doh.AddConditionParameter("@pId", pId);
            }
            else
            {
                doh.ConditionExpress = "pId>@pId ORDER BY pId";
                doh.AddConditionParameter("@pId", pId);
            }
            temp = doh.GetField("jdls_organization", "pId").ToString();
            if (temp != "")
            {
                doh.Reset();
                doh.ConditionExpress = "pId=@pId";
                doh.AddConditionParameter("@pId", temp);
                doh.AddFieldItem("pId", "-100000");
                doh.Update("jdls_organization");
                doh.Reset();
                doh.ConditionExpress = "id=@id";
                doh.AddConditionParameter("@id", id);
                doh.AddFieldItem("pId", temp);
                doh.Update("jdls_organization");
                doh.Reset();
                doh.ConditionExpress = "pId=@pId";
                doh.AddConditionParameter("@pId", "-100000");
                doh.AddFieldItem("pId", pId);
                doh.Update("jdls_organization");
                this._response = JsonResult(1, "成功移动");
            }
            else
                this._response = JsonResult(0, "无须移动");
        }
        private void ajaxDel()
        {
            Admin_Load("master", "json");
            string organizationId = f("id");
            doh.Reset();
            doh.ConditionExpress = "id=@id";
            doh.AddConditionParameter("@id", organizationId);
            doh.Delete("jdls_organization");
            doh.Reset();
            doh.ConditionExpress = "organizationid=@organizationid";
            doh.AddConditionParameter("@organizationid", organizationId);
            doh.Delete("jdls_organization_whiteip");
            
            this._response = JsonResult(1, "机构成功删除");
        }

        /// <summary>
        /// 执行机构的重建,审核,删除等操作
        /// </summary>
        /// <param name="oper"></param>
        /// <param name="ids"></param>
        private void ajaxBatchOper()
        {
            Admin_Load("master", "json");
            string act = q("act");
            string ids = f("ids");
            string[] idValue;
            idValue = ids.Split(',');
            string ClassId = string.Empty;
            if (act == "pass")
            {
                for (int i = 0; i < idValue.Length; i++)
                {
                    idValue[i] = JumbotDLS.Utils.Validator.StrToInt(idValue[i], 0).ToString();
                    doh.Reset();
                    doh.ConditionExpress = "id=@id";
                    doh.AddConditionParameter("@id", Str2Str(idValue[i]));
                    doh.AddFieldItem("Enabled", 1);
                    doh.Update("jdls_organization");
                }
            }
            else if (act == "nopass")
            {
                for (int i = 0; i < idValue.Length; i++)
                {
                    idValue[i] = JumbotDLS.Utils.Validator.StrToInt(idValue[i], 0).ToString();
                    doh.Reset();
                    doh.ConditionExpress = "id=@id";
                    doh.AddConditionParameter("@id", Str2Str(idValue[i]));
                    doh.AddFieldItem("Enabled", 0);
                    doh.Update("jdls_organization");
                }
            }
            else if (act == "nav")
            {
                for (int i = 0; i < idValue.Length; i++)
                {
                    idValue[i] = JumbotDLS.Utils.Validator.StrToInt(idValue[i], 0).ToString();
                    doh.Reset();
                    doh.ConditionExpress = "id=@id";
                    doh.AddConditionParameter("@id", Str2Str(idValue[i]));
                    doh.AddFieldItem("IsNav", 1);
                    doh.Update("jdls_organization");
                }
            }
            else if (act == "nonav")
            {
                for (int i = 0; i < idValue.Length; i++)
                {
                    idValue[i] = JumbotDLS.Utils.Validator.StrToInt(idValue[i], 0).ToString();
                    doh.Reset();
                    doh.ConditionExpress = "id=@id";
                    doh.AddConditionParameter("@id", Str2Str(idValue[i]));
                    doh.AddFieldItem("IsNav", 0);
                    doh.Update("jdls_organization");
                }
            }
            this._response = JsonResult(1, "操作成功");
        }
    }
}