﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="question_edit.aspx.cs" Inherits="JumbotDLS.WebFile.Admin._user_question_edit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta   http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title></title>
<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js"></script>
<link type="text/css" rel="stylesheet" href="../_data/global.css" />
<link   type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
</head>
<body>
<form id="form1" runat="server">
		<table class="formtable">
			<tr>
				<th> 姓名 </th>
				<td><asp:Label ID="lblUserName" runat="server" Text="Label"></asp:Label></td>
			</tr>
            			<tr>
				<th> 电话 </th>
				<td><asp:Label ID="lblPhone" runat="server" Text="Label"></asp:Label></td>
			</tr>
            			<tr>
				<th> 邮箱 </th>
				<td><asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label></td>
			</tr>
			<tr>
				<th> 留言时间 </th>
				<td><asp:Label ID="lblAddDate" runat="server" Text="Label"></asp:Label></td>
			</tr>
            <tr>
				<th> 留言标题 </th>
				<td><asp:Label ID="lblTitle" runat="server" Text="Label"></asp:Label></td>
			</tr>
			<tr>
				<th> 留言内容 </th>
				<td><asp:Label ID="lblContent" runat="server" Text="Label"></asp:Label></td>
			</tr>
		<tr>
			<th>回复内容</th>
			<td><asp:TextBox ID="txtReplyContent" runat="server" Height="97px" TextMode="MultiLine" Width="97%" CssClass="ipt"></asp:TextBox></span>
			</td>
		</tr>
			<tr>
				<th> 回复时间 </th>
				<td><asp:Label ID="lblReplyTime" runat="server" Text="Label"></asp:Label></td>
			</tr>
		</table>
	<div class="buttonok">
		<asp:Button ID="btnSave" runat="server" Text="确定" CssClass="btnsubmit" 
            onclick="btnSave_Click" />
		<input id="btnReset" type="button" value="取消" class="btncancel" onclick="parent.JumbotDLS.Popup.hide();" />
	</div>
</form>
</body>
</html>
