﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */
using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
using System.Data.OleDb;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _content_export_ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;
        public int _ModuleId = 0;
        public string _ModuleName = "";
        public string _ModuleType = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ccid"));
            _ModuleId = Str2Int(q("moduleid"));
            Admin_Load("", "json", true);
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxExport":
                    ajaxExport();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxExport()
        {
            Server.ScriptTimeout = 999999;//脚本过期时间
            doh.Reset();
            doh.ConditionExpress = "id=@id";
            doh.AddConditionParameter("@id", _ModuleId);
            object[] _value = doh.GetFields("jdls_module_article_module", "title,type");
            if (_value == null)
            {
                this._response = JsonResult(0, "参数有误");
                return;
            }
            _ModuleName = _value[0].ToString();
            _ModuleType = _value[1].ToString();
            string classid = Str2Str(f("classid"));
            string database = f("database");
            int page = Int_ThisPage();
            int PSize = 200;
            doh.Reset();
            doh.SqlCmd = "Select * FROM [jdls_module_article_module_field] WHERE [State]=1 AND  [ModuleId]=" + _ModuleId + " ORDER BY pid";
            DataTable dtModuleField = doh.GetDataTable();
            Dictionary<string, string> _param = new Dictionary<string, string>();
            for (int i = 0; i < dtModuleField.Rows.Count; i++)
            {
                //if(dtModuleField.Rows[i]["FieldName"].ToString().ToLower()!="summary")
                _param.Add(dtModuleField.Rows[i]["FieldName"].ToString().ToLower(), dtModuleField.Rows[i]["FieldTitle"].ToString());
            }
            dtModuleField.Clear();
            dtModuleField.Dispose();
            string _wherestr = "Module='" + _ModuleType + "' and (0=" + classid + " OR classid=" + classid + ")";
            doh.Reset();
            doh.ConditionExpress = _wherestr;
            string sqlStr = "";
            int _totalcount = doh.Count("jdls_module_article");
            string _SelectFields = "id,channelid,classid,(select folder from [jdls_normal_class] where id=jdls_module_article.classid) as 分类,SubClassList as 子分类";
            foreach (KeyValuePair<string, string> a in _param)
            {
                _SelectFields += "," + a.Key + " as [" + a.Value + "]";
            }
            sqlStr = JumbotDLS.Utils.SqlHelp.GetSql0(_SelectFields, "jdls_module_article", "Id", PSize, page, "asc", _wherestr);
            doh.Reset();
            doh.SqlCmd = sqlStr;
            DataTable dt = doh.GetDataTable();

            #region 人物模型的子库处理
            if (_ModuleType == "person0")
            {
                for (int i2 = 0; i2 < 20; i2++)
                {
                        dt.Columns.Add("教育背景" + (i2 + 1) + "学位", Type.GetType("System.String"));
                        dt.Columns.Add("教育背景" + (i2 + 1) + "机构", Type.GetType("System.String"));
                        dt.Columns.Add("教育背景" + (i2 + 1) + "开始时间", Type.GetType("System.String"));
                        dt.Columns.Add("教育背景" + (i2 + 1) + "结束时间", Type.GetType("System.String"));
                }
                for (int i3 = 0; i3 < 20; i3++)
                {
                    dt.Columns.Add("从业背景" + (i3 + 1) + "单位", Type.GetType("System.String"));
                    dt.Columns.Add("从业背景" + (i3 + 1) + "部门", Type.GetType("System.String"));
                    dt.Columns.Add("从业背景" + (i3 + 1) + "职位", Type.GetType("System.String"));
                    dt.Columns.Add("从业背景" + (i3 + 1) + "开始时间", Type.GetType("System.String"));
                    dt.Columns.Add("从业背景" + (i3 + 1) + "结束时间", Type.GetType("System.String"));
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //*
                    doh.Reset();
                    doh.SqlCmd = "select top 20 Title,Title2,CustomField01,CustomField02 from jdls_module_article where module='person_education' and parentid=" + dt.Rows[i]["id"].ToString() + " order by title";
                    DataTable dt2 = doh.GetDataTable();
                    for (int i2 = 0; i2 < dt2.Rows.Count; i2++)
                    {
                        dt.Rows[i]["教育背景" + (i2 + 1) + "学位"] = dt2.Rows[i2][3].ToString();
                        dt.Rows[i]["教育背景" + (i2 + 1) + "机构"] = dt2.Rows[i2][2].ToString();
                        dt.Rows[i]["教育背景" + (i2 + 1) + "开始时间"] = dt2.Rows[i2][0].ToString() == "0000-00-00" ? "" : dt2.Rows[i2][0].ToString();
                        dt.Rows[i]["教育背景" + (i2 + 1) + "结束时间"] = dt2.Rows[i2][1].ToString() == "0000-00-00" ? "" : dt2.Rows[i2][1].ToString();
                    }
                    dt2.Clear();
                    dt2.Dispose();
                    doh.Reset();
                    doh.SqlCmd = "select top 20  Title,Title2,CustomField01,CustomField02,CustomField03 from jdls_module_article where module='person_job' and parentid=" + dt.Rows[i]["id"].ToString() + " order by title";
                    DataTable dt3 = doh.GetDataTable();
                    for (int i3 = 0; i3 < dt3.Rows.Count; i3++)
                    {
                        dt.Rows[i]["从业背景" + (i3 + 1) + "单位"] = dt3.Rows[i3][2].ToString();
                        dt.Rows[i]["从业背景" + (i3 + 1) + "部门"] = dt3.Rows[i3][3].ToString();
                        dt.Rows[i]["从业背景" + (i3 + 1) + "职位"] = dt3.Rows[i3][4].ToString();
                        dt.Rows[i]["从业背景" + (i3 + 1) + "开始时间"] = dt3.Rows[i3][0].ToString() == "0000-00-00" ? "" : dt3.Rows[i3][0].ToString();
                        dt.Rows[i]["从业背景" + (i3 + 1) + "结束时间"] = dt3.Rows[i3][1].ToString() == "0000-00-00" ? "" : dt3.Rows[i3][1].ToString();
                    }
                    dt3.Clear();
                    dt3.Dispose();
                    // * */
                }
            }
            #endregion

            string outfile = "/_export/" + database;

            int _thiscount = dt.Rows.Count;
            int _success = 0, _error = 0;

            JumbotDLS.Utils.AsposeCellsHelper.OutputToExcel((page == 1 ? true : false), dt, Server.MapPath(outfile), false,ref _success, ref _error);


            /*
            if (page == 1)
            {
                JumbotDLS.Utils.DirFile.CopyFile("/_template/moban_" + _ModuleType + ".mdb", outfile, false);
            }
            using (JumbotDLS.DBUtility.DbOperHandler doh_this = new JumbotDLS.DBUtility.OleDbOperHandler(Server.MapPath(outfile)))
            {
                if (page == 1)
                {
                    if (_ModuleType != "person")//得先初始化一张表
                    {
                        string _sql = "CREATE TABLE [" + _ModuleName + "] (";
                        foreach (KeyValuePair<string, string> a in _param)
                        {
                            _sql += "[" + a.Value + "] varchar(255) NOT NULL DEFAULT \"\",";
                        }


                        _sql += "[Id] AutoIncrement primary key)";
                        doh_this.Reset();
                        doh_this.SqlCmd=_sql;
                        doh_this.ExecuteSqlNonQuery();
                    }
                }
                for (int i = 0; i < _thiscount; i++)
                {
                    try
                    {
                        doh_this.Reset();
                        foreach (DataColumn col in dt.Columns)
                        {
                            doh_this.AddFieldItem(col.ColumnName, dt.Rows[i][col.ColumnName].ToString());
                        }
                        doh_this.Insert(_ModuleName);
                        _success++;
                    }
                    catch
                    {
                        _error++;
                    }
                }
            }
            */
            dt.Clear();
            dt.Dispose();

            if (_thiscount == 0)//说明可以结束了
                this._response = JsonResult(1, "成功[" + _success + "],失败[" + _error + "]");
            else
                this._response = JsonResult(2, "成功[" + _success + "],失败[" + _error + "]");
        }
    }
}
