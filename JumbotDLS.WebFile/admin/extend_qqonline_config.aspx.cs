﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.IO;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _extend_qqonline_config : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("master", "html");
            if (!Page.IsPostBack)
            {
                string strXmlFile1 = HttpContext.Current.Server.MapPath("~/_data/config/extends/qqonline.config");
                JumbotDLS.DBUtility.XmlControl XmlTool1 = new JumbotDLS.DBUtility.XmlControl(strXmlFile1);
                this.txtSiteShowX.Text = XmlTool1.GetText("Root/siteshowx");
                this.txtSiteShowY.Text = XmlTool1.GetText("Root/siteshowy");
                this.rblSiteArea.SelectedValue = XmlTool1.GetText("Root/sitearea");
                this.rblSiteSkin.SelectedValue = XmlTool1.GetText("Root/siteskin");
                XmlTool1.Dispose();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string strXmlFile1 = HttpContext.Current.Server.MapPath("~/_data/config/extends/qqonline.config");
            JumbotDLS.DBUtility.XmlControl XmlTool1 = new JumbotDLS.DBUtility.XmlControl(strXmlFile1);
            XmlTool1.Update("Root/siteshowx", this.txtSiteShowX.Text);
            XmlTool1.Update("Root/siteshowy", this.txtSiteShowY.Text);
            XmlTool1.Update("Root/sitearea", this.rblSiteArea.SelectedValue);
            XmlTool1.Update("Root/siteskin", this.rblSiteSkin.SelectedValue);
            XmlTool1.Save();
            XmlTool1.Dispose();
            FinalMessage("成功保存", "close.htm", 0);
        }
    }
}
