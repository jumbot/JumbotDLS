﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Web;
using System.IO;
using System.Data;
using Newtonsoft.Json.Linq;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _other_upfile : JumbotDLS.UI.AdminCenter
    {
        private string _sAdminUploadPath;
        private string _sAdminUploadType;
        private int _sAdminUploadSize = 0;
        /// <summary>
        /// 转换方法
        /// </summary>
        /// <param name="size">字节值</param>
        /// <returns></returns>
        private String HumanReadableFilesize(double size)
        {
            String[] units = new String[] { "B", "KB", "MB", "GB", "TB", "PB" };
            double mod = 1024.0;
            int i = 0;
            while (size >= mod)
            {
                size /= mod;
                i++;
            }
            return Math.Round(size) + units[i];
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //请勿使用Session和Cookies来判断权限
            Admin_Load("ok", "html");
            string UploadType = q("type");
            if (!(new JumbotDLS.DAL.AdminDAL()).ChkAdminSign(q("adminid"), q("adminsign")))
            {
                Response.Write(JsonResult(0, "验证信息有误"));
                Response.End();
            }
            if (Request.Files.Count > 0)
            {
                HttpPostedFile oFile = Request.Files[0];//得到要上传文件
                if (oFile != null && oFile.ContentLength > 0)
                {
                    if (!JumbotDLS.Utils.FileValidation.IsSecureUploadPhoto(oFile))
                    {
                        SaveVisitLog(2, 0);
                        Response.Write(JsonResult(0, "不安全的图片格式，换一张吧。"));
                    }
                    else
                    {
                        try
                        {
                            string fileExtension = System.IO.Path.GetExtension(oFile.FileName).ToLower(); //上传文件的扩展名
                            string fileName = System.IO.Path.GetFileName(oFile.FileName).ToLower(); //上传文件名
                            string strXmlFile = HttpContext.Current.Server.MapPath("~/_data/config/upload_admin_other.config");
                            JumbotDLS.DBUtility.XmlControl XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
                            this._sAdminUploadPath = XmlTool.GetText("Root/" + UploadType + "/path").Replace("<#SiteDir#>", site.Dir);
                            this._sAdminUploadType = XmlTool.GetText("Root/" + UploadType + "/type");
                            this._sAdminUploadSize = Str2Int(XmlTool.GetText("Root/" + UploadType + "/size"), 1024);
                            XmlTool.Dispose();
                            if (this._sAdminUploadType.ToLower().Contains("*.*") || this._sAdminUploadType.ToLower().Contains("*" + fileExtension + ";"))//检测是否为允许的上传文件类型
                            {
                                if (this._sAdminUploadSize * 1024 >= oFile.ContentLength)//检测文件大小是否超过限制
                                {
                                    string DirectoryPath;
                                    DirectoryPath = this._sAdminUploadPath;
                                    JumbotDLS.Utils.DirFile.CreateDir(DirectoryPath);
                                    string FullPath = DirectoryPath + fileName;//最终文件路径
                                    oFile.SaveAs(Server.MapPath(FullPath));
                                    doh.Reset();
                                    doh.AddFieldItem("userid", q("adminid"));
                                    doh.AddFieldItem("title", fileName);
                                    doh.AddFieldItem("filepath", FullPath);
                                    doh.AddFieldItem("operip", JumbotDLS.Utils.IPHelp.ClientIP);
                                    int _id = doh.Insert("upload_temp");
                                    JArray ja = new JArray();
                                    if (fileExtension == ".xls" || fileExtension == ".xlsx")
                                    {
                                        ja = JumbotDLS.Utils.AsposeCellsHelper.GetSheets(Server.MapPath(FullPath));
                                    }
                                    if (JumbotDLS.Utils.FileValidation.IsSecureUpfilePhoto(Server.MapPath(FullPath)))
                                        Response.Write(JsonResult(1, FullPath.Replace("//", "/") + "|" + oFile.ContentLength + "|" + "", ja));
                                    else
                                    {
                                        SaveVisitLog(2, 0);
                                        Response.Write(JsonResult(0, "不安全的图片格式，换一张吧。"));
                                    }

                                }
                                else
                                    Response.Write(JsonResult(0, "文件大小不得超过" + HumanReadableFilesize(this.ModuleUploadSize * 1024) + "。"));
                            }
                            else
                                Response.Write(JsonResult(0, "只允许上传" + this.ModuleUploadType.ToLower() + ""));
                        }
                        catch (Exception ex)
                        {
                            JumbotDLS.Utils.Logs.Error("upload", ex + "");
                            Response.Write(JsonResult(0, "程序异常，上传未成功。"));
                        }
                    }
                }
                else
                    Response.Write(JsonResult(0, "请选择上传文件。"));
            }
            else
                Response.Write(JsonResult(0, "上传有误。"));
        }

    }
}
