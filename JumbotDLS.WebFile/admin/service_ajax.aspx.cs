﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _service_ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!CheckFormUrl())
            {
                Response.End();
            }
            Admin_Load("master", "json");
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxGetList":
                    ajaxGetList();
                    break;
                case "ajaxDel":
                    ajaxDel();
                    break;
                case "checkusername":
                    ajaxCheckUserName();
                    break;
                case "checkservicename":
                    ajaxCheckServiceName();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }
        private void ajaxCheckUserName()
        {
            doh.Reset();
            doh.ConditionExpress = "username=@username";
            doh.AddConditionParameter("@username", q("txtUserName"));
            if (doh.Exist("jdls_normal_user"))
                this._response = JsonResult(1, "有此用户");
            else
                this._response = JsonResult(0, "帐号不存在");
        }

        private void ajaxCheckServiceName()
        {
            if (q("id") == "0")
            {
                doh.Reset();
                doh.ConditionExpress = "servicename=@servicename";
                doh.AddConditionParameter("@servicename", q("txtServiceName"));
                if (doh.Exist("jdls_normal_user"))
                    this._response = JsonResult(0, "不可添加");
                else
                    this._response = JsonResult(1, "可以添加");
            }
            else
                this._response = JsonResult(0, "不可修改"); ;
        }
        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxGetList()
        {
            doh.Reset();
            doh.SqlCmd = "Select [Id],[ServiceId],[ServiceName],[GUID],[Email],[LastTime3],[LastIp3] FROM [jdls_normal_user] WHERE [ServiceId]>0 ORDER BY Serviceid desc";
            DataTable dt = doh.GetDataTable();
            this._response = "{\"result\" :\"1\",\"returnval\" :\"操作成功\"," + JumbotDLS.Utils.dtHelper.DT2JSON(dt) + "}";
        }
        private void ajaxDel()
        {
            string aId = f("id");
            doh.Reset();
            doh.ConditionExpress = "id=" + aId;
            doh.AddFieldItem("ServiceId", 0);
            doh.AddFieldItem("ServiceName", "");
            doh.AddFieldItem("GUID", "");
            doh.Update("jdls_normal_user");
            new JumbotDLS.DAL.Normal_UserDAL().RefreshServiceList();
            this._response = JsonResult(1, "成功删除");
        }
    }
}