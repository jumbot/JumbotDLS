﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _userorder_ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!CheckFormUrl())
            {
                Response.End();
            }
            Admin_Load("master", "json");
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxGetList":
                    ajaxGetList();
                    break;
                case "ajaxDel":
                    ajaxDel();
                    break;
                case "ajaxCheck":
                    ajaxCheck();
                    break;
                case "ajaxGetGoodsList":
                    ajaxGetGoodsList();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxGetList()
        {
            int page = Int_ThisPage();
            int PSize = Str2Int(q("pagesize"), 20);
            string whereStr = "1=1";
            doh.Reset();
            doh.ConditionExpress = whereStr;
            string sqlStr = "";
            int _totalcount = doh.Count("jdls_normal_user_order");
            sqlStr = JumbotDLS.Utils.SqlHelp.GetSql0("*,(select username from [jdls_normal_user] where id =jdls_normal_user_order.userid) as username", "jdls_normal_user_order", "Id", PSize, page, "desc", whereStr);
            doh.Reset();
            doh.SqlCmd = sqlStr;
            DataTable dt = doh.GetDataTable();
            this._response = "{\"result\" :\"1\"," +
                "\"returnval\" :\"操作成功\"," +
                "\"pagebar\" :\"" + JumbotDLS.Utils.PageBar.GetPageBar(3, "js", 2, _totalcount, PSize, page, "javascript:ajaxList($#page#$);") + "\"," +
                JumbotDLS.Utils.dtHelper.DT2JSON(dt, (PSize * (page - 1))) +
                "}";
            dt.Clear();
            dt.Dispose();
        }
        private void ajaxCheck()
        {
            string orderNum = f("ordernum");
            doh.Reset();
            doh.ConditionExpress = "ordernum=@ordernum and State>=0";
            doh.AddConditionParameter("@ordernum", orderNum);
            doh.AddFieldItem("State", 2);
            if (doh.Update("jdls_normal_user_order") == 1)
            {
                doh.Reset();
                doh.ConditionExpress = "ordernum=@ordernum and state>=0";
                doh.AddConditionParameter("@ordernum", orderNum);
                doh.AddFieldItem("State", 2);
                doh.Update("jdls_normal_user_goods");
                this._response = JsonResult(1, "设置成功");
            }
            else
                this._response = JsonResult(0, "设置失败");
        }
        private void ajaxDel()
        {
            string orderNum = f("ordernum");
            doh.Reset();
            doh.ConditionExpress = "ordernum=@ordernum and State=0";
            doh.AddConditionParameter("@ordernum", orderNum);
            if (doh.Delete("jdls_normal_user_order") == 1)
            {
                doh.Reset();
                doh.ConditionExpress = "ordernum=@ordernum and state=0";
                doh.AddConditionParameter("@ordernum", orderNum);
                doh.Delete("jdls_normal_user_goods");
                this._response = JsonResult(1, "成功作废");
            }
            else
                this._response = JsonResult(0, "只有未支付的订单才能作废");
        }
        /// <summary>
        /// 通过订单号获得商品
        /// </summary>
        private void ajaxGetGoodsList()
        {
            int page = 1;
            int PSize = 100;
            string _ordernum = JumbotDLS.Utils.Strings.FilterSymbol(q("ordernum"));
            string mode = q("mode");
            int totalCount = 0;
            string sqlStr = "";
            string whereStr = " OrderNum='" + _ordernum + "'";
            doh.Reset();
            doh.ConditionExpress = whereStr;
            totalCount = doh.Count("jdls_normal_user_goods");
            sqlStr = JumbotDLS.Utils.SqlHelp.GetSql0("*", "jdls_normal_user_goods", "Id", PSize, page, "desc", whereStr);
            doh.Reset();
            doh.SqlCmd = sqlStr;
            DataTable dt = doh.GetDataTable();
            this._response = "{\"result\" :\"1\"," +
                "\"returnval\" :\"操作成功\"," +
                "\"pagebar\" :\"" + JumbotDLS.Utils.PageBar.GetPageBar(3, "js", 2, totalCount, PSize, page, "javascript:ajaxList($#page#$);") + "\"," +
                JumbotDLS.Utils.dtHelper.DT2JSON(dt, (PSize * (page - 1))) +
                "}";
            dt.Clear();
            dt.Dispose();
        }
    }
}