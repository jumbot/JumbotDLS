﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _thumb_list : JumbotDLS.UI.AdminCenter
    {
        public string ListId = "";
        public string Title = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ccid"));
            Admin_Load("master", "stop");
            ListId = Str2Str(q("listid"));
        }
    }
}