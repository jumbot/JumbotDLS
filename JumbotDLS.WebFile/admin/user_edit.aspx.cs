﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _user_edit : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("master", "html");
            id = Str2Str(q("id"));
            JumbotDLS.DBUtility.WebFormHandler wh = new JumbotDLS.DBUtility.WebFormHandler(doh, "jdls_normal_user", btnSave);
            wh.AddBind(txtUserName, "UserName", true);
            wh.AddBind(rbtnState, "SelectedValue", "State", false);
            wh.AddBind(txtMax, "[Integral]", true);
            wh.AddBind(ddlGroup, "[Group]", false);
            this.txtUserName.ReadOnly = true;
            wh.ConditionExpress = "id=" + id;
            wh.Mode = JumbotDLS.DBUtility.OperationType.Modify;
            wh.validator = chkForm;
            wh.ModifyOk += new EventHandler(save_ok);
        }
        protected void bind_ok(object sender, EventArgs e)
        {
        }
        protected bool chkForm()
        {
            if (!CheckFormUrl())
                return false;
            if (!Page.IsValid)
                return false;
            return true;
        }
        protected void save_ok(object sender, EventArgs e)
        {
            string _uName = this.txtUserName.Text;
            JumbotDLS.DAL.Normal_UserDAL _User = new JumbotDLS.DAL.Normal_UserDAL();
            JumbotDLS.DAL.Normal_AdminlogsDAL _Adminlogs = new JumbotDLS.DAL.Normal_AdminlogsDAL();
            JumbotDLS.DAL.Normal_UserLogsDAL _Userlogs = new JumbotDLS.DAL.Normal_UserLogsDAL();

            if (this.txtUserPass.Text.Length > 0)
            {
                _User.ChangePsd(id, JumbotDLS.Utils.MD5.Last64(JumbotDLS.Utils.MD5.Lower32(this.txtUserPass.Text)));
                _Adminlogs.SaveLog(AdminId, "修改了【" + _uName + "】的登录密码");
            }
            if (this.txtPoints.Text != "0")
            {
                _User.AddPoints(id, Str2Int(this.txtPoints.Text));
                _Adminlogs.SaveLog(AdminId, "给【" + _uName + "】充将元:" + this.txtPoints.Text);
                _Userlogs.SaveLog(id, AdminName + "给你充了将元" + this.txtPoints.Text + "", 4);
            }
            if (this.ddlVIPYears.SelectedValue != "0")
            {
                _User.AddVIPYears(id, Str2Int(this.ddlVIPYears.SelectedValue));
                _Adminlogs.SaveLog(AdminId, "给【" + _uName + "】包了" + this.ddlVIPYears.SelectedValue + "年VIP服务");
                _Userlogs.SaveLog(id, AdminName + "给你包了" + this.ddlVIPYears.SelectedValue + "年VIP服务", 5);

            }
            if (this.txtIntegral.Text != "0")
            {
                _User.DeductIntegral(id, Str2Int(this.txtIntegral.Text));
                _Adminlogs.SaveLog(AdminId, "给【" + _uName + "】扣积分:" + this.txtIntegral.Text);
                _Userlogs.SaveLog(id, AdminName + "给你扣除" + this.txtIntegral.Text + "积分", 4);
            }
            FinalMessage("成功保存", "close.htm", 0);
        }
    }
}
