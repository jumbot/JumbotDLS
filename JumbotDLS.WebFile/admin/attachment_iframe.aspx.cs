﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _attachment_iframe : JumbotDLS.UI.AdminCenter
    {
        public string _Module = string.Empty;
        public string N = "";
        public string BindField = "";
        public string BindField2 = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            _Module = q("module");
            N = q("N");
            BindField = q("BindField");
            BindField2 = q("BindField2");
            Admin_Load("", "html");
        }
    }
}

