﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="content_export_form.aspx.cs" Inherits="JumbotDLS.WebFile.Admin._content_export_form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title>批量导出数据</title>
<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js"></script>
<link   type="text/css" rel="stylesheet" href="../_data/global.css" />
<link   type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
<style>
#ajaxResult p {line-height:20px;}
</style>
<script type="text/javascript">
function ajaxSubmit(page) {
    var _data = {
        classid: $("#ddlClassId").val(),
        database: $("#txtDatabase").val()
    };
    info = "正在导出第" + page + "批数据，请等待...";
    JumbotDLS.Loading.show(info);
    $.ajax({
        type: "post",
        dataType: "json",
        data: _data,
        url: "content_export_ajax.aspx?oper=ajaxExport&ccid=<%=ChannelId %>&moduleid=<%=_ModuleId %>&page=" + page + "&visittime=" + (new Date().getTime()),
        error: function (XmlHttpRequest, textStatus, errorThrown) { JumbotDLS.Loading.hide(); alert(XmlHttpRequest.responseText); },
        success: function (d) {
            switch (d.result) {
                case '-1':
                    JumbotDLS.Alert(d.returnval, "0", "top.location.href='index.aspx';");
                    break;
                case '0':
                    JumbotDLS.Alert(d.returnval, "0");
                    break;
                case '1':
                    $('#ajaxResult').append("第" + page + "批结果：" + d.returnval + "<br />");
                    JumbotDLS.Loading.hide();
                    location.href = "file.aspx?ext=xlsx&file=/_export/<%=strDatabase %>";
                    break;
                case '2': //说明还要继续
                    $('#ajaxResult').append("第" + page + "批结果：" + d.returnval + "<br />");
                    ajaxSubmit(page + 1);
                    break;
            }
        }
    });
}
</script>
</head>
<body>
<form id="form1" runat="server">
	<table class="formtable">
		<tr>
			<th> 导出栏目 </th>
			<td><asp:DropDownList ID="ddlClassId" runat="server"><asp:ListItem Text="===未分类===" Value="0"></asp:ListItem></asp:DropDownList>
			</td>
		</tr>
			<tr style="display:none;">
				<th> xlsx文件 </th>
				<td align="left"><asp:TextBox ID="txtDatabase" runat="server" Width="97%" CssClass="ipt"></asp:TextBox>
				</td>
			</tr>
        		<tr style="display:none;">
			<th> 最低浏览权限 </th>
			<td><asp:DropDownList ID="ddlReadGroup" runat="server"> </asp:DropDownList>
			(此功能在静态生成的频道无效)
			</td>
		</tr>

	</table>
	<table class="formtable">
		<tr>
			<th> 导出结果 </th>
			<td><p id="ajaxResult"></p></td>
		</tr>
	</table>
	<div class="buttonok">
		<input type="button" name="btnSave" value="确定" id="btnSave" class="btnsubmit" onclick="ajaxSubmit(1);"  />
		<input id="btnReset" type="button" value="返回" class="btncancel" onclick="parent.JumbotDLS.Popup.hide();" />
	</div>
</form>
<script type="text/javascript">    _jdls_SetDialogTitle();</script>
</body>
</html>
