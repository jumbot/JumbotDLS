﻿/*
 * 程序名称: JumbotDLS(知远数据库平台)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Web;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _class_list : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ccid"));
            Admin_Load(ChannelId + "-07", "html", true);
        }
    }
}