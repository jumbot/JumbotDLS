﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _organization_whiteip_edit : JumbotDLS.UI.AdminCenter
    {
        public string OrganizationId = "0";
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("whiteip-mng", "html");
            OrganizationId = Str2Str(q("organizationid"));
            id = Str2Str(q("id"));

            JumbotDLS.DBUtility.WebFormHandler wh = new JumbotDLS.DBUtility.WebFormHandler(doh, "jdls_organization_whiteip", btnSave);
            wh.AddBind(txtTitle, "Title", true);
            wh.AddBind(txtStartIP, "StartIP2", true);
            wh.AddBind(txtEndIP, "EndIP2", true);
            wh.AddBind(ref OrganizationId, "OrganizationId", false);
            if (id == "0")
            {
                wh.Mode = JumbotDLS.DBUtility.OperationType.Add;
            }
            else
            {
                wh.ConditionExpress = "id=" + id;
                wh.Mode = JumbotDLS.DBUtility.OperationType.Modify;
            }
            wh.validator = chkForm;
            wh.AddOk += new EventHandler(save_ok);
            wh.ModifyOk += new EventHandler(save_ok);
        }
        protected void bind_ok(object sender, EventArgs e)
        {
        }
        protected bool chkForm()
        {
            if (!CheckFormUrl())
                return false;
            if (!Page.IsValid)
                return false;
            long _startip = JumbotDLS.Utils.IPHelp.IP2Long(System.Net.IPAddress.Parse(this.txtStartIP.Text));
            long _endip = JumbotDLS.Utils.IPHelp.IP2Long(System.Net.IPAddress.Parse(this.txtEndIP.Text));
            if (_startip > _endip)
            {
                FinalMessage("起止IP输入有误", "", 1, 2);
                return false;
            }
            doh.Reset();
            doh.ConditionExpress = "organizationID<>" + OrganizationId + " and (id not in (select a.id from jdls_organization_whiteip a where a.EndIP<" + _startip + " OR a.StartIP>" + _endip + "))";
            if (doh.Exist("jdls_organization_whiteip"))
            {
                FinalMessage("IP配置与其他机构有冲突", "", 1, 2);
                return false;
            }
            return true;
        }
        protected void save_ok(object sender, EventArgs e)
        {
            if (id == "0")
            {
                JumbotDLS.DBUtility.DbOperEventArgs de = (JumbotDLS.DBUtility.DbOperEventArgs)e;
                id = de.id.ToString();
            }
            if (new JumbotDLS.DAL.Normal_WhiteIPDAL().UpdateIPData(id, this.txtStartIP.Text, this.txtEndIP.Text))
                FinalMessage("保存成功", "close.htm", 0);
            else
                FinalMessage("保存错误", "close.htm", 0);
        }
    }
}
