﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _module_article_ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!CheckFormUrl())
            {
                Response.End();
            }
            ChannelId = Str2Str(q("ccid"));
            id = Str2Str(q("id"));
            Admin_Load("", "json", true);
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxGetList":
                    ajaxGetList();
                    break;
                case "ajaxBatchOper":
                    ajaxBatchOper();
                    break;
                case "ajaxDel":
                    ajaxDel();
                    break;
                case "checkname":
                    ajaxCheckName();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxCheckName()
        {
            doh.Reset();
            doh.ConditionExpress = "title=@title and id<>" + id + " and channelid=" + ChannelId;
            doh.AddConditionParameter("@title", q("txtTitle"));
            if (doh.Exist("jdls_module_" + ChannelType))
                this._response = JsonResult(0, "不可保存");
            else
                this._response = JsonResult(1, "可以保存");
        }
        private void ajaxGetList()
        {
            Admin_Load(ChannelId + "-00", "json");
            string cid = Str2Str(q("cid"));
            string _k = q("k");
            string _f = q("f");
            string _s = q("s");
            string _p = q("p");
            string _t = q("t");
            string _d = q("d");
            int page = Int_ThisPage();
            int PSize = Str2Int(q("pagesize"), 20);
            this._response = GetContentList(cid, _f, _k, _d, _s, q("isimg"), q("istop"), q("isfocus"), q("ishead"), PSize, page);
        }
        private void ajaxDel()
        {
            Admin_Load(ChannelId + "-03", "json");
            if (AdminIsFounder)
            {
                string lId = f("id");
                doh.Reset();
                doh.ConditionExpress = "locked=0 and id=" + lId;
                //doh.ConditionExpress = "locked=0 and (userid=0 or userid=" + AdminId + ") and id=" + lId;
                if (doh.Delete("jdls_module_article") == 1)
                    this._response = JsonResult(1, "成功删除");
                else
                    this._response = JsonResult(0, "数据被锁定或无权限");
            }
            else
            {
                this._response = JsonResult(0, "为保证数据安全性，不允许直接删除数据，若要删除请联系周国庆");
            }

        }

        /// <summary>
        /// 执行批量操作
        /// </summary>
        /// <param name="oper"></param>
        /// <param name="ids"></param>
        private void ajaxBatchOper()
        {
            string act = q("act");
            string tocid = f("tocid");
            string ids = f("ids");
            BatchContent(act, tocid, ids, ChannelId, ChannelType, "json");
            this._response = JsonResult(1, "操作成功");
        }
    }
}