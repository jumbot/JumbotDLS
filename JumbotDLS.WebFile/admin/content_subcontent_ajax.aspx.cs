﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _content_subcontent_ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!CheckFormUrl())
            {
                Response.End();
            }
            ChannelId = Str2Str(q("ccid"));
            id = Str2Str(q("id"));
            Admin_Load(ChannelId + "-01", "json", true);
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxGetList":
                    ajaxGetList();
                    break;
                case "ajaxDel":
                    ajaxDel();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxGetList()
        {
            Admin_Load("", "json");
            string _parentid = Str2Str(q("parentid"));
            string _moduleid = q("moduleid");
            int page = Int_ThisPage();
            int PSize = Str2Int(q("pagesize"), 20);
            this._response = GetContentSubList(_parentid, _moduleid, PSize, page);
        }
        private void ajaxDel()
        {
            Admin_Load("", "json");
            string lId = f("id");
            doh.Reset();
            doh.ConditionExpress = "locked=0 and (userid=0 or userid=" + AdminId + ") and id=" + lId;
            if (doh.Delete("jdls_module_article") == 1)
                this._response = JsonResult(1, "成功删除");
            else
                this._response = JsonResult(0, "数据被锁定或无权限");
        }
        
    }
}