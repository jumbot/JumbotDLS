﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.IO;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _discuz_api : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("master", "html");
            if (!Page.IsPostBack)
            {
                string strXmlFile = HttpContext.Current.Server.MapPath("~/_data/config/discuz.config");
                JumbotDLS.DBUtility.XmlControl XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
                this.txtForumAPIKey.Text = XmlTool.GetText("Root/ForumAPIKey");
                this.txtForumSecret.Text = XmlTool.GetText("Root/ForumSecret");
                this.txtForumUrl.Text = XmlTool.GetText("Root/ForumUrl");
                this.txtForumIP.Text = XmlTool.GetText("Root/ForumIP");
                XmlTool.Dispose();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string strXmlFile = HttpContext.Current.Server.MapPath("~/_data/config/discuz.config");
            JumbotDLS.DBUtility.XmlControl XmlTool = new JumbotDLS.DBUtility.XmlControl(strXmlFile);
            XmlTool.Update("Root/ForumAPIKey", this.txtForumAPIKey.Text);
            XmlTool.Update("Root/ForumSecret", this.txtForumSecret.Text);
            XmlTool.Update("Root/ForumUrl", this.txtForumUrl.Text);
            XmlTool.Update("Root/ForumIP", this.txtForumIP.Text);
            XmlTool.Save();
            XmlTool.Dispose();
            SetupSystemDate();
            FinalMessage("保存成功!", "discuz_api.aspx", 0);
        }
    }
}
