﻿/*
 * KnowfarCms 北京子木将博科技有限公司
 */
using System;
using System.Data;
using System.IO;
using System.Web;
using System.Text;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _announcement_edit : JumbotDLS.UI.AdminCenter
    {
        public string MyFormFieldList = "";
        public string MyFormFieldRequiredList = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("announcement-mng", "html");
            id = Str2Str(q("id"));
            doh.Reset();
            JumbotDLS.DBUtility.WebFormHandler wh = new JumbotDLS.DBUtility.WebFormHandler(doh, "auth_announcement", btnSave);
            wh.AddBind(txtTitle, "Title", true);

            if (id == "0")
            {
                wh.Mode = JumbotDLS.DBUtility.OperationType.Add;
            }
            else
            {
                wh.ConditionExpress = "id=" + id;
                wh.Mode = JumbotDLS.DBUtility.OperationType.Modify;
            }
            wh.validator = chkForm;
            wh.BindBeforeAddOk += new EventHandler(bind_ok);
            wh.BindBeforeModifyOk += new EventHandler(bind_ok);
            wh.AddOk += new EventHandler(save_ok);
            wh.ModifyOk += new EventHandler(save_ok);
        }
        protected void bind_ok(object sender, EventArgs e)
        {
            if (id == "0")
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Format("<tr><th>{0}</th><td><textarea name='txt{1}' id='txt{1}' class='ipt' style='height:{4};width:{3};visibility:hidden;'>{2}</textarea>{5}</td></tr>\r\n", "内容", "Content", string.Empty, "97%", "400px", "<script>KindEditor.ready(function (K) {K.create('#txtContent', {cssPath: '../_libs/kindeditor-4.1.10/plugins/code/prettify.css',cssData: 'body {font-family: \"微软雅黑\"; font-size: 14px}',uploadJson: '../_libs/kindeditor-4.1.10/asp.net/upload_json.ashx?adminid=" + AdminId + "',fileManagerJson: '../_libs/kindeditor-4.1.10/asp.net/file_manager_json.ashx',allowFileManager: true,afterCreate: function () { var self = this; K.ctrl(document, 13, function () {self.sync();K('form[name=form1]')[0].submit(); });K.ctrl(self.edit.doc, 13, function () {self.sync();K('form[name=form1]')[0].submit();});}}); prettyPrint();});</script>"));

                MyFormFieldList = sb.ToString();
            }
            else//其他数据的绑定
            {
                doh.Reset();
                doh.SqlCmd = "Select * FROM [auth_announcement] WHERE [ID]=" + id;
                DataTable dtContent = doh.GetDataTable();
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Format("<tr><th>{0}</th><td><textarea name='txt{1}' id='txt{1}' class='ipt' style='height:{4};width:{3};visibility:hidden;'>{2}</textarea>{5}</td></tr>\r\n", "内容", "Content", dtContent.Rows[0]["Content"].ToString(), "97%", "400px", "<script>KindEditor.ready(function (K) {K.create('#txtContent', {cssPath: '../_libs/kindeditor-4.1.10/plugins/code/prettify.css',cssData: 'body {font-family: \"微软雅黑\"; font-size: 14px}',uploadJson: '../_libs/kindeditor-4.1.10/asp.net/upload_json.ashx?adminid=" + AdminId + "',fileManagerJson: '../_libs/kindeditor-4.1.10/asp.net/file_manager_json.ashx',allowFileManager: true,afterCreate: function () { var self = this; K.ctrl(document, 13, function () {self.sync();K('form[name=form1]')[0].submit(); });K.ctrl(self.edit.doc, 13, function () {self.sync();K('form[name=form1]')[0].submit();});}}); prettyPrint();});</script>"));

                MyFormFieldList = sb.ToString();
                dtContent.Clear();
                dtContent.Dispose();
            }


            StringBuilder sb2 = new StringBuilder();
            sb2.Append("$(\"#txtContent\").formValidator({ tipid: \"tipContent\", onshow: \"请输入内容\", onfocus: \"请输入内容\", oncorrect: \"OK\" }).InputValidator({ min: 1, onerror: \"请输入内容\" });\r\n");

            MyFormFieldRequiredList = sb2.ToString();
        }
        protected bool chkForm()
        {
            if (!CheckFormUrl())
                return false;
            if (!Page.IsValid)
                return false;
            return true;
        }
        protected void save_ok(object sender, EventArgs e)
        {
            if (id == "0")
            {
                JumbotDLS.DBUtility.DbOperEventArgs de = (JumbotDLS.DBUtility.DbOperEventArgs)e;
                id = de.id.ToString();
            }
            doh.Reset();
            doh.ConditionExpress = "id=@id";
            doh.AddConditionParameter("@id", id);
            doh.AddFieldItem("AdminName", AdminName);
            doh.AddFieldItem("Content", f("txtContent"));
            doh.Update("auth_announcement");
            FinalMessage("成功保存", "close.htm", 0);
        }
    }
}
