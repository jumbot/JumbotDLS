﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */
using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
using System.Data.OleDb;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _content_subcontent_import_form : JumbotDLS.UI.AdminCenter
    {
        public int _ModuleId = 0;
        public string _ParentId = "0";
        public string _ModuleName = "";
        public string _ModuleType = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ccid"));
            _ParentId = Str2Str(q("parentid"));
            _ModuleId = Str2Int(q("moduleid"));
            Admin_Load(ChannelId + "-01", "html", true);
            doh.Reset();
            doh.ConditionExpress = "id=@id";
            doh.AddConditionParameter("@id", _ModuleId);
            object[] _value = doh.GetFields("jdls_module_article_module", "title,type");
            if (_value == null)
            {
                FinalMessage("参数有误!", "", 1);
            }
            _ModuleName = _value[0].ToString();
            _ModuleType = _value[1].ToString();
        }
    }
}
