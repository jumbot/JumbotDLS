﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _javascriptcode : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("javascript-mng", "stop");
            id = Str2Str(q("id"));
            if (!Page.IsPostBack)
            {
                doh.Reset();
                doh.ConditionExpress = "id=@id";
                doh.AddConditionParameter("@id", id);
                string _code = doh.GetField("jdls_normal_javascript", "Code").ToString();
                this.ltlCode.Text = this.txtCode.Text = "<script charset=\"utf-8\" language=\"javascript\" type=\"text/javascript\" src=\"" + site.Url + site.Dir + "plus/javascript.aspx?code=" + _code + "\"></script>";
            }
        }
    }
}
