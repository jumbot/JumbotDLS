﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dict_list.aspx.cs" Inherits="JumbotDLS.WebFile.admin.dict_list" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta   name="robots" content="noindex, nofollow" />
<meta   http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title>新闻采集日志</title>
<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js"></script>
<link   type="text/css" rel="stylesheet" href="../_data/global.css" />
<link   type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
<script type="text/javascript" src="../statics/admin/js/common.js"></script>
<script type="text/javascript">
    var pagesize = 15;
    var page = thispage();

    $(document).ready(function () {
        ajaxList(page);

        $("#ddlCollitemList").change(function () {
            ajaxList(1);
        })
    });
    function ajaxList(currentpage) {
        if (currentpage != null) page = currentpage;
        top.JumbotDLS.Loading.show("正在加载数据,请等待...");
        var groupname = $("#ddlCollitemList").val();
        $.ajax({
            type: "get",
            dataType: "json",
            data: "page=" + currentpage + "&pagesize=" + pagesize + "&groupname=" + groupname + "&time=" + (new Date().getTime()),
            url: "dict_ajax.aspx?oper=ajaxGetList",
            error: function (XmlHttpRequest, textStatus, errorThrown) { top.JumbotDLS.Loading.hide(); alert(XmlHttpRequest.responseText); },
            success: function (d) {
                switch (d.result) {
                    case '-1':
                        top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                        break;
                    case '0':
                        top.JumbotDLS.Alert(d.returnval, "0");
                        break;
                    case '1':
                        top.JumbotDLS.Loading.hide();
                        $("#ajaxList").setTemplateElement("tplList", null, { filter_data: true });
                        $("#ajaxList").processTemplate(d);
                        $("#ajaxPageBar").html(d.pagerbar);
                        break;
                }
            }
        });
    }
    function AjaxDel(id) {
        top.JumbotDLS.Confirm("确定要删除该数据吗?", "getCurrentIframe().ajaxDel(" + id + ")");
    }
    function ajaxDel(id) {
        $.ajax({
            type: "post",
            dataType: "json",
            data: "id=" + id,
            url: "dict_ajax.aspx?oper=ajaxDel&clienttime=" + Math.random(),
            error: function (XmlHttpRequest, textStatus, errorThrown) { alert(XmlHttpRequest.responseText); },
            success: function (d) {
                switch (d.result) {
                    case '-1':
                        top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                        break;
                    case '0':
                        top.JumbotDLS.Alert(d.returnval, "0");
                        break;
                    case '1':
                        ajaxList(page);
                        break;
                }
            }
        });
    }
</script>

</head>
<body>
    </div>
    <form id="form1" runat="server">
	<table class="formtable mrg10T">
		<tr>
			<td>分组：<asp:DropDownList ID="ddlCollitemList" runat="server"></asp:DropDownList><input type="button" value="添加" class="btnsubmit" onclick="if ($('#ddlCollitemList').val()=='0'||$('#ddlCollitemList').val()==''){JumbotDLS.Alert('分组不能为全部', '0');return;} JumbotDLS.Popup.show('dict_edit.aspx?groupname=' + $('#ddlCollitemList').val() + '&id=0', 450, 220, true)" /></td>
		</tr>
	</table>
    
    </form>
<textarea class="template" id="tplList" style="display:none">
<table class="cooltable">
<thead>
	<tr>
		<th scope="col" style="width:80px;">ID</th>
		<th scope="col" style="width:120px;">所属分组</th>
		<th scope="col" style="width:*;">名称</th>
		<th scope="col" style="width:150px;">操作</th>
	</tr>
	</thead>
<tbody>
	{#foreach $T.table as record}
	<tr>
		<td align="center">{$T.record.id}</td>
		<td align="center">{$T.record.title}</td>
		<td align="center">{$T.record.itemtitle}</td>
		<td align="center">
			<a href="javascript:void(0);" onclick="JumbotDLS.Popup.show('dict_edit.aspx?id={$T.record.id}',450,220,true)">修改</a>
			<a href="javascript:void(0);" onclick="AjaxDel({$T.record.id})">删除</a>
		</td>
	</tr>
	{#/for}
</tbody>
</table>
</textarea>
    <div id="ajaxList" class="mrg10T">
    </div>
    <div id="ajaxPageBar" class="pages">
    </div>
</body>
</html>
