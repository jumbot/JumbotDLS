﻿/*
 * 程序名称: JumbotDLS(外军防务开源情报数据库)
 * 
 * 程序版本: V1.0
 * 
 * 程序开发: 知远战略与防务研究所
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _other_upload : JumbotDLS.UI.AdminCenter
    {

        public string UploadType;
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("", "html");
            UploadType = q("type");
        }
    }
}
