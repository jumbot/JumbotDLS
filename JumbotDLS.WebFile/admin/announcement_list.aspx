﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="announcement_list.aspx.cs" Inherits="JumbotDLS.WebFile.admin.announcement_list" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta   http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title></title>

<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js"></script>
<link type="text/css" rel="stylesheet" href="../_data/global.css" />
<link type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
<script type="text/javascript" src="../statics/admin/js/common.js"></script>
<script type="text/javascript">
    var pagesize = 15;
    var page = thispage();
    $(document).ready(function () {
        ajaxList(page);
    });
    function ajaxList(currentpage) {
        if (currentpage != null) page = currentpage;
        top.JumbotDLS.Loading.show("正在加载数据,请等待...", 260, 80);
        $.ajax({
            type: "get",
            dataType: "json",
            data: "page=" + currentpage + "&pagesize=" + pagesize + "&time=" + (new Date().getTime()),
            url: "announcement_ajax.aspx?oper=ajaxGetList",
            error: function (XmlHttpRequest, textStatus, errorThrown) { top.JumbotDLS.Loading.hide(); alert(XmlHttpRequest.responseText); },
            success: function (d) {
                switch (d.result) {
                    case '-1':
                        top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                        break;
                    case '0':
                        top.JumbotDLS.Alert(d.returnval, "0");
                        break;
                    case '1':
                        top.JumbotDLS.Loading.hide();
                        $("#ajaxList").setTemplateElement("tplList", null, { filter_data: true });
                        $("#ajaxList").processTemplate(d);
                        ActiveCoolTable();
                        break;
                }
            }
        });
    }
    function operater(act) {
        var ids = JoinSelect("selectID");
        if (ids == "") {
            top.JumbotDLS.Alert("没有任何选择项", "0");
            return;
        }
        top.JumbotDLS.Loading.show("正在处理...", 260, 80);
        $.ajax({
            type: "post",
            dataType: "json",
            data: "ids=" + ids,
            url: "announcement_ajax.aspx?oper=ajaxBatchOper&act=" + act + "&time=" + (new Date().getTime()),
            error: function (XmlHttpRequest, textStatus, errorThrown) { top.JumbotDLS.Loading.hide(); alert(XmlHttpRequest.responseText); },
            success: function (d) {
                switch (d.result) {
                    case '-1':
                        top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                        break;
                    case '0':
                        top.JumbotDLS.Alert(d.returnval, "0");
                        break;
                    case '1':
                        top.JumbotDLS.Message(d.returnval, "1");
                        ajaxList(page);
                        break;
                }
            }
        });
    }
    function ConfirmDelete(id) {
        top.JumbotDLS.Confirm("确定要删除吗?", "getCurrentIframe().ajaxDelete(" + id + ")");
    }
    function ajaxDelete(id) {
        $.ajax({
            type: "post",
            dataType: "json",
            data: "id=" + id,
            url: "announcement_ajax.aspx?oper=ajaxDelete&time=" + (new Date().getTime()),
            error: function (XmlHttpRequest, textStatus, errorThrown) { top.JumbotDLS.Loading.hide(); alert(XmlHttpRequest.responseText); },
            success: function (d) {
                switch (d.result) {
                    case '-1':
                        top.JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
                        break;
                    case '0':
                        top.JumbotDLS.Alert(d.returnval, "0");
                        break;
                    case '1':
                        top.JumbotDLS.Message(d.returnval, "1");
                        ajaxList(page);
                        break;
                }
            }
        });
    }
    </script>

</head>
<body>
    <div class="topnav">
        <span class="preload1"></span><span class="preload2"></span>
        <ul id="topnavbar">
            <li class="topmenu"><a href="javascript:void(0);" onclick="top.JumbotDLS.Popup.show('announcement_edit.aspx?id=0',-1,-1,true)" class="top_link"><span>添加公告</span></a>
            </li>
        </ul>

        <script>
            topnavbarStuHover();
        </script>

    </div>
<textarea id="tplList" style="display:none">
<table class="cooltable">
<thead>
	<tr>
		<th scope="col" style="width:40px;">ID</th>
		<th scope="col" width="*">标题</th>
		<th scope="col" style="width:160px;">发布人</th>
		<th scope="col" style="width:160px;">发布日期</th>
		<th scope="col" style="width:90px;">操作</th>
	</tr>
</thead>




<tbody>
	{#foreach $T.table as record}
	<tr>
		<td align="center">{$T.record.id}</td>
		<td align="center">{$T.record.title}</td>
		<td align="center">{$T.record.adminname}</td>
		<td align="left">{$T.record.addtime}</td>
		<td align="center">
           		<a href="javascript:void(0);" onclick="top.JumbotDLS.Popup.show('announcement_edit.aspx?id={$T.record.id}',-1,-1,true)">修改</a> <a href="javascript:void(0);" onclick="ConfirmDelete({$T.record.id})">删除</a>
		</td>
	</tr>
	{#/for}
</tbody>
</table>
</textarea>
    <div id="ajaxList" class="mrg10T">
    </div>
</body>
</html>
