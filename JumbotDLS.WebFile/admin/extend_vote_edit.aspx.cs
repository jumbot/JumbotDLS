﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _extend_vote_edit : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("master", "html");
            id = Str2Str(q("id"));
            if (!Page.IsPostBack)
            {
                doh.Reset();
                doh.SqlCmd = "SELECT [ID],[Title] FROM [jdls_normal_channel] ORDER BY Pid ASC";
                DataTable dtChannel = doh.GetDataTable();
                this.ddlChannelId.Items.Clear();
                this.ddlChannelId.Items.Add(new ListItem("===首页独有===", "0"));
                this.ddlChannelId.Items.Add(new ListItem("===整站公用===", "-1"));
                for (int i = 0; i < dtChannel.Rows.Count; i++)
                {
                    this.ddlChannelId.Items.Add(new ListItem(dtChannel.Rows[i]["Title"].ToString(), dtChannel.Rows[i]["Id"].ToString()));
                }
                dtChannel.Clear();
                dtChannel.Dispose();
            }
            doh.Reset();
            JumbotDLS.DBUtility.WebFormHandler wh = new JumbotDLS.DBUtility.WebFormHandler(doh, "jdls_extends_vote", btnSave);
            wh.AddBind(txtTitle, "Title", true);
            wh.AddBind(txtContent, "VoteText", true);
            wh.AddBind(rblType, "SelectedValue", "Type", false);
            wh.AddBind(ddlChannelId, "ChannelId", false);
            if (id != "0")
            {
                wh.ConditionExpress = "id=" + id;
                wh.Mode = JumbotDLS.DBUtility.OperationType.Modify;
            }
            else
                wh.Mode = JumbotDLS.DBUtility.OperationType.Add;
            wh.AddOk += new EventHandler(save_ok);
            wh.ModifyOk += new EventHandler(save_ok);
            wh.validator = chkForm;
        }
        protected bool chkForm()
        {
            if (!CheckFormUrl())
                return false;
            if (!Page.IsValid)
                return false;
            return true;
        }
        protected void save_ok(object sender, EventArgs e)
        {
            int voteTotal = txtContent.Text.Split('|').Length;
            string voteNum = "0";
            for (int i = 0; i < voteTotal - 1; i++)
            {
                voteNum += "|0";
            }
            if (id == "0")
            {
                JumbotDLS.DBUtility.DbOperEventArgs de = (JumbotDLS.DBUtility.DbOperEventArgs)e;
                id = de.id.ToString();
            }
            doh.Reset();
            doh.ConditionExpress = "id=" + id;
            doh.AddFieldItem("VoteNum", voteNum);
            doh.AddFieldItem("VoteTotal", 0);
            doh.AddFieldItem("lock", 1);
            doh.Update("jdls_extends_vote");
            FinalMessage("成功保存", "close.htm", 0);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            this.txtContent.Text = this.txtContent.Text.Replace("\r", "").Replace("\n", "").Replace("'", "").Replace("\\", "");
        }
    }
}
