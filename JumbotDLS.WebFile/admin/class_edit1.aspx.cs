﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _class_edit1 : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ccid"));
            id = Str2Str(q("id"));
            Admin_Load(ChannelId + "-07", "html", true);
            doh.Reset();
            doh.ConditionExpress = "id=@id";
            doh.AddConditionParameter("@id", ChannelId);
            if (!doh.Exist("jdls_normal_channel"))
            {
                Response.Write("频道选择有误!");
                Response.End();
                return;
            }
            if (!Page.IsPostBack)
            {
                doh.Reset();
                doh.SqlCmd = "SELECT [ID],[Title],[code] FROM [jdls_normal_class] WHERE [ChannelId]=" + ChannelId + " AND len(code)<" + (ChannelClassDepth * 4) + " ORDER BY code";
                DataTable dtClass = doh.GetDataTable();
                this.ddlParentId.Items.Add(new ListItem("根目录", "0"));
                for (int i = 0; i < dtClass.Rows.Count; i++)
                {
                    ListItem li = new ListItem();
                    li.Value = dtClass.Rows[i]["Id"].ToString();
                    li.Text = getListName(dtClass.Rows[i]["Title"].ToString(), dtClass.Rows[i]["code"].ToString());
                    this.ddlParentId.Items.Add(li);
                }
                dtClass.Clear();
                dtClass.Dispose();
            }
            JumbotDLS.DBUtility.WebFormHandler wh = new JumbotDLS.DBUtility.WebFormHandler(doh, "jdls_normal_class", btnSave);
            wh.AddBind(txtTitle, "Title", true);
            wh.AddBind(ddlParentId, "ParentId", false);
            wh.AddBind(txtInfo, "Info", true);
            wh.AddBind(txtIcon, "Icon", true);
            wh.AddBind(txtIconString, "IconString", true);
            wh.AddBind(rblIsNav, "SelectedValue", "IsNav", false);
            wh.AddBind(txtAliasPage, "AliasPage", true);
            wh.AddBind(rblIsTop, "SelectedValue", "IsTop", false);
            wh.AddBind(ref ChannelId, "ChannelId", false);
            wh.AddBind(rblTarget, "SelectedValue", "Target", true);
            if (id == "0")
            {
                wh.Mode = JumbotDLS.DBUtility.OperationType.Add;
            }
            else
            {
                wh.ConditionExpress = "id=" + id;
                wh.Mode = JumbotDLS.DBUtility.OperationType.Modify;
                this.ddlParentId.Enabled = false;
            }
            wh.BindBeforeModifyOk += new EventHandler(bind_ok);
            wh.AddOk += new EventHandler(save_ok);
            wh.ModifyOk += new EventHandler(save_ok);
            wh.validator = chkForm;
        }
        protected void bind_ok(object sender, EventArgs e)
        {
        }
        protected bool chkForm()
        {
            if (!CheckFormUrl())
                return false;
            if (!Page.IsValid)
                return false;
            return true;
        }
        protected void save_ok(object sender, EventArgs e)
        {
            string parentCode = string.Empty;
            if (this.ddlParentId.SelectedValue != "0")
            {
                doh.Reset();
                doh.ConditionExpress = "id=@id";
                doh.AddConditionParameter("@id", this.ddlParentId.SelectedValue);
                parentCode = doh.GetField("jdls_normal_class", "code").ToString();
            }
            string leftCode = string.Empty;
            string selfCode = string.Empty;
            string oldCode = string.Empty;
            string sourceCode = string.Empty;

            bool isEditCode = true;

            if (id != "0")
            {
                doh.Reset();
                doh.ConditionExpress = "id=@id";
                doh.AddConditionParameter("@id", id);
                oldCode = doh.GetField("jdls_normal_class", "code").ToString();
                sourceCode = oldCode;
                oldCode = oldCode.Substring(0, oldCode.Length - 4);
                if (oldCode == parentCode)
                    isEditCode = false;
            }

            if (isEditCode)
            {
                doh.Reset();
                doh.SqlCmd = "SELECT code FROM [jdls_normal_class] WHERE left(code," + parentCode.Length.ToString() + ")='" + parentCode + "' and len(code)=" + Convert.ToString(parentCode.Length + 4) + " AND [ChannelId]=" + ChannelId + " ORDER BY code desc";
                DataTable dtClass = doh.GetDataTable();
                if (dtClass.Rows.Count > 0)
                {
                    leftCode = dtClass.Rows[0]["code"].ToString();
                }
                if (leftCode.Length > 0)
                    selfCode = Convert.ToString(Convert.ToInt32(leftCode.Substring(leftCode.Length - 4, 4)) + 1).PadLeft(4, '0');
                else
                    selfCode = "0001";

                selfCode = parentCode + selfCode;

                if (id == "0")
                {
                    JumbotDLS.DBUtility.DbOperEventArgs de = (JumbotDLS.DBUtility.DbOperEventArgs)e;
                    doh.Reset();
                    doh.ConditionExpress = "id=@id";
                    doh.AddConditionParameter("@id", de.id);
                    doh.AddFieldItem("Code", selfCode);
                    doh.AddFieldItem("IsOut", 1);
                    doh.AddFieldItem("IsPost", 0);
                    doh.AddFieldItem("ThemeId", 0);
                    doh.AddFieldItem("ContentTheme", 0);
                    doh.AddFieldItem("PageSize", 0);
                    doh.AddFieldItem("TopicNum", 0);
                    doh.Update("jdls_normal_class");
                }
                else
                {
                    doh.Reset();
                    doh.ConditionExpress = "id=" + id;
                    doh.AddFieldItem("Code", selfCode);
                    doh.Update("jdls_normal_class");
                    doh.Reset();
                    doh.SqlCmd = "UPDATE [jdls_normal_class] SET [code]='" + selfCode + "'+Right(code,len(code)-" + sourceCode.Length.ToString() + ") where [code] LIKE '" + sourceCode + "%'";
                    doh.ExecuteSqlNonQuery();
                }
                dtClass.Clear();
                dtClass.Dispose();
            }
            FinalMessage("栏目成功保存", "close.htm", 0);
        }
    }
}
