﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Text;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _content_subcontent_edit : JumbotDLS.UI.AdminCenter
    {
        public int _ModuleId = 0;
        public string _ParentId = "0";
        public string _ModuleName = "";
        public string _ModuleType = "";
        public string MyFormFieldList = "";
        public string MyFormFieldRequiredList = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ccid"));
            _ParentId = Str2Str(q("parentid"));
            _ModuleId = Str2Int(q("moduleid"));
            id = Str2Str(q("id"));
            Admin_Load(ChannelId + "-01", "html", true);
            if (id == "0")
            {
                doh.Reset();
                doh.ConditionExpress = "id=@id";
                doh.AddConditionParameter("@id", _ModuleId);
                object[] _value = doh.GetFields("jdls_module_article_module", "title,type");
                if (_value == null)
                {
                    FinalMessage("参数有误!", "", 1);
                }
                _ModuleName = _value[0].ToString();
                _ModuleType = _value[1].ToString();
            }
            else
            {
                doh.Reset();
                doh.ConditionExpress = "id=@id";
                doh.AddConditionParameter("@id", id);

                _ModuleType = doh.GetField("jdls_module_article", "Module").ToString();
                doh.Reset();
                doh.ConditionExpress = "Type=@Type";
                doh.AddConditionParameter("@Type", _ModuleType);
                object[] _value = doh.GetFields("jdls_module_article_module", "id,title");
                if (_value == null)
                {
                    FinalMessage("参数有误!", "", 1);
                }
                _ModuleId = Str2Int(_value[0].ToString());
                _ModuleName = _value[1].ToString();
            }
            this.txtEditor.Text = AdminName;
            doh.Reset();
            JumbotDLS.DBUtility.WebFormHandler wh = new JumbotDLS.DBUtility.WebFormHandler(doh, "jdls_module_article", btnSave);
            wh.AddBind(txtEditor, "Editor", true);
            wh.AddBind(txtUserId, "UserId", false);
            wh.AddBind(rblLocked, "SelectedValue", "Locked", false);
            wh.AddBind(ref ChannelId, "ChannelId", false);
            wh.AddBind(ref _ParentId, "ParentId", false);
            wh.AddBind(chkIsEdit, "1", "IsPass", false);
            wh.AddBind(txtAddDate, "AddDate", true);
            if (id == "0")
            {
                wh.Mode = JumbotDLS.DBUtility.OperationType.Add;
            }
            else
            {
                wh.ConditionExpress = "parentid=" +_ParentId+" and id=" + id;
                wh.Mode = JumbotDLS.DBUtility.OperationType.Modify;
            }
            wh.BindBeforeAddOk += new EventHandler(bind_ok);
            wh.BindBeforeModifyOk += new EventHandler(bind_ok);
            wh.AddOk += new EventHandler(add_ok);
            wh.ModifyOk += new EventHandler(save_ok);
            wh.validator = chkForm;
        }
        /// <summary>
        /// 绑定数据后的处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bind_ok(object sender, EventArgs e)
        {
            if (id == "0")
            {
                this.txtAddDate.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                doh.Reset();
                doh.SqlCmd = "Select * FROM [jdls_module_article_module_field] WHERE [State]=1 AND SHowInAdminForm=1 AND [ModuleId]=" + _ModuleId + " ORDER BY pid";
                DataTable dtModuleField = doh.GetDataTable();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < dtModuleField.Rows.Count; i++)
                {
                    if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "input")
                    {
                        sb.Append(string.Format("<tr><th>{0}</th><td><input name='txt{1}' id='txt{1}' type='text' value='{2}' class='ipt' style='width:{3};' /></td></tr>\r\n", (dtModuleField.Rows[i]["FieldTitle"].ToString()), dtModuleField.Rows[i]["FieldName"].ToString(), string.Empty, dtModuleField.Rows[i]["InputWidth"].ToString()));
                        if (Str2Int(dtModuleField.Rows[i]["FieldIsUpload"].ToString()) > 0)
                        {
                            sb.Append("<script type='text/javascript'>function AttachmentOperater" + dtModuleField.Rows[i]["ID"].ToString() + "(path,type,size,field){if('" + dtModuleField.Rows[i]["FieldUploadModule"].ToString() + "'!='productprice'){$('#'+field).val(path);}else{ajaxExecutePriceFile(path,field)}}</script><tr><th>上传["+dtModuleField.Rows[i]["FieldTitle"].ToString()+"]</th><td><iframe src='attachment_iframe.aspx?N=" + dtModuleField.Rows[i]["ID"].ToString() + "&module=" + dtModuleField.Rows[i]["FieldUploadModule"].ToString() + "&BindField=txt" + dtModuleField.Rows[i]["FieldName"].ToString() + "' width='100%' height='30' scrolling='no' frameborder='0'></iframe></td></tr>");
                        }
                    }
                    if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "checkbox")
                    {
                        sb.Append(string.Format("<tr><th>{0}</th><td>", dtModuleField.Rows[i]["FieldTitle"].ToString()));
                        string _ThisItemGroupName = dtModuleField.Rows[i]["FieldItemGroupName"].ToString();
                        if (_ThisItemGroupName.Length > 0)//表示联动
                        {
                            doh.Reset();
                            doh.SqlCmd = "select ItemTitle,ItemValue FROM [jdls_module_article_module_field_item] where groupname='"+_ThisItemGroupName+"' order by PID";
                            DataTable dtGroup = doh.GetDataTable();
                            for (int j = 0; j < dtGroup.Rows.Count; j++)
                            {
                                sb.Append("<span style='margin-left:10px;'><input type=\"checkbox\" name=\"chk" + dtModuleField.Rows[i]["FieldName"].ToString() + "\" value=\"" + dtGroup.Rows[j]["ItemValue"] + "\"");
                                sb.Append("> " + dtGroup.Rows[j]["ItemTitle"] + "</span>");
                            }
                            dtGroup.Clear();
                            dtGroup.Dispose();
                        }
                        else
                        {
                            string[] _ThisItemTitle = dtModuleField.Rows[i]["FieldItemTitle"].ToString().Split('♂');
                            string[] _ThisItemValue = dtModuleField.Rows[i]["FieldItemValue"].ToString().Split('♂');

                            for (int j = 0; j < (_ThisItemValue.Length > _ThisItemTitle.Length ? _ThisItemTitle.Length : _ThisItemValue.Length); j++)
                            {
                                sb.Append("<span style='margin-left:10px;'><input type=\"checkbox\" name=\"chk" + dtModuleField.Rows[i]["FieldName"].ToString() + "\" value=\"" + _ThisItemValue[j] + "\"");
                                sb.Append("> " + _ThisItemTitle[j] + "</span>");
                            }
                        }
                        sb.Append("</td></tr>\r\n");

                    }
                    if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "radio")
                    {
                        sb.Append(string.Format("<tr><th>{0}</th><td>", dtModuleField.Rows[i]["FieldTitle"].ToString()));
                        string _ThisItemGroupName = dtModuleField.Rows[i]["FieldItemGroupName"].ToString();
                        if (_ThisItemGroupName.Length > 0)//表示联动
                        {
                            doh.Reset();
                            doh.SqlCmd = "select ItemTitle,ItemValue FROM [jdls_module_article_module_field_item] where groupname='" + _ThisItemGroupName + "' order by PID";
                            DataTable dtGroup = doh.GetDataTable();
                            for (int j = 0; j < dtGroup.Rows.Count; j++)
                            {
                                sb.Append("<span style='margin-left:10px;'><input type=\"radio\" name=\"rbl" + dtModuleField.Rows[i]["FieldName"].ToString() + "\" value=\"" + dtGroup.Rows[j]["ItemValue"] + "\"");
                                sb.Append("> " + dtGroup.Rows[j]["ItemTitle"] + "</span>");
                            }
                            dtGroup.Clear();
                            dtGroup.Dispose();
                        }
                        else
                        {
                            string[] _ThisItemTitle = dtModuleField.Rows[i]["FieldItemTitle"].ToString().Split('♂');
                            string[] _ThisItemValue = dtModuleField.Rows[i]["FieldItemValue"].ToString().Split('♂');

                            for (int j = 0; j < (_ThisItemValue.Length > _ThisItemTitle.Length ? _ThisItemTitle.Length : _ThisItemValue.Length); j++)
                            {
                                sb.Append("<span style='margin-left:10px;'><input type=\"radio\" name=\"rbl" + dtModuleField.Rows[i]["FieldName"].ToString() + "\" value=\"" + _ThisItemValue[j] + "\"");
                                sb.Append("> " + _ThisItemTitle[j] + "</span>");
                            }
                        }
                        sb.Append("</td></tr>\r\n");

                    }
                    if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "textarea")
                    {
                        sb.Append(string.Format("<tr><th>{0}</th><td><textarea name='txt{1}' id='txt{1}' class='ipt' style='height:{4};width:{3};'>{2}</textarea></td></tr>\r\n", (dtModuleField.Rows[i]["FieldTitle"].ToString()), dtModuleField.Rows[i]["FieldName"].ToString(), string.Empty, dtModuleField.Rows[i]["InputWidth"].ToString(), dtModuleField.Rows[i]["InputHeight"].ToString(), dtModuleField.Rows[i]["InputWidth"].ToString(), dtModuleField.Rows[i]["InputHeight"].ToString()));
                        if (Str2Int(dtModuleField.Rows[i]["FieldIsUpload"].ToString()) > 0)
                        {
                            sb.Append("<script type='text/javascript'>function AttachmentOperater" + dtModuleField.Rows[i]["ID"].ToString() + "(path,type,size,field){if('" + dtModuleField.Rows[i]["FieldUploadModule"].ToString() + "'!='productprice'){$('#'+field).val(path);}else{ajaxExecutePriceFile(path,field)}}</script><tr><th>上传["+dtModuleField.Rows[i]["FieldTitle"].ToString()+"]</th><td><iframe src='attachment_iframe.aspx?N=" + dtModuleField.Rows[i]["ID"].ToString() + "&module=" + dtModuleField.Rows[i]["FieldUploadModule"].ToString() + "&BindField=txt" + dtModuleField.Rows[i]["FieldName"].ToString() + "' width='100%' height='30' scrolling='no' frameborder='0'></iframe></td></tr>");
                        }
                    } 
                    if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "editor")
                    {
                        sb.Append(string.Format("<tr><th>{0}</th><td><textarea name='txt{1}' id='txt{1}' class='ipt' style='height:{4};width:{3};visibility:hidden;'>{2}</textarea>{5}</td></tr>\r\n", (dtModuleField.Rows[i]["FieldTitle"].ToString()), dtModuleField.Rows[i]["FieldName"].ToString(), string.Empty, dtModuleField.Rows[i]["InputWidth"].ToString(), dtModuleField.Rows[i]["InputHeight"].ToString(), "<script>KindEditor.ready(function (K) {K.create('#txt"+dtModuleField.Rows[i]["FieldName"].ToString()+"', {cssPath: '../_libs/kindeditor-4.1.10/plugins/code/prettify.css',uploadJson: '../_libs/kindeditor-4.1.10/asp.net/upload_json.ashx?adminid="+AdminId+"',fileManagerJson: '../_libs/kindeditor-4.1.10/asp.net/file_manager_json.ashx',allowFileManager: true,afterCreate: function () { var self = this; K.ctrl(document, 13, function () {self.sync();K('form[name=form1]')[0].submit(); });K.ctrl(self.edit.doc, 13, function () {self.sync();K('form[name=form1]')[0].submit();});}}); prettyPrint();});</script>"));
                    }
                }
                MyFormFieldList = sb.ToString();
                dtModuleField.Clear();
                dtModuleField.Dispose();
            }
            else//其他数据的绑定
            {
                this.txtAddDate.Text = this.txtAddDate.Text == "" ? DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") : Convert.ToDateTime(this.txtAddDate.Text).ToString("yyyy-MM-dd HH:mm:ss");
                doh.Reset();
                doh.SqlCmd = "Select * FROM [jdls_module_article] WHERE [ID]=" + id;
                DataTable dtContent = doh.GetDataTable();
                doh.Reset();
                doh.SqlCmd = "Select * FROM [jdls_module_article_module_field] WHERE [State]=1 AND SHowInAdminForm=1 AND  [ModuleId]=" + _ModuleId + " ORDER BY pid";
                DataTable dtModuleField = doh.GetDataTable();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < dtModuleField.Rows.Count; i++)
                {
                    if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "input")
                    {
                        sb.Append(string.Format("<tr><th>{0}</th><td><input name='txt{1}' id='txt{1}' type='text' value='{2}' class='ipt' style='width:{3};' /></td></tr>\r\n", (dtModuleField.Rows[i]["FieldTitle"].ToString()), dtModuleField.Rows[i]["FieldName"].ToString(), dtContent.Rows[0][dtModuleField.Rows[i]["FieldName"].ToString()].ToString(), dtModuleField.Rows[i]["InputWidth"].ToString()));
                        if (Str2Int(dtModuleField.Rows[i]["FieldIsUpload"].ToString()) > 0)
                        {
                            sb.Append("<script type='text/javascript'>function AttachmentOperater" + dtModuleField.Rows[i]["ID"].ToString() + "(path,type,size,field){if('" + dtModuleField.Rows[i]["FieldUploadModule"].ToString() + "'!='productprice'){$('#'+field).val(path);}else{ajaxExecutePriceFile(path,field)}}</script><tr><th>上传["+dtModuleField.Rows[i]["FieldTitle"].ToString()+"]</th><td><iframe src='attachment_iframe.aspx?N=" + dtModuleField.Rows[i]["ID"].ToString() + "&module=" + dtModuleField.Rows[i]["FieldUploadModule"].ToString() + "&BindField=txt" + dtModuleField.Rows[i]["FieldName"].ToString() + "' width='100%' height='30' scrolling='no' frameborder='0'></iframe></td></tr>");
                        }
                    }
                    if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "checkbox")
                    {
                        sb.Append(string.Format("<tr><th>{0}</th><td>", dtModuleField.Rows[i]["FieldTitle"].ToString()));
                        string _ThisItemGroupName = dtModuleField.Rows[i]["FieldItemGroupName"].ToString();
                        if (_ThisItemGroupName.Length > 0)//表示联动
                        {
                            doh.Reset();
                            doh.SqlCmd = "select ItemTitle,ItemValue FROM [jdls_module_article_module_field_item] where groupname='" + _ThisItemGroupName + "' order by PID";
                            DataTable dtGroup = doh.GetDataTable();
                            for (int j = 0; j < dtGroup.Rows.Count; j++)
                            {
                                sb.Append("<span style='margin-left:10px;'><input type=\"checkbox\" name=\"chk" + dtModuleField.Rows[i]["FieldName"].ToString() + "\" value=\"" + dtGroup.Rows[j]["ItemValue"] + "\"");
                                if (("," + dtContent.Rows[0][dtModuleField.Rows[i]["FieldName"].ToString()].ToString() + ",").Contains("," + dtGroup.Rows[j]["ItemValue"] + ","))
                                    sb.Append(" checked");
                                sb.Append("> " + dtGroup.Rows[j]["ItemTitle"] + "</span>");
                            }
                            dtGroup.Clear();
                            dtGroup.Dispose();
                        }
                        else
                        {
                            string[] _ThisItemTitle = dtModuleField.Rows[i]["FieldItemTitle"].ToString().Split('♂');
                            string[] _ThisItemValue = dtModuleField.Rows[i]["FieldItemValue"].ToString().Split('♂');

                            for (int j = 0; j < (_ThisItemValue.Length > _ThisItemTitle.Length ? _ThisItemTitle.Length : _ThisItemValue.Length); j++)
                            {
                                sb.Append("<span style='margin-left:10px;'><input type=\"checkbox\" name=\"chk" + dtModuleField.Rows[i]["FieldName"].ToString() + "\" value=\"" + _ThisItemValue[j] + "\"");
                                if (("," + dtContent.Rows[0][dtModuleField.Rows[i]["FieldName"].ToString()].ToString() + ",").Contains("," + _ThisItemValue[j] + ","))
                                    sb.Append(" checked");
                                sb.Append("> " + _ThisItemTitle[j] + "</span>");
                            }
                        }
                        sb.Append("</td></tr>\r\n");


                    }
                    if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "radio")
                    {
                        sb.Append(string.Format("<tr><th>{0}</th><td>", dtModuleField.Rows[i]["FieldTitle"].ToString()));
                        string _ThisItemGroupName = dtModuleField.Rows[i]["FieldItemGroupName"].ToString();
                        if (_ThisItemGroupName.Length > 0)//表示联动
                        {
                            doh.Reset();
                            doh.SqlCmd = "select ItemTitle,ItemValue FROM [jdls_module_article_module_field_item] where groupname='" + _ThisItemGroupName + "' order by PID";
                            DataTable dtGroup = doh.GetDataTable();
                            for (int j = 0; j < dtGroup.Rows.Count; j++)
                            {
                                sb.Append("<span style='margin-left:10px;'><input type=\"radio\" name=\"rbl" + dtModuleField.Rows[i]["FieldName"].ToString() + "\" value=\"" + dtGroup.Rows[j]["ItemValue"] + "\"");
                                if (("," + dtContent.Rows[0][dtModuleField.Rows[i]["FieldName"].ToString()].ToString() + ",").Contains("," + dtGroup.Rows[j]["ItemValue"] + ","))
                                    sb.Append(" checked"); 
                                sb.Append("> " + dtGroup.Rows[j]["ItemTitle"] + "</span>");
                            }
                            dtGroup.Clear();
                            dtGroup.Dispose();
                        }
                        else
                        {
                            string[] _ThisItemTitle = dtModuleField.Rows[i]["FieldItemTitle"].ToString().Split('♂');
                            string[] _ThisItemValue = dtModuleField.Rows[i]["FieldItemValue"].ToString().Split('♂');

                            for (int j = 0; j < (_ThisItemValue.Length > _ThisItemTitle.Length ? _ThisItemTitle.Length : _ThisItemValue.Length); j++)
                            {
                                sb.Append("<span style='margin-left:10px;'><input type=\"radio\" name=\"rbl" + dtModuleField.Rows[i]["FieldName"].ToString() + "\" value=\"" + _ThisItemValue[j] + "\"");
                                if (("," + dtContent.Rows[0][dtModuleField.Rows[i]["FieldName"].ToString()].ToString() + ",").Contains("," + _ThisItemValue[j] + ","))
                                    sb.Append(" checked");
                                sb.Append("> " + _ThisItemTitle[j] + "</span>");
                            }
                        }
                        sb.Append("</td></tr>\r\n");

                    }
                    if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "textarea")
                    {
                        sb.Append(string.Format("<tr><th>{0}</th><td><textarea name='txt{1}' id='txt{1}' class='ipt' style='height:{4};width:{3};'>{2}</textarea></td></tr>\r\n", (dtModuleField.Rows[i]["FieldTitle"].ToString()), dtModuleField.Rows[i]["FieldName"].ToString(), JumbotDLS.Utils.Strings.HtmlDecode(dtContent.Rows[0][dtModuleField.Rows[i]["FieldName"].ToString()].ToString()), dtModuleField.Rows[i]["InputWidth"].ToString(), dtModuleField.Rows[i]["InputHeight"].ToString()));
                        if (Str2Int(dtModuleField.Rows[i]["FieldIsUpload"].ToString()) > 0)
                        {
                            sb.Append("<script type='text/javascript'>function AttachmentOperater" + dtModuleField.Rows[i]["ID"].ToString() + "(path,type,size,field){if('" + dtModuleField.Rows[i]["FieldUploadModule"].ToString() + "'!='productprice'){$('#'+field).val(path);}else{ajaxExecutePriceFile(path,field)}}</script><tr><th>上传["+dtModuleField.Rows[i]["FieldTitle"].ToString()+"]</th><td><iframe src='attachment_iframe.aspx?N=" + dtModuleField.Rows[i]["ID"].ToString() + "&module=" + dtModuleField.Rows[i]["FieldUploadModule"].ToString() + "&BindField=txt" + dtModuleField.Rows[i]["FieldName"].ToString() + "' width='100%' height='30' scrolling='no' frameborder='0'></iframe></td></tr>");
                        }
                    }
                    if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "editor")
                        sb.Append(string.Format("<tr><th>{0}</th><td><textarea name='txt{1}' id='txt{1}' class='ipt' style='height:{4};width:{3};visibility:hidden;'>{2}</textarea>{5}</td></tr>\r\n", (dtModuleField.Rows[i]["FieldTitle"].ToString()), dtModuleField.Rows[i]["FieldName"].ToString(), dtContent.Rows[0][dtModuleField.Rows[i]["FieldName"].ToString()].ToString(), dtModuleField.Rows[i]["InputWidth"].ToString(), dtModuleField.Rows[i]["InputHeight"].ToString(), "<script>KindEditor.ready(function (K) {K.create('#txt" + dtModuleField.Rows[i]["FieldName"].ToString() + "', {cssPath: '../_libs/kindeditor-4.1.10/plugins/code/prettify.css',uploadJson: '../_libs/kindeditor-4.1.10/asp.net/upload_json.ashx?adminid=" + AdminId + "',fileManagerJson: '../_libs/kindeditor-4.1.10/asp.net/file_manager_json.ashx',allowFileManager: true,afterCreate: function () { var self = this; K.ctrl(document, 13, function () {self.sync();K('form[name=form1]')[0].submit(); });K.ctrl(self.edit.doc, 13, function () {self.sync();K('form[name=form1]')[0].submit();});}}); prettyPrint();});</script>"));
                }
                MyFormFieldList = sb.ToString();
                dtModuleField.Clear();
                dtModuleField.Dispose();
                dtContent.Clear();
                dtContent.Dispose();
            }


            doh.Reset();
            doh.SqlCmd = "Select * FROM [jdls_module_article_module_field] WHERE [State]=1 AND [FieldRequired]=1 AND SHowInAdminForm=1 AND  [ModuleId]=" + _ModuleId + " ORDER BY pid";
            DataTable dtModuleField2 = doh.GetDataTable();
            StringBuilder sb2 = new StringBuilder();
            for (int i = 0; i < dtModuleField2.Rows.Count; i++)
            {
                    sb2.Append("$(\"#txt"+dtModuleField2.Rows[i]["FieldName"].ToString()+"\").formValidator({ tipid: \"tip"+dtModuleField2.Rows[i]["FieldName"].ToString()+"\", onshow: \"请输入"+dtModuleField2.Rows[i]["FieldTitle"].ToString()+"\", onfocus: \"请输入"+dtModuleField2.Rows[i]["FieldTitle"].ToString()+"\", oncorrect: \"OK\" }).InputValidator({ min: 1, onerror: \"请输入"+dtModuleField2.Rows[i]["FieldTitle"].ToString()+"\" });\r\n");
                
            }
            MyFormFieldRequiredList = sb2.ToString();
            dtModuleField2.Clear();
            dtModuleField2.Dispose();
        }
        protected bool chkForm()
        {
            if (!CheckFormUrl())
                return false;
            if (!Page.IsValid)
                return false;
            return true;
        }
        /// <summary>
        /// 添加成功后
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void add_ok(object sender, EventArgs e)
        {
            doh.Reset();
            doh.SqlCmd = "Select * FROM [jdls_module_article_module_field] WHERE [State]=1 AND SHowInAdminForm=1 AND [ModuleId]=" + _ModuleId + " ORDER BY pid";
            DataTable dtModuleField = doh.GetDataTable();
            JumbotDLS.DBUtility.DbOperEventArgs de = (JumbotDLS.DBUtility.DbOperEventArgs)e;
            id = de.id.ToString();
            doh.Reset();
            doh.ConditionExpress = "id=@id";
            doh.AddConditionParameter("@id", id);
            #region  保存扩展字段
            for (int i = 0; i < dtModuleField.Rows.Count; i++)
            {
                if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "input")
                    doh.AddFieldItem(dtModuleField.Rows[i]["FieldName"].ToString(), JumbotDLS.Utils.Strings.HtmlEncode(f(string.Format("txt{0}", dtModuleField.Rows[i]["FieldName"].ToString()))));
                if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "checkbox")
                    doh.AddFieldItem(dtModuleField.Rows[i]["FieldName"].ToString(), JumbotDLS.Utils.Strings.HtmlEncode(f(string.Format("chk{0}", dtModuleField.Rows[i]["FieldName"].ToString()))));
                if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "radio")
                    doh.AddFieldItem(dtModuleField.Rows[i]["FieldName"].ToString(), JumbotDLS.Utils.Strings.HtmlEncode(f(string.Format("rbl{0}", dtModuleField.Rows[i]["FieldName"].ToString()))));
                if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "textarea")
                    doh.AddFieldItem(dtModuleField.Rows[i]["FieldName"].ToString(), JumbotDLS.Utils.Strings.HtmlEncode(f(string.Format("txt{0}", dtModuleField.Rows[i]["FieldName"].ToString()))));
                if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "editor")
                    doh.AddFieldItem(dtModuleField.Rows[i]["FieldName"].ToString(), f(string.Format("txt{0}", dtModuleField.Rows[i]["FieldName"].ToString())));
            }
            #endregion
            if(f("txtOutUrl").Length>0)
                doh.AddFieldItem("Target", "_blank");
            else
                doh.AddFieldItem("Target", "");
            doh.AddFieldItem("Module", _ModuleType);
            doh.Update("jdls_module_article");
            dtModuleField.Clear();
            dtModuleField.Dispose();
            FinalMessage("成功保存", "close.htm", 0);
        }
        /// <summary>
        /// 修改成功后
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void save_ok(object sender, EventArgs e)
        {
            doh.Reset();
            doh.SqlCmd = "Select * FROM [jdls_module_article_module_field] WHERE [State]=1 AND SHowInAdminForm=1 AND  [ModuleId]=" + _ModuleId + " ORDER BY pid";
            DataTable dtModuleField = doh.GetDataTable();
            doh.Reset();
            doh.ConditionExpress = "id=@id";
            doh.AddConditionParameter("@id", id);
            #region  保存扩展字段
            for (int i = 0; i < dtModuleField.Rows.Count; i++)
            {
                if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "input")
                    doh.AddFieldItem(dtModuleField.Rows[i]["FieldName"].ToString(), JumbotDLS.Utils.Strings.HtmlEncode(f(string.Format("txt{0}", dtModuleField.Rows[i]["FieldName"].ToString()))));
                if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "checkbox")
                    doh.AddFieldItem(dtModuleField.Rows[i]["FieldName"].ToString(), JumbotDLS.Utils.Strings.HtmlEncode(f(string.Format("chk{0}", dtModuleField.Rows[i]["FieldName"].ToString()))));
                if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "radio")
                    doh.AddFieldItem(dtModuleField.Rows[i]["FieldName"].ToString(), JumbotDLS.Utils.Strings.HtmlEncode(f(string.Format("rbl{0}", dtModuleField.Rows[i]["FieldName"].ToString()))));
                if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "textarea")
                    doh.AddFieldItem(dtModuleField.Rows[i]["FieldName"].ToString(), JumbotDLS.Utils.Strings.HtmlEncode(f(string.Format("txt{0}", dtModuleField.Rows[i]["FieldName"].ToString()))));
                if (dtModuleField.Rows[i]["FieldType"].ToString().ToLower() == "editor")
                    doh.AddFieldItem(dtModuleField.Rows[i]["FieldName"].ToString(), f(string.Format("txt{0}", dtModuleField.Rows[i]["FieldName"].ToString())));
            }
            #endregion
            if (f("txtOutUrl").Length > 0)
                doh.AddFieldItem("Target", "_blank");
            else
                doh.AddFieldItem("Target", "");
            doh.Update("jdls_module_article");
            dtModuleField.Clear();
            dtModuleField.Dispose();
            FinalMessage("成功保存", "close.htm", 0);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
        }
    }
}
