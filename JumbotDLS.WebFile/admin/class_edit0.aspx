﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="class_edit0.aspx.cs" Inherits="JumbotDLS.WebForm.Admin._class_edit0" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta   name="robots" content="noindex, nofollow" />
<meta   http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title>栏目编辑</title>
<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js"></script>
<link type="text/css" rel="stylesheet" href="../_data/global.css" />
<link type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
<link rel="stylesheet" href="../_libs/kindeditor-4.1.10/themes/default/default.css" />
<link rel="stylesheet" href="../_libs/kindeditor-4.1.10/plugins/code/prettify.css" />
<script charset="utf-8" src="../_libs/kindeditor-4.1.10/kindeditor.js"></script>
<script charset="utf-8" src="../_libs/kindeditor-4.1.10/lang/zh_CN.js"></script>
<script charset="utf-8" src="../_libs/kindeditor-4.1.10/plugins/code/prettify.js"></script>
<script type="text/javascript" src="../statics/admin/js/common.js"></script>
<script type="text/javascript" src="../_libs/pinyin.js"></script>
<script type="text/javascript">
var ccid = joinValue('ccid');
$(document).ready(function(){
	$('.tip-r').jtip({gravity: 'r',fade: false});
	$.formValidator.initConfig({onError:function(msg){alert(msg);}});
	$("#txtTitle").formValidator({ tipid: "tipTitle", onshow: "请输入栏目名称", onfocus: "请输入栏目名称" }).InputValidator({ min: 2, onerror: "请输入栏目名称" });
	$("#txtPageSize").formValidator({tipid:"tipPageSize",onshow:"请填写5-50的数字,推荐20",onfocus:"请填写5-100的数字,推荐20"}).RegexValidator({regexp:"^\([5-9]{1}|[1-9]{1}[0-9]{1}|100)$",onerror:"请填写5-100的数字"});
	$("#txtFolder").formValidator({tipid:"tipFolder",onshow:"只支持英文字母、数字和短线",onfocus:"一旦保存将无法修改"}).RegexValidator({regexp:"^\([0-9a-zA-Z\-]+)$",onerror:"只支持英文字母、数字和短线"});
	$("#txtAliasPage").formValidator({empty:true,tipid:"tipAliasPage",onshow:"指定文件路径(第1页)，不输入即为默认。如/aa/bb/cc.html",onfocus:"动态频道只支持aspx结尾，请慎重输入"}).RegexValidator({regexp:"^\(/[_\-a-zA-Z0-9\.]+(/[_\-a-zA-Z0-9\.]+)*\.(aspx|htm(l)?|shtm(l)?))$",onerror:"以“/”开头，后缀支持aspx|htm(l)|shtm(l)，如/aa/bb/cc.html"});
	$("#txtSortRank").formValidator({tipid:"tipSortRank",onshow:"请填写数字",onfocus:"请填写数字"}).RegexValidator({regexp:"^\([0-9]+)$",onerror:"请填写数字"});
});
function ajaxChinese2Pinyin(chinese,t)
{
	$.ajax({
		type:		"post",
		dataType:	"json",
		data:		"chinese="+encodeURIComponent(chinese)+"&t="+t+"&clienttime="+Math.random(),
		url:		"ajax.aspx?oper=ajaxChinese2Pinyin",
		error:		function(XmlHttpRequest,textStatus, errorThrown){JumbotDLS.Loading.hide();alert(XmlHttpRequest.responseText); },
		success:	function(d){
			switch (d.result)
			{
			case '-1':
				JumbotDLS.Alert(d.returnval, "0", "top.window.location='login.aspx';");
				break;
			case '0':
				JumbotDLS.Alert(d.returnval, "0");
				break;
			case '1':
				$("#txtFolder").val(d.returnval);
				break;
			}
		}
	});
}
/*最后的表单验证*/
function CheckFormSubmit(){
	if($.formValidator.PageIsValid('1'))
	{
	    JumbotDLS.Loading.show("正在处理，请等待...");
		return true;
	}else{
		return false;
	}
}

var PhotoInput = 'txtImg';
function FillPhoto(photo) {
    $('#' + PhotoInput).val(photo);
}
//插入上传附件
function AttachmentOperater(path, type, size) {
    switch (type) {
        case 'gif':
        case 'jpg':
        case 'jpeg':
            html = '<br /><a href="' + path + '" target="_blank"><img src="' + path + '"></a><br />';
            break;
        case 'mp3':
        case 'wma':
            html = '<br /><object width="350" height="64" classid="CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6" codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,7,1112" align="baseline" border="0" standby="Loading Microsoft Windows Media Player components..." type="application/x-oleobject"><param name="URL" value="' + path + '"><param name="autoStart" value="true"><param name="invokeURLs" value="false"><param name="playCount" value="100"><param name="defaultFrame" value="datawindow"><embed src="' + path + '" align="baseline" border="0" width="350" height="68" type="application/x-mplayer2" pluginspage="" name="MediaPlayer1" showcontrols="1" showpositioncontrols="0" showaudiocontrols="1" showtracker="1" showdisplay="0" showstatusbar="1" autosize="0" showgotobar="0" showcaptioning="0" autostart="1" autorewind="0" animationatstart="0" transparentatstart="0" allowscan="1" enablecontextmenu="1" clicktoplay="0" defaultframe="datawindow" invokeurls="0"></embed></object>';
            break;
        case 'asf':
        case 'avi':
        case 'wmv':
            html = '<br /><object classid="clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95" codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,0,02,902" type="application/x-oleobject" standby="Loading..." width="400" height="300"><param name="FileName" VALUE="' + path + '" /><param name="ShowStatusBar" value="-1" /><param name="AutoStart" value="true" /><embed type="application/x-mplayer2" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" src="' + path + '" autostart="true" width="400" height="300" /></object><br />';
            break;
        case 'swf':
            html = '<br /><object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="400" height="300"><param name="movie" value="' + path + '" /><param name="quality" value="high" /><param name="AllowScriptAccess" value="never" /><embed src="' + path + '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="400" height="300" /></object><br />';
            break;
        default:
            html = '<br /><a href="' + path + '"><img border="0" src="<% = site.Dir%><% = ChannelDir%>/images/' + type + '.gif" alt="点击下载" />点击下载</a>(' + size + ')<br />';
            break;
    }
    KindEditor.insertHtml("#txtContent", html);
}
KindEditor.ready(function (K) {
    var editor1 = K.create('#txtContent', {
        cssPath: '../_libs/kindeditor-4.1.10/plugins/code/prettify.css',
        uploadJson: '../_libs/kindeditor-4.1.10/asp.net/upload_json.ashx?adminid=<%=AdminId %>',
        fileManagerJson: '../_libs/kindeditor-4.1.10/asp.net/file_manager_json.ashx',
        allowFileManager: true,
        afterCreate: function () {
            var self = this;
            K.ctrl(document, 13, function () {
                self.sync();
                K('form[name=form1]')[0].submit();
            });
            K.ctrl(self.edit.doc, 13, function () {
                self.sync();
                K('form[name=form1]')[0].submit();
            });
        }
    });
    prettyPrint();
});
</script>
</head>
<body>
<form id="form1" runat="server" onsubmit="return CheckFormSubmit()">
      <table class="formtable mrg10T">
        <tr>
          <th> 栏目名称 </th>
          <td><asp:TextBox ID="txtTitle" runat="server" MaxLength="100" Width="97%" CssClass="ipt"></asp:TextBox>
            <span id="tipTitle" style="width:200px;"> </span></td>
        </tr>
        <tr>
          <th> 子分类列表</th>
          <td><asp:TextBox ID="txtSubClassList" runat="server" MaxLength="400" Width="97%" CssClass="ipt" Height="97px" TextMode="MultiLine"></asp:TextBox>
            <br />多个分类之间用半角分号</td>
        </tr>

        <tr>
			<th> 允许管理的类型</th>
			<td><asp:RadioButtonList ID="rblDefaultModuleId" runat="server" EnableViewState="True" RepeatColumns="30">
            <items></items>
			</asp:RadioButtonList></td>
		</tr>
        <tr style="display:none;">
          <th>序号</th>
          <td><asp:TextBox ID="txtSortRank" runat="server" MaxLength="3" Width="40px" CssClass="ipt">0</asp:TextBox>
            <span id="tipSortRank" style="width:200px;"> </span><br />
            <span class="red">在同级栏目下请勿重复</span></td>
        </tr>
        <tr>
          <th> 静态页面目录</th>
          <td><asp:TextBox ID="txtFolder" runat="server" MaxLength="40" Width="200px" CssClass="ipt"></asp:TextBox>
            <span id="tipFolder" style="width:200px;"> </span></td>
        </tr>
        <tr style="display:none;">
          <th>Icon类名</th>
          <td><asp:TextBox ID="txtIcon" runat="server" Width="100px" CssClass="ipt"></asp:TextBox>
          </td>
        </tr>
        <tr style="display:none;">
          <th>Icon字符串</th>
          <td><asp:TextBox ID="txtIconString" runat="server" Width="200px" CssClass="ipt"></asp:TextBox>
          </td>
        </tr>
		<tr>
			<th> 上级栏目 </th>
			<td><asp:DropDownList ID="ddlParentId" runat="server"> </asp:DropDownList>
				(该频道只支持<%=ChannelClassDepth %>级分类) 
                <script>                    if ('<%=q("parentid") %>' != '') $('#ddlParentId').val('<%=q("parentid") %>');</script>
                </td>
		</tr>
        <tr>
          <th> 最低浏览权限 </th>
          <td><asp:DropDownList ID="ddlReadGroup" runat="server"> </asp:DropDownList>
            (此功能在静态生成的频道无效) </td>
        </tr>
        <tr>
          <th> 栏目页模板 </th>
          <td><asp:DropDownList ID="ddlThemeId" runat="server"> </asp:DropDownList>
          </td>
        </tr>
        <tr>
          <th> 内容页模板 </th>
          <td><asp:DropDownList ID="ddlContentTheme" runat="server"> </asp:DropDownList>
          </td>
        </tr>
        <tr>
          <th> 支持分页显示</th>
          <td><asp:RadioButtonList ID="rblIsPaging" runat="server" EnableViewState="False" RepeatColumns="2">
              <asp:ListItem Value="1" Selected="True">是(用于终极栏目)</asp:ListItem>
              <asp:ListItem Value="0">否(用于父级栏目)</asp:ListItem>
             </asp:RadioButtonList></td>
        </tr>
        <tr>
          <th> 终极列表每页记录 </th>
          <td><asp:TextBox ID="txtPageSize" runat="server" Width="39px" CssClass="ipt">20</asp:TextBox>
            <span id="tipPageSize" style="width:200px;"> </span></td>
        </tr>
        <tr style="display:<%=MainChannel.IsPost? "" : "none"%>;">
          <th> 允许会员投稿</th>
          <td><asp:RadioButtonList ID="rblIsPost" runat="server" EnableViewState="False" RepeatColumns="2">
              <asp:ListItem Value="1">是</asp:ListItem>
              <asp:ListItem Value="0" Selected="True">否</asp:ListItem>
              </asp:RadioButtonList></td>
        </tr>
        <tr style="display:none;">
          <th> 在“栏目导航”显示</th>
          <td><asp:RadioButtonList ID="rblIsNav" runat="server" EnableViewState="False" RepeatColumns="2">
              <asp:ListItem Value="1" Selected="True">是</asp:ListItem>
              <asp:ListItem Value="0">否</asp:ListItem>
              </asp:RadioButtonList></td>
        </tr>
        <tr style="display:none;">
          <th> 在“当前位置”显示</th>
          <td><asp:RadioButtonList ID="rblIsTop" runat="server" EnableViewState="False" RepeatColumns="2">
              <asp:ListItem Value="1" Selected="True">是</asp:ListItem>
              <asp:ListItem Value="0">否</asp:ListItem>
              </asp:RadioButtonList></td>
        </tr>
        <tr style="display:none;">
          <th> 指定文件路径(第1页) </th>
          <td><asp:TextBox ID="txtAliasPage" runat="server" Width="97%" CssClass="ipt"></asp:TextBox>
            <br />
            <span id="tipAliasPage" style="width:600px;"></span></td>
        </tr>
        <tr style="display:none;">
          <th> 栏目标识图 </th>
          <td><asp:TextBox ID="txtImg" runat="server" MaxLength="150" Width="500px" CssClass="ipt"></asp:TextBox>
            <a href="javascript:void(0);" onclick="PhotoInput = 'txtImg';JumbotDLS.Popup.show('cut2thumb_upload.aspx?ccid=<%=ChannelId %>&photo='+encodeURIComponent($('#txtImg').val()),-100,-100,true);"><img src="../statics/admin/images/thumb_create.png" align="absmiddle" style="border:0px;" /></a> <a href="javascript:void(0);" onclick="if($('#txtImg').val()!=''){JumbotDLS.Popup.show('cut2thumb_preview.aspx?photo='+encodeURIComponent($('#txtImg').val()),-100,-100,true);}else{alert('请先制作缩略图')}"><img src="../statics/admin/images/thumb_preview.png" align="absmiddle" style="border:0px;" /></a> </td>
        </tr>

        <tr style="display:none;">
          <th>Meta Description</th>
          <td><asp:TextBox ID="txtInfo" runat="server" Height="97px" TextMode="MultiLine" Width="97%" CssClass="ipt"></asp:TextBox>
          </td>
        </tr>
        <tr style="display:none;">
          <th>Meta Keywords</th>
          <td><asp:TextBox ID="txtKeywords" runat="server" Width="97%" CssClass="ipt"></asp:TextBox>
          </td>
        </tr>
        <tr style="display:none;">
          <th> 栏目内容 </th>
          <td><textarea id="txtContent" runat="server" cols="100" rows="8" style="width:100%;height:350px;visibility:hidden;" ></textarea></td>
        </tr>
      </table>
  <div class="buttonok">
    <asp:Button ID="btnSave" runat="server" Text="确定" CssClass="btnsubmit" 
            onclick="btnSave_Click" />
    <input id="btnReset" type="button" value="取消" class="btncancel" onclick="parent.JumbotDLS.Popup.hide();" />
  </div>
</form>
<script type="text/javascript">_jdls_SetDialogTitle();</script>
</body>
</html>
