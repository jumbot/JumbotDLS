﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JumbotDLS.WebFile.Admin
{
    public partial class _File : JumbotDLS.UI.FrontHtml
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(q("file").StartsWith("/_export"))
                this.DownloadZipFile(q("ext"), q("file"), (q("name") == "" ? "export_" + DateTime.Now.ToString("yyyyMMddHHmmssffff") : q("name")) + "." + q("ext"));
        }
        /// <summary>
        /// 下载Zip文件
        /// </summary>
        /// <param name="_filePath"></param>
        /// <param name="_fileName"></param>
        /// <returns></returns>
        public bool DownloadZipFile(string _fileExt,string _filePath, string _fileName)
        {
            if (_fileExt == "")
                _fileExt = JumbotDLS.Utils.DirFile.GetFileExt(_filePath);
            try
            {
                Response.Clear();
                Response.Buffer = true;
                Response.ContentEncoding = System.Text.Encoding.UTF8;
                if (_fileExt == "doc" || _fileExt == "docx")
                    Response.ContentType = "application/vnd.ms-word";
                else if (_fileExt == "xls" ||_fileExt == "xlsx")
                    Response.ContentType = "application/vnd.ms-excel";
                else if (_fileExt == "txt")
                    Response.ContentType = "text/xml";
                else if (_fileExt == "xml")
                    Response.ContentType = "text/xml";
                else
                    Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + System.Web.HttpUtility.UrlEncode(_fileName, System.Text.Encoding.UTF8));
                string filename = Server.MapPath(_filePath);
                Response.TransmitFile(filename);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}