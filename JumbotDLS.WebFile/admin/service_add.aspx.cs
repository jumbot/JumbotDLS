﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _service_add : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("master", "html");
            id = Str2Str(q("id"));
            JumbotDLS.DBUtility.WebFormHandler wh = new JumbotDLS.DBUtility.WebFormHandler(doh, "jdls_normal_user", btnSave);
            wh.AddBind(lblUserName, "UserName", true);
            wh.AddBind(txtServiceName, "ServiceName", true);
            this.txtServiceName.ReadOnly = false;
            wh.ConditionExpress = "id=" + id;
            wh.Mode = JumbotDLS.DBUtility.OperationType.Modify;
            wh.validator = chkForm;
            wh.ModifyOk += new EventHandler(save_ok);
        }
        protected void bind_ok(object sender, EventArgs e)
        {
        }
        protected bool chkForm()
        {
            if (!CheckFormUrl())
                return false;
            if (!Page.IsValid)
                return false;
            doh.Reset();
            doh.SqlCmd = "SELECT ServiceId FROM [jdls_normal_user] WHERE [ServiceName]='" + txtServiceName.Text + "'";
            if (doh.GetDataTable().Rows.Count > 0)
            {
                FinalMessage("用户名重复", "", 1);
                return false;
            }
            return true;
        }
        protected void save_ok(object sender, EventArgs e)
        {
            doh.Reset();
            doh.ConditionExpress = "id=" + id;
            doh.AddFieldItem("ServiceId", id);
            doh.AddFieldItem("GUID", "00000000-0000-0000-0000-" + id.PadLeft(12, '0'));
            doh.Update("jdls_normal_user");
            new JumbotDLS.DAL.Normal_UserDAL().RefreshServiceList();
            FinalMessage("成功保存", "close.htm", 0);
        }
    }
}
