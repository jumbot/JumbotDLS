﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JumbotDLS.Utils;
using JumbotDLS.Common;

namespace JumbotDLS.WebFile.Admin
{
    public partial class _cut2thumb_upload : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ccid"));
            Admin_Load("", "html", true);
            this.ThumbsSize.Items.Clear();
            ListItem li;
            doh.Reset();
            doh.ConditionExpress = "id=@id";
            doh.AddConditionParameter("@id", ChannelId);
            string _defaultThumbs = doh.GetField("jdls_normal_channel", "DefaultThumbs").ToString();
            DataTable dtThumbs = new JumbotDLS.DAL.Normal_ThumbsDAL().GetDataTable(ChannelId);
            for (int i = 0; i < dtThumbs.Rows.Count; i++)
            {
                li = new ListItem();
                li.Value = dtThumbs.Rows[i]["iWidth"].ToString() + "|" + dtThumbs.Rows[i]["iHeight"].ToString();
                li.Text = dtThumbs.Rows[i]["Title"].ToString();
                if (_defaultThumbs == dtThumbs.Rows[i]["ID"].ToString())
                    li.Selected = true;
                else
                    li.Selected = false;
                this.ThumbsSize.Items.Add(li);
            }
            dtThumbs.Clear();
            dtThumbs.Dispose();
            if (q("photo") != "")
            {
                NewsCollection nc = new NewsCollection();
                this.Image1.ImageUrl = nc.LocalFileUrl(site.Url, site.MainSite, q("photo"), ChannelUploadPath, true, 0, 0);
            }
        }
    }
}

