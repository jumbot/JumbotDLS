﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _database1 : JumbotDLS.UI.AdminCenter
    {
        public string _bakname = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("master", "html");
            if (!Page.IsPostBack)
            {
                _bakname = "backup" + DateTime.Now.ToString("yyyyMMddHH") + ".bak";
            }
        }
    }
}
