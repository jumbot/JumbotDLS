﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JumbotDLS.WebFile.admin
{
    public partial class dict_edit : JumbotDLS.UI.AdminCenter
    {
        public string tpPath = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("master", "html");
            id = Str2Str(q("id"));
            string groupname = q("groupname");

            JumbotDLS.DBUtility.WebFormHandler wh = new JumbotDLS.DBUtility.WebFormHandler(doh, "jdls_module_article_module_field_item", btnSave);
            wh.AddBind(txtTitle, "ItemTitle", true);
            if (id == "0")
            {
                doh.Reset();
                doh.SqlCmd = "select top 1 Itemvalue  from jdls_module_article_module_field_item where groupname='" + groupname + "' order by convert(int,Itemvalue) desc";
                DataTable dt = doh.GetDataTable();
                string value = "0";
                if (dt.Rows.Count>0)
                    value = dt.Rows[0][0].ToString();
                value=(int.Parse(value)+1).ToString();
                string pid = "0";
                doh.SqlCmd = "select top 1 pid  from jdls_module_article_module_field_item where groupname='" + groupname + "' order by pid desc";
                dt = doh.GetDataTable();
                if (dt.Rows.Count > 0)
                    pid = dt.Rows[0][0].ToString();
                pid=(int.Parse(pid)+1).ToString();
                wh.AddBind(ref value, "ItemValue", true);
                wh.AddBind(ref pid, "pid", true);
                wh.AddBind(ref groupname, "GroupName", true);
                wh.Mode = JumbotDLS.DBUtility.OperationType.Add;
            }
            else
            {
                wh.ConditionExpress = "id=" + id;
                wh.Mode = JumbotDLS.DBUtility.OperationType.Modify;
            }
            wh.validator = chkForm;
            wh.AddOk += new EventHandler(save_ok);
            wh.ModifyOk += new EventHandler(save_ok);
        }
        protected void bind_ok(object sender, EventArgs e)
        {
        }
        protected bool chkForm()
        {
            if (!CheckFormUrl())
                return false;
            if (!Page.IsValid)
                return false;
            doh.Reset();
            doh.ConditionExpress = "ItemValue=@ItemTitle and groupname=@groupname and id<>" + id;
            doh.AddConditionParameter("@ItemTitle", txtTitle.Text);
            doh.AddConditionParameter("@groupname", q("groupname"));
            if (doh.Exist("jdls_module_article_module_field_item"))
            {
                FinalMessage("该名称已存在!", "", 1);
                return false;
            }
            return true;
        }
        protected void save_ok(object sender, EventArgs e)
        {
            FinalMessage("成功保存", "close.htm", 0);
        }
    }
}