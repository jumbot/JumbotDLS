﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _content_subcontent_list : JumbotDLS.UI.AdminCenter
    {
        public int ModuleId = 0;
        public string ParentId = "0";
        public string EditFile = "content_subcontent_edit.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ccid"));
            ParentId = Str2Str(q("parentid"));
            ModuleId = Str2Int(q("moduleid"));
            Admin_Load(ChannelId + "-01", "html", true);
        }
    }
}