﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _adv_view : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Str2Str(q("id"));
            Admin_Load("adv-mng", "stop");
            this.txtASPXTmpTag.Text = "<!--#include virtual=\"/_data/html/more/" + id + ".htm\" -->";
            this.txtSHTMTmpTag.Text = "<!--#include virtual=\"/_data/shtm/more/" + id + ".htm\" -->";
            this.txtJSTmpTag.Text = "<script type=\"text/javascript\" src=\"/_data/style/more/" + id + ".js\"></script>";
            this.Literal1.Text = new JumbotDLS.DAL.AdvDAL().GetAdvBody(id);
        }
    }
}
