﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="class_edit1.aspx.cs" Inherits="JumbotDLS.WebFile.Admin._class_edit1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html   xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta   name="robots" content="noindex, nofollow" />
<meta   http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title></title>
<script type="text/javascript" src="../_libs/jquery.tools.pack.js"></script>
<script type="text/javascript" src="../_data/global.js"></script>
<link type="text/css" rel="stylesheet" href="../_data/global.css" />
<link type="text/css" rel="stylesheet" href="../statics/admin/css/common.css" />
<script type="text/javascript" src="../statics/admin/js/common.js"></script>
<script type="text/javascript" src="../_libs/pinyin.js"></script>

<script type="text/javascript">
	var ccid = joinValue('ccid');
$(document).ready(function(){
	$.formValidator.initConfig({onError:function(msg){alert(msg);}});
	$("#txtTitle").formValidator({tipid:"tipTitle",onshow:"请输入栏目名称",onfocus:"请输入4-30个字符(2-15个汉字)",oncorrect:"OK"}).InputValidator({min:4,max:30,onerror:"请输入4-30个字符(2-15个汉字)"});
	$("#txtAliasPage").formValidator({ tipid: "tipAliasPage", onshow: "请输入链接地址", onfocus: "请输入链接地址", oncorrect: "OK" }).InputValidator({ min: 1, max: 100, onerror: "输入有误,请确认" });

});
    </script>
</head>
<body>
<form id="form1" runat="server" onsubmit="return $.formValidator.PageIsValid('1')">
	<table class="formtable">
		<tr>
			<th> 栏目名称 </th>
			<td><asp:TextBox ID="txtTitle" runat="server" MaxLength="40" Width="200px" CssClass="ipt"></asp:TextBox>
				<span id="tipTitle" style="width:200px;"> </span></td>
		</tr>
		<tr>
			<th> 上级栏目 </th>
			<td><asp:DropDownList ID="ddlParentId" runat="server"> </asp:DropDownList>
				(该频道只支持<%=ChannelClassDepth %>级分类) 
                <script>                    if ('<%=q("parentid") %>' != '') $('#ddlParentId').val('<%=q("parentid") %>');</script>
                </td>
		</tr>
		<tr>
			<th> 栏目简介 </th>
			<td><asp:TextBox ID="txtInfo" runat="server" Width="300px" CssClass="ipt"></asp:TextBox>
			</td>
		</tr>
        <tr>
          <th> 在“栏目导航”显示</th>
          <td><asp:RadioButtonList ID="rblIsNav" runat="server" EnableViewState="False" RepeatColumns="2"> <Items>
              <asp:ListItem Value="1" Selected="True">是</asp:ListItem>
              <asp:ListItem Value="0">否</asp:ListItem>
              </Items> </asp:RadioButtonList></td>
        </tr>
		<tr>
			<th> 链接地址 </th>
			<td><asp:TextBox ID="txtAliasPage" runat="server" MaxLength="100" Width="97%" CssClass="ipt"></asp:TextBox>
				<span id="tipAliasPage" style="width:250px"></span>
			</td>
		</tr>
        <tr>
          <th>Icon类名</th>
          <td><asp:TextBox ID="txtIcon" runat="server" Width="100px" CssClass="ipt"></asp:TextBox>
          </td>
        </tr>
        <tr>
          <th>Icon字符串</th>
          <td><asp:TextBox ID="txtIconString" runat="server" Width="200px" CssClass="ipt"></asp:TextBox>
          </td>
        </tr>
		<tr>
			<th> 允许在“当前位置”显示</th>
			<td><asp:RadioButtonList ID="rblIsTop" runat="server" EnableViewState="False" RepeatColumns="2">
					<Items>
						<asp:ListItem Value="1" Selected="True">是</asp:ListItem>
						<asp:ListItem Value="0">否</asp:ListItem>
					</Items>
				</asp:RadioButtonList></td>
		</tr>
			<tr>
				<th> 链接方式 </th>
				<td><asp:RadioButtonList ID="rblTarget" runat="server" RepeatDirection="Horizontal">
                						<asp:ListItem Value="" Selected="True">默认</asp:ListItem>
						<asp:ListItem Value="_self">原窗口</asp:ListItem>
						<asp:ListItem Value="_blank">新窗口</asp:ListItem>
					</asp:RadioButtonList>
				</td>
			</tr>
	</table>
	<div class="buttonok">
		<asp:Button ID="btnSave" runat="server" Text="确定" CssClass="btnsubmit" />
		<input id="btnReset" type="button" value="取消" class="btncancel" onclick="parent.JumbotDLS.Popup.hide();" />
	</div>
</form>
<script type="text/javascript">_jdls_SetDialogTitle();</script>
</body>
</html>
