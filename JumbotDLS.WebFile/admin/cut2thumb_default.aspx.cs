﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _cut2thumb_default : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ccid"));
            Admin_Load("", "html", true);
            string FrameName = q("fname");
            string TempPhoto = q("tphoto").Replace(site.Url, "").Split('?')[0];
            string fileExtension = "." + JumbotDLS.Utils.DirFile.GetFileExt(TempPhoto);//缩略图后缀名
            string ToWidth = q("tow");
            string ToHeight = q("toh");
            string CutType = q("type");
            if (CutType == "1")//手工裁剪
            {
                string printhtml = "";
                printhtml += "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Frameset//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd\">\r\n";
                printhtml += "<html   xmlns=\"http://www.w3.org/1999/xhtml\">\r\n";
                printhtml += "<head>\r\n";
                printhtml += "<meta   http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n";
                printhtml += "<title>图片裁剪</title>\r\n";
                printhtml += "<script type=\"text/javascript\" src=\"../_libs/jquery.tools.pack.js\"></script>\r\n";
                printhtml += "<script type=\"text/javascript\" src=\"../_data/global.js\"></script>\r\n";
                printhtml += "</head>\r\n";
                printhtml += "<frameset rows=\"30,*\" cols=\"*\" framespacing=\"0\" frameborder=\"no\" border=\"0\">\r\n";
                printhtml += "<frame src=\"cut2thumb_process_bar.aspx?ccid=" + ChannelId + "&tphoto=" + TempPhoto + "&tow=" + ToWidth + "&toh=" + ToHeight + "\" name=\"topFrame\" id=\"topFrame\" />\r\n";
                printhtml += "<frame src=\"cut2thumb_process_body.aspx?ccid=" + ChannelId + "&tphoto=" + TempPhoto + "&tow=" + ToWidth + "&toh=" + ToHeight + "\" name=\"mainFrame\" scrolling=\"auto\" noresize=\"noresize\" id=\"mainFrame\" />\r\n";
                printhtml += "</frameset>\r\n";
                printhtml += "<noframes><body>\r\n";
                printhtml += "</body>\r\n";
                printhtml += "</noframes></html>\r\n";
                Response.Write(printhtml);
            }
            else//自动缩放
            {
                string DirectoryPath = ChannelUploadPath + DateTime.Now.ToString("yyMMdd");
                JumbotDLS.Utils.DirFile.CreateDir(DirectoryPath);

                string sFileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + "_thumb" + fileExtension;  // 文件名称
                string thumbnailPath = Server.MapPath(DirectoryPath + "/" + sFileName);        // 服务器端文件路径

                JumbotDLS.Utils.ImageHelp.LocalImage2Thumbs(Server.MapPath(TempPhoto), thumbnailPath, Convert.ToInt32(ToWidth), Convert.ToInt32(ToHeight), CutType);
                Response.Write("<script>parent.FillPhoto('" + DirectoryPath + "/" + sFileName + "');parent.JumbotDLS.Popup.hide();</script>");
            }
        }
    }
}
