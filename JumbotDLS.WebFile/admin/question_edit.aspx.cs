﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _user_question_edit : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin_Load("question-mng", "html");
            id = Str2Str(q("id"));
            doh.Reset();
            JumbotDLS.DBUtility.WebFormHandler wh = new JumbotDLS.DBUtility.WebFormHandler(doh, "jdls_normal_question", btnSave);
            wh.AddBind(lblUserName, "UserName", true);
            wh.AddBind(lblEmail, "Email", true);
            wh.AddBind(lblPhone, "Phone", true);
            wh.AddBind(lblAddDate, "AddDate", true);
            wh.AddBind(lblTitle, "Title", true);
            wh.AddBind(lblContent, "Content", true);
            wh.AddBind(txtReplyContent, "ReplyContent", true);
            wh.AddBind(lblReplyTime, "ReplyTime", true);
            wh.ConditionExpress = "id=" + id.ToString();
                wh.Mode = JumbotDLS.DBUtility.OperationType.Modify;
            wh.BindBeforeModifyOk += new EventHandler(bind_ok);
            wh.ModifyOk += new EventHandler(save_ok);
            wh.validator = chkForm;
        }
        /// <summary>
        /// 绑定数据后的处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bind_ok(object sender, EventArgs e)
        {
            this.txtReplyContent.Text = JumbotDLS.Utils.Strings.HtmlDecode(this.txtReplyContent.Text);
        }
        protected bool chkForm()
        {
            if (!CheckFormUrl())
                return false;
            if (!Page.IsValid)
                return false;
            return true;
        }
        protected void save_ok(object sender, EventArgs e)
        {
            FinalMessage("成功保存", "close.htm", 0);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            this.txtReplyContent.Text = GetCutString(JumbotDLS.Utils.Strings.HtmlEncode(this.txtReplyContent.Text), 200);
            this.lblReplyTime.Text = System.DateTime.Now.ToString();
        }
    }
}
