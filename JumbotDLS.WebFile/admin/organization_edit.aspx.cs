﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _organization_edit : JumbotDLS.UI.AdminCenter
    {
        public string OrganizationId = "0";
        protected void Page_Load(object sender, EventArgs e)
        {
            OrganizationId = Str2Str(q("ccid"));
            Admin_Load("master", "html");
            JumbotDLS.DBUtility.WebFormHandler wh = new JumbotDLS.DBUtility.WebFormHandler(doh, "jdls_organization", btnSave);
            wh.AddBind(txtTitle, "Title", true);
            wh.AddBind(txtInfo, "Info", true);
            wh.AddBind(txtAddress, "Address", true);
            wh.AddBind(txtZipCode, "ZipCode", true);
            wh.AddBind(txtPhone, "Phone", true);
            wh.AddBind(txtEmail, "Email", true);
            wh.AddBind(rblEnabled, "SelectedValue", "Enabled", false);
            wh.AddBind(txtBeginTime, "BeginTime", true);
            wh.AddBind(txtEndTime, "EndTime", true);
            wh.AddBind(txtTotalCount, "TotalCount", false);
            if (OrganizationId != "0")
            {
                wh.ConditionExpress = "id=" + OrganizationId;
                wh.Mode = JumbotDLS.DBUtility.OperationType.Modify;
            }
            else
            {
                wh.Mode = JumbotDLS.DBUtility.OperationType.Add;
            }
            wh.validator = chkForm;
            wh.BindBeforeAddOk += new EventHandler(bind_ok);
            wh.BindBeforeModifyOk += new EventHandler(bind_ok);
            wh.AddOk += new EventHandler(add_ok);
            wh.ModifyOk += new EventHandler(save_ok);
        }
        protected void bind_ok(object sender, EventArgs e)
        {
            if (OrganizationId != "0")
            {
                this.txtBeginTime.Text = Convert.ToDateTime(this.txtBeginTime.Text).ToString("yyyy-MM-dd");
                this.txtEndTime.Text = Convert.ToDateTime(this.txtEndTime.Text).ToString("yyyy-MM-dd");
            }
            else
            {
                this.txtBeginTime.Text = System.DateTime.Now.ToString("yyyy-MM-dd");
                this.txtEndTime.Text = System.DateTime.Now.AddMonths(1).ToString("yyyy-MM-dd");
            }
        }
        protected bool chkForm()
        {
            if (!CheckFormUrl())
                return false;
            if (!Page.IsValid)
                return false;
            doh.Reset();
            doh.ConditionExpress = "Title=@title and id<>" + OrganizationId;
            doh.AddConditionParameter("@title", txtTitle.Text);
            if (doh.Exist("jdls_organization"))
            {
                FinalMessage("机构名重复!", "", 1);
                return false;
            }
            return true;
        }
        protected void add_ok(object sender, EventArgs e)
        {
            JumbotDLS.DBUtility.DbOperEventArgs de = (JumbotDLS.DBUtility.DbOperEventArgs)e;
            id = de.id.ToString();
            doh.Reset();
            doh.ConditionExpress = "1=1";
            int pId = doh.MaxValue("jdls_organization", "pId");
            doh.Reset();
            doh.ConditionExpress = "id=@id";
            doh.AddConditionParameter("@id", id);
            doh.AddFieldItem("pId", pId + 1);
            doh.AddFieldItem("IsNav", 0);
            doh.Update("jdls_organization");
            FinalMessage("机构成功保存", "close.htm", 0);
        }
        protected void save_ok(object sender, EventArgs e)
        {
            FinalMessage("机构成功保存", "close.htm", 0);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
        }
    }
}
