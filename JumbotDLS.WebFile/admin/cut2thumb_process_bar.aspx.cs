﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using JumbotDLS.Utils;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _cut2thumb_process_bar : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ccid"));
            Admin_Load("", "html", true);
            if (!Page.IsPostBack)
            {
                string TempPhoto = q("tphoto").Replace(site.Url, "");
                string ToWidth = q("tow");
                string ToHeight = q("toh");
                this.w.Text = this.tow.Value = ToWidth;
                this.h.Text = this.toh.Value = ToHeight;
                this.PhotoUrl.Value = TempPhoto;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int tow, toh, x, y, w, h;
            string file;
            tow = Convert.ToInt16(this.tow.Value.ToString());
            toh = Convert.ToInt16(this.toh.Value.ToString());
            x = Convert.ToInt16(this.x.Text);
            y = Convert.ToInt16(this.y.Text);
            w = Convert.ToInt16(this.w.Text);
            h = Convert.ToInt16(this.h.Text);

            file = Server.MapPath(this.PhotoUrl.Value.ToString());
            string fileExtension = "." + JumbotDLS.Utils.DirFile.GetFileExt(this.PhotoUrl.Value.ToString());//缩略图后缀名
            string DirectoryPath = ChannelUploadPath + DateTime.Now.ToString("yyMMdd");
            JumbotDLS.Utils.DirFile.CreateDir(DirectoryPath);

            string sFileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + "_thumb" + fileExtension;  // 文件名称
            string thumbnailPath = Server.MapPath(DirectoryPath + "/" + sFileName);        // 服务器端文件路径

            JumbotDLS.Utils.ImageHelp.MakeMyThumbs(file, thumbnailPath, tow, toh, x, y, w, h);
            WriteJs("-1", "parent.parent.FillPhoto('" + DirectoryPath + "/" + sFileName + "');parent.parent.JumbotDLS.Popup.hide();");
        }

    }
}
