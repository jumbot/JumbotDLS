﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using JumbotDLS.Common;
namespace JumbotDLS.WebFile.Admin
{
    public partial class _article_CollFilters_edit0 : JumbotDLS.UI.AdminCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string FilterType = "0";
            ChannelId = Str2Str(q("ccid"));
            id = Str2Str(q("id"));
            Admin_Load(ChannelId + "-01", "stop", true);
            bool isItem = true;
            if (!Page.IsPostBack)
            {
                doh.Reset();
                doh.SqlCmd = "SELECT Id,Title FROM [jdls_module_article_collitem] WHERE [ChannelId]=" + ChannelId + " ORDER BY id asc";
                DataTable dtCollItem = doh.GetDataTable();
                if (dtCollItem.Rows.Count < 1)
                    isItem = false;
                this.ddlItemName.DataSource = dtCollItem;
                this.ddlItemName.DataTextField = "Title";
                this.ddlItemName.DataValueField = "Id";
                this.ddlItemName.DataBind();
                dtCollItem.Clear();
                dtCollItem.Dispose();
            }
            if (!isItem)
            {
                FinalMessage("参数错误", site.Dir + "admin/close.htm", 0);
                this.btnSave.Enabled = false;
                return;
            }
            JumbotDLS.DBUtility.WebFormHandler wh = new JumbotDLS.DBUtility.WebFormHandler(doh, "jdls_module_article_collfilters", btnSave);
            wh.AddBind(txtTitle, "Title", true);
            wh.AddBind(ddlItemName, "ItemId", false);
            wh.AddBind(rblPublic, "SelectedValue", "PublicTf", false);
            wh.AddBind(rblFlag, "SelectedValue", "Flag", false);
            wh.AddBind(rblObject, "SelectedValue", "Filter_Object", false);
            wh.AddBind(ref FilterType, "Filter_Type", false);
            wh.AddBind(txtContent, "Filter_Content", true);
            wh.AddBind(txtRep, "Filter_Rep", true);
            wh.AddBind(ref ChannelId, "ChannelId", false);
            if (id == "0")
            {
                wh.Mode = JumbotDLS.DBUtility.OperationType.Add;
            }
            else
            {
                wh.ConditionExpress = "id=" + id;
                wh.Mode = JumbotDLS.DBUtility.OperationType.Modify;
            }
            wh.BindBeforeModifyOk += new EventHandler(bind_ok);
            wh.AddOk += new EventHandler(save_ok);
            wh.ModifyOk += new EventHandler(save_ok);
            wh.validator = chkForm;
        }
        /// <summary>
        /// 绑定数据后的处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bind_ok(object sender, EventArgs e)
        {
        }
        protected bool chkForm()
        {
            if (!CheckFormUrl())
                return false;
            if (!Page.IsValid)
                return false;
            return true;
        }
        protected void save_ok(object sender, EventArgs e)
        {
            if (id == "0")
            {
                JumbotDLS.DBUtility.DbOperEventArgs de = (JumbotDLS.DBUtility.DbOperEventArgs)e;
                id = de.id.ToString();
            }
            FinalMessage("成功保存", site.Dir + "admin/close.htm", 0);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
        }
    }
}
