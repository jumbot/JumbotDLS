﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
namespace JumbotDLS.WebFile.Digg
{
    public partial class _index : JumbotDLS.UI.FrontHtml
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!CheckFormUrl())
            {
                Response.End();
            }
            Server.ScriptTimeout = 8;//脚本过期时间
            GetDiggHTML();
            Response.Write(this._response);
        }
        private void GetDiggHTML()
        {
            if (!ModuleIsOK(q("cType")))
            {
                this._response = "";
                return;
            }
            string ChannelType = q("cType");
            string _TemplateContent = JumbotDLS.Utils.DirFile.ReadFile("~/themes/_p_digg.htm");
            JumbotDLS.TEngine.TemplateManager manager = JumbotDLS.TEngine.TemplateManager.FromString(_TemplateContent);
            JumbotDLS.Entity.Normal_Digg digg = new JumbotDLS.DAL.Normal_DiggDAL().GetDigg(ChannelType, Str2Str(q("id")));
            manager.SetValue("digg", digg);
            manager.SetValue("site", site);
            string _content = manager.Process();
            this._response = JumbotDLS.Utils.Strings.Html2Js(_content);
        }

    }
}
