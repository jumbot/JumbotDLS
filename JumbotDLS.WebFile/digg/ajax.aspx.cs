﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
namespace JumbotDLS.WebFile.Digg
{
    public partial class _ajax : JumbotDLS.UI.FrontAjax
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 8;//脚本过期时间
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxDiggAdd":
                    ajaxDiggAdd();
                    break;
                default:
                    DefaultResponse();
                    break;
            }

            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxDiggAdd()
        {
            if (!ModuleIsOK(q("cType")))
            {
                this._response = JsonResult(0, "error");
                return;
            }
            string _channeltype = q("cType");
            string _contentid = Str2Str(q("id"));
            int _diggnum = 0;
            if (JumbotDLS.Utils.Cookie.GetValue(_channeltype + "DiggNum" + _contentid) == null)
            {
                doh.Reset();
                doh.ConditionExpress = "channeltype=@channeltype and contentid=@contentid";
                doh.AddConditionParameter("@channeltype", _channeltype);
                doh.AddConditionParameter("@contentid", _contentid);
                _diggnum = doh.Add("jdls_normal_digg", "DiggNum");
                JumbotDLS.Utils.Cookie.SetObj(_channeltype + "DiggNum" + _contentid, "ok");
            }
            else
            {
                doh.Reset();
                doh.ConditionExpress = "channeltype=@channeltype and contentid=@contentid";
                doh.AddConditionParameter("@channeltype", _channeltype);
                doh.AddConditionParameter("@contentid", _contentid);
                _diggnum = Str2Int(doh.GetField("jdls_normal_digg", "DiggNum").ToString(), 0);
            }
            this._response = JsonResult(1, _diggnum.ToString());
        }
    }
}
