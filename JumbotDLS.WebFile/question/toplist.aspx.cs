﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
namespace JumbotDLS.WebFile.Question
{
    public partial class _toplist : JumbotDLS.UI.FrontHtml
    {
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            int TopNum = (Str2Int(q("top"), 0) < 1 || Str2Int(q("top"), 0) > 20) ? 10 : Str2Int(q("top"), 0);
            int PSize = Str2Int(JumbotDLS.Utils.XmlCOM.ReadConfig(site.Dir + "_data/config/question", "PageSize"), 10);
            string classid = Str2Str(q("classid"));
            string ResponseStr = new JumbotDLS.DAL.Normal_QuestionDAL().GetTopList(TopNum, PSize, classid);
            if (q("act") == "ajax")
                Response.Write(ResponseStr);
            else
                Response.Write(JumbotDLS.Utils.Strings.Html2Js(ResponseStr));
        }
    }
}
