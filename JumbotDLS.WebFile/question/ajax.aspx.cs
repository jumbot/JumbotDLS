﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
using JumbotDLS.Common;
using JumbotDLS.Utils;
namespace JumbotDLS.WebFile.Question
{
    public partial class _ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!CheckFormUrl())
            {
                Response.End();
            }
            Server.ScriptTimeout = 8;//脚本过期时间
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxQuestionAdd":
                    ajaxQuestionAdd();
                    break;
                case "ajaxQuestionList":
                    ajaxQuestionList();
                    break;
                case "ajaxQuestionReply":
					ajaxQuestionReply();
                    break;
				case "ajaxQuestionDelete":
					ajaxQuestionDelete();
                    break;
                default:
                    DefaultResponse();
                    break;
            }

            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxQuestionAdd()
        {
            string _userid, _username, _parentid="0";
            string _code = f("code");
            int _GuestPost = Str2Int(JumbotDLS.Utils.XmlCOM.ReadConfig(site.Dir + "_data/config/question", "GuestPost"), 0);
            int _NeedCheck = Str2Int(JumbotDLS.Utils.XmlCOM.ReadConfig(site.Dir + "_data/config/question", "NeedCheck"), 0);
            int _PostTimer = Str2Int(JumbotDLS.Utils.XmlCOM.ReadConfig(site.Dir + "_data/config/question", "PostTimer"), 0);
            int _State;
            string _realcode = "";
            if (!JumbotDLS.Common.ValidateCode.CheckValidateCode(_code, ref _realcode))
            {
                this._response = JsonResult(0, "验证码应该是" + _realcode);
                return;
            }
                #region  判断时间周期
                int totalCount = 0;
                string whereStr = "[ParentId]=0 and ip=@ip";
                if (DBType == "0")
                    whereStr += " and datediff('s',adddate,'" + DateTime.Now.ToString() + "')<" + _PostTimer;
                else
                    whereStr += " and datediff(s,adddate,'" + DateTime.Now.ToString() + "')<" + _PostTimer;
                doh.Reset();
                doh.ConditionExpress = whereStr;
                doh.AddConditionParameter("@ip", Const.GetUserIp);
                totalCount = doh.Count("jdls_normal_question");
                if (totalCount > 0)//说明周期内留过言
                {
                    this._response = JsonResult(0, _PostTimer + "秒内只能留言一次!");
                    return;
                }
                #endregion
                if (Cookie.GetValue(site.CookiePrev + "user") != null)
                {
                    _userid = Str2Str(Cookie.GetValue(site.CookiePrev + "user", "id"));
                    _username = Cookie.GetValue(site.CookiePrev + "user", "name");
                    _State = (_NeedCheck == 0) ? 1 : 0;
                }
                else
                {
                    if (_GuestPost == 0)//游客不允许留言
                    {
                        this._response = JsonResult(0, "请登录后再进行留言");
                        return;
                    }
                    _userid = "0";
                    _username = JumbotDLS.Utils.Strings.HtmlEncode(f("name"));
                    _State = (_NeedCheck == 0) ? 1 : 0;
                }
            doh.Reset();
            doh.AddFieldItem("ParentId", _parentid);
            doh.AddFieldItem("AddDate", DateTime.Now.ToString());
            doh.AddFieldItem("Title", JumbotDLS.Utils.Strings.Left(JumbotDLS.Utils.Strings.HtmlEncode(f("title")),20));
            doh.AddFieldItem("Content", GetCutString(JumbotDLS.Utils.Strings.HtmlEncode(f("content")), 200));
            doh.AddFieldItem("IP", Const.GetUserIp);
            doh.AddFieldItem("UserId", _userid);
            doh.AddFieldItem("UserName", _username);
            doh.AddFieldItem("Email", f("email"));
            doh.AddFieldItem("Phone", f("phone"));
            doh.AddFieldItem("IsPass", _State);
            doh.AddFieldItem("ClassId", f("classid"));
            doh.AddFieldItem("ContentId", f("contentid"));
            doh.Insert("jdls_normal_question");
            this._response = JsonResult(1, "发表成功，请等待管理员审核");
        }
        private void ajaxQuestionList()
        {
            int PSize = Str2Int(JumbotDLS.Utils.XmlCOM.ReadConfig(site.Dir + "_data/config/question", "PageSize"), 10);
            int page = Str2Int(q("page"), 1);
            string classid = Str2Str(q("classid"));
            string contentid = Str2Str(q("contentid"));
            int totalCount = 0;
            string sqlStr = "";
            string whereStr = "[ParentId]=0 AND [ClassId]=" + classid+" AND [ContentId]=" + contentid;//分页条件(不带A.)
            doh.Reset();
            doh.ConditionExpress = whereStr;
            totalCount = doh.Count("jdls_normal_question");
            sqlStr = JumbotDLS.Utils.SqlHelp.GetSql0("*", "jdls_normal_question", "Id", PSize, page, "desc",whereStr);
            doh.Reset();
            doh.SqlCmd = sqlStr;
            DataTable dt = doh.GetDataTable();
            this._response = "{\"result\" :\"1\"," +
                "\"returnval\" :\"操作成功\"," +
                "\"totalcount\" :\""+totalCount+"\"," +
                "\"pagebar\" :\"" + JumbotDLS.Utils.PageBar.GetPageBar(4, "js", 2, totalCount, PSize, page, "javascript:ajaxQuestionList($#page#$);") + "\"," +
                JumbotDLS.Utils.dtHelper.DT2JSON(dt, (PSize * (page - 1))) +
                "}";
            dt.Clear();
            dt.Dispose();
        }
        private void ajaxQuestionDelete()
        {
            string _contentid = Str2Str(f("contentid"));
            string _questionid = Str2Str(f("questionid"));
            if (Cookie.GetValue(this.site.CookiePrev + "user") == null)
            {
                this._response = JsonResult(0, "请登录后再删除");
                return;
            }
            string _username = Cookie.GetValue(this.site.CookiePrev + "user", "name");
            doh.Reset();
            doh.ConditionExpress = "username=@username and id=@contentid and ispass=1";
            doh.AddConditionParameter("@username", _username);
            doh.AddConditionParameter("@contentid", _contentid);
            if (!doh.Exist("jdls_module_article"))
            {
                this._response = JsonResult(0, "权限好像不对吧？");
                return;
            }
            doh.Reset();
            doh.ConditionExpress = "id=" + _questionid;
            if (doh.Delete("jdls_normal_question") == 1)
            {
                this._response = JsonResult(1, "删除成功");
                return;
            }
            this._response = JsonResult(0, "删除失败");
        }
        private void ajaxQuestionReply()
        {
            string _contentid = Str2Str(f("contentid"));
            string _questionid = Str2Str(f("questionid"));
            string _replycontent = f("replycontent");
            if (Cookie.GetValue(this.site.CookiePrev + "user") == null)
            {
                this._response = JsonResult(0, "请登录后再进行回复留言");
                return;
            }
            string _username = Cookie.GetValue(this.site.CookiePrev + "user", "name");
            doh.Reset();
            doh.ConditionExpress = "username=@username and id=@contentid and ispass=1";
            doh.AddConditionParameter("@username", _username);
            doh.AddConditionParameter("@contentid", _contentid);
            if (!doh.Exist("jdls_module_article"))
            {
                this._response = JsonResult(0, "权限好像不对吧？");
                return;
            }
            doh.Reset();
            doh.ConditionExpress = "id=@questionid and contentid=@contentid";
            doh.AddConditionParameter("@questionid", _questionid);
            doh.AddConditionParameter("@contentid", _contentid);
            doh.AddFieldItem("ispass", 1);
            doh.AddFieldItem("ReplyContent", _replycontent);
            doh.AddFieldItem("ReplyTime", DateTime.Now.ToString());
            if (doh.Update("jdls_normal_question") == 1)
            {
                this._response = JsonResult(1, "回复成功");
                return;
            }
            this._response = JsonResult(0, "回复失败");
        }
    }
}
