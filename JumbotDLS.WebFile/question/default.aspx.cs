﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
namespace JumbotDLS.WebFile.Question
{
    public partial class _default : JumbotDLS.UI.FrontHtml
    {
        protected void Page_Unload(object sender, EventArgs e)
        {
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string PageStr = JumbotDLS.Utils.DirFile.ReadFile("~/themes/system_question_index.htm");
            ReplaceSiteTags(ref PageStr);
            Response.Write(PageStr);//直接输出
        }
    }
}
