﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using JumbotDLS.Utils;
namespace JumbotDLS.WebFile.Extends.Vote
{
    public partial class _ajax : JumbotDLS.UI.AdminCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!CheckFormUrl())
            {
                Response.End();
            }
            Server.ScriptTimeout = 8;//脚本过期时间
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxPluginVoteAdd":
                    ajaxPluginVoteAdd();
                    break;
                default:
                    DefaultResponse();
                    break;
            }

            Response.Write(this._response);
        }

        private void DefaultResponse()
        {
            this._response = JsonResult(0, "未知操作");
        }
        private void ajaxPluginVoteAdd()
        {
            string _voteid = Str2Str(q("id"));
            if (JumbotDLS.Utils.Cookie.GetValue("Vote" + _voteid) != null)
                this._response = "请不要重复投票!";
            else
            {
                string[] voteResult = q("vote").Split(',');
                doh.Reset();
                doh.SqlCmd = "SELECT Title,VoteText,VoteNum,VoteTotal FROM [jdls_extends_vote] WHERE [Id]=" + _voteid + " And [Lock]=0";
                DataTable dtVote = doh.GetDataTable();
                if (dtVote.Rows.Count > 0)
                {
                    string[] voteText = dtVote.Rows[0]["VoteText"].ToString().Split('|');
                    string[] voteNum = dtVote.Rows[0]["VoteNum"].ToString().Split('|');
                    string[] userVote = new string[voteText.Length];
                    string res = "";
                    for (int i = 0; i < voteResult.Length; i++)
                    {
                        voteNum[Str2Int(voteResult[i]) - 1] = (Str2Int(voteNum[Str2Int(voteResult[i]) - 1]) + 1).ToString();
                    }
                    for (int i = 0; i < voteText.Length; i++)
                    {
                        res += "|" + voteNum[i];
                    }
                    res = res.Substring(1, res.Length - 1);

                    doh.Reset();
                    doh.ConditionExpress = "lock=0 and [Id]=" + _voteid;
                    doh.AddFieldItem("VoteNum", res);
                    doh.Update("jdls_extends_vote");
                    doh.Reset();
                    doh.ConditionExpress = "lock=0 and [Id]=" + _voteid;
                    doh.Add("jdls_extends_vote", "VoteTotal");
                    JumbotDLS.Utils.Cookie.SetObj("Vote" + _voteid, "ok");
                    this._response = "ok";
                }
                else
                    this._response = "数据错误,请稍后重试!";
                dtVote.Clear();
                dtVote.Dispose();
            }
        }
    }
}
