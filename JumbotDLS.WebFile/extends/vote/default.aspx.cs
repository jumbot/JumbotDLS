﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Web;
namespace JumbotDLS.WebFile.Extends.Vote
{
    public partial class _index : JumbotDLS.UI.FrontHtml
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckExtendState("Vote", "html");
            Server.ScriptTimeout = 8;//脚本过期时间
            string ContentStr = LoadPlugin_Vote(Str2Str(q("id")));
            Response.Write(ContentStr);//直接输出
        }
        /// <summary>
        /// 生成链接页
        /// </summary>
        /// <returns></returns>
        private string LoadPlugin_Vote(string id)
        {
            string PageStr = string.Empty;
            PageStr = JumbotDLS.Utils.DirFile.ReadFile("~/themes/extends_vote_index.htm");
            ReplaceSiteTags(ref PageStr);
            doh.Reset();
            doh.SqlCmd = "SELECT [Id],[Title],[VoteText],[VoteNum],[VoteTotal],[Type],[Lock] FROM [jdls_extends_vote] WHERE [Id]=" + id;
            DataTable dtVote = doh.GetDataTable();
            if (dtVote.Rows.Count > 0)
            {
                for (int i = 0; i < dtVote.Columns.Count; i++)
                {
                    PageStr = PageStr.Replace("{$Vote" + dtVote.Columns[i].ColumnName + "}", dtVote.Rows[0][i].ToString());
                }
                PageStr = PageStr.Replace("{$VoteSON}", p__getVoteJSON(id));

            }
            else
                return "参数错误,请不要修改参数提交";
            dtVote.Clear();
            dtVote.Dispose();
            return PageStr;
        }
        /// <summary>
        /// 显示调查信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private string p__getVoteJSON(string id)
        {
            string TempStr = "";
            TempStr = "";
            doh.Reset();
            doh.SqlCmd = "SELECT [Title],[VoteText],[VoteNum],[VoteTotal] FROM [jdls_extends_vote] WHERE [Id]=" + id + " And [Lock]=0";
            DataTable dtVote = doh.GetDataTable();
            if (dtVote.Rows.Count > 0)
            {
                string[] voteText = dtVote.Rows[0]["VoteText"].ToString().Split('|');
                string[] voteNum = dtVote.Rows[0]["VoteNum"].ToString().Split('|');
                int voteTotal = Convert.ToInt32(dtVote.Rows[0]["VoteTotal"].ToString());
                TempStr += "[";
                for (int i = 0; i < voteText.Length; i++)
                {
                    if (i > 0)
                        TempStr += ",";
                    int _persent = (voteTotal > 0) ? (100 * Convert.ToInt32(voteNum[i]) / voteTotal) : 0;
                    TempStr += "{";
                    TempStr += "no:" + (i + 1) + ",";
                    TempStr += "text:'" + voteText[i] + "',";
                    TempStr += "persent:'" + _persent + "',";
                    TempStr += "votenum:'" + voteNum[i] + "'";
                    TempStr += "}";
                }
                TempStr += "]";
            }
            dtVote.Clear();
            dtVote.Dispose();
            return TempStr;

        }
    }
}
