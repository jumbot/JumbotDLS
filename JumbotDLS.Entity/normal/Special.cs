﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 专题-------表映射实体
    /// </summary>
    public class Normal_Specials
    {
        public Normal_Specials()
        { }
        public List<Normal_Special> DT2List(DataTable _dt)
        {
            if (_dt == null) return null;
            return JumbotDLS.Utils.dtHelper.DT2List<Normal_Special>(_dt);
        }
    }
    public class Normal_Special
    {
        public Normal_Special()
        { }
        private string _id;
        private string _title;
        private string _info;
        private string _source;
        /// <summary>
        /// 编号
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 专题标题
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 专题简介
        /// </summary>
        public string Info
        {
            set { _info = value; }
            get { return _info; }
        }
        /// <summary>
        /// 专题文件名
        /// </summary>
        public string Source
        {
            set { _source = value; }
            get { return _source; }
        }


    }
}

