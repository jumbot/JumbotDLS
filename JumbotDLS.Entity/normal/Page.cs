﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 单页内容-------表映射实体
    /// </summary>

    public class Normal_Page
    {
        public Normal_Page()
        { }

        private string _id;
        private string _title;
        private string _source;
        private string _outurl;
        /// <summary>
        /// 编号
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 模板文件名
        /// </summary>
        public string Source
        {
            set { _source = value; }
            get { return _source; }
        }
        /// <summary>
        /// 静态文件路径
        /// </summary>
        public string OutUrl
        {
            set { _outurl = value; }
            get { return _outurl; }
        }

    }
}

