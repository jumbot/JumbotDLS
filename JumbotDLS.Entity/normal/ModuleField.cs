﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
namespace JumbotDLS.Entity
{

    public class ModuleFields
    {
        public ModuleFields()
        { }
        public List<ModuleField> DT2List(DataTable _dt)
        {
            if (_dt == null) return null;
            return JumbotDLS.Utils.dtHelper.DT2List<ModuleField>(_dt);
        }
    }
    public class ModuleField
    {
        public ModuleField()
        { }
        #region Model
        private int _id;
        private int _moduleid = 0;
        private int _pid = 0;
        private string _fieldtitle;
        private string _fieldinfo;
        private string _fieldname;
        private string _fieldtype = "input";
        private string _fieldlength = "half";
        private int _FieldRequired=0;
        private int _FieldIsUpload = 0;
        private string _FieldUploadModule;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ModuleID
        {
            set { _moduleid = value; }
            get { return _moduleid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int PID
        {
            set { _pid = value; }
            get { return _pid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FieldTitle
        {
            set { _fieldtitle = value; }
            get { return _fieldtitle; }
        }
        public string FieldInfo
        {
            set { _fieldinfo = value; }
            get { return _fieldinfo; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FieldName
        {
            set { _fieldname = value; }
            get { return _fieldname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FieldType
        {
            set { _fieldtype = value; }
            get { return _fieldtype; }
        }
        public string FieldLength
        {
            set { _fieldlength = value; }
            get { return _fieldlength; }
        }
        public int FieldRequired
        {
            set { _FieldRequired = value; }
            get { return _FieldRequired; }
        }
        public string FieldUploadModule
        {
            set { _FieldUploadModule = value; }
            get { return _FieldUploadModule; }
        }
        public int FieldIsUpload
        {
            set { _FieldIsUpload = value; }
            get { return _FieldIsUpload; }
        }
        #endregion Model

    }
}

