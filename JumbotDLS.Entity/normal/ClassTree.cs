﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 栏目树实体
    /// </summary>
    public class Normal_ClassTree
    {
        public Normal_ClassTree()
        { }
        private string _id;
        private string _parentid;
        private string _name = string.Empty;
        private string _link = string.Empty;
        private string _codeleft4 = string.Empty;
        private string _rssurl = string.Empty;
        private bool _haschild = false;
        private List<Normal_ClassTree> _subchild;
        /// <summary>
        /// 栏目编号
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        public string ParentId
        {
            set { _parentid = value; }
            get { return _parentid; }
        }
        /// <summary>
        /// 栏目名称
        /// </summary>
        public string Name
        {
            set { _name = value; }
            get { return _name; }
        }
        /// <summary>
        /// 栏目链接
        /// </summary>
        public string Link
        {
            set { _link = value; }
            get { return _link; }
        }
        /// <summary>
        /// 前4位code
        /// </summary>
        public string CodeLeft4
        {
            set { _codeleft4 = value; }
            get { return _codeleft4; }
        }
        /// <summary>
        /// RSS地址
        /// </summary>
        public string RssUrl
        {
            set { _rssurl = value; }
            get { return _rssurl; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool HasChild
        {
            set { _haschild = value; }
            get { return _haschild; }
        }
        public List<Normal_ClassTree> SubChild
        {
            set { _subchild = value; }
            get { return _subchild; }
        }
    }
}

