﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 外站调用-------表映射实体
    /// </summary>

    public class Normal_Javascript
    {
        public Normal_Javascript()
        { }

        private string _id;
        private string _title;
        private string _code;
        private int _channelid;
        private int _classid;
        private int _selectnumber;
        private int _titlelen;
        private string _jumbotlicode;
        /// <summary>
        /// 
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Code
        {
            set { _code = value; }
            get { return _code; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ChannelId
        {
            set { _channelid = value; }
            get { return _channelid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ClassId
        {
            set { _classid = value; }
            get { return _classid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int SelectNumber
        {
            set { _selectnumber = value; }
            get { return _selectnumber; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int TitleLen
        {
            set { _titlelen = value; }
            get { return _titlelen; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string JumbotLiCode
        {
            set { _jumbotlicode = value; }
            get { return _jumbotlicode; }
        }


    }
}

