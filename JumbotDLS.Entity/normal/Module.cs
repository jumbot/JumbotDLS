﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
namespace JumbotDLS.Entity
{
    public class Modules
    {
        public Modules()
        { }
        public List<Module> DT2List(DataTable _dt)
        {
            if (_dt == null) return null;
            return JumbotDLS.Utils.dtHelper.DT2List<Module>(_dt);
        }
    }
    /// <summary>
    /// 模型-------表映射实体
    /// </summary>

    public class Module
    {
        public Module()
        { }

        private string _id;
        private string _title;
        private string _type;
        private int _pid;
        private int _enabled;
        private int _locked;
        private string _searchfieldvalues;
        private string _searchfieldtexts;
        /// <summary>
        /// 
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Type
        {
            set { _type = value; }
            get { return _type; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int PId
        {
            set { _pid = value; }
            get { return _pid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Enabled
        {
            set { _enabled = value; }
            get { return _enabled; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Locked
        {
            set { _locked = value; }
            get { return _locked; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string SearchFieldValues
        {
            set { _searchfieldvalues = value; }
            get { return _searchfieldvalues; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string SearchFieldTexts
        {
            set { _searchfieldtexts = value; }
            get { return _searchfieldtexts; }
        }


    }
}

