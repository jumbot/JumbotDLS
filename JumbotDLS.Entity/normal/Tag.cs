﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 标签-------表映射实体
    /// </summary>
    public class Normal_Tags
    {
        public Normal_Tags()
        { }
        public List<Normal_Tag> DT2List(DataTable _dt)
        {
            if (_dt == null) return null;
            return JumbotDLS.Utils.dtHelper.DT2List<Normal_Tag>(_dt);
        }
    }
    public class Normal_Tag
    {
        public Normal_Tag()
        { }

        private string _id;
        private int _channelid;
        private string _title;
        private int _clicktimes;
        /// <summary>
        /// 编号
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 频道ID
        /// </summary>
        public int ChannelId
        {
            set { _channelid = value; }
            get { return _channelid; }
        }
        /// <summary>
        /// 标签名
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 点击数
        /// </summary>
        public int ClickTimes
        {
            set { _clicktimes = value; }
            get { return _clicktimes; }
        }


    }
}

