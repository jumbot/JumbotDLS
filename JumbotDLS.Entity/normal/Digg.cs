﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 顶客-------表映射实体
    /// </summary>

    public class Normal_Digg
    {
        public Normal_Digg()
        { }

        private string _id;
        private int _contentid;
        private string _channeltype;
        private int _diggnum;
        /// <summary>
        /// 
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ContentId
        {
            set { _contentid = value; }
            get { return _contentid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChannelType
        {
            set { _channeltype = value; }
            get { return _channeltype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int DiggNum
        {
            set { _diggnum = value; }
            get { return _diggnum; }
        }


    }
}

