﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 缩略图尺寸-------表映射实体
    /// </summary>

    public class Normal_Thumbs
    {
        public Normal_Thumbs()
        { }

        private string _id;
        private int _channelid;
        private string _title;
        private int _iwidth;
        private int _iheight;
        /// <summary>
        /// 
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ChannelId
        {
            set { _channelid = value; }
            get { return _channelid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int iWidth
        {
            set { _iwidth = value; }
            get { return _iwidth; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int iHeight
        {
            set { _iheight = value; }
            get { return _iheight; }
        }


    }
}

