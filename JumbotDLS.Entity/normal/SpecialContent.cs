﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 专题内容-------表映射实体
    /// </summary>

    public class Normal_SpecialContent
    {
        public Normal_SpecialContent()
        { }

        private string _id;
        private string _title;
        private int _sid;
        private int _channelid;
        private int _contentid;
        /// <summary>
        /// 编号
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 内容标题
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 所属专题ID
        /// </summary>
        public int sId
        {
            set { _sid = value; }
            get { return _sid; }
        }
        /// <summary>
        /// 所属频道ID
        /// </summary>
        public int ChannelId
        {
            set { _channelid = value; }
            get { return _channelid; }
        }
        /// <summary>
        /// 指向内容的ID
        /// </summary>
        public int ContentId
        {
            set { _contentid = value; }
            get { return _contentid; }
        }


    }
}

