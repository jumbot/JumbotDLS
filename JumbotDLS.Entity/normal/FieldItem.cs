﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 字段选择项-------表映射实体
    /// </summary>
    public class Normal_FieldItems
    {
        public Normal_FieldItems()
        { }
        public List<Normal_FieldItem> DT2List(DataTable _dt)
        {
            if (_dt == null) return null;
            return JumbotDLS.Utils.dtHelper.DT2List<Normal_FieldItem>(_dt);
        }
    }
    public class Normal_FieldItem
    {
        public Normal_FieldItem()
        { }
        private string _id;
        private string _itemtitle;
        private string _itemvalue;
        private string _count;
        /// <summary>
        /// 编号
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 文字
        /// </summary>
        public string ItemTitle
        {
            set { _itemtitle = value; }
            get { return _itemtitle; }
        }
      
        /// <summary>
        /// 值
        /// </summary>
        public string ItemValue
        {
            set { _itemvalue = value; }
            get { return _itemvalue; }
        }
        public string Count
        {
            set { _count = value; }
            get { return _count; }
        }
    }
}

