﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 会员组-------表映射实体
    /// </summary>

    public class Normal_UserGroup
    {
        public Normal_UserGroup()
        { }

        private string _id;
        private string _groupname;
        private string _setting;
        private int _islogin;
        private int _usertotal;
        /// <summary>
        /// 
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string GroupName
        {
            set { _groupname = value; }
            get { return _groupname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Setting
        {
            set { _setting = value; }
            get { return _setting; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int IsLogin
        {
            set { _islogin = value; }
            get { return _islogin; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int UserTotal
        {
            set { _usertotal = value; }
            get { return _usertotal; }
        }


    }
}

