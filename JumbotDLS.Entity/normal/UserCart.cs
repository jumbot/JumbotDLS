﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 会员购物车-------表映射实体
    /// </summary>

    public class Normal_UserCart
    {
        public Normal_UserCart()
        { }

        private int _id;
        private int _productid = 0;
        private string _productlink = "";
        private int _buycount = 0;
        private DateTime _carttime = DateTime.Now;
        private int _state = 0;
        private int _userid = 0;
        /// <summary>
        /// 
        /// </summary>
        public int Id
        {
            set { _id = value; }
            get { return _id; }
        }
        public int ProductId
        {
            set { _productid = value; }
            get { return _productid; }
        }
        /// <summary>
        /// 产品链接
        /// </summary>
        public string ProductLink
        {
            set { _productlink = value; }
            get { return _productlink; }
        }
        /// <summary>
        /// 订购数量
        /// </summary>
        public int BuyCount
        {
            set { _buycount = value; }
            get { return _buycount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CartTime
        {
            set { _carttime = value; }
            get { return _carttime; }
        }
        /// <summary>
        /// 状态
        /// 0表示未未处理；1表示已处理
        /// </summary>
        public int State
        {
            set { _state = value; }
            get { return _state; }
        }
        /// <summary>
        /// 会员编号
        /// </summary>
        public int UserId
        {
            set { _userid = value; }
            get { return _userid; }
        }

    }
}

