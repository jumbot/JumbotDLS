﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 非法IP-------表映射实体
    /// </summary>

    public class Normal_WhiteIP
    {
        public Normal_WhiteIP()
        { }

        private string _id;
        private long _startip;
        private string _startip2;
        private long _endip;
        private string _endip2;
        private int _enabled;
        /// <summary>
        /// 编号
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 起始IP，已换算成long型
        /// </summary>
        public long StartIP
        {
            set { _startip = value; }
            get { return _startip; }
        }
        /// <summary>
        /// 起始IP，如192.168.1.1
        /// </summary>
        public string StartIP2
        {
            set { _startip2 = value; }
            get { return _startip2; }
        }
        /// <summary>
        /// 截止IP，已换算成long型
        /// </summary>
        public long EndIP
        {
            set { _endip = value; }
            get { return _endip; }
        }
        /// <summary>
        /// 截止IP，如192.168.1.100
        /// </summary>
        public string EndIP2
        {
            set { _endip2 = value; }
            get { return _endip2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Enabled
        {
            set { _enabled = value; }
            get { return _enabled; }
        }


    }
}

