﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 语言包实体（主要用以解析程序自动生成内容中的相关信息）
    /// </summary>

    public class Language
    {
        public Language()
        { }

        private string _home;
        private string _more;
        /// <summary>
        /// 首页
        /// </summary>
        public string Home
        {
            set { _home = value; }
            get { return _home; }
        }
        /// <summary>
        /// 更多
        /// </summary>
        public string More
        {
            set { _more = value; }
            get { return _more; }
        }
    }
}

