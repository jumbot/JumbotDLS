﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
namespace JumbotDLS.Entity
{
    /// <summary>
    ///  机构-------表映射实体
    /// </summary>

    public class Normal_Organization
    {
        public Normal_Organization()
        { }

        private string _id = "0";
        private string _title = string.Empty;
        private string _info = string.Empty;
        private string _logo = string.Empty;
        private string _erwei = string.Empty;
        private string _address = string.Empty;
        private string _zipcode = string.Empty;
        private string _phone = string.Empty;
        private string _email = string.Empty;
        private int _pid = 0;
        private int _templateid = 0;
        private bool _enabled = false;
        private int _totalcount = 0;
        private int _usedcount = 0;
        private DateTime _begintime;
        private DateTime _endtime;
        /// <summary>
        /// 编号
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 频道名称
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 频道简介
        /// </summary>
        public string Info
        {
            set { _info = value; }
            get { return _info; }
        }
        public string Logo
        {
            set { _logo = value; }
            get { return _logo; }
        }
        public string ErWei
        {
            set { _erwei = value; }
            get { return _erwei; }
        }
        public string Address
        {
            set { _address = value; }
            get { return _address; }
        }
        public string ZipCode
        {
            set { _zipcode = value; }
            get { return _zipcode; }
        }
        public string Phone
        {
            set { _phone = value; }
            get { return _phone; }
        }
        public string Email
        {
            set { _email = value; }
            get { return _email; }
        }
        /// <summary>
        /// 权值
        /// </summary>
        public int pId
        {
            set { _pid = value; }
            get { return _pid; }
        }
        /// <summary>
        /// 模板ID
        /// </summary>
        public int TemplateId
        {
            set { _templateid = value; }
            get { return _templateid; }
        }
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool Enabled
        {
            set { _enabled = value; }
            get { return _enabled; }
        }
        /// <summary>
        /// 总共可检测的篇数
        /// </summary>
        public int TotalCount
        {
            set { _totalcount = value; }
            get { return _totalcount; }
        }
        /// <summary>
        /// 已经消耗的篇数
        /// </summary>
        public int UsedCount
        {
            set { _usedcount = value; }
            get { return _usedcount; }
        }
        private int _defaultyybao;
        public int DefaultYYbao
        {
            set { _defaultyybao = value; }
            get { return _defaultyybao; }
        }
        public DateTime BeginTime
        {
            set { _begintime = value; }
            get { return _begintime; }
        }
        public DateTime EndTime
        {
            set { _endtime = value; }
            get { return _endtime; }
        }
    }
}

