﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 发信日志-------表映射实体
    /// </summary>

    public class Email_Sendlogs
    {
        public Email_Sendlogs()
        { }

        private string _id;
        private int _adminid;
        private string _sendtitle;
        private string _sendusers;
        private DateTime _sendtime;
        private string _sendip;
        /// <summary>
        /// 
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int AdminId
        {
            set { _adminid = value; }
            get { return _adminid; }
        }
        /// <summary>
        /// 发信标题
        /// </summary>
        public string SendTitle
        {
            set { _sendtitle = value; }
            get { return _sendtitle; }
        }
        /// <summary>
        /// 发信收件人
        /// </summary>
        public string SendUsers
        {
            set { _sendusers = value; }
            get { return _sendusers; }
        }
        /// <summary>
        /// 发信时间
        /// </summary>
        public DateTime SendTime
        {
            set { _sendtime = value; }
            get { return _sendtime; }
        }
        /// <summary>
        /// 发信IP
        /// </summary>
        public string SendIP
        {
            set { _sendip = value; }
            get { return _sendip; }
        }


    }
}

