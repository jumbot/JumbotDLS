﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Text;

namespace JumbotDLS.Entity
{
    public class Client
    {   
        public Client()
        { }
        private string _UserId = "0";
        private string _OrgId = "0";
        private string _OrgName = "";
        private string _UserName = "";
        private int _UserGroupId = 0;
        private Double _UserPoint = 0;
        private int _UserIntegral = 0;
        /// <summary>
        /// 
        /// </summary>
        public string UserId
        {
            set { _UserId = value; }
            get { return _UserId; }
        }
        public string OrgId
        {
            set { _OrgId = value; }
            get { return _OrgId; }
        }
        public string OrgName
        {
            set { _OrgName = value; }
            get { return _OrgName; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UserName
        {
            set { _UserName = value; }
            get { return _UserName; }
        }
        public int UserGroupId
        {
            set { _UserGroupId = value; }
            get { return _UserGroupId; }
        }
        /// <summary>
        /// 账户余额
        /// </summary>
        public Double UserPoint
        {
            set { _UserPoint = value; }
            get { return _UserPoint; }
        }
        /// <summary>
        /// 账户积分
        /// </summary>
        public int UserIntegral
        {
            set { _UserIntegral = value; }
            get { return _UserIntegral; }
        }
    }
}
