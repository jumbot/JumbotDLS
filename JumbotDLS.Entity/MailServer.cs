﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Data;
using System.Collections;
namespace JumbotDLS.Entity
{
    public class MailServer
    {
        private IList m_FromAddresss;
        private IList m_FromNames;
        private IList m_FromPwds;
        private IList m_SmtpHosts;
        private IList m_SmtpPorts;
        private IList m_Useds;
        public MailServer()
        {
        }
        /// <summary>
        /// 发件人地址
        /// </summary>
        public IList FromAddresss
        {
            get { return m_FromAddresss; }
            set { m_FromAddresss = value; }
        }
        /// <summary>
        /// 发件人称呼
        /// </summary>
        public IList FromNames
        {
            get { return m_FromNames; }
            set { m_FromNames = value; }
        }
        /// <summary>
        /// 发件人密码
        /// </summary>
        public IList FromPwds
        {
            get { return m_FromPwds; }
            set { m_FromPwds = value; }
        }
        /// <summary>
        /// 发件服务器smtp
        /// </summary>
        public IList SmtpHosts
        {
            get { return m_SmtpHosts; }
            set { m_SmtpHosts = value; }
        }
        /// <summary>
        /// 发件服务器端口
        /// </summary>
        public IList SmtpPorts
        {
            get { return m_SmtpPorts; }
            set { m_SmtpPorts = value; }
        }
        /// <summary>
        /// 成功发送次数
        /// </summary>
        public IList Useds
        {
            get { return m_Useds; }
            set { m_Useds = value; }
        }
    }
}
