﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 员工
    /// </summary>

    public class Employee
    {
        public Employee()
        { }

        private string _id;
        private string _username;
        private int _employeeid;
        private string _employeename;
        private string _AdminOpenID;
        private string _EmployeeMobile;
        private string _EmployeeAppList;
        private DateTime _lasttime4;
        private string _lastip4;

        /// <summary>
        /// 对应的会员ID
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        public int EmployeeId
        {
            set { _employeeid = value; }
            get { return _employeeid; }
        }
        /// <summary>
        /// 名称
        /// </summary>
        public string EmployeeName
        {
            set { _employeename = value; }
            get { return _employeename; }
        }
        public string AdminOpenID
        {
            set { _AdminOpenID = value; }
            get { return _AdminOpenID; }
        }
        public string EmployeeMobile
        {
            set { _EmployeeMobile = value; }
            get { return _EmployeeMobile; }
        }
        /// <summary>
        /// 管理员权限值，比如:1-1,1-2
        /// </summary>
        public string EmployeeAppList
        {
            set { _EmployeeAppList = value; }
            get { return _EmployeeAppList; }
        }
        /// <summary>
        /// 最后登录时间
        /// </summary>
        public DateTime LastTime4
        {
            set { _lasttime4 = value; }
            get { return _lasttime4; }
        }
        public string LastIP4
        {
            set { _lastip4 = value; }
            get { return _lastip4; }
        }
        private string _LeaveAuditUserID1;
        public string LeaveAuditUserID1
        {
            set { _LeaveAuditUserID1 = value; }
            get { return _LeaveAuditUserID1; }
        }
        private string _LeaveAuditUserName1;
        public string LeaveAuditUserName1
        {
            set { _LeaveAuditUserName1 = value; }
            get { return _LeaveAuditUserName1; }
        }

        //private string _LeaveAuditWeixinID1;
        //public string LeaveAuditWeixinID1
        //{
        //    set { _LeaveAuditWeixinID1 = value; }
        //    get { return _LeaveAuditWeixinID1; }
        //}

        //private string _LeaveAuditMobile1;
        //public string LeaveAuditMobile1
        //{
        //    set { _LeaveAuditMobile1 = value; }
        //    get { return _LeaveAuditMobile1; }
        //}



        private string _LeaveAuditUserID2;
        public string LeaveAuditUserID2
        {
            set { _LeaveAuditUserID2 = value; }
            get { return _LeaveAuditUserID2; }
        }
        private string _LeaveAuditUserName2;
        public string LeaveAuditUserName2
        {
            set { _LeaveAuditUserName2 = value; }
            get { return _LeaveAuditUserName2; }
        }
        //private string _LeaveAuditWeixinID2;
        //public string LeaveAuditWeixinID2
        //{
        //    set { _LeaveAuditWeixinID2 = value; }
        //    get { return _LeaveAuditWeixinID2; }
        //}

        //private string _LeaveAuditMobile2;
        //public string LeaveAuditMobile2
        //{
        //    set { _LeaveAuditMobile2 = value; }
        //    get { return _LeaveAuditMobile2; }
        //}

        private string _LeaveNotifyUserID;
        public string LeaveNotifyUserID
        {
            set { _LeaveNotifyUserID = value; }
            get { return _LeaveNotifyUserID; }
        }
        private string _LeaveNotifyUserName;
        public string LeaveNotifyUserName
        {
            set { _LeaveNotifyUserName = value; }
            get { return _LeaveNotifyUserName; }
        }


        private string _ReimbursementAuditUserID1;
        public string ReimbursementAuditUserID1
        {
            set { _ReimbursementAuditUserID1 = value; }
            get { return _ReimbursementAuditUserID1; }
        }
        private string _ReimbursementAuditUserName1;
        public string ReimbursementAuditUserName1
        {
            set { _ReimbursementAuditUserName1 = value; }
            get { return _ReimbursementAuditUserName1; }
        }
        private string _ReimbursementAuditUserID2;
        public string ReimbursementAuditUserID2
        {
            set { _ReimbursementAuditUserID2 = value; }
            get { return _ReimbursementAuditUserID2; }
        }
        private string _ReimbursementAuditUserName2;
        public string ReimbursementAuditUserName2
        {
            set { _ReimbursementAuditUserName2 = value; }
            get { return _ReimbursementAuditUserName2; }
        }


        private string _ReimbursementNotifyUserID;
        public string ReimbursementNotifyUserID
        {
            set { _ReimbursementNotifyUserID = value; }
            get { return _ReimbursementNotifyUserID; }
        }
        private string _ReimbursementNotifyUserName;
        public string ReimbursementNotifyUserName
        {
            set { _ReimbursementNotifyUserName = value; }
            get { return _ReimbursementNotifyUserName; }
        }
    }
}

