﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 文章实体列表
    /// </summary>
    public class Module_Articles
    {
        public Module_Articles()
        { }
        public List<Module_Article> DT2List(DataTable _dt)
        {
            if (_dt == null) return null;
            return JumbotDLS.Utils.dtHelper.DT2List<Module_Article>(_dt);
        }
    }
    /// <summary>
    /// 文章-------表映射实体
    /// </summary>
    public class Module_Article
    {
        public Module_Article()
        { }

        private string _id;
        private string _channelid;
        private string _channelishtml = "0";
        private string _classid;
        private string _title;
        private string _identifier;
        private string _title2;
        private string _tcolor;
        private DateTime _adddate;
        private string _summary;
        private string _editor;
        private string _author;
        private string _authorunit;
        private string _tags;
        private int _viewnum;
        private int _ispass;
        private int _isimg;
        private string _img;
        private string _focusimg;
        private int _istop;
        private int _isfocus;
        private int _ishead;
        private int _userid;
        private int _readgroup;
        private string _sourcefrom;
        private string _content;
        private string _firstpage;
        private string _aliaspage;
        private string _customfield01;
        private string _customfield02;
        private string _customfield03;
        private string _customfield04;
        private string _customfield05;
        private string _customfield06;
        private string _customfield07;
        private string _customfield08;
        private string _customfield09;
        private string _customfield10;
        private string _customfield11;
        private string _customfield12;
        private string _customfield13;
        private string _customfield14;
        private string _customfield15;
        private string _customfield16;
        private string _customfield17;
        private string _customfield18;
        private string _customfield19;
        private string _customfield20;
        private string _outurl;
        private string _target;
        private string _address;
        private string _email;
        private string _phone;
        private string _website;
        private string _module;
        private string _date;
        private string _fulltexturl;
        private string _source;
        private string _publisher;
        /// <summary>
        /// 编号
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 频道编号
        /// </summary>
        public string ChannelId
        {
            set { _channelid = value; }
            get { return _channelid; }
        }
        public string Module
        {
            set { _module = value; }
            get { return _module; }
        }
        public string ChannelIsHtml
        {
            set { _channelishtml = value; }
            get { return _channelishtml; }
        }
        /// <summary>
        /// 栏目编号
        /// </summary>
        public string ClassId
        {
            set { _classid = value; }
            get { return _classid; }
        }
        /// <summary>
        /// 文章标题
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        public string Identifier
        {
            set { _identifier = value; }
            get { return _identifier; }
        }
        public string Title2
        {
            set { _title2 = value; }
            get { return _title2; }
        }
        /// <summary>
        /// 标题颜色
        /// </summary>
        public string TColor
        {
            set { _tcolor = value; }
            get { return _tcolor; }
        }
        /// <summary>
        /// 录入时间
        /// </summary>
        public DateTime AddDate
        {
            set { _adddate = value; }
            get { return _adddate; }
        }
        /// <summary>
        /// 简介
        /// </summary>
        public string Summary
        {
            set { _summary = value; }
            get { return _summary; }
        }
        /// <summary>
        /// 编辑(默认为录入内容的用户名)
        /// </summary>
        public string Editor
        {
            set { _editor = value; }
            get { return _editor; }
        }
        /// <summary>
        /// 作者
        /// </summary>
        public string Author
        {
            set { _author = value; }
            get { return _author; }
        }
        public string AuthorUnit
        {
            set { _authorunit = value; }
            get { return _authorunit; }
        }
        public string Publisher
        {
            set;
            get;
        }
        public string PublishNO
        {
            set;
            get;
        }
        /// <summary>
        /// 标签
        /// </summary>
        public string Tags
        {
            set { _tags = value; }
            get { return _tags; }
        }
        /// <summary>
        /// 浏览次数
        /// </summary>
        public int ViewNum
        {
            set { _viewnum = value; }
            get { return _viewnum; }
        }
        /// <summary>
        /// 状态(0表示未审,1表示审核)
        /// </summary>
        public int IsPass
        {
            set { _ispass = value; }
            get { return _ispass; }
        }
        /// <summary>
        /// 是否有图片
        /// </summary>
        public int IsImg
        {
            set { _isimg = value; }
            get { return _isimg; }
        }
        /// <summary>
        /// 缩略图
        /// </summary>
        public string Img
        {
            set { _img = value; }
            get { return _img; }
        }
        public string FocusImg
        {
            set { _focusimg = value; }
            get { return _focusimg; }
        }
        /// <summary>
        /// 是否推荐
        /// </summary>
        public int IsTop
        {
            set { _istop = value; }
            get { return _istop; }
        }
        /// <summary>
        /// 是否焦点
        /// </summary>
        public int IsFocus
        {
            set { _isfocus = value; }
            get { return _isfocus; }
        }
        /// <summary>
        /// 是否置顶
        /// </summary>
        public int IsHead
        {
            set { _ishead = value; }
            get { return _ishead; }
        }
        /// <summary>
        /// 投稿者ID(0表示管理员发布)
        /// </summary>
        public int UserId
        {
            set { _userid = value; }
            get { return _userid; }
        }
        /// <summary>
        /// 最低阅读权限
        /// </summary>
        public int ReadGroup
        {
            set { _readgroup = value; }
            get { return _readgroup; }
        }
        /// <summary>
        /// 内容来源/出处
        /// </summary>
        public string SourceFrom
        {
            set { _sourcefrom = value; }
            get { return _sourcefrom; }
        }
        /// <summary>
        /// 文章内容
        /// </summary>
        public string Content
        {
            set { _content = value; }
            get { return _content; }
        }
        /// <summary>
        /// 链接地址
        /// </summary>
        public string FirstPage
        {
            set { _firstpage = value; }
            get { return _firstpage; }
        }
        public string AliasPage
        {
            set { _aliaspage = value; }
            get { return _aliaspage; }
        }
        public string CustomField01
        {
            set { _customfield01 = value; }
            get { return _customfield01; }
        }
        public string CustomField02
        {
            set { _customfield02 = value; }
            get { return _customfield02; }
        }
        public string CustomField03
        {
            set { _customfield03 = value; }
            get { return _customfield03; }
        }
        public string CustomField04
        {
            set { _customfield04 = value; }
            get { return _customfield04; }
        }
        public string CustomField05
        {
            set { _customfield05 = value; }
            get { return _customfield05; }
        }
        public string CustomField06
        {
            set { _customfield06 = value; }
            get { return _customfield06; }
        }
        public string CustomField07
        {
            set { _customfield07 = value; }
            get { return _customfield07; }
        }
        public string CustomField08
        {
            set { _customfield08 = value; }
            get { return _customfield08; }
        }
        public string CustomField09
        {
            set { _customfield09 = value; }
            get { return _customfield09; }
        }
        public string CustomField10
        {
            set { _customfield10 = value; }
            get { return _customfield10; }
        }
        public string CustomField11
        {
            set { _customfield11 = value; }
            get { return _customfield11; }
        }
        public string CustomField12
        {
            set { _customfield12 = value; }
            get { return _customfield12; }
        }
        public string CustomField13
        {
            set { _customfield13 = value; }
            get { return _customfield13; }
        }
        public string CustomField14
        {
            set { _customfield14 = value; }
            get { return _customfield14; }
        }
        public string CustomField15
        {
            set { _customfield15 = value; }
            get { return _customfield15; }
        }
        public string CustomField16
        {
            set { _customfield16 = value; }
            get { return _customfield16; }
        }
        public string CustomField17
        {
            set { _customfield17 = value; }
            get { return _customfield17; }
        }
        public string CustomField18
        {
            set { _customfield18 = value; }
            get { return _customfield18; }
        }
        public string CustomField19
        {
            set { _customfield19 = value; }
            get { return _customfield19; }
        }
        public string CustomField20
        {
            set { _customfield20 = value; }
            get { return _customfield20; }
        }
        public string OutUrl
        {
            set { _outurl = value; }
            get { return _outurl; }
        }
        public string Target
        {
            set { _target = value; }
            get { return _target; }
        }
        public string Address
        {
            set { _address = value; }
            get { return _address; }
        }
        public string Email
        {
            set { _email = value; }
            get { return _email; }
        }
        public string Phone
        {
            set { _phone = value; }
            get { return _phone; }
        }
        public string BaiduMapX
        {
            set;
            get;
        }
        public string BaiduMapY
        {
            set;
            get;
        }
        public string WebSite
        {
            set;
            get;
        }
        public string Date
        {
            set { _date = value; }
            get { return _date; }
        }
        public string FullTextUrl
        {
            set { _fulltexturl = value; }
            get { return _fulltexturl; }
        }
        public string Source
        {
            set { _source = value; }
            get { return _source; }
        }
        public string Country
        {
            set;
            get;
        }
        public string Arms
        {
            set;
            get;
        }
        public string Type
        {
            set;
            get;
        }
    }
}

