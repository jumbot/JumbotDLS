﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 站点信息
    /// </summary>
    public class Site
    {
        public Site()
        { }
        private string m_Name;
        private string m_Name2;
        private string m_Url;
        private string m_Dir;
        private string m_Home;
        private string m_TitleTail;
        private string m_Keywords;
        private string m_Description;
        private bool m_IsHtml = true;
        private bool m_AllowReg;
        private bool m_CheckReg;
        private string m_StaticExt;
        private string m_ICP;
        private string m_SiteID;
        private int m_AdminGroupId = 0;
        private string m_CookieDomain;
        private string m_CookiePath;
        private string m_CookiePrev;
        private string m_CookieKeyCode;
        private string m_MainDomain;
        private bool m_UrlReWriter = true;
        private bool m_ExecuteSql = false;
        private int m_CreatePages = 20;
        private string m_ForumAPIKey;
        private string m_ForumUrl;
        private string m_ForumIP;
        private bool m_ForumAutoRegister;
        //用于调试的key
        private string m_DebugKey;
        private int m_MailOnceCount = 15;
        private int m_MailTimeCycle = 300;
        private string m_MailPrivateKey;
        private bool m_AdminCheckUserState;
        private bool m_MainSite = false;
        private bool m_WanSite = false;
        private string m_Version = System.Configuration.ConfigurationManager.AppSettings["JumbotDLS:Version"];
        private int m_ProductMaxBuyCount = 20;
        private int m_ProductMaxCartCount = 20;
        private int m_ProductMaxOrderCount = 5;
        private bool m_ProductPaymentUsingPoints = true;
        private string m_PassportTheme = "default";
        private int m_SiteDataSize = 10000;
        private int m_SiteStartYear = 2007;
        /// <summary>
        /// 网站全称
        /// </summary>
        public string Name
        {
            set { m_Name = value; }
            get { return m_Name; }
        }
        /// <summary>
        /// 网站简称
        /// </summary>
        public string Name2
        {
            set { m_Name2 = value; }
            get { return m_Name2; }
        }
        /// <summary>
        /// 网站地址
        /// </summary>
        public string Url
        {
            set { m_Url = value; }
            get { return m_Url; }
        }

        /// <summary>
        /// 安装目录
        /// </summary>
        public string Dir
        {
            set { m_Dir = value; }
            get { return m_Dir; }
        }
        /// <summary>
        /// 首页地址
        /// </summary>
        public string Home
        {
            set { m_Home = value; }
            get { return m_Home; }
        }
        /// <summary>
        /// 标题尾巴
        /// </summary>
        public string TitleTail
        {
            set { m_TitleTail = value; }
            get { return m_TitleTail; }
        }
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keywords
        {
            set { m_Keywords = value; }
            get { return m_Keywords; }
        }
        /// <summary>
        /// 网站描述
        /// </summary>
        public string Description
        {
            set { m_Description = value; }
            get { return m_Description; }
        }
        /// <summary>
        /// 是否静态
        /// </summary>
        public bool IsHtml
        {
            set { m_IsHtml = value; }
            get { return m_IsHtml; }
        }
        /// <summary>
        /// 允许注册
        /// </summary>
        public bool AllowReg
        {
            set { m_AllowReg = value; }
            get { return m_AllowReg; }
        }
        /// <summary>
        /// 注册需要审核
        /// </summary>
        public bool CheckReg
        {
            set { m_CheckReg = value; }
            get { return m_CheckReg; }
        }
        /// <summary>
        /// 静态后缀
        /// </summary>
        public string StaticExt
        {
            set { m_StaticExt = value; }
            get { return m_StaticExt; }
        }
        /// <summary>
        /// 备案号
        /// </summary>
        public string ICP
        {
            set { m_ICP = value; }
            get { return m_ICP; }
        }
        /// <summary>
        /// 网站授权号
        /// </summary>
        public string SiteID
        {
            set { m_SiteID = value; }
            get { return m_SiteID; }
        }
        /// <summary>
        /// 管理员组的编号
        /// </summary>
        public int AdminGroupId
        {
            set { m_AdminGroupId = value; }
            get { return m_AdminGroupId; }
        }
        /// <summary>
        /// Cookie作用域
        /// </summary>
        public string CookieDomain
        {
            set { m_CookieDomain = value; }
            get { return m_CookieDomain; }
        }
        /// <summary>
        /// Cookie作用路径
        /// </summary>
        public string CookiePath
        {
            set { m_CookiePath = value; }
            get { return m_CookiePath; }
        }
        /// <summary>
        /// Cookie前缀
        /// </summary>
        public string CookiePrev
        {
            set { m_CookiePrev = value; }
            get { return m_CookiePrev; }
        }
        /// <summary>
        /// Cookie加密密钥
        /// </summary>
        public string CookieKeyCode
        {
            set { m_CookieKeyCode = value; }
            get { return m_CookieKeyCode; }
        }
        public string MainDomain
        {
            set { m_MainDomain = value; }
            get { return m_MainDomain; }
        }
        /// <summary>
        /// 是否启用伪静态
        /// </summary>
        public bool UrlReWriter
        {
            set { m_UrlReWriter = value; }
            get { return m_UrlReWriter; }
        }
        /// <summary>
        /// 可以在线执行SQL
        /// </summary>
        public bool ExecuteSql
        {
            set { m_ExecuteSql = value; }
            get { return m_ExecuteSql; }
        }
        /// <summary>
        /// 默认缓存的页数
        /// </summary>
        public int CreatePages
        {
            set { m_CreatePages = value; }
            get { return m_CreatePages; }
        }
        /// <summary>
        /// 论坛API Key
        /// </summary>
        public string ForumAPIKey
        {
            set { m_ForumAPIKey = value; }
            get { return m_ForumAPIKey; }
        }
        /// <summary>
        /// 论坛链接地址
        /// </summary>
        public string ForumUrl
        {
            set { m_ForumUrl = value; }
            get { return m_ForumUrl; }
        }
        public string ForumIP
        {
            set { m_ForumIP = value; }
            get { return m_ForumIP; }
        }
        public bool ForumAutoRegister
        {
            set { m_ForumAutoRegister = value; }
            get { return m_ForumAutoRegister; }
        }
        /// <summary>
        /// 用于调试的Key
        /// </summary>
        public string DebugKey
        {
            set { m_DebugKey = value; }
            get { return m_DebugKey; }
        }
        /// <summary>
        /// 单次发信的收件人数量
        /// </summary>
        public int MailOnceCount
        {
            set { m_MailOnceCount = value; }
            get { return m_MailOnceCount; }
        }
        /// <summary>
        /// 单个邮箱发信的间隔周期, 单位为秒
        /// </summary>
        public int MailTimeCycle
        {
            set { m_MailTimeCycle = value; }
            get { return m_MailTimeCycle; }
        }
        /// <summary>
        /// 客户端发信私钥
        /// </summary>
        public string MailPrivateKey
        {
            set { m_MailPrivateKey = value; }
            get { return m_MailPrivateKey; }
        }
        /// <summary>
        /// 后台登陆时是否确认前台的登录状况(add:2011-03-07)
        /// </summary>
        public bool AdminCheckUserState
        {
            set { m_AdminCheckUserState = value; }
            get { return m_AdminCheckUserState; }
        }
        /// <summary>
        /// 是否为主站
        /// </summary>
        public bool MainSite
        {
            set { m_MainSite = value; }
            get { return m_MainSite; }
        }
        /// <summary>
        /// 是否为外网网站
        /// </summary>
        public bool WanSite
        {
            set { m_WanSite = value; }
            get { return m_WanSite; }
        }
        public string Version
        {
            set { m_Version = value; }
            get { return m_Version; }
        }
        /// <summary>
        /// 一样商品次最多能购买的件数
        /// </summary>
        public int ProductMaxBuyCount
        {
            set { m_ProductMaxBuyCount = value; }
            get { return m_ProductMaxBuyCount; }
        }
        /// <summary>
        /// 购物车最多能存放的数量
        /// </summary>
        public int ProductMaxCartCount
        {
            set { m_ProductMaxCartCount = value; }
            get { return m_ProductMaxCartCount; }
        }
        /// <summary>
        /// 最大未付款的订单，当订单超过这个数就不允许再进行购买
        /// 主要控制垃圾订单的数量
        /// </summary>
        public int ProductMaxOrderCount
        {
            set { m_ProductMaxOrderCount = value; }
            get { return m_ProductMaxOrderCount; }
        }
        /// <summary>
        /// 1表示使用Points支付，0表示实时充值支付
        /// </summary>
        public bool ProductPaymentUsingPoints
        {
            set { m_ProductPaymentUsingPoints = value; }
            get { return m_ProductPaymentUsingPoints; }
        }

        public string PassportTheme
        {
            set { m_PassportTheme = value; }
            get { return m_PassportTheme; }
        }
        /// <summary>
        /// 整站数据量
        /// </summary>
        public int SiteDataSize
        {
            set { m_SiteDataSize = value; }
            get { return m_SiteDataSize; }
        }
        /// <summary>
        /// 网站运行年份
        /// </summary>
        public int SiteStartYear
        {
            set { m_SiteStartYear = value; }
            get { return m_SiteStartYear; }
        }
        private string m_ComputerCode = "";
        private int m_ValidDate = 20200101;
        private string m_CompanyName = "";
        private string m_ValidateCode = "";
        private bool m_SiteIsGenuine = false;

        public string ComputerCode
        {
            set { m_ComputerCode = value; }
            get { return m_ComputerCode; }
        }
        /// <summary>
        /// 有效日期
        /// </summary>
        public int ValidDate
        {
            set { m_ValidDate = value; }
            get { return m_ValidDate; }
        }
        public string CompanyName
        {
            set { m_CompanyName = value; }
            get { return m_CompanyName; }
        }
        public string ValidateCode
        {
            set { m_ValidateCode = value; }
            get { return m_ValidateCode; }
        }
        /// <summary>
        /// 系统是否正版
        /// </summary>
        public bool SiteIsGenuine
        {
            set { m_SiteIsGenuine = value; }
            get { return m_SiteIsGenuine; }
        }
    }
}
