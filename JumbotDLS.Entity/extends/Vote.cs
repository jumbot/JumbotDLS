﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 投票-------表映射实体
    /// </summary>
    public class Extends_VoteItem
    {
        private string _itemtext;
        private int _itemclicks;
        public Extends_VoteItem()
        { }
        public Extends_VoteItem(string itemtext, int itemclicks)
        {
            this._itemtext = itemtext;
            this._itemclicks = itemclicks;
        }
        public string ItemText
        {
            set { _itemtext = value; }
            get { return _itemtext; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ItemClicks
        {
            set { _itemclicks = value; }
            get { return _itemclicks; }
        }
    }
    public class Extends_Votes
    {
        public Extends_Votes()
        { }
        public List<Extends_Vote> DT2List(DataTable _dt)
        {
            if (_dt == null) return null;
            return JumbotDLS.Utils.dtHelper.DT2List<Extends_Vote>(_dt);
        }
    }
    public class Extends_Vote
    {
        public Extends_Vote()
        { }
        private string _id;
        private string _title;
        private List<Extends_VoteItem> _item;
        private int _votetotal;
        private int _type;
        private int _lock;
        /// <summary>
        /// 
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 
        /// </summary>
        public List<Extends_VoteItem> Item
        {
            set { _item = value; }
            get { return _item; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int VoteTotal
        {
            set { _votetotal = value; }
            get { return _votetotal; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Type
        {
            set { _type = value; }
            get { return _type; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Lock
        {
            set { _lock = value; }
            get { return _lock; }
        }
    }
}

