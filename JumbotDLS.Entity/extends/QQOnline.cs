﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// QQ在线客服-------表映射实体
    /// </summary>

    public class Extends_QQOnline
    {
        public Extends_QQOnline()
        { }
        public Extends_QQOnline(
            string id,
            string qqid,
            string title,
            string tcolor,
            string face
            )
        {
            this._id = id;
            this._qqid = qqid;
            this._title = title;
            this._tcolor = tcolor;
            this._face = face;
        }
        private string _id;
        private string _qqid;
        private string _title;
        private string _tcolor;
        private string _face;
        /// <summary>
        /// 
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string QQID
        {
            set { _qqid = value; }
            get { return _qqid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string TColor
        {
            set { _tcolor = value; }
            get { return _tcolor; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Face
        {
            set { _face = value; }
            get { return _face; }
        }
    }
}

