﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
namespace JumbotDLS.Entity
{
    /// <summary>
    /// 天气-------映射实体
    /// </summary>

    public class Weather
    {
        public Weather()
        { }
        /// <summary>
        /// 城市名称
        /// </summary>
        public string CityName
        {
            set;
            get;
        }
        /// <summary>
        /// 城市编号
        /// </summary>
        public string CityId
        {
            set;
            get;
        }
        /// <summary>
        /// 今天日期
        /// </summary>
        public string Date1
        {
            set;
            get;
        }
        /// <summary>
        /// 今天天气
        /// </summary>
        public string Weather1
        {
            set;
            get;
        }
        /// <summary>
        /// 今天天气图
        /// </summary>
        public string Img1
        {
            set;
            get;
        }
        /// <summary>
        /// 今天温度
        /// </summary>
        public string Temperature1
        {
            set;
            get;
        }
        /// <summary>
        /// 今天风向
        /// </summary>
        public string WindDirection1
        {
            set;
            get;
        }
        /// <summary>
        /// 今天风力
        /// </summary>
        public string WindForce1
        {
            set;
            get;
        }
        /// <summary>
        /// 明天日期
        /// </summary>
        public string Date2
        {
            set;
            get;
        }
        /// <summary>
        /// 明天天气
        /// </summary>
        public string Weather2
        {
            set;
            get;
        }
        /// <summary>
        /// 明天天气图
        /// </summary>
        public string Img2
        {
            set;
            get;
        }
        /// <summary>
        /// 明天温度
        /// </summary>
        public string Temperature2
        {
            set;
            get;
        }
        /// <summary>
        /// 明天风向
        /// </summary>
        public string WindDirection2
        {
            set;
            get;
        }
        /// <summary>
        /// 明天风力
        /// </summary>
        public string WindForce2
        {
            set;
            get;
        }
        /// <summary>
        /// 后天日期
        /// </summary>
        public string Date3
        {
            set;
            get;
        }
        /// <summary>
        /// 后天天气
        /// </summary>
        public string Weather3
        {
            set;
            get;
        }
        /// <summary>
        /// 后天天气图
        /// </summary>
        public string Img3
        {
            set;
            get;
        }
        /// <summary>
        /// 后天温度
        /// </summary>
        public string Temperature3
        {
            set;
            get;
        }
        /// <summary>
        /// 后天风向
        /// </summary>
        public string WindDirection3
        {
            set;
            get;
        }
        /// <summary>
        /// 后天风力
        /// </summary>
        public string WindForce3
        {
            set;
            get;
        }
    }
}

