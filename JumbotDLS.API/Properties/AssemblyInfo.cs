﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的常规信息通过下列属性集
// 控制。更改这些属性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("JumbotDLS.API")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("北京子木将博科技有限公司")]
[assembly: AssemblyProduct("JumbotDLS.API")]
[assembly: AssemblyCopyright("jumbot.net (C) 2014-2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(true)]
[assembly: AssemblyDelaySign(false)] 
[assembly: AssemblyVersion("1.2.1.0214")]
