﻿using System;

namespace JumbotDLS.Utils.fastJSON
{
    internal class Getters
    {
        public string Name;
        public JSON.GenericGetter Getter;
    }
}
