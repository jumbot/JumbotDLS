﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.IO;
using System.Data;
using System.Collections;
using Aspose.Cells;
using Newtonsoft.Json.Linq;
namespace JumbotDLS.Utils
{
    /// <summary>
    /// ExcelManage 的摘要说明。
    /// </summary>
    public static class AsposeCellsHelper
    {
        /// <summary>
        /// 获得某个excel的所有sheet
        /// </summary>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        public static JArray GetSheets(String strFileName)
        {
            Workbook book = new Workbook(strFileName);
            WorksheetCollection myColection = book.Worksheets;
            JArray ja = new JArray();
            for (int i = 0; i < myColection.Count; i++)
            {
                ja.Add(myColection[i].Name);
            }
            return ja;
        }

        public static DataTable ReadExcel(String strFileName, string strSheetName)
        {
            try
            {
                Workbook book = new Workbook(strFileName);
                int _i = 0;
                if (strSheetName != "")
                {
                    WorksheetCollection myColection = book.Worksheets;
                    for (int i = 0; i < myColection.Count; i++)
                    {
                        if (myColection[i].Name.ToLower() == strSheetName.ToLower())
                        {
                            _i = i;
                            break;
                        }
                    }
                }
                Worksheet sheet = book.Worksheets[_i];
                Cells cells = sheet.Cells;
                return cells.ExportDataTableAsString(0, 0, cells.MaxDataRow + 1, cells.MaxDataColumn + 1, true);
            }
            catch (Exception ex)
            {
                JumbotDLS.Utils.Logs.Error("ReadExcel", ex + "");
                return null;
            }
        }
        public static bool DeleteSheet(string ExcelFilePath, string SheetName)
        {
            try
            {
                Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook(ExcelFilePath);
                Aspose.Cells.Worksheet worksheet = workbook.Worksheets[SheetName];
                worksheet.SetVisible(false, false);
                if (ExcelFilePath.ToLower().EndsWith(".xls"))
                    workbook.Save(ExcelFilePath, Aspose.Cells.SaveFormat.Excel97To2003);//保存至2003格式
                else
                    workbook.Save(ExcelFilePath, Aspose.Cells.SaveFormat.Xlsx);//保存至2010格式     
                workbook = null;
                return true;
            }
            catch (Exception ex)
            {
                JumbotDLS.Utils.Logs.Error("delete_excel_sheet", "" + ex);
                return false;
            }
        }
        /// <summary>
        /// 将数据导出至Excel
        /// </summary>
        /// <param name="Table">DataTable对象</param>
        /// <param name="ExcelFilePath">Excel文件路径</param>
        /// <returns></returns>
        public static bool OutputToExcel(bool NeedCreateFile, DataTable dt, string ExcelFilePath, bool AutoFitColumns, ref int SuccessCount, ref int FailureCount)
        {
            if ((dt.TableName.Trim().Length == 0) || (dt.TableName.ToLower() == "table"))
            {
                dt.TableName = "Sheet1";
            }
            if (dt.Rows.Count == 0)
                return false;
            try
            {
                //创建一个工作簿
                Aspose.Cells.Workbook workbook = NeedCreateFile ? (new Aspose.Cells.Workbook()) : (new Aspose.Cells.Workbook(ExcelFilePath));
                //创建一个 sheet 表
                Aspose.Cells.Worksheet worksheet = NeedCreateFile ? workbook.Worksheets[0] : workbook.Worksheets[dt.TableName];


                Aspose.Cells.Cell cell;
                Aspose.Cells.Style style = workbook.CreateStyle();

                int rowIndex = 0;   //行的起始下标为 0
                int colIndex = 0;   //列的起始下标为 0
                if (NeedCreateFile)
                {
                    //设置 sheet 表名称
                    worksheet.Name = dt.TableName;

                    //设置列名
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        //获取第一行的每个单元格
                        cell = worksheet.Cells[rowIndex, colIndex + i];
                        cell.PutValue(dt.Columns[i].ColumnName);//设置列名
                        style.Font.Name = "Arial";//设置字体
                        style.Font.IsBold = true;//设置字体加粗
                        style.Font.Size = 14;//设置字体大小
                        style.Font.Color = System.Drawing.Color.Black;//设置字体颜色
                        style.BackgroundColor = System.Drawing.Color.LightGreen;//设置背景色
                    }
                }
                rowIndex = rowIndex + worksheet.Cells.Rows.Count;

                //写入数据
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    try
                    {
                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            cell = worksheet.Cells[rowIndex + i, colIndex + j];

                            cell.PutValue(dt.Rows[i][j]);
                        }
                        SuccessCount++;
                    }
                    catch (Exception exp)
                    {
                        string str = exp.Message;
                        JumbotDLS.Utils.Logs.Error("dt2excel", "" + exp);
                        FailureCount++;
                    }

                }

                //自动列宽
                if (AutoFitColumns)
                    worksheet.AutoFitColumns();
                //创建文件
                //if (NeedCreateFile)
                {
                    if (ExcelFilePath.ToLower().EndsWith(".xls"))
                        workbook.Save(ExcelFilePath, Aspose.Cells.SaveFormat.Excel97To2003);//保存至2003格式
                    else
                        workbook.Save(ExcelFilePath, Aspose.Cells.SaveFormat.Xlsx);//保存至2010格式     
                }

                worksheet = null;
                workbook = null;
                return true;
            }
            catch (Exception ex)
            {
                JumbotDLS.Utils.Logs.Error("dt2excel", "" + ex);
                return false;
            }


        }
    }
}