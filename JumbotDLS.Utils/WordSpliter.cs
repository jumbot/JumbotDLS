﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
namespace JumbotDLS.Utils
{
    /// <summary>
    /// 分词类
    /// </summary>
    public static class WordSpliter
    {
        /// <summary>
        /// 得到分词关键字
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetKeyword(string key, string splitchar)
        {
            JumbotDLS.Utils.ShootSeg.Segment seg = new JumbotDLS.Utils.ShootSeg.Segment();
            seg.InitWordDics();
            seg.EnablePrefix = true;
            seg.Separator = splitchar;
            return seg.SegmentText(key, false).Trim();
        }
        public static string GetKeyword(string key)
        {
            return GetKeyword(key," ");
        }
    }
}