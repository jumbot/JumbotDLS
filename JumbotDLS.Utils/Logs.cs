﻿

using System;
using System.Web;
using System.Web.Caching;
using System.Text;

namespace JumbotDLS.Utils
{
    /// <summary>
    /// Logs操作类
    /// </summary>
    public static class Logs
    {
        public static string LogsRoot = System.Configuration.ConfigurationManager.AppSettings["JumbotDLS:LogsRoot"];
        private static int LOG_LEVENL = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["JumbotDLS:LOG_LEVENL"] + "");

        public static string GetLogFilePath(string _file)
        {
            return GetLogFilePath(_file, true);
        }
        public static string GetLogFilePath(string _file, bool _useDateFolder)
        {
            string LogFilePath = LogsRoot + "\\" + (_useDateFolder ? (DateTime.Now.ToString("yyyyMMdd") + "\\") : "") + _file + ".log";
            return LogFilePath;
        }

        public static void Debug(string _file, string _output)
        {
            Debug(_file, _output, true);
        }

        public static void Debug(string _file, string _output, bool _useDateFolder)
        {
            if (LOG_LEVENL >= 3)
            {
                SaveLogs("debug_" + _file, _output, _useDateFolder);
            }
        }

        public static void Info(string _file, string _output, bool _useDateFolder)
        {
            if (LOG_LEVENL >= 2)
            {
                SaveLogs("info_" + _file, _output, _useDateFolder);
            }
        }
        public static void Info(string _file, string _output)
        {
            Info(_file, _output, true);
        }

        public static void Error(string _file, string _output, bool _useDateFolder)
        {
            if (LOG_LEVENL >= 1)
            {
                SaveLogs("error_" + _file, _output, _useDateFolder);
            }
        }
        public static void Error(string _file, string _output)
        {
            Error(_file, _output, true);
        }

        private static void SaveLogs(string _file, string _output)
        {
            SaveLogs(_file, _output, true);
        }

        private static void SaveLogs(string _file, string _output, bool _useDateFolder)
        {
            string LogFilePath = LogsRoot + "\\" + (_useDateFolder ? (DateTime.Now.ToString("yyyyMMdd") + "\\") : "") + _file + ".log";
            try
            {
                if (!System.IO.Directory.Exists(LogsRoot + "\\" + (_useDateFolder ? (DateTime.Now.ToString("yyyyMMdd") + "\\") : "")))
                    System.IO.Directory.CreateDirectory(LogsRoot + "\\" + (_useDateFolder ? (DateTime.Now.ToString("yyyyMMdd") + "\\") : ""));
                System.IO.FileStream fs = new System.IO.FileStream(LogFilePath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                System.IO.StreamWriter sw = new System.IO.StreamWriter(fs);
                sw.BaseStream.Seek(0, System.IO.SeekOrigin.End);
                sw.WriteLine(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + _output);
                sw.Flush();
                sw.Close();
                fs.Close();
            }
            catch
            {
            }

        }
    }
}
