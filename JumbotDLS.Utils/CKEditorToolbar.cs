﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.IO;
namespace JumbotDLS.Utils
{
    /// <summary>
    /// CKEditorToolbar
    /// </summary>
    public static class CKEditorToolbar
    {
        /// <summary>
        /// 简单模式
        /// </summary>
        public static object[] Simple
        {
            get
            {
                return new object[]{
                new object[] {"Source", "-", "JustifyLeft", "JustifyCenter", "JustifyRight", "-","Styles","FontSize",},
				new object[] { "Bold", "Italic", "-", "NumberedList", "BulletedList", "-", "Link", "Unlink"},
			};
            }
        }
        /// <summary>
        /// 最简模式
        /// </summary>
        public static object[] Basic
        {
            get
            {
                return new object[]{
				new object[] { "Bold","Italic","-","OrderedList","UnorderedList","-","Link","Unlink"}
                };
            }
        }
    }
}
