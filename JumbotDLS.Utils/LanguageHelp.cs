﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using Newtonsoft.Json;
namespace JumbotDLS.Utils
{
    /// <summary>
    /// 语言包
    /// </summary>
    public class LanguageHelp
    {
        public LanguageHelp()
        { }
        /// <summary>
        /// 绑定语言包(V6之后深入开发)
        /// </summary>
        /// <param name="_lng">如cn表示中文，en表示英文</param>
        /// <returns></returns>
        public Dictionary<string, object> GetEntity(string _lng)
        {
            string json = JumbotDLS.Utils.DirFile.ReadFile("~/_data/languages/" + _lng + ".js");
            json = JumbotDLS.Utils.Strings.GetHtml(json, "//<!--语言包begin", "//-->语言包end");
            return (Dictionary<string, object>)JumbotDLS.Utils.fastJSON.JSON.Instance.ToObject(json);
        }
    }
}
