﻿
using System.Web;
using System;
using Newtonsoft.Json.Linq;
namespace JumbotDLS.Utils
{
    public static class mpweixinHelper
    {



        /// <summary>
        /// 获得AccessToken
        /// </summary>
        /// <param name="_errmsg"></param>
        /// <returns></returns>
        public static string GetAccessToken(ref string _errmsg)
        {
            string _access_token = "" + JumbotDLS.Utils.Cache.Get("mpweixin_access_token");
            if (_access_token == "")
            {
                string _url = "http://oms.knowfar.org.cn/weixin/token_weixin.aspx?mode=2";
                try
                {
                    string _json = JumbotDLS.Utils.HttpHelper.Get_Http(_url, 8000,System.Text.Encoding.UTF8);
                    JObject jo = JObject.Parse(_json);
                    _access_token = jo["access_token"].ToString();
                    JumbotDLS.Utils.Cache.Insert("mpweixin_access_token", _access_token, 120, 1);
                }
                catch
                {
                    _url = "http://oms.knowfar.org.cn/weixin/token_weixin.aspx?mode=3";
                    try
                    {
                        string _json = JumbotDLS.Utils.HttpHelper.Get_Http(_url, 8000,System.Text.Encoding.UTF8);
                        _access_token = _json.Trim();
                        JumbotDLS.Utils.Cache.Insert("mpweixin_access_token", _access_token, 120, 1);
                    }
                    catch (Exception ex2)
                    {
                        _errmsg = ex2.ToString();
                        JumbotDLS.Utils.Logs.Error("mpweixin", _errmsg);
                    }
                }
            }


            return _access_token;
        }


        /// <summary>
        /// 给指定微信ID 推送消息
        /// </summary>
        /// <param name="_toweixinid"></param>
        /// <param name="_message"></param>
        /// <returns></returns>
        public static bool SendMessage(string _toweixinid, string _message, ref string _errmsg)
        {
            string _access_token = GetAccessToken(ref _errmsg);
            if (_access_token != "")
            {
                string _posturl = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + _access_token;
                JObject jo = new JObject();
                jo.Add("touser", _toweixinid);
                jo.Add("msgtype", "text");
                JObject jo2 = new JObject();
                jo2.Add("content", _message);
                jo.Add("text", jo2);
                string _postdata = jo.ToString();
                try
                {
                    string _json = JumbotDLS.Utils.HttpHelper.Post_Http(_posturl, _postdata, System.Text.Encoding.UTF8);
                    jo = JObject.Parse(_json);
                    if (("" + jo["errcode"]) == "0")
                        return true;
                    else
                    {
                        _errmsg = "" + jo["errmsg"];
                        JumbotDLS.Utils.Logs.Error("mpweixin", _errmsg);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    _errmsg = ex.ToString();
                    JumbotDLS.Utils.Logs.Error("mpweixin", _errmsg);
                    return false;
                }
            }
            return false;
        }

        /// <summary>
        /// 创建自定义菜单
        /// </summary>
        /// <param name="_body"></param>
        /// <returns></returns>
        public static bool CreateMenu(string _body, ref string _errmsg)
        {
            string _access_token = GetAccessToken(ref _errmsg);
            if (_access_token != "")
            {
                try
                {
                    string _posturl = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" + _access_token;
                    JObject jo = JObject.Parse(_body);
                    string _postdata = jo.ToString();
                    try
                    {
                        string _json = JumbotDLS.Utils.HttpHelper.Post_Http(_posturl, _postdata, System.Text.Encoding.UTF8);
                        jo = JObject.Parse(_json);
                        if (("" + jo["errcode"]) == "0")
                            return true;
                        else
                        {
                            _errmsg = "" + jo["errmsg"];
                            JumbotDLS.Utils.Logs.Info("mpweixin", _errmsg);
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        _errmsg = ex.ToString();
                        JumbotDLS.Utils.Logs.Error("mpweixin", _errmsg);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    _errmsg = ex.ToString();
                    JumbotDLS.Utils.Logs.Error("mpweixin", _errmsg);
                    return false;
                }
            }
            return false;
        }

        /// <summary>
        /// 获得素材列表
        /// </summary>
        /// <param name="_body"></param>
        /// <param name="_errmsg"></param>
        /// <returns></returns>
        public static bool GetMediaList(string _body, ref string _errmsg)
        {
            string _access_token = GetAccessToken(ref _errmsg);
            if (_access_token != "")
            {
                try
                {
                    string _posturl = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=" + _access_token;
                    JObject jo = JObject.Parse(_body);
                    string _postdata = jo.ToString();
                    try
                    {
                        string _json = JumbotDLS.Utils.HttpHelper.Post_Http(_posturl, _postdata, System.Text.Encoding.UTF8);
                        jo = JObject.Parse(_json);
                        if (jo["errcode"] == null)
                        {
                            _errmsg = _json;
                            return true;
                        }
                        else
                        {
                            _errmsg = "" + jo["errmsg"];
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        _errmsg = ex.ToString();
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    _errmsg = ex.ToString();
                    return false;
                }
            }
            return false;
        }

        /// <summary>
        /// 获得微信信息
        /// </summary>
        /// <param name="_openid"></param>
        /// <param name="_errmsg"></param>
        /// <returns></returns>
        public static bool GetUserInfo(string _openid, ref string _errmsg)
        {
            string _access_token = GetAccessToken(ref _errmsg);
            if (_access_token != "")
            {
                try
                {
                    string UserInfoUrl = "https://api.weixin.qq.com/cgi-bin/user/info?access_token={0}&openid={1}&lang=zh_CN";
                    string _geturl = string.Format(UserInfoUrl, _access_token, _openid);
                    try
                    {
                        string _json = JumbotDLS.Utils.HttpHelper.Get_Http(_geturl,8000, System.Text.Encoding.UTF8);
                        JObject jo = JObject.Parse(_json);
                        if (jo["errcode"] == null)
                        {
                            _errmsg = _json;
                            return true;
                        }
                        else
                        {
                            _errmsg = "" + jo["errmsg"];
                            JumbotDLS.Utils.Logs.Error("mpweixin_userinfo", _openid + "\r\n" + _errmsg);
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        _errmsg = ex.ToString();
                        JumbotDLS.Utils.Logs.Error("mpweixin_userinfo", _openid + "\r\n" + _errmsg);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    _errmsg = ex.ToString();
                    JumbotDLS.Utils.Logs.Error("mpweixin_userinfo", _openid + "\r\n" + _errmsg);
                    return false;
                }
            }
            return false;
        }
    }
}
