﻿/*
 * 程序名称: JumbotDLS(海关统一资源平台)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */


using System;
using System.Text;
using System.Management;//需要在项目中添加System.Management引用
using System.Security.Cryptography;
namespace JumbotDLS.Utils
{
    public static class SoftReg
    {
        /// <summary>
        /// 取得设备硬盘的卷标号
        /// </summary>
        /// <returns></returns>
        private static string GetDiskVolumeSerialNumber()
        {
            ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObject disk = new ManagementObject("win32_logicaldisk.deviceid=\"c:\"");
            disk.Get();
            return disk.GetPropertyValue("VolumeSerialNumber").ToString();
        }
        /// <summary>
        /// 获得CPU的序列号
        /// </summary>
        /// <returns></returns>
        private static string getCpu()
        {
            string strCpu = null;
            ManagementClass myCpu = new ManagementClass("win32_Processor");
            ManagementObjectCollection myCpuConnection = myCpu.GetInstances();
            foreach (ManagementObject myObject in myCpuConnection)
            {
                strCpu = myObject.Properties["Processorid"].Value.ToString();
                break;
            }
            return strCpu;
        }

        /// <summary>
        /// 生成机器码,有短线
        /// </summary>
        /// <returns></returns>
        public static string GetMNum()
        {
            string strNum = getCpu() + GetDiskVolumeSerialNumber();//获得24位Cpu和硬盘序列号
            return getFormatCode(strNum);
        }
        private static int[] intCode = new int[127];//存储密钥
        private static int[] intNumber = new int[25];//存机器码的Ascii值
        private static char[] Charcode = new char[25];//存储机器码字
        private static void setIntCode()//给数组赋值小于10的数
        {
            for (int i = 1; i < intCode.Length; i++)
            {
                intCode[i] = i % 9;
            }
        }
        /// <summary>
        /// 生成标准格式
        /// </summary>
        /// <param name="strNum"></param>
        /// <returns></returns>
        private static string getFormatCode(string strNum)
        {
            string strMNum = strNum.Substring(0, 24);
            return strMNum.Substring(0, 8) + "-" + strMNum.Substring(8, 8) + "-" + strMNum.Substring(16, 8);
        }
        /// <summary>
        /// 生成授权码
        /// </summary>
        /// <param name="MNum"></param>
        /// <param name="CName"></param>
        /// <returns></returns>
        public static string GetRNum(string MNum, int ValidDate, string CName)
        {
            string _MD5 = JumbotDLS.Utils.MD5.Upper32(MNum + ValidDate + CName);
            return getFormatCode(_MD5);
        }
        /// <summary>
        /// 判断授权码是否正确
        /// </summary>
        /// <param name="MNum"></param>
        /// <param name="CName"></param>
        /// <param name="RNum"></param>
        /// <returns></returns>
        public static bool CheckRNum(string MNum, int ValidDate, string CName, string RNum)
        {
            if (ValidDate < JumbotDLS.Utils.Validator.StrToInt(System.DateTime.Now.ToString("yyyyMMdd"), 20130101))
                return false;
            return GetRNum(MNum, ValidDate, CName) == RNum;
        }
    }
}