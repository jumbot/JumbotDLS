﻿/*
 * 程序名称: JumbotDLS(将博数字资源管理平台系统，亦名为：将博数字图书馆系统)
 * 
 * 程序版本: 1.x
 * 
 * 程序开发: 北京子木将博科技有限公司
 * 
 * 
 * 
 * 
 * 
 */

using System;
namespace JumbotDLS.DBUtility
{
    public class DbOperEventArgs : System.EventArgs
    {
        public int id;
        public DbOperEventArgs(int _id)
        {
            id = _id;
        }
    }
}
